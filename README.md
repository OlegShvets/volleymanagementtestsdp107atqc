# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

This project is an automatic test for the project VolleyManagement. Created by the group Dp107ATQC.


### How do I get set up? ###
* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

1. 
* Creating a user in the database through the Management Studio SQL:
 ![1.jpg](https://bitbucket.org/repo/Lypx6k/images/2237518449-1.jpg)
* Registration.
![2.jpg](https://bitbucket.org/repo/Lypx6k/images/2901596828-2.jpg)
* Create a new user.
![3.jpg](https://bitbucket.org/repo/Lypx6k/images/1652601041-3.jpg)
* Mark in the checkboxes in the user properties.
![4.jpg](https://bitbucket.org/repo/Lypx6k/images/38409415-4.jpg)
2.
Running the visual studio. Сommand сmd: sqllocalDb i V11.0.
![5.png](https://bitbucket.org/repo/Lypx6k/images/3969087946-5.png)
3.
Instance pipe name prescribes in config.properties. "db.instance =LOCALDB#4DE4CAB9"
![6.png](https://bitbucket.org/repo/Lypx6k/images/4037295526-6.png)
4. 
Put the chrome driver to the system folder.
Windows - C:\Windows\System32
Linux - /usr/bin/chromedriver
5.
Register the path to the Сhrome Driver in WebDriverUtils.
![7.png](https://bitbucket.org/repo/Lypx6k/images/2010446368-7.png)


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact