package com.softserveinc.volleymanagementtests.dal;

import com.softserveinc.volleymanagementtests.specification.TestConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * Created by Sergey Tsyganovskiy on 05.03.2016.
 * This class is a wrapper for sql queries.
 */

public class SqlQuery implements AutoCloseable {
    private final Connection connection;
    private final Statement statement;
    private PreparedStatement preparedStatement;

    public SqlQuery(String connectionString) throws ClassNotFoundException, SQLException {
        Class.forName(new TestConfig().getJDBCDriver());
        connection = DriverManager.getConnection(connectionString);
        statement = connection.createStatement();
    }

    /**
     * This is a wrapper for SQl functions: DELETE, INSERT, UPDATE
     * Returns number of modified rows
     */
    public int modify(String query) throws SQLException {
        return statement.executeUpdate(query);
    }

    public int [] modifyBatch(List<String> queries) throws SQLException {
        for (String query: queries) {
            statement.addBatch(query);
        }
        return statement.executeBatch();
    }

    /**
     * This is a wrapper for SQl function SELECT
     * Returns ResultSet object
     */
    public ResultSet select(String query) throws SQLException {
        return statement.executeQuery(query);
    }

    /**
     * Returns IDs of mofified rows
     */
    public ResultSet getKey(String query) throws SQLException {
        if (preparedStatement != null) {
            preparedStatement.close();
        }
        preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.executeUpdate();
        return preparedStatement.getGeneratedKeys();
    }

    @Override
    public void close() throws SQLException {
        statement.close();
        connection.close();
    }
}
