package com.softserveinc.volleymanagementtests.dal.models;

/**
 * @author Danil Zhyliaiev
 */
public class ContributorDal {
    private Integer id;
    private String name;
    private Integer teamId;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getTeamId() {
        return teamId;
    }

    public static ContributorBuilder newBuilder() {
        return new ContributorDal().new ContributorBuilder();
    }

    public class ContributorBuilder {
        public ContributorBuilder setId(final Integer idArg) {
            id = idArg;
            return this;
        }

        public ContributorBuilder setName(final String nameArg) {
            name = nameArg;
            return this;
        }

        public ContributorBuilder setTeamId(final Integer teamNameArg) {
            teamId = teamNameArg;
            return this;
        }

        public ContributorDal build() {
            return ContributorDal.this;
        }
    }
}
