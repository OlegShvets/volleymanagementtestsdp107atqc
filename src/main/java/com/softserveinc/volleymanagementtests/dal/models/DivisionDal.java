package com.softserveinc.volleymanagementtests.dal.models;

import java.util.Objects;

/**
 * Created by stas on 11.03.2017.
 */
public final class DivisionDal {

    private Integer id;
    private String name;
    private Integer tournamentId;

    private DivisionDal() {}

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getTournamentId() {
        return tournamentId;
    }

    public DivisionDal setTournamentId(Integer tournamentId) {
        this.tournamentId = tournamentId;
        return this;
    }

    public static DivisionBuilder newBuilder() {
        return new DivisionDal().new DivisionBuilder();
    }

    public class DivisionBuilder {

        public DivisionBuilder setId(int id) {
            DivisionDal.this.id = id;
            return this;
        }

        public DivisionBuilder setName(final String name) {
            DivisionDal.this.name = name;
            return this;
        }

        public DivisionBuilder setTournamentId(int tournamentId) {
            DivisionDal.this.tournamentId = tournamentId;
            return this;
        }


        public DivisionDal build() {
            return DivisionDal.this;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DivisionDal that = (DivisionDal) o;
        return Objects.equals(id, that.id)
                && Objects.equals(name, that.name)
                && Objects.equals(tournamentId, that.tournamentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, tournamentId);
    }
}