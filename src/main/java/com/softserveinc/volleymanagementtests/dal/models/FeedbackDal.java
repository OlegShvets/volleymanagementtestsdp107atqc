package com.softserveinc.volleymanagementtests.dal.models;

import java.util.Date;
import java.util.Objects;

/**
 * Created by stas on 16.03.2017.
 */
public final class FeedbackDal {

    private int id;
    private String userEmail;
    private String content;
    private Date feedbackDate;
    private int statusCode;
    private Date updateDate;
    private String adminName;
    private String userEnv;

    public int getId() {
        return id;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public String getContent() {
        return content;
    }

    public Date getFeedbackDate() {
        return feedbackDate;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public String getAdminName() {
        return adminName;
    }

    public String getUserEnv() {
        return userEnv;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FeedbackDal that = (FeedbackDal) o;
        return statusCode == that.statusCode
                && Objects.equals(userEmail, that.userEmail)
                && Objects.equals(content, that.content)
                && Objects.equals(feedbackDate, that.feedbackDate)
                && Objects.equals(updateDate, that.updateDate)
                && Objects.equals(adminName, that.adminName)
                && Objects.equals(userEnv, that.userEnv);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userEmail, content, feedbackDate, statusCode,
                updateDate, adminName, userEnv);
    }

    public static FeedbackBuilder newBuilder() {
        return new FeedbackDal().new FeedbackBuilder();
    }

    public class FeedbackBuilder {

        private FeedbackBuilder(){}

        public FeedbackBuilder setId( int id) {
            FeedbackDal.this.id = id;
            return this;
        }

        public FeedbackBuilder setUserEmail(final String email) {
            FeedbackDal.this.userEmail = email;
            return this;
        }

        public FeedbackBuilder setContent(final String content) {
            FeedbackDal.this.content = content;
            return  this;
        }

        public FeedbackBuilder setFeedBackDate(final Date feedbackDate) {
            FeedbackDal.this.feedbackDate = feedbackDate;
            return this;
        }

        public FeedbackBuilder setStatusCode(int statusCode) {
            FeedbackDal.this.statusCode = statusCode;
            return this;
        }

        public FeedbackBuilder setUpdateDate(final Date updateDate) {
            FeedbackDal.this.updateDate = updateDate;
            return this;
        }

        public FeedbackBuilder setAdminName(final String adminName){
            FeedbackDal.this.adminName = adminName;
            return this;
        }

        public FeedbackBuilder setUserEnv(final String userEnv){
            FeedbackDal.this.userEnv = userEnv;
            return this;
        }

        public FeedbackDal build() {
            return FeedbackDal.this;
        }
    }




}
