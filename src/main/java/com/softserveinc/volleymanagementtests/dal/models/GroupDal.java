package com.softserveinc.volleymanagementtests.dal.models;

import java.util.Objects;

/**
 * Created by stas on 12.03.2017.
 */
public final class GroupDal {
    private Integer id;
    private String name;
    private Integer divisionId;

    private GroupDal() {}

    public static GroupDalBuilder newBuilder() {
        return new GroupDal().new GroupDalBuilder();
    }

    public Integer getDivisionId() {
        return divisionId;
    }

    public GroupDal setDivisionId(Integer divisionId) {
        this.divisionId = divisionId;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public class GroupDalBuilder {

        public GroupDalBuilder setId(int id) {
            GroupDal.this.id = id;
            return this;
        }

        public GroupDalBuilder setName(final String name) {
            GroupDal.this.name = name;
            return this;
        }

        public GroupDalBuilder setDivisionId(int divisionId) {
            GroupDal.this.divisionId = divisionId;
            return this;
        }

        public GroupDal build() {
            return GroupDal.this;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupDal groupDal = (GroupDal) o;
        return Objects.equals(id, groupDal.id)
                && Objects.equals(name, groupDal.name)
                && Objects.equals(divisionId, groupDal.divisionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, divisionId);
    }
}