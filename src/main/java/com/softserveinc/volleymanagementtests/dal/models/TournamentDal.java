package com.softserveinc.volleymanagementtests.dal.models;

import java.util.Date;
import java.util.Objects;

/**
 * Created by stas on 09.03.2017.
 */
public final class TournamentDal {
    private Integer id;
    private String name;
    private Integer schemeCode;
    private Integer seasonOffset;
    private String description;
    private String regulationsLink;
    private Date tournamentStart;
    private Date tournamentEnd;
    private Date transferStart;
    private Date transferEnd;
    private Date applyingPeriodStart;
    private Date applyingPeriodEnd;


    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getSchemeCode() {
        return schemeCode;
    }

    public int getSeasonOffset() {
        return seasonOffset;
    }

    public String getDescription() {
        return description;
    }

    public String getRegulationsLink() {
        return regulationsLink;
    }

    public Date getTournamentStart() {
        return tournamentStart;
    }

    public Date getTournamentEnd() {
        return tournamentEnd;
    }

    public Date getTransferStart() {
        return transferStart;
    }

    public Date getTransferEnd() {
        return transferEnd;
    }

    public Date getApplyingPeriodStart() {
        return applyingPeriodStart;
    }

    public Date getApplyingPeriodEnd() {
        return applyingPeriodEnd;
    }

    private TournamentDal(){}

    public static TournamentDalBuilder newBuilder() {
        return new TournamentDal().new TournamentDalBuilder();
    }


    public class TournamentDalBuilder {
        public TournamentDalBuilder setId(Integer id) {
            TournamentDal.this.id  =  id;
            return this;
        }

        public TournamentDalBuilder setName(String name) {
            TournamentDal.this.name = name;
            return this;
        }

        public TournamentDalBuilder setSchemeCode(final Integer schemeCode) {
            TournamentDal.this.schemeCode = schemeCode;
            return this;
        }

        public TournamentDalBuilder setSeasonOffset(final Integer offset) {
            TournamentDal.this.seasonOffset = offset;
            return this;
        }

        public TournamentDalBuilder setDescription(final String description) {
            TournamentDal.this.description = description;
            return this;
        }

        public TournamentDalBuilder setRegulationsLink(final String regulationsLink) {
            TournamentDal.this.regulationsLink = regulationsLink;
            return this;
        }

        public TournamentDalBuilder setTournamentStart(final Date tournamentStart) {
            TournamentDal.this.tournamentStart = tournamentStart;
            return this;
        }

        public TournamentDalBuilder setTournamentEnd(final Date tournamentEnd) {
            TournamentDal.this.tournamentEnd = tournamentEnd;
            return this;
        }

        public TournamentDalBuilder setTransferStart(final Date transferStart) {
            TournamentDal.this.transferStart = transferStart;
            return this;
        }

        public TournamentDalBuilder setTransferEnd(final Date transferEnd) {
            TournamentDal.this.transferEnd = transferEnd;
            return this;
        }

        public TournamentDalBuilder setApplyingPeriodStart(final Date applyingPeriodStart) {
            TournamentDal.this.applyingPeriodStart = applyingPeriodStart;
            return this;
        }

        public TournamentDalBuilder setApplyingPeriodEnd(final Date applyingPeriodEnd) {
            TournamentDal.this.applyingPeriodEnd = applyingPeriodEnd;
            return this;
        }

        public TournamentDal build() {
            return TournamentDal.this;
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TournamentDal that = (TournamentDal) o;
        return Objects.equals(id, that.id)
                && Objects.equals(name, that.name)
                && Objects.equals(schemeCode, that.schemeCode)
                && Objects.equals(seasonOffset, that.seasonOffset)
                && Objects.equals(description, that.description)
                && Objects.equals(regulationsLink, that.regulationsLink)
                && Objects.equals(tournamentStart, that.tournamentStart)
                && Objects.equals(tournamentEnd, that.tournamentEnd)
                && Objects.equals(transferStart, that.transferStart)
                && Objects.equals(transferEnd, that.transferEnd)
                && Objects.equals(applyingPeriodStart, that.applyingPeriodStart)
                && Objects.equals(applyingPeriodEnd, that.applyingPeriodEnd);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, schemeCode, seasonOffset, description, regulationsLink,
                tournamentStart, tournamentEnd, transferStart, transferEnd,
                applyingPeriodStart, applyingPeriodEnd);
    }
}
