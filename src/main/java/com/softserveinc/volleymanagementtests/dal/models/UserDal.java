package com.softserveinc.volleymanagementtests.dal.models;

import java.util.Objects;

/**
 * Created by stas on 19.03.2017.
 */
public final class UserDal {

    private final int id;
    private final String userName;
    private final String fullName;
    private final String cellPhone;
    private final String email;
    private final String playerId;
    private int isBlocked;


    public int getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getFullName() {
        return fullName;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public String getEmail() {
        return email;
    }

    public String getPlayerId() {
        return playerId;
    }

    public int getIsBlocked() {
        return isBlocked;
    }


    private UserDal(UserDal.UserDalBuilder userDalBuilder) {
        id = userDalBuilder.id;
        userName = userDalBuilder.userName;
        fullName = userDalBuilder.fullName;
        cellPhone = userDalBuilder.cellPhone;
        email = userDalBuilder.email;
        playerId = userDalBuilder.playerId;
        isBlocked = userDalBuilder.isBlocked;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDal userDal = (UserDal) o;
        return id == userDal.id
                && playerId.equals(userDal.playerId)
                && isBlocked == userDal.isBlocked &&
                Objects.equals(userName, userDal.userName)
                && Objects.equals(fullName, userDal.fullName) &&
                Objects.equals(cellPhone, userDal.cellPhone)
                && Objects.equals(email, userDal.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userName, fullName, cellPhone, email, playerId, isBlocked);
    }

    public static class UserDalBuilder {
        private int id;
        private String userName;
        private String fullName;
        private String cellPhone;
        private String email;
        private String playerId;
        private int isBlocked;

        public UserDalBuilder setId(int id) {
            this.id = id;
            return this;
        }

        public UserDalBuilder setName(final String name) {
            this.userName = name;
            return this;
        }

        public UserDalBuilder setFullName(final String fullName) {
            this.fullName = fullName;
            return this;
        }

        public UserDalBuilder setCellPhone(final String cellPhone) {
            this.cellPhone = cellPhone;
            return this;
        }

        public UserDalBuilder setEmail(final String email) {
            this.email = email;
            return this;
        }

        public UserDalBuilder setPlayerId(final String playerId) {
            this.playerId = playerId;
            return this;
        }

        public UserDalBuilder setBlocked(int blocked) {
            this.isBlocked = blocked;
            return this;
        }

        public UserDal build() {
            return new UserDal(this);
        }

    }
}
