package com.softserveinc.volleymanagementtests.dal.queries;

/**
 * @author Danil Zhyliaiev
 */
public enum  ContributorQueries {
    SELECT_ALL("SELECT Id, Name, ContributorTeamId"
            + " FROM dbo.Contributors;"),

     SELECT_BY_TEAM_ID("SELECT Id, Name, ContributorTeamId"
            + " FROM dbo.Contributors WHERE ContributorTeamId = %s;");

    private String query;

    ContributorQueries(String query) {
        this.query = query;
    }


    @Override
    public String toString() {
        return query;
}
}
