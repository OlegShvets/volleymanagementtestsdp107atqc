package com.softserveinc.volleymanagementtests.dal.queries;

/**
 * @author Danil Zhyliaiev
 */
public enum ContributorTeamsQueries {
    SELECT_ALL("SELECT Id, Name, CourseDirection"
            + " FROM dbo.ContributorTeams;"),

    SELECT_BY_ID("SELECT Id, Name, CourseDirection"
            + " FROM dbo.ContributorTeams WHERE Id = %s;");


    private String query;
    ContributorTeamsQueries(String query) {
        this.query = query;
    }

    @Override
    public String toString() {
       return query;
    }
}
