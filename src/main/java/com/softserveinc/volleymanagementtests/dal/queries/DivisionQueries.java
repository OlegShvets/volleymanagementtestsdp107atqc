package com.softserveinc.volleymanagementtests.dal.queries;

/**
 * Created by stas on 11.03.2017.
 */
public enum DivisionQueries {

    INSERT("INSERT INTO dbo.Divisions " +
            "(Name, TournamentId)  VALUES (N'%s',%d)"),

    SELECT_BY_TRMENT_ID("Select id from dbo.Divisions " +
            " where tournamentId ='%s'"),

    DEL_DIV_BY_ID_QUERY("Delete from dbo.Divisions" +
            " WHERE TournamentId = '%s'");


    private final String query;

    DivisionQueries(String query) {
        this.query = query;
    }


    @Override
    public String toString() {
        return query;
    }
}
