package com.softserveinc.volleymanagementtests.dal.queries;

/**
 * Created by stas on 16.03.2017.
 */
public enum FeedBackQueries {

    INSERT("INSERT INTO dbo.Feedbacks " +
            "(UsersEmail, Content, Date, StatusCode, UpdateDate, AdminName," +
            "UserEnvironment)" +
            "VALUES  " +
            "('%s','%s','%s',%d,'%s','%s','%s')"),

    SELECT_BY_ID("SELECT * FROM dbo.Feedbacks WHERE Id = %s"),
    SELECT_BY_EMAIL("SELECT * FROM dbo.Feedbacks WHERE UsersEmail = '%s'"),
    DELETE_BY_ID("DELETE from dbo.Feedbacks Where id = '%s'"),
    DEL_BY_EMAIL("DELETE from dbo.Feedbacks Where UsersEmail = '%s'");


    private final String query;
    FeedBackQueries(String query) {
        this.query = query;
    }

    @Override
    public String toString() {
        return query;
    }

}
