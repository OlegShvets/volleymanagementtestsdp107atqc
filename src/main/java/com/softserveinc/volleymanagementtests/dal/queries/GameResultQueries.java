package com.softserveinc.volleymanagementtests.dal.queries;

/**
 * Created by stas on 14.03.2017.
 */
public enum GameResultQueries {

    INSERT("INSERT INTO dbo.GameResults " +
            "(TournamentId, HomeTeamId, AwayTeamId, HomeSetsScore, AwaySetsScore, IsTechnicalDefeat," +
            " HomeSet1Score, AwaySet1Score, HomeSet2Score, AwaySet2Score, HomeSet3Score, AwaySet3Score" +
            "HomeSet4Score, AwaySet4Score, HomeSet5Score, AwaySet5Score, StartTime, RoundNumber, GameNumber)  " +
            "VALUES  " +
            "(%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,N'%s',%d,%d)"),

    INSERT_FOR_PLAYOFF("INSERT INTO dbo.GameResults " +
            "(TournamentId, HomeSetsScore, AwaySetsScore, IsTechnicalDefeat," +
            " HomeSet1Score, AwaySet1Score, HomeSet2Score, AwaySet2Score, HomeSet3Score, AwaySet3Score," +
            "HomeSet4Score, AwaySet4Score, HomeSet5Score, AwaySet5Score,  RoundNumber, GameNumber)  " +
            "VALUES  " +
            "(%d,0,0,0,0,0,0,0,0,0,0,0,0,0,%d,%d)"),

    DEL_GAMES_FOR_TRMENT("DELETE from dbo.GameResults" +
            " WHERE TournamentId = %s");


    private final String query;
    GameResultQueries(String query) {
        this.query = query;
    }

    @Override
    public String toString() {
        return query;
    }
}