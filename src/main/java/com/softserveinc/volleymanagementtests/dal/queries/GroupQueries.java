package com.softserveinc.volleymanagementtests.dal.queries;

/**
 * Created by stas on 12.03.2017.
 */
public enum GroupQueries {

    INSERT_QUERY("INSERT INTO dbo.Groups " +
            "(Name, DivisionId)  VALUES (N'%s',%d)"),

    DEL_GROUP_BY_ID("DELETE from dbo.GROUPS" +
            " WHERE DivisionId = %s");

    private final String query;

    GroupQueries(String query) {
        this.query = query;
    }

    @Override
    public String toString() {
        return query;
    }
}
