package com.softserveinc.volleymanagementtests.dal.queries;

/**
 * @author J.Bodnar
 */
public enum PlayerQueries {

    SELECT_ALL("SELECT Id, FirstName, "                          // all rows
            + "LastName, BirthYear, Height, Weight "
            + "FROM dbo.Players "),

    SELECT_BY_ID(SELECT_ALL.toString() + "WHERE Id = %d"),

    SELECT_TEAM_MEMBERS(SELECT_ALL.toString() + "WHERE TeamId = %d"),

    SELECT_BY_ALL_FIELDS(SELECT_ALL.toString() + "WHERE " +
            "FirstName = N'%s' AND " + "LastName = N'%s' " +
            "AND birthYear = %d AND height " +
            "= %d AND weight = %d"),

    SELECT_BY_LAST_NAME("SELECT * FROM Players WHERE LastName LIKE N'%s'"),

    SELECT_BY_FIRST_NAME("SELECT * FROM Players WHERE FirstName LIKE N'%s'"),
    INSERT("INSERT INTO dbo.Players " +
            "(FirstName, LastName, BirthYear, Height, Weight)  VALUES  " +
            "(N'%s',N'%s',%d,%d,%d)"),

    DELETE("DELETE dbo.Players"),

    DELETE_BY_ALL_FIELDS(DELETE.toString() + " WHERE FirstName " +
            "= N'%s' AND LastName = N'%s'" +
            " AND birthYear = '%d' AND height = '%d' " +
            "AND weight = '%d'"),

    DELETE_BY_ID(DELETE + " WHERE id = '%d'"),

    UPDATE_TEAM("UPDATE dbo.Players SET TeamId = %d WHERE id = %d"),

    UPDATE("UPDATE dbo.Players SET TeamId = NULL WHERE TeamID = %d"),

    UPDATE_TO_NULL("UPDATE dbo.Players SET TeamId = NULL WHERE TeamID is not NULL"),

    SET_LINK_REQUEST("INSERT INTO dbo.Requests" +
            "(UserId,PlayerId)" +
            "VALUES (N'%s',N'%s')");


    private String query;

    PlayerQueries(String query) {
        this.query = query;
    }

    @Override
    public String toString() {
        return query;
    }
}
