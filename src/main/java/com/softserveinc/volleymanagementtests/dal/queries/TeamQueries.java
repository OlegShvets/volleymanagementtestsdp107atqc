package com.softserveinc.volleymanagementtests.dal.queries;

/* @author S.Tsyganovskiy */

public enum TeamQueries {

    INSERT_QUERY("INSERT INTO dbo.Teams " +
            "(Name, Coach, Achievements, CaptainId)  VALUES  " +
            "(N'%s',N'%s',N'%s',%d) "),

    SELECT_ALL("SELECT * FROM dbo.Teams"),

    SELECT_BY_ID("SELECT * FROM dbo.Teams WHERE Id = %d"),

    DELETE_BY_ID_QUERY("DELETE dbo.Teams  WHERE Id = %d"),

    DELETE_ALL_QUERY("DELETE dbo.Teams"),

    SELECT_BY_NAME_QUERY("SELECT * FROM dbo.Teams WHERE Name = N'%s'");


    private String query;

    TeamQueries(String query) {
        this.query = query;
    }

    @Override
    public String toString() {
        return query;
    }
}
