package com.softserveinc.volleymanagementtests.dal.queries;

/**
 * Created by stas on 09.03.2017.
 */
public enum TournamentQueries {

    INSERT("INSERT INTO dbo.Tournaments " +
            "(Name, SchemeCode, SeasonOffset, Description, RegulationsLink, GamesStart, GamesEnd," +
            "TransferStart, TransferEnd, ApplyingPeriodStart, ApplyingPeriodEnd, LastTimeUpdated)" +
            "VALUES " +
            "(N'%s',%d, %d, N'%s',N'%s',N'%s',N'%s',N'%s',N'%s',N'%s',N'%s',%s)"),

    SELECT_BY_ID("SELECT * FROM dbo.Tournaments WHERE Id = %s"),

    SELECT_BY_NAME("SELECT * FROM dbo.Tournaments WHERE Name = %s"),

    SELECT_ALL("SELECT * FROM dbo.Tournaments"),

    DELETE_BY_ID("DELETE dbo.Tournaments  WHERE id = '%s'"),

    DELETE_BY_NAME("DELETE dbo.Tournaments  WHERE Name = '%s'"),

    DEL_BY_TOUR_ID("DELETE dbo.TournamentTeam" +
            " where TournamentId = %s"),

    INSERT_TEAM("INSERT INTO dbo.TournamentTeam" +
            "(TournamentId, TeamId) VALUES (%d, %d)");


    private final String query;

    TournamentQueries(String query) {
        this.query = query;
    }

    @Override
    public String toString() {
        return query;
    }
}
