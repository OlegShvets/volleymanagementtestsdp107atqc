package com.softserveinc.volleymanagementtests.dal.queries;

/**
 * Created by stas on 19.03.2017.
 */
public enum UserQueries {

    INSERT("INSERT INTO dbo.Users " +
            "(UserName, FullName, CellPhone, Email, isBlocked)  VALUES  " +
            "(N'%s',N'%s',N'%s',N'%s',N'%s')"),

    SELECT_BY_ID("SELECT * from dbo.Users where id = '%s'"),
    SELECT_BY_NAME("SELECT * from dbo.Users where UserName = '%s'"),
    DELETE_BY_ID("DELETE dbo.Users  WHERE id = %d"),
    DELETE_BY_NAME("DELETE dbo.Users  WHERE UserName = '%s'"),
    DELETE_FROM_ROLE_MAP("DELETE dbo.UserToRoleMap where UserId = '%s'"),
    MAP_ROLE_TO_USER("INSERT INTO dbo.UserToRoleMap (UserId, RoleId) VALUES " +
            " (N'%s', N'%s')"),
    MAP_PLAYER_TO_USER("UPDATE dbo.Users SET PlayerId = '%s' " +
            "WHERE id = '%s'");


    private final String query;
    UserQueries(String query) {
        this.query = query;
    }


    @Override
    public String toString() {
        return query;
    }
}
