package com.softserveinc.volleymanagementtests.dal.repositories;

import com.softserveinc.volleymanagementtests.dal.SqlQuery;
import com.softserveinc.volleymanagementtests.dal.queries.ContributorQueries;
import com.softserveinc.volleymanagementtests.dal.repositories.exceptions.DBException;
import com.softserveinc.volleymanagementtests.specification.TestConfig;
import com.softserveinc.volleymanagementtests.dal.models.ContributorDal;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Danil Zhyliaiev
 */
public class ContributorRepository {
    public List<ContributorDal> getAll() {
        try (SqlQuery sqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            ResultSet resultSet = sqlQuery.select(ContributorQueries.SELECT_ALL.toString());
            List<ContributorDal> allContributors = new ArrayList<>();

            while (resultSet.next()) {
                allContributors.add(getContributorModel(resultSet));
            }

            return allContributors;
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("reading", e);
        }

    }

    public List<ContributorDal> getTeamMembersById(int teamId) {
        try (SqlQuery sqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            ResultSet resultSet = sqlQuery.select(
                    String.format(ContributorQueries.SELECT_BY_TEAM_ID.toString(), teamId));
            List<ContributorDal> allContributors = new ArrayList<>();

            while (resultSet.next()) {
                allContributors.add(getContributorModel(resultSet));
            }

            return allContributors;
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("reading", e);
        }
    }

    private ContributorDal getContributorModel(final ResultSet resultSet) {
        try {
            Integer id = resultSet.getInt("Id");
            if (resultSet.wasNull()) {
                id = null;
            }

            Integer teamId = resultSet.getInt("ContributorTeamId");
            if (resultSet.wasNull()) {
                teamId = null;
            }

            return new ContributorDal().newBuilder()
                    .setId(id)
                    .setName(resultSet.getString("Name"))
                    .setTeamId(teamId)
                    .build();
        } catch (SQLException e) {
            throw new DBException("reading", e);
        }
    }
}
