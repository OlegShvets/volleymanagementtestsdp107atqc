package com.softserveinc.volleymanagementtests.dal.repositories;

import com.softserveinc.volleymanagementtests.dal.SqlQuery;
import com.softserveinc.volleymanagementtests.dal.models.ContributorTeamDal;
import com.softserveinc.volleymanagementtests.dal.queries.ContributorTeamsQueries;
import com.softserveinc.volleymanagementtests.dal.repositories.exceptions.DBException;
import com.softserveinc.volleymanagementtests.specification.TestConfig;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Danil Zhyliaiev
 */
public class ContributorTeamRepository {
    public List<ContributorTeamDal> getAll(){
        try (SqlQuery sqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            List<ContributorTeamDal> allTeams;
            try (ResultSet resultSet = sqlQuery.select(ContributorTeamsQueries
                    .SELECT_ALL.toString())) {
                allTeams = new ArrayList<>();

                while (resultSet.next()) {
                    allTeams.add(getContributorTeamModel(resultSet));
                }
            }

            return allTeams;
        }catch (SQLException | ClassNotFoundException e) {
            throw new DBException("reading", e);
        }
    }

    public ContributorTeamDal getById(int id){
        try (SqlQuery sqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            ResultSet resultSet = sqlQuery.select(
                    String.format(ContributorTeamsQueries.SELECT_BY_ID.toString(), id));
            resultSet.next();

            return getContributorTeamModel(resultSet);
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("reading", e);
        }
    }

    private ContributorTeamDal getContributorTeamModel(
            final ResultSet resultSet) {
        try {
            Integer id = resultSet.getInt("Id");
            if (resultSet.wasNull()) {
                id = null;
            }

            return ContributorTeamDal.newBuilder()
                    .setId(id)
                    .setName(resultSet.getString("Name"))
                    .setCourseDirection(resultSet.getString("CourseDirection"))
                    .build();
        } catch (SQLException e) {
            throw new DBException("reading", e);
        }
    }
}
