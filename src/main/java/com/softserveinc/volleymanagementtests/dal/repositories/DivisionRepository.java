package com.softserveinc.volleymanagementtests.dal.repositories;

import com.softserveinc.volleymanagementtests.dal.SqlQuery;
import com.softserveinc.volleymanagementtests.dal.models.DivisionDal;
import com.softserveinc.volleymanagementtests.dal.queries.DivisionQueries;
import com.softserveinc.volleymanagementtests.dal.repositories.exceptions.DBException;
import com.softserveinc.volleymanagementtests.specification.TestConfig;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by stas on 11.03.2017.
 */
public final class DivisionRepository {

    public int create(final DivisionDal division) {
        ResultSet resultSet;
        try (SqlQuery SqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            resultSet = SqlQuery.getKey(String.format(DivisionQueries.INSERT.toString(),
                    division.getName(), division.getTournamentId()
            ));
            resultSet.next();
            return resultSet.getInt(1);
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("writing", e);
        }
    }

    public int getDivisionIdByTournamentId(int tournamentId) {
        ResultSet resultSet;
        try (SqlQuery SqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            resultSet = SqlQuery.select(String.format(DivisionQueries
                    .SELECT_BY_TRMENT_ID.toString(), tournamentId
            ));
            resultSet.next();
            return resultSet.getInt(1);
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("reading", e);
        }
    }
}
