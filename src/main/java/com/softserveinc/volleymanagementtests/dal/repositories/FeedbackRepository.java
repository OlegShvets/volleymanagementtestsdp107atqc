package com.softserveinc.volleymanagementtests.dal.repositories;

import com.softserveinc.volleymanagementtests.dal.SqlQuery;
import com.softserveinc.volleymanagementtests.dal.models.FeedbackDal;
import com.softserveinc.volleymanagementtests.dal.queries.*;
import com.softserveinc.volleymanagementtests.dal.repositories.exceptions.DBException;
import com.softserveinc.volleymanagementtests.specification.TestConfig;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by stas on 16.03.2017.
 */
public final class FeedbackRepository {

    public int create(final FeedbackDal feedback) {

        try (SqlQuery SqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            ResultSet resultset = SqlQuery.getKey(String.format(FeedBackQueries.INSERT.toString(),
                    feedback.getUserEmail(), feedback.getContent(),
                    dateFormat.format(feedback.getFeedbackDate()),
                    feedback.getStatusCode(), dateFormat.format(feedback.getUpdateDate()),
                    feedback.getAdminName(), feedback.getUserEnv()));
            resultset.next();
            return resultset.getInt(1);
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("writing", e);
        }
    }


    private List<FeedbackDal> selectByPredicate(final String predicate, final String query) {
        List<FeedbackDal> feedbackList = new ArrayList<>();
        try (SqlQuery SqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            ResultSet resultSet = SqlQuery.select(
                    String.format(query, predicate));
            while (resultSet.next()) {
                feedbackList.add(getFeedBackDalModel(resultSet));
            }

        }catch (SQLException | ClassNotFoundException e) {
            throw new DBException("reading", e);
        }
        return feedbackList;
    }

    public FeedbackDal getById(int id) {
        List<FeedbackDal> feedbackDalList = selectByPredicate(String.valueOf(id),
                FeedBackQueries.SELECT_BY_ID.toString());
        if (!feedbackDalList.isEmpty()) {
            return feedbackDalList.get(0);
        }
        throw new NoSuchElementException(String.format("No feedback with id %s in DB", id));
    }

    public List<FeedbackDal> getByEmail(final String email) {
        return selectByPredicate(email,
                FeedBackQueries.SELECT_BY_EMAIL.toString());
    }


    public void deleteById(int id) {
        deleteByPredicate(String.valueOf(id), FeedBackQueries.DELETE_BY_ID.toString());
    }

    public void deleteByEmail(final String email) {
        deleteByPredicate(email, FeedBackQueries.DEL_BY_EMAIL.toString());
    }


    private void deleteByPredicate(final String predicate, final String query) {
        try (SqlQuery sqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            sqlQuery.modify(String.format(query, predicate));
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("deleting", e);
        }
    }

    private FeedbackDal getFeedBackDalModel(final ResultSet resultSet) {
        try {
            String admin = resultSet.getString("AdminName");
            if (resultSet.wasNull()) {
                admin = null;
            }
            return FeedbackDal.newBuilder()
                    .setId(resultSet.getInt("Id"))
                    .setUserEmail(resultSet.getString("UsersEmail"))
                    .setContent(resultSet.getString("Content"))
                    .setFeedBackDate(resultSet.getDate("Date"))
                    .setStatusCode(resultSet.getInt("StatusCode"))
                    .setUpdateDate(resultSet.getDate("UpdateDate"))
                    .setAdminName(admin)
                    .setUserEnv(resultSet.getString("UserEnvironment"))
                    .build();
        } catch (SQLException e) {
            throw new DBException("reading", e);
        }
    }


}
