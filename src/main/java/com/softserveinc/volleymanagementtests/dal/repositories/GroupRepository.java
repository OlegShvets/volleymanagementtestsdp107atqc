package com.softserveinc.volleymanagementtests.dal.repositories;

import com.softserveinc.volleymanagementtests.dal.SqlQuery;
import com.softserveinc.volleymanagementtests.dal.models.GroupDal;
import com.softserveinc.volleymanagementtests.dal.queries.GroupQueries;
import com.softserveinc.volleymanagementtests.dal.repositories.exceptions.DBException;
import com.softserveinc.volleymanagementtests.specification.TestConfig;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by stas on 12.03.2017.
 */
public final class GroupRepository {

    public int create(final GroupDal group) {
        try (SqlQuery SqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            ResultSet resultSet = SqlQuery.getKey(String.format(
                    GroupQueries.INSERT_QUERY.toString(),
                    group.getName(), group.getDivisionId()
            ));
            resultSet.next();
            return resultSet.getInt(1);

        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("writing", e);
        }
    }
}
