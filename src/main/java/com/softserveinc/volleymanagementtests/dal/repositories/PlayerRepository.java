package com.softserveinc.volleymanagementtests.dal.repositories;

import com.softserveinc.volleymanagementtests.dal.SqlQuery;
import com.softserveinc.volleymanagementtests.dal.models.PlayerDal;
import com.softserveinc.volleymanagementtests.dal.queries.PlayerQueries;
import com.softserveinc.volleymanagementtests.dal.queries.TeamQueries;
import com.softserveinc.volleymanagementtests.dal.repositories.exceptions.DBException;
import com.softserveinc.volleymanagementtests.specification.TestConfig;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class PlayerRepository {


    public List<PlayerDal> getAll() {
        try (SqlQuery SqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            ResultSet resultSet = SqlQuery.select(PlayerQueries.SELECT_ALL.toString());
            List<PlayerDal> playerModelsList = new ArrayList<>();
            if (resultSet != null) {
                while (resultSet.next()) {
                    playerModelsList.add(getPlayerModel(resultSet));
                }
            }
            return playerModelsList;
        }catch (SQLException | ClassNotFoundException e) {
            throw new DBException("reading", e);
        }
    }

    public PlayerDal getById(int id) {
        try (SqlQuery SqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            ResultSet resultSet = SqlQuery.select(
                    String.format(PlayerQueries.SELECT_BY_ID.toString(), id));
            resultSet.next();
            return getPlayerModel(resultSet);
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("reading", e);
        }
    }

    public void setPlayerToTeam(int playerId, int teamId) {
        try (SqlQuery SqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            SqlQuery.modify(String.format(PlayerQueries.UPDATE_TEAM.toString(), teamId, playerId));
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("writing", e);
        }
    }

    public List<PlayerDal> getTeamMembers(int teamId) {
        try (SqlQuery SqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            ResultSet resultSet = SqlQuery.select(String.format(
                    PlayerQueries.SELECT_TEAM_MEMBERS.toString(),
                    teamId));
            List<PlayerDal> playerModelsList = new ArrayList<>();
            if (resultSet != null) {
                while (resultSet.next()) {
                    playerModelsList.add(getPlayerModel(resultSet));
                }
            }
            return playerModelsList;
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("reading", e);
        }

    }

    public void setPlayerLinkRequest(int userId, int playerId) {
        try (SqlQuery SqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            SqlQuery.modify(String.format(
                    PlayerQueries.SET_LINK_REQUEST.toString(),
                    userId, playerId));
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("updating", e);
        }
    }


    public PlayerDal create(PlayerDal player) {
        try (SqlQuery SqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            ResultSet resulеSetKeys = SqlQuery.getKey(String.format(
                    PlayerQueries.INSERT.toString(),
                    player.getFirstName().replace("'", "''"),
                    player.getLastName().replace("'", "''"),
                    player.getBirthYear(),
                    player.getHeight(),
                    player.getWeight()));

            resulеSetKeys.next();
            return getById(resulеSetKeys.getInt(1));
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("writing", e);
        }
    }


    public int deletePlayer(PlayerDal player) {
        int affectedRows;
        try (SqlQuery sqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            affectedRows = sqlQuery.modify(String.format(
                    PlayerQueries.DELETE_BY_ALL_FIELDS.toString(), player.getFirstName(),
                    player.getLastName(), player.getBirthYear(),
                    player.getHeight(), player.getWeight()));
            return affectedRows;
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("deleting", e);
        }
    }


    public int deleteById(Integer id) {
        int affectedRows;
        try (SqlQuery sqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            affectedRows = sqlQuery.modify(String.format(
                    PlayerQueries.DELETE_BY_ID.toString(), id));
            return affectedRows;
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("deleting", e);
        }
    }

    public int deleteAll() {
        int affectedRows;
        try (SqlQuery sqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            sqlQuery.modify(PlayerQueries.UPDATE_TO_NULL.toString());
            sqlQuery.modify(TeamQueries.DELETE_ALL_QUERY.toString());
            affectedRows = sqlQuery.modify(PlayerQueries.DELETE.toString());
            return affectedRows;
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("deleting", e);
        }
    }

    public PlayerDal getPlayerModel(ResultSet resultSet) {
        try {
            Integer birthYear = resultSet.getInt("birthYear");
            if (resultSet.wasNull()) {
                birthYear = null;
            }

            Integer height = resultSet.getInt("height");
            if (resultSet.wasNull()) {
                height = null;
            }

            Integer weight = resultSet.getInt("weight");
            if (resultSet.wasNull()) {
                weight = null;
            }

            return new PlayerDal.PlayerDalBuilder()
                    .id(resultSet.getInt("Id"))
                    .firstName(resultSet.getString("FirstName"))
                    .lastName(resultSet.getString("LastName"))
                    .birthYear(birthYear)
                    .height(height)
                    .weight(weight)
                    .build();
        } catch (SQLException e) {
            throw new DBException("reading", e);
        }
    }

    public boolean isPlayerExistsInDataBaseByLastName(String lastName) {
        try (SqlQuery SqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            ResultSet selectedPlayers
                    = SqlQuery.select(String.format(
                    PlayerQueries.SELECT_BY_LAST_NAME.toString(), lastName.replace("'", "''")));
            return selectedPlayers.next();
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("reading", e);
        }
    }

    public boolean isPlayerExistsInDataBaseByFirstName(String firstName) {
        try (SqlQuery SqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            ResultSet selectedPlayers
                    = SqlQuery.select(String.format(
                    PlayerQueries.SELECT_BY_FIRST_NAME.toString(), firstName.replace("'", "''")));
            return selectedPlayers.next();
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("reading", e);
        }
    }

    public boolean isPlayerExistsInDataBase(PlayerDal player) {
        ResultSet resultSetReturnedPlayers;
        try (SqlQuery sqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            resultSetReturnedPlayers
                    = sqlQuery.select(String.format(
                    PlayerQueries.SELECT_BY_ALL_FIELDS.toString(),
                    player.getFirstName().replace("'", "''"),
                    player.getLastName().replace("'", "''"),
                    player.getBirthYear(), player.getHeight(),
                    player.getWeight()));
            return resultSetReturnedPlayers.next();
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("reading", e);
        }
    }
}
