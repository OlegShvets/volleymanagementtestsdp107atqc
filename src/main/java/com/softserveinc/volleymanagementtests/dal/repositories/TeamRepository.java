package com.softserveinc.volleymanagementtests.dal.repositories;

/* @author S.Tsyganovskiy */

import com.softserveinc.volleymanagementtests.dal.SqlQuery;
import com.softserveinc.volleymanagementtests.dal.models.TeamDal;
import com.softserveinc.volleymanagementtests.dal.queries.PlayerQueries;
import com.softserveinc.volleymanagementtests.dal.queries.TeamQueries;
import com.softserveinc.volleymanagementtests.dal.repositories.exceptions.DBException;
import com.softserveinc.volleymanagementtests.specification.TestConfig;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class TeamRepository {

    public int create(TeamDal team) {
        try (SqlQuery SqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            ResultSet resulSetKeys = SqlQuery.getKey(String.format(TeamQueries
                            .INSERT_QUERY.toString(),
                    team.getName().replace("'", "''"), team.getCoach(),
                    team.getAchievements(), team.getCaptainId()));
            resulSetKeys.next();
            int key = resulSetKeys.getInt(1);
            team.setId(key);
            return key;
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("writing", e);
        }
    }


    public TeamDal getById(int id) {
        try (SqlQuery SqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            ResultSet resultSet = SqlQuery.select(
                    String.format(TeamQueries.SELECT_BY_ID.toString(), id));
            resultSet.next();
            return getTeamModel(resultSet);
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("reading", e);
        }
    }

    public List<TeamDal> getAll() {
        try (SqlQuery SqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            ResultSet resultSet = SqlQuery.select(TeamQueries.SELECT_ALL.toString());
            List<TeamDal> teamModelsList = new ArrayList<>();
            if (resultSet != null) {
                while (resultSet.next()) {
                    teamModelsList.add(getTeamModel(resultSet));
                }
            }
            return teamModelsList;
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("reading", e);
        }
    }

    public int deleteById(Integer id) {
        int affectedRows;
        try (SqlQuery sqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            sqlQuery.modify(String.format(PlayerQueries.UPDATE.toString(), id));
            affectedRows = sqlQuery.modify(String.format(TeamQueries.DELETE_BY_ID_QUERY.toString(), id));
            return affectedRows;
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("deleting", e);
        }
    }

    public int deleteAll() {
        int affectedRows;
        try (SqlQuery sqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            sqlQuery.modify(PlayerQueries.UPDATE_TO_NULL.toString());
            affectedRows = sqlQuery.modify(TeamQueries.DELETE_ALL_QUERY.toString());
            return affectedRows;
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("deleting", e);
        }
    }

    public boolean isTeamExistsInDataBaseByName(String name) {
        try (SqlQuery SqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            ResultSet selectedTeams
                    = SqlQuery.select(String.format(TeamQueries
                    .SELECT_BY_NAME_QUERY.toString(), name.replace("'", "''")));
            return selectedTeams.next();
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("reading", e);
        }
    }

    public boolean isTeamExistsInDataBaseById(int id) {
        try (SqlQuery SqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            ResultSet selectedTeams
                    = SqlQuery.select(String.format(TeamQueries.SELECT_BY_ID.toString(), id));
            return selectedTeams.next();
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("reading", e);
        }
    }

    private TeamDal getTeamModel(ResultSet resultSet) {
        try {
            String achievements = resultSet.getString("Achievements");
            if (resultSet.wasNull()) {
                achievements = null;
            }

            String coach = resultSet.getString("Coach");
            if (resultSet.wasNull()) {
                coach = null;
            }
            return TeamDal.newBuilder()
                    .setId(resultSet.getInt("Id"))
                    .setName(resultSet.getString("Name"))
                    .setCoach(coach)
                    .setAchievements(achievements)
                    .setCaptainId(resultSet.getInt("CaptainId"))
                    .build();
        } catch (SQLException e) {
            throw new DBException("reading", e);
        }
    }
}