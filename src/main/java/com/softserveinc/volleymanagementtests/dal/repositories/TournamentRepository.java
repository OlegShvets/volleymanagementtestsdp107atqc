package com.softserveinc.volleymanagementtests.dal.repositories;

import com.softserveinc.volleymanagementtests.dal.SqlQuery;
import com.softserveinc.volleymanagementtests.dal.models.TeamDal;
import com.softserveinc.volleymanagementtests.dal.models.TournamentDal;
/*import com.softserveinc.volleymanagementtests.dal.queries.GameResultQueries;*/
import com.softserveinc.volleymanagementtests.dal.queries.DivisionQueries;
import com.softserveinc.volleymanagementtests.dal.queries.GameResultQueries;
import com.softserveinc.volleymanagementtests.dal.queries.GroupQueries;
import com.softserveinc.volleymanagementtests.dal.queries.TournamentQueries;
import com.softserveinc.volleymanagementtests.dal.repositories.exceptions.DBException;
import com.softserveinc.volleymanagementtests.specification.TestConfig;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by stas on 09.03.2017.
 */
public final class TournamentRepository {

    public int create(final TournamentDal trnmnt) {
        try (SqlQuery sqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            ResultSet resultset = sqlQuery.getKey(String.format(
                    TournamentQueries.INSERT.toString(),
                    trnmnt.getName(), trnmnt.getSchemeCode(), trnmnt.getSeasonOffset(),
                    trnmnt.getDescription(), trnmnt.getRegulationsLink(),
                    dateFormat.format(trnmnt.getTournamentStart()),
                    dateFormat.format(trnmnt.getTournamentEnd()),
                    dateFormat.format(trnmnt.getTransferStart()),
                    dateFormat.format(trnmnt.getTransferEnd()),
                    dateFormat.format(trnmnt.getApplyingPeriodStart()),
                    dateFormat.format(trnmnt.getApplyingPeriodEnd()),
                    dateFormat.format(new Date())));
            resultset.next();
            int id = resultset.getInt(1);
            trnmnt.setId(id);
            return id;
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("writing", e);
        }
    }

    public TournamentDal getById(int id) {
        List<TournamentDal> tournamentDalList = selectByPredicate(String.valueOf(id),
                TournamentQueries.SELECT_BY_ID.toString());
        if (!tournamentDalList.isEmpty()) {
            return tournamentDalList.get(0);
        }
        throw new NoSuchElementException(String.format("No tournament with id %s in DB", id));


    }

    public TournamentDal getByName(final String name) {
        List<TournamentDal> tournamentDalList = selectByPredicate(name,
                TournamentQueries.SELECT_BY_NAME.toString());
        if (!tournamentDalList.isEmpty()) {
            return tournamentDalList.get(0);
        }
        throw new DBException(String.format("No tournament with name %s in DB", name));

    }

    public boolean isTournamentExistsInDbById(int id) {
        try {
            getById(id);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }


    public boolean areTournamentsExistInDbByName(final String name) {
        try {
            getByName(name);
            return true;
        } catch (DBException e) {
            return false;
        }

    }

    private List<TournamentDal> selectByPredicate(final String predicate, final String query) {
        List<TournamentDal> tournamentModelList = new ArrayList<>();
        try (SqlQuery sqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            ResultSet resultSet = sqlQuery.select(
                    String.format(query, addSingleQuotesIfNeeded(predicate)));
            while (resultSet.next()) {
                tournamentModelList.add(getTournamentDalModel(resultSet));
            }

        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("reading", e);
        }
        return tournamentModelList;
    }

    private String addSingleQuotesIfNeeded(final String predicate) {
        if (!predicate.matches("[0-9]+")) {
            return "'" + predicate + "'";
        }
        return predicate;
    }

    public List<TournamentDal> getAllTournaments() {
        List<TournamentDal> tournamentModelList = new ArrayList<>();
        try (SqlQuery sqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            ResultSet resultSet = sqlQuery.select(TournamentQueries.SELECT_ALL.toString());
            if (resultSet != null) {
                while (resultSet.next()) {
                    tournamentModelList.add(getTournamentDalModel(resultSet));
                }
            }
            return tournamentModelList;
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("reading", e);
        }
    }

    public void deleteById(int id) {
           deleteByPredicate(String.valueOf(id), GameResultQueries.DEL_GAMES_FOR_TRMENT.toString());
           deleteByPredicate(String.valueOf(id), TournamentQueries.DEL_BY_TOUR_ID.toString());
           int divisionKey = new DivisionRepository().getDivisionIdByTournamentId(id);
           deleteByPredicate(String.valueOf(divisionKey), GroupQueries.DEL_GROUP_BY_ID.toString());
           deleteByPredicate(String.valueOf(id), DivisionQueries.DEL_DIV_BY_ID_QUERY.toString());
           deleteByPredicate(String.valueOf(id), TournamentQueries.DELETE_BY_ID.toString());
       }

    public void deleteByName(String name) {
        deleteByPredicate(name, TournamentQueries.DELETE_BY_NAME.toString());
    }


    private void deleteByPredicate(final String predicate, final String query) {
        try (SqlQuery sqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            sqlQuery.modify(String.format(query, predicate));
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("deleting", e);
        }
    }


    public void addTeamToTournament(final TournamentDal tournament, final List<TeamDal> teams) {
        try (SqlQuery sqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            List<String> queries = new ArrayList<>();
            for (TeamDal teamDal : teams) {
                queries.add(String.format(TournamentQueries.INSERT_TEAM.toString(),
                        tournament.getId(), teamDal.getId()));
            }
            sqlQuery.modifyBatch(queries);

            /*special case for playoff scheme*/
            if (tournament.getSchemeCode() == 4) {
                List<String> gameResultqueries = new ArrayList<>();
                Integer round;
                for (int i = 1; i <= teams.size(); i++) {

                    round = getRoundForTournamentsPlayOff(teams.size(), i);
                    gameResultqueries.add(String.format(GameResultQueries
                                    .INSERT_FOR_PLAYOFF.toString(),
                            tournament.getId()
                            , round
                            , i));
                }
                sqlQuery.modifyBatch(gameResultqueries);
                Round.resetRound();
            }

        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("writing", e);
        }
    }


    private static class Round  {
        private static int round = 1;
        static int getRound() {
            return round;
        }
        static void incRound() {
            round++;
        }
        static void resetRound() {
          round = 1;
        }
    }


    private int getRoundForTournamentsPlayOff(int teamSize, int currentGameNumber)
    {
        int tempGameNumber = currentGameNumber - teamSize / 2;
        if (teamSize == 2 ) {
            return Round.getRound();
        }
        if (tempGameNumber == 1) {
            Round.incRound();
            return Round.getRound();

        } else if (tempGameNumber > 1 && teamSize != currentGameNumber) {
            getRoundForTournamentsPlayOff(teamSize / 2, tempGameNumber);
        }
        return Round.getRound();
    }

    private TournamentDal getTournamentDalModel(final ResultSet resultSet) {
        try {
            String description = resultSet.getString("Description");
            if (resultSet.wasNull()) {
                description = null;
            }
            String regulationsLink = resultSet.getString("RegulationsLink");
            if (resultSet.wasNull()) {
                regulationsLink = null;
            }
            Date transferStart = resultSet.getDate("TransferStart");
            if (resultSet.wasNull()) {
                transferStart = null;
            }
            Date transferEnd = resultSet.getDate("TransferEnd");
            if (resultSet.wasNull()) {
                transferEnd = null;
            }
            return TournamentDal.newBuilder()
                    .setId(resultSet.getInt("Id"))
                    .setName(resultSet.getString("Name"))
                    .setSchemeCode(resultSet.getInt("SchemeCode"))
                    .setSeasonOffset(resultSet.getInt("SeasonOffset"))
                    .setDescription(description)
                    .setRegulationsLink(regulationsLink)
                    .setTournamentStart(resultSet.getDate("GamesStart"))
                    .setTournamentEnd(resultSet.getDate("GamesEnd"))
                    .setTransferStart(transferStart)
                    .setTournamentEnd(transferEnd)
                    .setApplyingPeriodStart(resultSet.getDate("ApplyingPeriodStart"))
                    .setApplyingPeriodEnd(resultSet.getDate("ApplyingPeriodEnd"))
                    .build();
        } catch (SQLException e) {
            throw new DBException("reading", e);
        }
    }
}