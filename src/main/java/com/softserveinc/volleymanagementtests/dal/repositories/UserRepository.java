package com.softserveinc.volleymanagementtests.dal.repositories;

import com.softserveinc.volleymanagementtests.dal.SqlQuery;
import com.softserveinc.volleymanagementtests.dal.models.UserDal;
import com.softserveinc.volleymanagementtests.dal.queries.*;
import com.softserveinc.volleymanagementtests.dal.repositories.exceptions.DBException;
import com.softserveinc.volleymanagementtests.specification.TestConfig;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by stas on 19.03.2017.
 */
public final class UserRepository {


    public int create(final UserDal user) {
        try (SqlQuery SqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            ResultSet resultset = SqlQuery.getKey(String.format(
                    UserQueries.INSERT.toString(),
                    user.getUserName(), user.getFullName(),
                    user.getCellPhone(), user.getEmail(),
                    user.getIsBlocked()));
            resultset.next();
            return resultset.getInt(1);
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("writing", e);
        }
    }

    public void mapRoleToUser(int userId, int roleId) {
        try (SqlQuery SqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            SqlQuery.modify(String.format(
                    UserQueries.MAP_ROLE_TO_USER.toString(),
                    userId, roleId));
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("update", e);
        }
    }

    public void setPlayerIdToUser(int userId, int playerid) {
        try (SqlQuery SqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            SqlQuery.modify(String.format(
                    UserQueries.MAP_PLAYER_TO_USER.toString(),
                    playerid, userId));
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("writing", e);
        }
    }


    public UserDal getById(int id) {
        List<UserDal> userDalList = selectByPredicate(String.valueOf(id),
                UserQueries.SELECT_BY_ID.toString());
        if (!userDalList.isEmpty()) {
            return userDalList.get(0);
        }
        throw new NoSuchElementException(String.format("No user with id %s in DB", id));

    }


    public UserDal getByName(final String name) {
        List<UserDal> userDalList = selectByPredicate(name,
                UserQueries.SELECT_BY_NAME.toString());
        if (!userDalList.isEmpty()) {
            return userDalList.get(0);
        }
        throw new NoSuchElementException(String.format("No user with name %s in DB", name));
    }


    public boolean isUserExistsInDbById(int id) {
        try {
            getById(id);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }


    public boolean areUsersExistInDbByName(String name) {
        try {
            getByName(name);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }


    public void deleteById(int id) {
        deleteByPredicate(String.valueOf(id),
                UserQueries.DELETE_FROM_ROLE_MAP.toString());
        deleteByPredicate(String.valueOf(id), UserQueries.DELETE_BY_ID.toString());
    }

    public void deleteByName(String name) {
        UserDal userDal = getByName(name);
        deleteByPredicate(String.valueOf(userDal.getId()),
                UserQueries.DELETE_FROM_ROLE_MAP.toString());
        deleteByPredicate(name, UserQueries.DELETE_BY_NAME.toString());
    }


    private void deleteByPredicate(String predicate, String query) {
        try (SqlQuery sqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            sqlQuery.modify(String.format(query, predicate));
        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("deleting", e);
        }
    }


    private List<UserDal> selectByPredicate(String predicate, String query) {
        List<UserDal> userModelList = new ArrayList<>();
        try (SqlQuery SqlQuery = new SqlQuery(new TestConfig().getDBConnectionString())) {
            ResultSet resultSet = SqlQuery.select(
                    String.format(query, predicate));
            while (resultSet.next()) {
                userModelList.add(getusertDalModel(resultSet));
            }

        } catch (SQLException | ClassNotFoundException e) {
            throw new DBException("reading", e);
        }
        return userModelList;
    }



    private UserDal getusertDalModel(ResultSet resultSet) {
        try {
        return new UserDal.UserDalBuilder()
                .setId(resultSet.getInt("id"))
                .setName(resultSet.getString("UserName"))
                .setFullName(resultSet.getString("FullName"))
                .setCellPhone(resultSet.getString("CellPhone"))
                .setEmail(resultSet.getString("Email"))
                .setPlayerId(resultSet.getString("PlayerId"))
                .setBlocked(resultSet.getInt("isBlocked"))
                .build();
    } catch (SQLException e) {
            throw new DBException("reading", e);
        }
    }
}
