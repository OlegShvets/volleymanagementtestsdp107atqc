package com.softserveinc.volleymanagementtests.pages;

import com.softserveinc.volleymanagementtests.specification.TestConfig;

/**
 * System under test page locators are stored here.
 * in order the possibility the web driver to go directly to the
 * specified page location (not "getting the base url->clicking links" to reach the needed page)
 * is present.
 */
public final class PageUrls {

    public final String BASE_URL = new TestConfig().getSystemUnderTestBaseUrl();

    public final String PLAYER_CREATE_URL =
            String.format("%s/Players/Create", new TestConfig().getSystemUnderTestBaseUrl());

    public final String TEAM_CREATE_URL =
            String.format("%s/Teams/Create", new TestConfig().getSystemUnderTestBaseUrl());

    public final String PLAYERS_LIST_URL =
            String.format("%s/Players", new TestConfig().getSystemUnderTestBaseUrl());

    public final String TURNAMENTS_URL =
            String.format("%s", new TestConfig().getSystemUnderTestBaseUrl());

    public final String CREATE_TURNAMENTS_URL =
            String.format("%s/Tournaments/Create", new TestConfig().getSystemUnderTestBaseUrl());

    public final String TEAMS_LIST_URL =
            String.format("%s/Teams", new TestConfig().getSystemUnderTestBaseUrl());

    public final String CONTRIBUTORS_LIST_URL =
            String.format("%s/ContributorsTeam", new TestConfig().getSystemUnderTestBaseUrl());

    public final String GOOGLE_LOGIN_URL =
            String.format("%s/Account/Login", new TestConfig().getSystemUnderTestBaseUrl());

    public final String TOUR_PLAYOFF_2 =
            String.format("%s/Tournaments/Details/8", new TestConfig().getSystemUnderTestBaseUrl());

    public final String ADMIN_MAIN_PAGE =
            String.format("%s/Admin", new TestConfig().getSystemUnderTestBaseUrl());
}
