package com.softserveinc.volleymanagementtests.pages.admin.admindetailpage;

import com.softserveinc.volleymanagementtests.pages.admin.admindetailpage.uimaps.AdminDetailsUIMap;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;

public class AdminDetailsPage {
    public AdminDetailsUIMap control;
    
    public AdminDetailsPage() {
        this.control = new AdminDetailsUIMap();
    }
    
    public Label getNameOfTable() {
        return control.getNameOfTable();
    }
    
    public Label getId() {
        return control.getId();
    }
    
    public Label getEmail() {
        return control.getEmail();
    }
    
    public Label getContent() {
        return control.getContent();
    }
    
    public Label getData() {
        return control.getData();
    }
    
    public Label getStatus() {
        return control.getStatus();
    }
    
    public Label getAdminName() {
        return control.getAdminName();
    }
    
    public Label getReplyDate() {
        return control.getReplyDate();
    }
}
    
