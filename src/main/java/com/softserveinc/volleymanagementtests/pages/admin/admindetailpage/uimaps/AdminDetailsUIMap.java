package com.softserveinc.volleymanagementtests.pages.admin.admindetailpage.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import org.openqa.selenium.By;

public class AdminDetailsUIMap {
    private Label nameOfTable;
    private Label id;
    private Label email;
    private Label content;
    private Label data;
    private Label status;
    private Label adminName;
    private Label replyDate;
    
    public AdminDetailsUIMap() {
        this.nameOfTable = new LabelImpl(new Control(new ContextVisible(By.className("panel-heading"))));
        this.id = new LabelImpl(new Control(new ContextVisible(
                By.cssSelector("tbody > tr:nth-child(1) > td:nth-child(2)"))));
        this.email = new LabelImpl(new Control(new ContextVisible(
                By.cssSelector("tbody > tr:nth-child(2) > td:nth-child(2)"))));
        this.content = new LabelImpl(new Control(new ContextVisible(
                By.cssSelector("tbody > tr:nth-child(3) > td:nth-child(2)"))));
        this.data = new LabelImpl(new Control(new ContextVisible(
                By.cssSelector("tbody > tr:nth-child(4) > td:nth-child(2)"))));
        this.status = new LabelImpl(new Control(new ContextVisible(
                By.cssSelector("tbody > tr:nth-child(5) > td:nth-child(2)"))));
        this.adminName = new LabelImpl(new Control(new ContextVisible(
                By.cssSelector("tbody > tr:nth-child(6) > td:nth-child(2)"))));
        this.replyDate = new LabelImpl(new Control(new ContextVisible(
                By.cssSelector("tbody > tr:nth-child(7) > th:nth-child(2)"))));
    }
    
    public Label getNameOfTable() {
        return nameOfTable;
    }
    
    public Label getId() {
        return id;
    }
    
    public Label getEmail() {
        return email;
    }
    
    public Label getContent() {
        return content;
    }
    
    public Label getData() {
        return data;
    }
    
    public Label getStatus() {
        return status;
    }
    
    public Label getAdminName() {
        return adminName;
    }
    
    public Label getReplyDate() {
        return replyDate;
    }
}
