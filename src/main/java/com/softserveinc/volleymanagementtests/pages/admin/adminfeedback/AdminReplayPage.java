package com.softserveinc.volleymanagementtests.pages.admin.adminfeedback;

import com.softserveinc.volleymanagementtests.pages.admin.requestpage.AdminFeedBackPage;
import com.softserveinc.volleymanagementtests.pages.admin.adminfeedback.uimaps.AdminReplayUIMap;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;

public class AdminReplayPage {
    private AdminReplayUIMap close;
    
    public AdminReplayPage() {
        this.close = new AdminReplayUIMap();
    }
    
    public Label nameOfPopup() {
        return close.getNameOfPopup();
    }
    
    public AdminFeedBackPage close() {
        close.getClose().click();
        return new AdminFeedBackPage();
    }
    
    public AdminFeedBackPage send() {
        close.getSend().click();
        return new AdminFeedBackPage();
    }
    
    public TextInput getMessage() {
        return close.getMessage();
    }
    
    public AdminReplayPage clearMessage() {
        close.getMessage().clear();
        return this;
    }
    
    public AdminReplayPage writeMessage(String message) {
        close.getMessage().type(message);
        return this;
    }
}
