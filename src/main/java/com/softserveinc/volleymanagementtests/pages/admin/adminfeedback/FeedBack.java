package com.softserveinc.volleymanagementtests.pages.admin.adminfeedback;

import com.softserveinc.volleymanagementtests.pages.admin.admindetailpage.AdminDetailsPage;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;

public class FeedBack {
    private String id;
    private String email;
    private String content;
    private String data;
    private String status;
    private Link details;
    private Link close;
    private Link replay;
    private boolean isClosed = false;
    
    public FeedBack(String id, String email, String content, String date, String status,
                    Link details, Link close, Link reply) {
        this.id = id;
        this.email = email;
        this.content = content;
        this.data = date;
        this.status = status;
        this.details = details;
        this.close = close;
        this.replay = reply;
        if (this.status.equals("Closed")){
            this.isClosed = true;
        }
    }
    
    public AdminDetailsPage openDetails() {
        details.click();
        return new AdminDetailsPage();
    }
    
    public FeedBack tapOnClose() {
        close.click();
        return this;
    }
    
    public AdminReplayPage getReplay() {
        if (isClosed){
            throw new UnsupportedOperationException("FeedBack is closed");
        }
        replay.click();
        return new AdminReplayPage();
    }
    
    public String getId() {
        return id;
    }
    
    public String getEmal() {
        return email;
    }
    
    public String getContent() {
        return content;
    }
    
    public String getDate() {
        return data;
    }
    
    public String getStatus() {
        return status;
    }

}
