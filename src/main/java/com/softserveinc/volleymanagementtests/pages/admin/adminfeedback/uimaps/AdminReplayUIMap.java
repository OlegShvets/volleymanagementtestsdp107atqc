package com.softserveinc.volleymanagementtests.pages.admin.adminfeedback.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.TextInputImpl;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;
import org.openqa.selenium.By;

public class AdminReplayUIMap {
    private Label nameOfPopup;
    private Button close;
    private Button send;
    private TextInput message;
    
    public AdminReplayUIMap() {
        this.nameOfPopup = new LabelImpl(new Control(new ContextVisible(By.cssSelector("form h2"))));
        this.close = new ButtonImpl(new Control(new ContextVisible(By.id("close"))));
        this.send = new ButtonImpl(new Control(new ContextVisible(By.id("submit"))));
        this.message = new TextInputImpl(new Control(new ContextVisible(By.id("feedbackMessage"))));
    }
    
    public Label getNameOfPopup() {
        return nameOfPopup;
    }
    
    public Button getClose() {
        return close;
    }
    
    public Button getSend() {
        return send;
    }
    
    public TextInput getMessage() {
        return message;
    }
}
