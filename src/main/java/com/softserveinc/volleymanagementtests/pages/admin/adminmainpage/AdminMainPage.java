package com.softserveinc.volleymanagementtests.pages.admin.adminmainpage;


import com.softserveinc.volleymanagementtests.pages.admin.adminmainpage.uimaps.AdminMainPageUIMap;
import com.softserveinc.volleymanagementtests.pages.admin.dashboardpage.DashboardPage;
import com.softserveinc.volleymanagementtests.pages.admin.requestpage.AdminFeedBackPage;
import com.softserveinc.volleymanagementtests.pages.admin.rolespage.RolesPage;
import com.softserveinc.volleymanagementtests.pages.admin.users.UsersListPage;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;

/**
 * Created by Natalia on 17.03.2017.
 */
public class AdminMainPage {

    private AdminMainPageUIMap controls;

    public AdminMainPage() {controls = new AdminMainPageUIMap();
    }
    public final AdminMainPage searchForPlayer(final String playerName) {
        controls.getSearchByTextInput().clear();
        controls.getSearchByTextInput().type(playerName);
        controls.getSearchByTextInput().submit();
        return this;
    }
    public AdminMainPage clickSearchButton(){
        controls.getSearchButton().click();
        return new AdminMainPage();
    }

    public Button getSearchButton() {
        return controls.getSearchButton();
    }

    public DashboardPage clickDashboard(){
        controls.getDashboard().click();
        return new DashboardPage();
    }

    public Link getDashBoardLink() {
        return controls.getDashboard();
    }

    public RolesPage clickRoles(){
        controls.getRoles().click();
        return new RolesPage();
    }

    public Link getRolesLink() {
        return controls.getRoles();
    }

    public AdminFeedBackPage clickRequests(){
        controls.getRequests().click();
        return new AdminFeedBackPage();
    }

    public Link getRequestsLink() {
        return controls.getRequests();
    }

    public UsersListPage clickAllUsers(){
        controls.getAllusers().click();
        return new UsersListPage();
    }

    public Link getAllUsersLink() {
        return controls.getAllusers();
    }

    public UsersListPage clickActiveUsers(){
        controls.getActiveUsers().click();
        return new UsersListPage();
    }

    public Link getActiveUsersLink() {
        return controls.getActiveUsers();
    }

}

