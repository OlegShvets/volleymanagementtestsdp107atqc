package com.softserveinc.volleymanagementtests.pages.admin.adminmainpage.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LinkImpl;
import com.softserveinc.volleymanagementtests.tools.controls.TextInputImpl;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;
import org.openqa.selenium.By;

/**
 * Created by Natalia on 17.03.2017.
 */
public class AdminMainPageUIMap {

    public final TextInput getSearchByTextInput() {
        return new TextInputImpl(new Control(new ContextVisible(
                By.cssSelector("#side-menu > li.sidebar-search > div > input"))));
    }

    public final Button getSearchButton() {
        return new ButtonImpl(new Control(new ContextVisible(
                By.cssSelector("#side-menu > li.sidebar-search > div > span > button"))));
    }

    public final Link getDashboard() {
        return new LinkImpl(new Control(new ContextVisible(
                By.cssSelector("#side-menu > li:nth-child(2) > a"))));
    }

    public final Link getRoles() {
        return new LinkImpl(new Control(new ContextVisible(
                By.cssSelector("#side-menu > li:nth-child(3) > a"))));
    }

    public final Link getRequests() {
        return new LinkImpl(new Control(new ContextVisible(
                By.cssSelector("#side-menu > li:nth-child(4) > a"))));
    }

    public final Link getAllusers() {
        return new LinkImpl(new Control(new ContextVisible(
                By.cssSelector("#side-menu > li:nth-child(5) > a"))));
    }

    public final Link getActiveUsers() {
        return new LinkImpl(new Control(new ContextVisible(
                By.cssSelector("#side-menu > li:nth-child(6) > a"))));
    }
}

