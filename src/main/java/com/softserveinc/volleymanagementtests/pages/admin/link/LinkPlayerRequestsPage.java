package com.softserveinc.volleymanagementtests.pages.admin.link;

import com.softserveinc.volleymanagementtests.pages.admin.link.uimaps.LinkPlayerRequestsPageUIMap;
import com.softserveinc.volleymanagementtests.pages.admin.link.uimaps.LinkPlayerRowUIMap;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;

import java.util.NoSuchElementException;
import java.util.function.Predicate;

/**
 * Created by skir on 3/21/2017.
 */
public final class LinkPlayerRequestsPage {
    private final LinkPlayerRequestsPageUIMap controls;

    public LinkPlayerRequestsPage() {
        this.controls = new LinkPlayerRequestsPageUIMap();
    }

    public UserLinkDetailsPage openUserDetailsForUserName(final String userName) {
        findTableRowByUserName(userName)
                .getPlayerDetailsButton().click();
        return new UserLinkDetailsPage();
    }

    public UserLinkDetailsPage openUserDetailsForUserID(int id) {
        findTableRowByUserId(id)
                .getUserDetailsButton()
                .click();
        return new UserLinkDetailsPage();
    }

    public PlayerToLinkDetailsPage openPlayersDetailsForPlayer(final String playerName) {
        findTableRowByUserName(playerName)
                .getPlayerDetailsButton()
                .click();
        return new PlayerToLinkDetailsPage();
    }

    public LinkPlayerRequestsPage declineLinkRequestForPlayerName(final String playerName) {
        findTableRowByPlayerName(playerName)
                .getDeclineForPlayerButton()
                .click();
        return this;
    }

    public Label userNameInTheRowWithPlayer(final String playerName) {
        return findRowByPredicate(row -> row.getPlayerNameLabel()
                .getText().split(" ")[1]
                .equals(playerName))
                .getUserNameLabel();
    }

    public LinkPlayerRequestsPage confirmLinkRequestForPlayerName(final String playerName) {
        findTableRowByPlayerName(playerName)
                .getConfirmForPlayerButton()
                .click();
        return this;
    }


    public boolean isPlayerPresentInTheList(final String playerName) {
       return isRowExistsByPredicate(row -> row.getPlayerNameLabel()
        .getText().split(" ")[0].equals(playerName));
    }

    public boolean isUserPresentInTheList(final String userName) {
        return isRowExistsByPredicate(row -> row.getUserNameLabel()
                .getText().split(" ")[0].equals(userName));
    }


    public LinkPlayerRowUIMap findTableRowByUserName(final String userName) {
        return findRowByPredicate(row -> row.getUserNameLabel()
                .getText().split(" ")[1]
                .equals(userName));
    }

    public LinkPlayerRowUIMap findTableRowByUserId(int userId) {
        return findRowByPredicate(row -> Integer.parseInt(row.getUserId().getText()) == userId);
    }



    private LinkPlayerRowUIMap findTableRowByPlayerName(final String playerName) {
        return findRowByPredicate(row -> row.getPlayerNameLabel()
                .getText().split(" ")[0]
                .equals(playerName));
    }


    private LinkPlayerRowUIMap findRowByPredicate(final Predicate<LinkPlayerRowUIMap> predicate) {
       return controls
                .getLinkPlayerTableRows()
                .stream()
                .filter(predicate)
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("No such row"));
    }

    private boolean isRowExistsByPredicate(final Predicate<LinkPlayerRowUIMap> predicate) {
        return controls
                .getLinkPlayerTableRows()
                .stream()
                .anyMatch(predicate);
    }
}
