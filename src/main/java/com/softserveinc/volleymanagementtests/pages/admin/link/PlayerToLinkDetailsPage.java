package com.softserveinc.volleymanagementtests.pages.admin.link;

import com.softserveinc.volleymanagementtests.pages.admin.link.uimaps.PlayerToLinkDetailsUIMap;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;

/**
 * Created by skir on 3/22/2017.
 */
public final class PlayerToLinkDetailsPage {

    private final PlayerToLinkDetailsUIMap playerToLinkDetailsUIMap;

    public PlayerToLinkDetailsPage() {
        playerToLinkDetailsUIMap = new PlayerToLinkDetailsUIMap();
    }

    public Label getPageTitle() {
        return playerToLinkDetailsUIMap.getPageTitle();
    }

    public Label getPlayerId() {
        return playerToLinkDetailsUIMap.getPlayerId();
    }

    public Label getPlayerFirstName() {
        return playerToLinkDetailsUIMap.getPlayerFirstName();
    }

    public Label getPlayerLastName() {
        return playerToLinkDetailsUIMap.getPlayerLastName();
    }

    public Label getPlayerTeamId() {
        return playerToLinkDetailsUIMap.getPlayerTeamId();
    }

    public Label getPlayerHeight() {
        return playerToLinkDetailsUIMap.getPlayerHeight();
    }

    public Label getPlayerWeight() {
        return playerToLinkDetailsUIMap.getPlayerWeight();
    }

    public LinkPlayerRequestsPage clickBackToLinkPlayerRequestPage() {
        playerToLinkDetailsUIMap
                .getBackToListLink()
                .click();
        return new LinkPlayerRequestsPage();
    }

}
