package com.softserveinc.volleymanagementtests.pages.admin.link;

import com.softserveinc.volleymanagementtests.pages.admin.link.uimaps.UserLinkDetailsUIMap;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;

/**
 * Created by skir on 3/22/2017.
 */
public final class UserLinkDetailsPage {
    private final UserLinkDetailsUIMap userLinkDetailsUIMap;


    public UserLinkDetailsPage() {
        userLinkDetailsUIMap = new UserLinkDetailsUIMap();
    }

    public Label getPageTitle() {
        return userLinkDetailsUIMap.getPageTitle();
    }

    public Label getUserId() {
        return userLinkDetailsUIMap.getUserId();
    }

    public Label getUserName() {
        return userLinkDetailsUIMap.getUserName();
    }

    public Label getUserEmail() {
        return userLinkDetailsUIMap.getUserEmail();
    }

    public Label getUserPhoneNumber() {
        return userLinkDetailsUIMap.getUserEmail();
    }

    public Label getUserFullName() {
        return userLinkDetailsUIMap.getUserFullName();
    }

    public Label getUserLinkedPlayerId() {
        return userLinkDetailsUIMap.getUserId();
    }

    public LinkPlayerRequestsPage clickBackToLinkPlayerRequestsPage() {
        userLinkDetailsUIMap
                .getBackToListLink()
                .click();
        return new LinkPlayerRequestsPage();
    }
}
