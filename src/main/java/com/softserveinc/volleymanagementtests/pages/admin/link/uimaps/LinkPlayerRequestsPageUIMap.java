package com.softserveinc.volleymanagementtests.pages.admin.link.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.ControlListWrapper;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.WebElementsList;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by skir on 3/21/2017.
 */
public final class LinkPlayerRequestsPageUIMap {


    public Label getHeaderLabel() {
        return new LabelImpl(new Control(
                new ContextVisible(By.id(".panel-heading"))));
    }

    public List<LinkPlayerRowUIMap> getLinkPlayerTableRows() {
        List<LinkPlayerRowUIMap> rows = new ArrayList<>();
        int tableSize = new WebElementsList(new ControlListWrapper(
                new ContextVisible(By.cssSelector(".table.table-striped>tbody tr"))))
                .asLabels().size();
        for (int i = 2; i <= tableSize; i++) {
            rows.add(new LinkPlayerRowUIMap(
                    String.format(".table-striped>tbody>tr:nth-of-type(%d)", i)));
        }

        return rows;
    }




}
