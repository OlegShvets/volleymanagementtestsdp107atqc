package com.softserveinc.volleymanagementtests.pages.admin.link.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import org.openqa.selenium.By;



/**
 * Created by skir on 3/21/2017.
 */
public final class LinkPlayerRowUIMap {
    String parentCss;

    public LinkPlayerRowUIMap(String parentCss) {
        this.parentCss = parentCss;
    }


    public Label getUserId() {
        return new LabelImpl(new Control(
                new ContextVisible(By
                        .cssSelector(String.format("%s > td:nth-of-type(1)",
                                parentCss)))));
    }

    public Label getUserNameLabel() {
        return new LabelImpl(new Control(
                new ContextVisible(By
                        .cssSelector(String.format("%s > td:nth-of-type(2)",
                                parentCss)))));
    }

    public Button getUserDetailsButton() {
        return new ButtonImpl(new Control(
                new ContextVisible(By
                        .cssSelector(String.format("%s > td:nth-of-type(2) > a",
                                parentCss)))));
    }

    public Label getPlayerNameLabel() {
       return new LabelImpl(new Control(
                new ContextVisible(By
                        .cssSelector(String.format("%s > td:nth-of-type(3)",
                                parentCss)))));
    }

    public Button getPlayerDetailsButton() {
        return new ButtonImpl(new Control(
                new ContextVisible(By
                        .cssSelector(String.format("%s > td:nth-of-type(3) .btn-info",
                                parentCss)))));
    }

    public Button getDeclineForPlayerButton() {
        return new ButtonImpl(new Control(
                new ContextVisible(By
                        .cssSelector(String.format("%s > td:nth-of-type(4) .btn-danger",
                                parentCss)))));
    }

    public Button getConfirmForPlayerButton() {
        return new ButtonImpl(new Control(
                new ContextVisible(By
                        .cssSelector(String.format("%s > td:nth-of-type(4) .btn-success",
                                parentCss)))));
    }
}
