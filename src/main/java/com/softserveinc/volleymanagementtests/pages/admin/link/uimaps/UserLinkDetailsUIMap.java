package com.softserveinc.volleymanagementtests.pages.admin.link.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LinkImpl;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import org.openqa.selenium.By;

/**
 * Created by skir on 3/22/2017.
 */
public class UserLinkDetailsUIMap {

    public Label getPageTitle() {
        return new LabelImpl(new Control(
                new ContextVisible(By.cssSelector(".col-lg-12>fieldset>legend"))));
    }

    public Label getUserId() {
        return new LabelImpl(new Control(
                new ContextVisible(By.cssSelector(".display-field:nth-of-type(2)"))));
    }

    public Label getUserName() {
        return new LabelImpl(new Control(
                new ContextVisible(By.cssSelector(".display-field:nth-of-type(4)"))));
    }

    public Label getUserEmail() {
        return new LabelImpl(new Control(
                new ContextVisible(By.cssSelector(".display-field:nth-of-type(6)"))));
    }

    public Label getUserPhoneNumber() {
        return new LabelImpl(new Control(
                new ContextVisible(By.cssSelector(".display-field:nth-of-type(8)"))));
    }

    public Label getUserFullName() {
        return new LabelImpl(new Control(
                new ContextVisible(By.cssSelector(".display-field:nth-of-type(10)"))));
    }

    public Label getLinkedPlayerId() {
        return new LabelImpl(new Control(
                new ContextVisible(By.cssSelector(".display-field:nth-of-type(12)"))));
    }

    public Label getLinkedPlayerLastName() {
        return new LabelImpl(new Control(
                new ContextVisible(By.cssSelector(".display-field:nth-of-type(14)"))));
    }

    public Link getBackToListLink() {
        return new LinkImpl(new Control
                (new ContextVisible(By.cssSelector(".col-lg-12 a"))));
    }













}
