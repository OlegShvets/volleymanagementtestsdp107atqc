package com.softserveinc.volleymanagementtests.pages.admin.requestpage;

import com.softserveinc.volleymanagementtests.pages.admin.adminfeedback.FeedBack;
import com.softserveinc.volleymanagementtests.pages.admin.adminmainpage.AdminMainPage;
import com.softserveinc.volleymanagementtests.pages.admin.link.LinkPlayerRequestsPage;
import com.softserveinc.volleymanagementtests.pages.admin.requestpage.uimaps.AdminFeedBackUIMap;
import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import org.openqa.selenium.By;

import java.util.List;

public class AdminFeedBackPage {
    private AdminFeedBackUIMap control;
    
    public AdminFeedBackPage() {
        this.control = new AdminFeedBackUIMap();
    }
    
    public Label getNameOfTable() {
        return control.getNameOfTable();
    }
    
    public List<FeedBack> getFeedBackGroup() {
        return control.getFeedBackGroup();
    }
    
    public FeedBack getFeedBackByID(String ID) {
        return control.getFeedBackByID(ID);
    }

    public Label getTitle() {
        Label title = new LabelImpl(new Control(new ContextVisible(By.cssSelector("head > title"))));
        return title;
    }

    public LinkPlayerRequestsPage clickPlayerLinkRequests() {
        control.getLinkPlayerToUser()
                .click();
        return new LinkPlayerRequestsPage();
    }

    public ApplicationsToTournamentPage clickApplyTeamForTrnmentRequests() {
        control.getApplyTeamToTour().click();
        return new ApplicationsToTournamentPage();
    }


    public AdminMainPage getAdminMainPage() {
        return control.getAdminMainPage();
    }
}
