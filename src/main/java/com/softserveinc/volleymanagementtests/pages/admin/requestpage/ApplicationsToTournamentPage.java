package com.softserveinc.volleymanagementtests.pages.admin.requestpage;

import com.softserveinc.volleymanagementtests.pages.admin.requestpage.uimaps.ApplicationsToTournamentPageUIMap;

/**
 * Created by Иван on 23-Mar-17.
 */
public class ApplicationsToTournamentPage {
    private ApplicationsToTournamentPageUIMap controls;

    public ApplicationsToTournamentPage() {
        this.controls = new ApplicationsToTournamentPageUIMap();
    }

    public ApplicationsToTournamentPage clickConfirmButton(int index){
        controls.getConfirmButtonList().get(index-1).click();
        return this;
    }

    public ReasonForDeclinePage clickDeclineButton(int index){
        controls.getDeclineButtonList().get(index-1).click();
        return new ReasonForDeclinePage();
    }

}
