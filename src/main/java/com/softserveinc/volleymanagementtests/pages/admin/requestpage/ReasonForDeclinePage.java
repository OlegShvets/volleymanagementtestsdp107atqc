package com.softserveinc.volleymanagementtests.pages.admin.requestpage;

import com.softserveinc.volleymanagementtests.pages.admin.requestpage.uimaps.ReasonForDeclimeUIMap;

/**
 * Created by Иван on 23-Mar-17.
 */
public class ReasonForDeclinePage {
    private ReasonForDeclimeUIMap controls;

    public ReasonForDeclinePage() {
        this.controls = new ReasonForDeclimeUIMap();
    }

    public ReasonForDeclinePage setReasonForDecline(String message){
        controls.getReasonForDeclineTextInput().type(message);
        return this;
    }

    public ApplicationsToTournamentPage clickDeclineButton(){
        controls.getDeclineButton().click();
        return new ApplicationsToTournamentPage();
    }


}
