package com.softserveinc.volleymanagementtests.pages.admin.requestpage.uimaps;

import com.softserveinc.volleymanagementtests.pages.admin.adminfeedback.FeedBack;
import com.softserveinc.volleymanagementtests.pages.admin.adminmainpage.AdminMainPage;
import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LinkImpl;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class AdminFeedBackUIMap {
    private Label nameOfTable;
    private List<FeedBack> feedBackGroup = new ArrayList<>();
    private Link linkPlayerToUser;
    
    public AdminFeedBackUIMap() {

    }
    
    public Label getNameOfTable() {
        return nameOfTable = new LabelImpl(new Control(new ContextVisible(
                By.cssSelector("div[class='panel-heading']"))));
    }
    
    public List<FeedBack> getFeedBackGroup() {
        initializeGroupOfFeedBack();
        return feedBackGroup;
    }
    
    public FeedBack getFeedBackByID(String ID) {
        initializeGroupOfFeedBack();
        for (FeedBack feedBackSearch : feedBackGroup) {
            if (ID.equals(feedBackSearch.getId())) {
                FeedBack feedBack = feedBackSearch;
                return feedBack;
            }
        }
        throw new RuntimeException("No such FeedBack");
    }
    
    private void initializeGroupOfFeedBack() {
        int row = 2;
        int closeRow = 2;
        int replayRow = 2;

        List<WebElement> groupOfFeedBack = new WebDriverUtils().getDriver().findElements(By
                .cssSelector("tbody > tr"));

        for (WebElement element : groupOfFeedBack) {
            if (element.getText().toLowerCase().contains("id")) {
                continue;
            }
            String id = element.findElement(By.cssSelector("tr > td:nth-child(1)")).getText();
            String email = element.findElement(By.cssSelector("tr > td:nth-child(2)")).getText();
            String content = element.findElement(By.cssSelector("tr > td:nth-child(3)")).getText();
            String date = element.findElement(By.cssSelector("tr > td:nth-child(4)")).getText();
            String status = element.findElement(By.cssSelector("tr > td:nth-child(5)")).getText();

            Link details = new LinkImpl(new Control(new ContextVisible(By.cssSelector
                    ("tr:nth-child(" + row++ + ") > td:nth-child(6) > a:nth-child(1)"))));
            Link close = new LinkImpl(new Control(new ContextVisible(By.cssSelector
                    ("tr:nth-child(" + closeRow++ + ") > td:nth-child(6) > a:nth-child(3)"))));

            Link reply = new LinkImpl(new Control(new ContextVisible(By.cssSelector
                    ("tr:nth-child(" + replayRow++ + ") > td:nth-child(6) > #popup"))));

            FeedBack feedBack = new FeedBack(id, email, content, date, status, details, close, reply);
            feedBackGroup.add(feedBack);
        }
    }

    public Link getApplyTeamToTour() {
        return new LinkImpl(new Control(
                new ContextVisible(By.cssSelector(".col-lg-12>li:first-of-type>a"))));

    }


    public Link getLinkPlayerToUser() {
        return new LinkImpl(new Control(new ContextVisible(
                By.cssSelector(".col-lg-12 >li:nth-of-type(2)>a"))));
    }


    
    public AdminMainPage getAdminMainPage() {
        return new AdminMainPage();
    }
}
