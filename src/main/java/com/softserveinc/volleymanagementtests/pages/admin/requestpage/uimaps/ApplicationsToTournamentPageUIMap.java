package com.softserveinc.volleymanagementtests.pages.admin.requestpage.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.ControlListWrapper;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImplementDec;
import com.softserveinc.volleymanagementtests.tools.controls.WebElementsList;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Иван on 23-Mar-17.
 */
public class ApplicationsToTournamentPageUIMap {

    public List<Button> getConfirmButtonList(){
        int tableCOunt = new WebElementsList(new ControlListWrapper(
                new ContextVisible(By.cssSelector("table > tbody"))))
                .asLabels().size();

        List<Button> confirmButtonsList = new ArrayList<>();
        for (int i = 1; i <= tableCOunt; i++) {
            confirmButtonsList.add(new ButtonImplementDec(By.cssSelector(String.format("tr:nth-child(%s) > td:nth-child(5) > a.btn.btn-success.btn-xs", i + 1))));
        }
        return confirmButtonsList;
    }

    public List<Button> getDeclineButtonList(){
        int tableCOunt = new WebElementsList(new ControlListWrapper(
                new ContextVisible(By.cssSelector("table > tbody"))))
                .asLabels().size();

        List<Button> confirmButtonsList = new ArrayList<>();
        for (int i = 1; i <= tableCOunt; i++) {
            confirmButtonsList.add(new ButtonImplementDec(By.cssSelector(String.format("tr:nth-child(%s) > td:nth-child(5) > a.btn.btn-danger.btn-xs", i + 1))));
        }
        return confirmButtonsList;
    }


}
