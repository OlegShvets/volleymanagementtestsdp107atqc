package com.softserveinc.volleymanagementtests.pages.admin.requestpage.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImplementDec;
import com.softserveinc.volleymanagementtests.tools.controls.TextInputImpl;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;
import org.openqa.selenium.By;

/**
 * Created by Иван on 23-Mar-17.
 */
public class ReasonForDeclimeUIMap {

    public TextInput getReasonForDeclineTextInput() {
        return new TextInputImpl(new Control(new ContextVisible(
                By.cssSelector("#Message"))));
    }

    public Button getDeclineButton() {
        return new ButtonImplementDec(By.cssSelector("#send-decline"));
    }
}
