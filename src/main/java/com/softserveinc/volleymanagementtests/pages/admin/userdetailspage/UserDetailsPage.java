package com.softserveinc.volleymanagementtests.pages.admin.userdetailspage;

import com.softserveinc.volleymanagementtests.pages.admin.userdetailspage.uimaps.UserDetailsUIMap;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;

import java.util.List;

public class UserDetailsPage {
    private UserDetailsUIMap control;
    
    public UserDetailsPage() {
        this.control = new UserDetailsUIMap();
    }
    
    public UserDetailsPage clickOnBlockButton() {
        control.getBlock().click();
        return this;
    }

    public Button getBlockButton() {
        return control.getBlock();
    }
    
    public Label getId() {
        return control.getId();
    }
    
    public Label email() {
        return control.getEmail();
    }
    
    public Label userName() {
        return control.getUserName();
    }
    
    public Label personName() {
        return control.getPersonName();
    }
    
    public Label phoneOfUser() {
        return control.getPhone();
    }
    
    public Label loginProvider() {
        return control.getLoginProvider();
    }

    public Label getNoLinkedRoles() {
        return control.getNoRolesAssignedLabel();
    }

    public List<Label> usersRoles() {
        return control.getListOfRolesNames();
    }

    public List<Label> userRolesId() {
        return control.getListOfRolesId();
    }

    public Label getNoLinkedPlayers() {
        return control.getNoPlayersAssignedLabel();
    }


    public Label getLinkedPlayerName() {
        return control.getLinkedPlayerName();
    }

    public Label getLinkedPlayerLastName() {
        return control.getLinkedPlayerLastName();
    }

    public Label getLinkedPlayerYearOfBirth() {
        return control.getLinkedPlayerYearOfBirth();
    }

    public Label getLinkedPlayerHeight() {
        return control.getLinkedPlayerHeight();
    }

    public Label getLinkedPlayerWeight() {
        return control.getLinkedPlayerWeight();
    }
}
