package com.softserveinc.volleymanagementtests.pages.admin.userdetailspage.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.ControlListWrapper;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.WebElementsList;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import org.openqa.selenium.By;

import java.util.List;

public final class UserDetailsUIMap {


    private List<Label> roleNames;


    public Button getBlock() {
        return new ButtonImpl(new Control(new ContextVisible(
                By.cssSelector("a.btn.btn-success.btn-xs"))));
    }
    
    public Label getId() {
        return new LabelImpl(new Control(new ContextVisible(
                By.cssSelector("tbody > tr:nth-child(1) > td:nth-child(2)"))));
    }
    
    public Label getEmail() {
        return new LabelImpl(new Control(new ContextVisible(
                By.cssSelector("tbody > tr:nth-child(3) > td:nth-child(2)"))));
    }
    
    public Label getUserName() {
        return new LabelImpl(new Control(new ContextVisible(
                By.cssSelector("tbody > tr:nth-child(4) > td:nth-child(2)"))));
    }
    
    public Label getPersonName() {
        return new LabelImpl(new Control(new ContextVisible(
                By.cssSelector("tbody > tr:nth-child(5) > td:nth-child(2)"))));
    }
    
    public Label getPhone() {
        return new LabelImpl(new Control(new ContextVisible(
                By.cssSelector("tbody > tr:nth-child(6) > td:nth-child(2)"))));
    }
    
    public Label getLoginProvider() {
        return new LabelImpl(new Control(new ContextVisible(
                By.cssSelector("table:nth-child(4) > tbody > tr > td:nth-child(1)"))));
    }
    
    public Label getRoleName() {
        return new LabelImpl(new Control(new ContextVisible(
                By.cssSelector("table:nth-child(6) > tbody > tr:nth-child(2) > td:nth-child(2)"))));
    }


    public Label getNoRolesAssignedLabel() {
        return new LabelImpl(new Control(
                new ContextVisible(By.cssSelector("table:nth-child(6) > tbody .text-center"))));
    }

    public List<Label> getListOfRolesId() {
        return new WebElementsList(new ControlListWrapper(
                new ContextVisible(By.cssSelector("table:nth-child(6)>tbody tr >td:nth-child(1)"))))
                .asLabels();
    }


    public List<Label> getListOfRolesNames() {
        return new WebElementsList(new ControlListWrapper(
                new ContextVisible(By.cssSelector("table:nth-child(6)>tbody tr >td:nth-child(2)"))))
                .asLabels();
    }

    public Label getNoPlayersAssignedLabel() {
        return new LabelImpl(new Control(
                new ContextVisible(By.cssSelector("table:nth-child(8) > tbody .text-center"))));
    }


    public Label getLinkedPlayerName() {
        return new LabelImpl(new Control(new ContextVisible(
                By.cssSelector("table:nth-child(8)>tbody tr:nth-child(1) >td:nth-child(2)"))));
    }

    public Label getLinkedPlayerLastName() {
        return new LabelImpl(new Control(new ContextVisible(
                By.cssSelector("table:nth-child(8)>tbody tr:nth-child(2) >td:nth-child(2)"))));
    }

    public Label getLinkedPlayerYearOfBirth() {
        return new LabelImpl(new Control(new ContextVisible(
                By.cssSelector("table:nth-child(8)>tbody tr:nth-child(3) >td:nth-child(2)"))));
    }

    public Label getLinkedPlayerHeight() {
        return new LabelImpl(new Control(new ContextVisible(
                By.cssSelector("table:nth-child(8)>tbody tr:nth-child(4) >td:nth-child(2)"))));
    }

    public Label getLinkedPlayerWeight() {
        return new LabelImpl(new Control(new ContextVisible(
                By.cssSelector("table:nth-child(8)>tbody tr:nth-child(5) >td:nth-child(2)"))));
    }
}
