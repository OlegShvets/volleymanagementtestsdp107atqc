package com.softserveinc.volleymanagementtests.pages.admin.users;


import com.softserveinc.volleymanagementtests.pages.admin.userdetailspage.UserDetailsPage;
import com.softserveinc.volleymanagementtests.pages.admin.users.uimaps.UserDataRowUIMap;
import com.softserveinc.volleymanagementtests.pages.admin.users.uimaps.UsersListUIMap;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import org.openqa.selenium.NoSuchElementException;

/**
 * Created by Natalia on 19.03.2017.
 */
public class UsersListPage {

    private UsersListUIMap activeUsersUIMap;

    public UsersListPage() {
        activeUsersUIMap = new UsersListUIMap();
    }

    public Label getUserListLabel() {
        return activeUsersUIMap.getUserListLabel();
    }

    public UserDetailsPage clickUserDetails(String userName) {
        findUserRow(userName)

                .getUserDetails()
                .click();
        return new UserDetailsPage();
    }

    public UsersListPage clickUsersBlock(String userName) {
        findUserRow(userName)
                .getUserBlock()
                .click();
        return this;
    }

    public boolean isUserWithNameIsPresent(String name) {
        try {
            findUserRow(name);
            return true;
        }
        catch (NoSuchElementException e) {
            return false;
        }
    }

    public Button getDetailsButtonForUser(String userName) {
        UserDataRowUIMap userDataRow = findUserRow(userName);
        return userDataRow.getUserDetails();
    }

    public Button getUserBlockButtonForUser(String userName) {
        UserDataRowUIMap userDataRow = findUserRow(userName);
        return userDataRow.getUserBlock();

    }

    public UserDataRowUIMap findUserRow(String userFullName) {
        return activeUsersUIMap
                .getUserDataRows()
                .stream().filter(userDataRow -> userDataRow
                        .getUserName().getText().equals(userFullName))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("no such user name"));
    }

    public Label UserTableNameLabel() {
        return activeUsersUIMap.UserTableNameLabel();
    }

    public UserDataRowUIMap getUserListTableRow(int num ) {
        return activeUsersUIMap.getUserDataRows().get(num);
    }

    public UserDataRowUIMap findUserRowByMail(String userMail) {
        return activeUsersUIMap
                .getUserDataRows()
                .stream().filter(userDataRow -> userDataRow
                        .getUserEmail().getText().equals(userMail))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("no such user name"));
    }
}
