package com.softserveinc.volleymanagementtests.pages.admin.users.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import org.openqa.selenium.By;

/**
 * Created by stas on 20.03.2017.
 */
public class UserDataRowUIMap {


    private String parentCss;

    public UserDataRowUIMap(String parentCss) {
        this.parentCss = parentCss;
    }

    public Label getUserId() {
        return new LabelImpl(new Control(new ContextVisible(
                By.cssSelector(String.format("%s > td:nth-of-type(1)",
                        parentCss)))));
    }


    public Label getUserName() {
        return new LabelImpl(new Control(
                new ContextVisible(
                        By.cssSelector(String.format("%s > td:nth-of-type(2)",
                                parentCss)))));
    }

    public Label getUserEmail() {
        return new LabelImpl(
                new Control(new ContextVisible(
                        By.cssSelector(String.format("%s > td:nth-of-type(3)",
                                parentCss)))));
    }

    public Button getUserDetails() {
        return new ButtonImpl(
                new Control(new ContextVisible(
                        By.cssSelector(
                                String.format("%s>td:nth-of-type(4)>.btn-primary",
                                        parentCss)))));
    }

    public Button getUserBlock() {
        return new ButtonImpl(new Control(
                new ContextVisible(
                        By.cssSelector(
                                String.format("%s>td:nth-of-type(4)>.btn-success",
                                        parentCss)))));
    }

}
