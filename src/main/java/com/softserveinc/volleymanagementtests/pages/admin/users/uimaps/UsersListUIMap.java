package com.softserveinc.volleymanagementtests.pages.admin.users.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.ControlListWrapper;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.WebElementsList;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stas on 19.03.2017.
 */
public final class UsersListUIMap {

    public Label getUserListLabel() {
        return new LabelImpl(
                new Control(new ContextVisible(
                        By.cssSelector(".panel-heading"))));
    }

    public List<UserDataRowUIMap> getUserDataRows() {
        int usersTableSize = new WebElementsList(
                new ControlListWrapper(
                        new ContextVisible(By.cssSelector("table > tbody > tr"))))
                .asLabels().size();
        List<UserDataRowUIMap> userRows = new ArrayList<>();
        for (int i = 2; i <= usersTableSize; i++) {
            userRows.add(new UserDataRowUIMap(
                    String.format("table > tbody > tr:nth-of-type(%d)", i)));
        }
        return userRows;
    }

    public Label UserTableNameLabel() {
        return new LabelImpl(new Control(new ContextVisible(
                By.cssSelector("tbody > tr:nth-child(1) > th:nth-child(2)"))));
    }

    public Button UserTableDetailsButton() {
        return new ButtonImpl(new Control(new ContextVisible(
                By.cssSelector("tr:nth-child(2) > td:nth-child(4) > a.btn.btn-primary.btn-xs"))));
    }

    public Button UserTableBlockButton() {
        return new ButtonImpl(new Control(new ContextVisible(
                By.cssSelector("tr:nth-child(2) > td:nth-child(4) > a.btn.btn-success.btn-xs"))));
    }


}
