package com.softserveinc.volleymanagementtests.pages.contributors;

import com.softserveinc.volleymanagementtests.pages.contributors.uimaps.ContributorListUIMap;
import com.softserveinc.volleymanagementtests.testdata.contributor.ContributorTeam;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;

import java.util.ArrayList;
import java.util.List;

/**
 * Class provides methods to interact with Contributor List Page.
 *
 * @author Danil Zhyliaiev
 */
public class ContributorListPage {
    private ContributorListUIMap controls;

    public ContributorListPage() {
        this.controls = new ContributorListUIMap();
    }

    public String getPageTitle() {
        return controls.getPageHeader().getText();
    }

    public int getTeamsCount() {
        return controls.getTeams().size();
    }

    public String getTeamNameByPosition(int position) {
        return controls.getTeams().get(position).getTeamName().getText();
    }

    public List<String> getTeamMembersByPosition(int position) {
        List<String> teamMembers = new ArrayList<>();

        for (Label teamMember : controls.getTeams().get(position).getTeamMembers()) {
            teamMembers.add(teamMember.getText());
        }

        return teamMembers;
    }

    public List<ContributorTeam> getAllTeams() {
        List<ContributorTeam> teams = new ArrayList<>();

        for (int i = 0; i < getTeamsCount(); i++) {
            teams.add(new ContributorTeam().newBuilder()
                    .setTeamName(getTeamNameByPosition(i))
                    .setMembers(getTeamMembersByPosition(i))
                    .build());
        }

        return teams;
    }
}

