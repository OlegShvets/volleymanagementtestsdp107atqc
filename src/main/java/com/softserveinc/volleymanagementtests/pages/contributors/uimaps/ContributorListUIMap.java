package com.softserveinc.volleymanagementtests.pages.contributors.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.ControlListWrapper;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.WebElementsList;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides access to web elements on Contributor List page.
 *
 * @author Danil Zhyliaiev
 */
public class ContributorListUIMap {

    public ContributorListUIMap() {
        if (!(new WebDriverUtils().getCurrentUrl().endsWith("ContributorsTeam"))
                || !(getPageHeader().getText().equals("Contributors"))) {
            throw new RuntimeException("This is not Contributors list page.");
        }
    }

    public Label getPageHeader() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "#page-wrapper > div > div > div > h2"))));
    }

    public List<TeamPanel> getTeams() {
        int panelCount = new WebElementsList(new ControlListWrapper(new ContextVisible
                (By.cssSelector("div.panel")))).asLabels().size();
        List<TeamPanel> teams = new ArrayList<>();
        for (int i = 1; i <= panelCount; i++) {
            teams.add(new TeamPanel(String.format("div.panel:nth-child(%s)", i + 1)));
        }
        return teams;
    }

    public class TeamPanel {
        private final String parentCss;

        private TeamPanel(String parentCss) {
            this.parentCss = parentCss;
        }

        public Label getTeamName() {
            return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                    parentCss + " > div.panel-heading"))));
        }

        public List<Label> getTeamMembers() {
            return new WebElementsList(new ControlListWrapper(new ContextVisible
                    (By.cssSelector(parentCss + " > div.panel-body > div.col-md-4")))).asLabels();
        }
    }
}
