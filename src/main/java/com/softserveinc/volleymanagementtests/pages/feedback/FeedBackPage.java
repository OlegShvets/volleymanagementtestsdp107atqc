package com.softserveinc.volleymanagementtests.pages.feedback;

import com.softserveinc.volleymanagementtests.dal.models.FeedbackDal;
import com.softserveinc.volleymanagementtests.pages.feedback.uimaps.FeedBackUIMap;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Checkbox;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;

/**
 * Created by stas on 08.03.2017.
 */
public class FeedBackPage {
    private FeedBackUIMap feedBackUIMap;

    public FeedBackPage() {
        feedBackUIMap = new FeedBackUIMap();
    }

    public FeedBackPage typeEmail(String email) {
        TextInput emailField = feedBackUIMap.getEmailField();
        emailField.clear();
        emailField.type(email);
        return this;
    }

    public TextInput getEmail() {
        return feedBackUIMap.getEmailField();
    }

    public FeedBackPage typeDescription(String description) {
        TextInput descrField = feedBackUIMap.getDescriptionField();
        descrField.clear();
        descrField.type(description);
        return this;
    }

    public TextInput getDescription() {
        return feedBackUIMap.getDescriptionField();
    }

    public Label getDescValidationError() {
        return feedBackUIMap.getDescValidationLabel();
    }

    public TextInput getAdditInformation() {
        return feedBackUIMap.getAdditionalInfoField();
    }

    public FeedBackPage checkAgreeCheckBox() {
        Checkbox agreeCheckBox = feedBackUIMap.getAgreeCheckBox();
        if (!agreeCheckBox.isChecked()) {
            agreeCheckBox.check();
        }
        return this;
    }


    public MainPage setFeedbackDataAndSend(FeedbackDal feedbackDal) {
        typeEmail(feedbackDal.getUserEmail())
                .typeDescription(feedbackDal.getContent())
                .sendFeedBack();
        return new MainPage();
    }


    public FeedBackPage sendFeedBack() {
        feedBackUIMap.getSendButton().click();
        return this;
    }

    public Label getFeedBackResponse() {
        return feedBackUIMap.getSendResponseLabel();
    }

    public MainPage returnToPrevPage() {
        feedBackUIMap.getBackToPrevPageButton().click();
        return new MainPage();
    }


}
