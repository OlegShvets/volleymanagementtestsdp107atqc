package com.softserveinc.volleymanagementtests.pages.feedback.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.controls.*;
import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImpl;
import com.softserveinc.volleymanagementtests.tools.controls.CheckboxImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.TextInputImpl;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Checkbox;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;
import org.openqa.selenium.By;

/**
 * Created by stas on 08.03.2017.
 */
public class FeedBackUIMap {


    public TextInput getEmailField() {
            return new TextInputImpl(new Control(
                    new ContextVisible(By.id("UsersEmail"))));

    }

    public TextInput getDescriptionField() {
            return new TextInputImpl(new Control(
                    new ContextVisible(By.id("Content"))));

    }

    public Label getDescValidationLabel() {
            return new LabelImpl(new Control(
                    new ContextVisible(By.cssSelector(
                    "div:nth-child(3) > div > span"))));
    }

    public TextInput getAdditionalInfoField() {
            return new TextInputImpl(new Control(
                    new ContextVisible(By.id
                    ("UserEnvironment"))));
    }

    public Checkbox getAgreeCheckBox() {
        return new CheckboxImplementDec(By.id("isUserAgree"));
    }


    public Button getSendButton() {
        return new ButtonImplementDec(By.id("send-feedback"));
    }

    public Label getSendResponseLabel() {
            return new LabelImpl(new Control(
                    new ContextVisible(By.cssSelector(
                    "#responsePlace > span"))));
    }

    public Button getBackToPrevPageButton() {
        return new ButtonImplementDec(By.id("feedback-return"));
    }


}
