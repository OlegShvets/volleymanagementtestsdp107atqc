package com.softserveinc.volleymanagementtests.pages.login;

import com.softserveinc.volleymanagementtests.pages.login.uimaps.GoogleAuthPageUIMap;

/**
 * Created by Иван on 19-Mar-17.
 */
public class GoogleAuthPage {

    private GoogleAuthPageUIMap googleAuthPageUIMap;

    public GoogleAuthPage() {
        googleAuthPageUIMap = new GoogleAuthPageUIMap();
    }

    public GoogleAuthPage clickGoogleNextButton() {
        googleAuthPageUIMap.getNextButton().click();
        return this;
    }

    public GoogleAuthPage setUserEmail(String input) {
        googleAuthPageUIMap.getEmailTextInput().type(input);
        return this;
    }

    public GoogleAuthPage clickGoogleAuthButton() {
        googleAuthPageUIMap.getSignInButton().click();
        return this;
    }

    public GoogleAuthPage setUserPassword(String input) {
        googleAuthPageUIMap.getPasswordTextInput().type(input);
        return this;
    }
    public GoogleAuthPage clickSubmitApproveButton() {
        googleAuthPageUIMap.getSubmitApproveButton().click();
        return this;
    }

    public void logInWithGoogleAccount(String passwrod, String user) {
        this.setUserEmail(user).clickGoogleNextButton().setUserPassword(passwrod).clickGoogleAuthButton();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.clickSubmitApproveButton();
    }

}
