package com.softserveinc.volleymanagementtests.pages.login;

import com.softserveinc.volleymanagementtests.pages.login.uimaps.LoginPageUIMap;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;

/**
 * Created by stas on 02.03.2017.
 */
public class GoogleLoginPage {

    private LoginPageUIMap loginPageUIMap;

    public GoogleLoginPage() {
        loginPageUIMap = new LoginPageUIMap();
    }

    public MainPage clickGoogleLoginButton() {
        loginPageUIMap.getGoogleLoginButton().click();
        return new MainPage();
    }

    public GoogleLoginPage loginWithBlockedUser() {
        loginPageUIMap.getGoogleLoginButton().click();
        return this;
    }

    public Label getErrorMessageOnBlockedPage() {
        return loginPageUIMap.getErrorMessage();
    }
}
