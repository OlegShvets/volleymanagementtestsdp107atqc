package com.softserveinc.volleymanagementtests.pages.login;

import com.softserveinc.volleymanagementtests.pages.login.uimaps.LoginPageUIMap;
import com.softserveinc.volleymanagementtests.specification.TestConfig;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import org.openqa.selenium.By;

import java.util.concurrent.TimeUnit;

/**
 * Created by stas on 02.03.2017.
 */
public class LoginPage {

    private LoginPageUIMap loginPageUIMap;

    public LoginPage() {
        loginPageUIMap = new LoginPageUIMap();
    }

    public LoginPage clickGoogleLoginButton() {
        loginPageUIMap.getGoogleLoginButton().click();
        return this;
    }

    public Button getGoogleLoginButton() {
        return loginPageUIMap.getGoogleLoginButton();
    }

    private LoginPage clickUserOptionsButton() {
        loginPageUIMap.getUserOptionsButton().click();
        return this;
    }

    public Button getUserOptionsButton() {
        return loginPageUIMap.getUserOptionsButton();
    }

    private LoginPage clickLoginButton() {
        loginPageUIMap.getLoginButton().click();
        return this;
    }

    public Button getLoginButton() {
        return loginPageUIMap.getLoginButton();
    }

    public void logInWithCurrentProfile() {
        new WebDriverUtils().load(new TestConfig().getSystemUnderTestBaseUrl());
        clickUserOptionsButton();
        if (!checkLoggedIn()) {
            clickLoginButton();
            clickGoogleLoginButton();
        }
    }

    private boolean checkLoggedIn() {
        new WebDriverUtils().getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        boolean result = new WebDriverUtils().getDriver().findElements(By.linkText("User Profile")).size() > 0;
        new WebDriverUtils().getDriver().manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        return result;
    }
}
