package com.softserveinc.volleymanagementtests.pages.login.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImplementDec;
import com.softserveinc.volleymanagementtests.tools.controls.TextInputImpl;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import org.openqa.selenium.By;

/**
 * Created by Иван on 19-Mar-17.
 */
public class GoogleAuthPageUIMap {

    public TextInputImpl getEmailTextInput() {
        return new TextInputImpl(new Control(new ContextVisible(By.id("Email"))));
    }

    public Button getNextButton() {
        return new ButtonImplementDec(By.id("next"));
    }

    public TextInputImpl getPasswordTextInput() {
        return new TextInputImpl(new Control(new ContextVisible(By.id("Passwd"))));
    }

    public Button getSignInButton() {
        return new ButtonImplementDec(By.id("signIn"));
    }

    public Button getSubmitApproveButton() {
        return new ButtonImplementDec(By.id("submit_approve_access"));
    }
}
