package com.softserveinc.volleymanagementtests.pages.login.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImpl;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImplementDec;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import org.openqa.selenium.By;

/**
 * Created by stas on 02.03.2017.
 */
public class GoogleLoginPageUIMap {

    private Button googleLoginButton;
    private Button userOptionsButton;
    private Button loginButton;

    public Button getGoogleLoginButton() {
        return new ButtonImplementDec(By.cssSelector(".btn"));
    }


}
