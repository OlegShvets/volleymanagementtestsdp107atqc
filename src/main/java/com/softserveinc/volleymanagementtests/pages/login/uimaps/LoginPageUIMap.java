package com.softserveinc.volleymanagementtests.pages.login.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImplementDec;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import org.openqa.selenium.By;

/**
 * Created by stas on 02.03.2017.
 */
public class LoginPageUIMap {

    public Button getGoogleLoginButton() {
        return new ButtonImplementDec(By.cssSelector(".btn"));
    }

    public Button getUserOptionsButton() {
        return new ButtonImplementDec(By.xpath("//i"));
    }

    public Button getLoginButton() {
        return new ButtonImplementDec(By.linkText("Login"));
    }

    public Label getErrorMessage() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector("div > p"))));
    }
}
