package com.softserveinc.volleymanagementtests.pages.main;

import com.softserveinc.volleymanagementtests.pages.contributors.ContributorListPage;
import com.softserveinc.volleymanagementtests.pages.feedback.FeedBackPage;
import com.softserveinc.volleymanagementtests.pages.useractions.UserActionsPage;
import com.softserveinc.volleymanagementtests.pages.main.uimaps.MainPageUIMap;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.pages.team.TeamListPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.TournamentsPage;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;

/**
 * Created by stas on 07.03.2017.
 */
public class MainPage {

    private MainPageUIMap mainMenuUIMap;

    public MainPage() {
        mainMenuUIMap = new MainPageUIMap();
    }

    public TournamentsPage clickVolleyManageLink() {
        mainMenuUIMap.getAppLink().click();
        return new TournamentsPage();
    }

    public Link getVolleyManageLink() {
        return mainMenuUIMap.getAppLink();
    }

    public TournamentsPage clickTournamentsPage() {
        mainMenuUIMap.getTournamentsLink().click();
        return new TournamentsPage();
    }

    public PlayersListPage clickPlayerListPage() {
        mainMenuUIMap.getPlayersLink().click();
        return new PlayersListPage();
    }

    public TeamListPage clickTeamListPage() {
        mainMenuUIMap.getTeamsLink().click();
        return new TeamListPage();
    }

    public ContributorListPage clickContributorsListPage() {
        mainMenuUIMap.getContributorsLink().click();
        return new ContributorListPage();
    }

    public UserActionsPage clickUserMenu() {
        mainMenuUIMap.getUserOptionsButton().click();
        return new UserActionsPage();
    }

    public FeedBackPage clickFeedBackLink() {
        mainMenuUIMap.getFeedbackLink().click();
        return new FeedBackPage();
    }


}
