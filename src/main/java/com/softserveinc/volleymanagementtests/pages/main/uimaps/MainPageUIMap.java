package com.softserveinc.volleymanagementtests.pages.main.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImplementDec;
import com.softserveinc.volleymanagementtests.tools.controls.LinkImpl;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import org.openqa.selenium.By;

/**
 * Created by stas on 07.03.2017.
 */
public class MainPageUIMap {

    public Link getAppLink() {
        return new LinkImpl(new Control(new ContextVisible(By.cssSelector
                ("nav > div.navbar-header > a"))));
    }

    public Button getUserOptionsButton() {
        return new ButtonImplementDec(By.xpath("//i"));
    }

    public Link getFeedbackLink() {
        return new LinkImpl(new Control(new ContextVisible(By.cssSelector
                ("nav > ul > li:nth-child(2) > a"))));
    }


    public Link getTournamentsLink() {
        return new LinkImpl(new Control(new ContextVisible(By.cssSelector
                ("#side-menu > li:nth-child(1) > a"))));
    }

    public Link getPlayersLink() {
        return new LinkImpl(new Control(new ContextVisible(By.cssSelector
                ("#side-menu > li:nth-child(2) > a"))));
    }

    public Link getTeamsLink() {
        return new LinkImpl(new Control(new ContextVisible(By.cssSelector
                ("#side-menu > li:nth-child(3) > a"))));
    }

    public Link getContributorsLink() {
        return new LinkImpl(new Control(new ContextVisible(By.cssSelector
                ("#side-menu > li:nth-child(4) > a"))));
    }
}
