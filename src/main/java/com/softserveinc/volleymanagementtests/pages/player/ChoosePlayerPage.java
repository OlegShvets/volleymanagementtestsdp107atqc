package com.softserveinc.volleymanagementtests.pages.player;

import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.player.uimaps.chooseplayersuimaps.ChoosePlayersUIMap;
import com.softserveinc.volleymanagementtests.pages.player.uimaps.chooseplayersuimaps.PlayerTableRowUIMap;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;

import java.util.List;

/**
 * UI Map contain all Create Player locators and initializes controls using
 * lazy initialization approach.
 *
 * @author S.Tsyganovskiy
 */

public class ChoosePlayerPage {
    private ChoosePlayersUIMap controls;

    public ChoosePlayerPage() {
        controls = new ChoosePlayersUIMap();
    }


    public Label getFirstAndLastName() {
        return controls.getFirstAndLastName();
    }


    public Label getChoosePlayer() {
        return controls.getChoosePlayer();
    }

    public List<PlayerTableRowUIMap> getPlayersTableRows() {
        return controls.getPlayersTableRows();
    }

    public ChoosePlayerPage addPlayerToTeam(String lastNameAndFirstName) {
        List<PlayerTableRowUIMap> playersTableRows = controls.getPlayersTableRows();
        for (PlayerTableRowUIMap i : playersTableRows) {
            if (i.getLastAndFirstNameDetailsLabel().getText().equals(lastNameAndFirstName)) {
                i.getAddPlayerToTeamButton().click();
            }
        }
        return this;
    }

    public List<Link> getPaginatior() {
        return controls.getPaginator();
    }

    private boolean goNextPage() {
        List<Link> paginationLinks = controls.getPaginator();
        boolean isNextPageExists = false;
        for (Link i : paginationLinks) {
            if (i.getText().equals("»")) {
                isNextPageExists = true;
                i.click();
                controls.remap();
            }
        }
        return isNextPageExists;
    }


    public void chooseCaptain(Player player) {
        List<PlayerTableRowUIMap> playersTableRowList;
        do {
            playersTableRowList = controls.getPlayersTableRows();
            for (PlayerTableRowUIMap i : playersTableRowList) {
                if (i.getLastAndFirstNameDetailsLabel().getText()
                        .equals(player.getLastName() + " " + player.getFirstName())) {
                    i.submitAddPlayerToTeamButton();
                    new WebDriverUtils().switchToNewWindow();
                    return;
                }
            }
        } while (goNextPage());
    }

    public void choosePlayers(List<Player> list) {
        List<PlayerTableRowUIMap> playersTableRowList;
        do {
            playersTableRowList = controls.getPlayersTableRows();
            for (PlayerTableRowUIMap i : playersTableRowList) {
                for (Player j : list) {
                    if (i.getLastAndFirstNameDetailsLabel().getText()
                            .equals(j.getLastName() + " " + j.getFirstName())) {
                        i.submitAddPlayerToTeamButton();
                    }
                }
            }

        } while (goNextPage());
        new WebDriverUtils().getDriver().close();
        new WebDriverUtils().switchToNewWindow();
    }

    public MainPage goToMainPage() {
        return new MainPage();
    }

}
