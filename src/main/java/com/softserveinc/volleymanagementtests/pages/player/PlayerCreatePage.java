package com.softserveinc.volleymanagementtests.pages.player;

import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.player.uimaps.PlayerCreateUIMap;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;

/**
 * Object that represents Create Player page and provide methods to interact
 * with it.
 *
 * @author S.Tsyganovskiy
 */
public class PlayerCreatePage {

    private PlayerCreateUIMap createPlayerUIMap;

    public PlayerCreatePage remap() {
        createPlayerUIMap = null;
        createPlayerUIMap = new PlayerCreateUIMap();
        return this;
    }

    public PlayerCreatePage() {
        createPlayerUIMap = new PlayerCreateUIMap();
    }

    public final Label getCreateNewPlayerLabel() {
        return createPlayerUIMap.getCreateNewPlayer();
    }

    public final Label getPlayerLabel() {
        return createPlayerUIMap.getPlayer();
    }

    public final Label getNameLabel() {
        return createPlayerUIMap.getNameLabel();
    }

    public final PlayerCreatePage inputFirstName(final String name) {
        createPlayerUIMap.getNameInput().type(name);
        return this;
    }

    public final TextInput getInputFirstName() {
        return createPlayerUIMap.getNameInput();
    }


    public final Label getLastNameLabel() {
        return createPlayerUIMap.getLastNameLabel();
    }


    public final PlayerCreatePage inputLastName(final String lastName) {
        createPlayerUIMap.getLastNameInput().type(lastName);
        return this;
    }


    public final TextInput getInputLastName() {
        return createPlayerUIMap.getLastNameInput();
    }


    public final Label getBirthYearLabel() {
        return createPlayerUIMap.getBirthYearLabel();
    }


    public final PlayerCreatePage inputBirthYear(final String birthYear) {
        createPlayerUIMap.getBirthYearInput().type(birthYear);
        return this;
    }

    public final TextInput getInputBirthYear() {
        return createPlayerUIMap.getBirthYearInput();
    }


    public final Label getHeigthLabel() {
        return createPlayerUIMap.getHeigthLabel();
    }


    public final PlayerCreatePage inputHeigth(final String heigth) {
        createPlayerUIMap.getHeigthInput().type(heigth);
        return this;
    }

    public final TextInput getInputHeight() {
        return createPlayerUIMap.getHeigthInput();
    }


    public final Label getWeightLabel() {
        return createPlayerUIMap.getWeigthLabel();
    }


    public final PlayerCreatePage inputWeigth(final String weigth) {
        createPlayerUIMap.getWeigthInput().type(weigth);
        return this;
    }

    public final TextInput getInputWeight() {
        return createPlayerUIMap.getWeigthInput();
    }


    public final Button getCreatePlayerButton() {
        return createPlayerUIMap.getButtonCreate();
    }


    public final PlayerCreatePage submitButtonCreate() {
        createPlayerUIMap.getButtonCreate().click();
        return this;
    }

    public final PlayersListPage clickBackToList() {
        createPlayerUIMap.getBackToListLink().click();
        return new PlayersListPage();
    }


    public final Label getErrorInputFirstNameMessage() {
        return createPlayerUIMap.getErrorMessageFirstNameInput();
    }


    public final Label getErrorInputLastNameMessage() {
        return createPlayerUIMap.getErrorMessageLastNameInput();
    }


     public final Label getErrorInputBirthYearMessage() {
        return createPlayerUIMap.getErrorMessageBirthYearInput();
    }


    public final Label getErrorInputHeigthMessage() {
        return createPlayerUIMap.getErrorMessageHeigthInput();
    }


    public final Label getErrorInputWeightMessage() {
        return createPlayerUIMap.getErrorMessageWeigthInput();
    }


    public final PlayerCreatePage logInAsCaptain() {
        return this;
    }


    public final PlayerCreatePage setAllPlayerFields(final Player player) {
        inputFirstName(player.getFirstName());
        inputLastName(player.getLastName());
        inputBirthYear(player.getBirthYear());
        inputHeigth(player.getHeight());
        inputWeigth(player.getWeight());
        return this;
    }

    public MainPage goToMainPage() {
        return new MainPage();
    }


}
