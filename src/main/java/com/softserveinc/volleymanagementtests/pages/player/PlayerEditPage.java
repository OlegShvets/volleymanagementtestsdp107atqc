package com.softserveinc.volleymanagementtests.pages.player;

import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.player.uimaps.PlayerEditUIMap;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;

import java.util.List;

/**
 * Object that represents Edit Player page and provide methods to interact
 * with it.
 *
 * @author Danil Zhyliaiev
 */
public class PlayerEditPage {
    private PlayerEditUIMap controls;

    public PlayerEditPage() {
        controls = new PlayerEditUIMap();
    }

    public final Label getEditPlayerLabel() {
        return controls.getEditPlayerLabel();
    }

    public final Label getPlayerLabel() {
        return controls.getPlayerLabel();
    }

    public final Label getFirstNameLabel() {
        return controls.getFirstNameLabel();
    }

    public final TextInput getFirstNameInput() {
        return controls.getFirstNameInput();
    }

    public final Label getLastNameLabel() {
        return controls.getLastNameLabel();
    }

    public final TextInput getLastNameInput() {
        return controls.getLastNameInput();
    }

    public final Label getBirthYearLabel() {
        return controls.getBirthYearLabel();
    }

    public final TextInput getBirthYearInput() {
        return controls.getBirthYearInput();
    }

    public final Label getHeightLabel() {
        return controls.getHeightLabel();
    }

    public final TextInput getHeightInput() {
        return controls.getHeightInput();
    }

    public final Label getWeightLabel() {
        return controls.getWeightLabel();
    }

    public final TextInput getWeightInput() {
        return controls.getWeightInput();
    }

    public final Button getSaveButton() {
        return controls.getSaveButton();
    }

    public final Link getBackToListLink() {
        return controls.getBackToListLink();
    }

    public final Label getNameErrorLabel() {
        return controls.getNameErrorLabel();
    }

    public final Label getLastNameErrorLabel() {
        return controls.getLastNameErrorLabel();
    }

    public final Label getBirthYearErrorLabel() {
        return controls.getBirthYearErrorLabel();
    }

    public final Label getBirthYearValidationErrorLabel() {
        return controls.getBirthYearValidationErrorLabel();
    }

    public final Label getHeightErrorLabel() {
        return controls.getHeightErrorLabel();
    }

    public final Label getHeightValidationErrorLabel() {
        return controls.getHeightValidationErrorLabel();
    }

    public final Label getWeightErrorLabel() {
        return controls.getWeightErrorLabel();
    }

    public final Label getWeightValidationErrorLabel() {
        return controls.getWeightValidationErrorLabel();
    }

    public final PlayerEditPage setFirstName(final String firstName) {
        getFirstNameInput().clear();
        getFirstNameInput().type(firstName);
        return this;
    }

    public final PlayerEditPage setLastName(final String lastName) {
        getLastNameInput().clear();
        getLastNameInput().type(lastName);
        return this;
    }

    public final PlayerEditPage setBirthYear(final String birthYear) {
        getBirthYearInput().clear();
        getBirthYearInput().type(birthYear);
        return this;
    }

    public final PlayerEditPage setHeight(final String height) {
        getHeightInput().clear();
        getHeightInput().type(height);
        return this;
    }

    public final PlayerEditPage setWeight(final String weight) {
        getWeightInput().clear();
        getWeightInput().type(weight);
        return this;
    }

    public final PlayerEditPage setPlayer(final Player player) {
        setFirstName(player.getFirstName());
        setLastName(player.getLastName());
        setBirthYear(player.getBirthYear());
        setHeight(player.getHeight());
        setWeight(player.getWeight());
        return this;
    }

    public final PlayersListPage saveWithValidFields() {
        getSaveButton().click();
        return new PlayersListPage();
    }

    public final PlayerEditPage saveWithInvalidFields() {
        getSaveButton().click();
        controls = new PlayerEditUIMap();
        return this;
    }

    public final PlayersListPage returnBackToList() {
        getBackToListLink().click();
        return new PlayersListPage();
    }

    public final int getPlayerID() {
        String curURL = new WebDriverUtils().getCurrentUrl();
        String idStr = curURL.substring(curURL.lastIndexOf('/') + 1);
        return Integer.parseInt(idStr);
    }

    public final List<Label> getErrorLabels() {
        return controls.getErrorLabels();
    }

    public final Player readPlayer() {
        return new Player().newBuilder()
                .setFirstName(getFirstNameInput().getText())
                .setLastName(getLastNameInput().getText())
                .setBirthYear(getBirthYearInput().getText())
                .setHeight(getHeightInput().getText())
                .setWeight(getWeightInput().getText())
                .build();


    }

    public MainPage goToMainPage() {
        return new MainPage();
    }
}
