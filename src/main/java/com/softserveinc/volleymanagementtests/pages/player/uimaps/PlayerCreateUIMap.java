package com.softserveinc.volleymanagementtests.pages.player.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImplementDec;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LinkImpl;
import com.softserveinc.volleymanagementtests.tools.controls.TextInputImpl;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;
import org.openqa.selenium.By;

/**
 * UI Map contain all Create Player locators and initializes controls using
 * lazy initialization approach.
 *
 * @author S.Tsyganovskiy
 */
public class PlayerCreateUIMap {


    public PlayerCreateUIMap() {

        if (!(new WebDriverUtils().getCurrentUrl().contains("/Players/Create"))) {
            throw new RuntimeException("This is not \"Create player\" page.");
        }
    }

    public final Label getCreateNewPlayer() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector("#content > h2"))));
    }


    public final Label getPlayer() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector("fieldset > legend"))));
    }


    public final Label getNameLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector("[for=\"FirstName\"]"))));
    }


    public final TextInput getNameInput() {
        return new TextInputImpl(new Control(new ContextVisible(By.id("FirstName"))));
    }


    public final Label getLastNameLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector("[for=\"LastName\"]"))));
    }


    public final TextInput getLastNameInput() {
        return new TextInputImpl(new Control(new ContextVisible(By.id("LastName"))));
    }


    public final Label getBirthYearLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector("[for=\"BirthYear\"]"))));
    }


    public final TextInput getBirthYearInput() {
        return new TextInputImpl(new Control(new ContextVisible(By.id("BirthYear"))));
    }


    public final Label getHeigthLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(("[for=\"Height\"]")))));
    }


    public final TextInput getHeigthInput() {
        return new TextInputImpl(new Control(new ContextVisible(By.id("Height"))));
    }


    public final Label getWeigthLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(("[for=\"Weight\"]")))));
    }


    public final TextInput getWeigthInput() {
        return new TextInputImpl(new Control(new ContextVisible(By.id("Weight"))));
    }


    public final Button getButtonCreate() {
        return new ButtonImplementDec(By.cssSelector("[type=\"submit\"]"));
    }


    public final Link getBackToListLink() {
        return new LinkImpl(new Control(new ContextVisible(By.cssSelector("[href$=\"Players\"]"))));
    }


    // Getters for error messages


    public final Label getErrorMessageFirstNameInput() {
        return new LabelImpl(new Control(new ContextVisible(By.id("FirstName-error"))));
    }


    public final Label getErrorMessageLastNameInput() {
        return new LabelImpl(new Control(new ContextVisible(By.id("LastName-error"))));
    }


    public final Label getErrorMessageBirthYearInput() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector("span[data-valmsg-for='BirthYear']"))));
    }


    public final Label getErrorMessageHeigthInput() {
        return new LabelImpl(new Control(new ContextVisible(By.id("Height-error"))));
    }


    public final Label getErrorMessageWeigthInput() {
        return new LabelImpl(new Control(new ContextVisible(By.id("Weight-error"))));
    }

}
