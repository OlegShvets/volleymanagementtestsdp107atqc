package com.softserveinc.volleymanagementtests.pages.player.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LinkImpl;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import org.openqa.selenium.By;

/**
 * UI Map contains all controls of Player Details Page as fields.
 * And methods initialising controls with their locators
 *
 * @author J.Bodnar
 */
public class PlayerDetailsUIMap {


    private Button linkToMyAccount;
    private Label adminApprovalText;


    /**
     * Default constructor.
     * Verify if the page-url matches Player Details  page
     *
     * @throws RuntimeException thrown if url does not match Player Details page.
     */
    public PlayerDetailsUIMap() {
       /* if (!(WebDriverUtils.getCurrentUrl().endsWith("Players")
                || WebDriverUtils.getCurrentUrl().contains("Details"))) {
            throw new RuntimeException("It is not Players Details List page.");
        }*/
    }

    public final Label getFirstNameValue() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "div.display-field:nth-child(4)"))));
    }

    public final Label getLastNameValue() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "div.display-field:nth-child(6)"))));
    }

    public final Label getBirthYearValue() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "div.display-field:nth-child(8)"))));
    }

    public final Label getHeightValue() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "div.display-field:nth-child(10)"))));
    }

    public final Label getWeightValue() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "div.display-field:nth-child(12)"))));
    }

    public final Link getEditLink() {
        return new LinkImpl(new Control(new ContextVisible(By.cssSelector(
                ".col-lg-12 > p:nth-child(3) > a:nth-child(1)"))));
    }

    public final Link getBackLink() {
        return new LinkImpl(new Control(new ContextVisible(By.cssSelector(
                ".col-lg-12 > p:nth-child(3) > a:nth-child(2)"))));
    }


    public final Button getLinkToMyAccount() {
        if (linkToMyAccount == null) {
            linkToMyAccount = new ButtonImpl(new Control(
                    new ContextVisible(By.id("linkUser"))));
        }
        return linkToMyAccount;
    }

    public final Label getAdminApprovalText() {
        if (adminApprovalText == null) {
            adminApprovalText = new LabelImpl(new Control(
                    new ContextVisible(By.id("ajaxResultMessage"))));
        }
        return adminApprovalText;
    }


}
