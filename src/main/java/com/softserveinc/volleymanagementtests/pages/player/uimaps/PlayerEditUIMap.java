package com.softserveinc.volleymanagementtests.pages.player.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.ControlListWrapper;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.*;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;
import org.openqa.selenium.By;

import java.util.List;

/**
 * UI Map contain all Edit Player locators and initializes controls using
 * Lazy initialization approach.
 *
 * @author Danil Zhyliaiev
 */
public class PlayerEditUIMap {

    public PlayerEditUIMap() {
        if (!new WebDriverUtils().getCurrentUrl().contains("/Players/Edit/")) {
            throw new RuntimeException("This is not the 'Edit Player' page");
        }
    }

    public final Label getEditPlayerLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector("#content > h2"))));
    }

    public final Label getPlayerLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "#content > form > fieldset > legend"))));
    }

    public final Label getFirstNameLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "label[for=\"FirstName\"]"))));
    }

    public final TextInput getFirstNameInput() {
        return new TextInputImpl(new Control(new ContextVisible(By.id("FirstName"))));
    }

    public final Label getLastNameLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "label[for=\"LastName\"]"))));
    }

    public final TextInput getLastNameInput() {
        return new TextInputImpl(new Control(new ContextVisible(By.id("LastName"))));
    }

    public final Label getBirthYearLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "label[for=\"BirthYear\"]"))));
    }

    public final TextInput getBirthYearInput() {
        return new TextInputImpl(new Control(new ContextVisible(By.id("BirthYear"))));
    }

    public final Label getHeightLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "label[for=\"Height\"]"))));
    }

    public final TextInput getHeightInput() {
        return new TextInputImpl(new Control(new ContextVisible(By.id("Height"))));
    }

    public final Label getWeightLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "label[for=\"Weight\"]"))));
    }

    public final TextInput getWeightInput() {
        return new TextInputImpl(new Control(new ContextVisible(By.id("Weight"))));
    }


    public final Button getSaveButton() {
        return new ButtonImplementDec(By.cssSelector("input[type=\"submit\"]"));
    }

    public final Link getBackToListLink() {
        return new LinkImpl(new Control(new ContextVisible(By.cssSelector
                ("form + div > a"))));
    }

    public final Label getNameErrorLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.id("FirstName-error"))));
    }

    public final Label getLastNameErrorLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.id("LastName-error"))));
    }

    public final Label getBirthYearErrorLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.id("BirthYear-error"))));
    }

    public Label getBirthYearValidationErrorLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "span[data-valmsg-for=BirthYear]"))));
    }

    public final Label getHeightErrorLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.id(
                "Height-error"))));
    }

    public Label getHeightValidationErrorLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "span[data-valmsg-for=Height]"))));
    }

    public final Label getWeightErrorLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.id("Weight-error"))));
    }

    public Label getWeightValidationErrorLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "span[data-valmsg-for=Weight]"))));
    }

    public List<Label> getErrorLabels() {
        return new WebElementsList(new ControlListWrapper(new ContextVisible
                (By.cssSelector("[id$=error]")))).asLabels();
    }
}
