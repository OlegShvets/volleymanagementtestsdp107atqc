package com.softserveinc.volleymanagementtests.pages.player.uimaps;

import com.softserveinc.volleymanagementtests.pages.contracts.Remapable;
import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.ControlListWrapper;
import com.softserveinc.volleymanagementtests.tools.controls.*;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Alert;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

/**
 * Karabaza Anton on 23.02.2016.
 */
public class PlayersListUIMap implements Remapable {
    private Link playersLink;

    /**
     * Represents main header on Players list page.
     */
    private Label playersLabel;
    /**
     * Represents label near SearchByText text-box on Edit Players list page.
     */
    private Label searchByTextLabel;
    /**
     * Represents SearchByText text-box on Players list page.
     */
    private TextInput searchByTextInput;
    /**
     * Represents Create new player link on Players list page.
     */
    private Link createNewPlayerLink;
    /**
     * Represents list of players on current Players list page.
     */
    private List<PlayersTableRow> playersList;
    /**
     * Represents list of page links on Players list page.
     */
    private List<Link> paginator;
    /**
     * an alert popup on the page.
     */
    private Alert alert;

    /**
     * Constructor. Verifies that user is on Players list page.
     *
     * @throws RuntimeException thrown if user is not on Players list page.
     */
    public PlayersListUIMap() {
       /* if (!(WebDriverUtils.getCurrentUrl().endsWith("Players")
                || WebDriverUtils.getCurrentUrl().contains("Players?page")
                || WebDriverUtils.getCurrentUrl().contains("Players?Length"))) {
            throw new RuntimeException("This is not Players List page.");
        }*/
    }

    /**
     * Gets players list page main header as label.
     *
     * @return main header as {@code LabelImpl} object.
     */
    public final Label getPlayersLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector("div > h2"))));
    }

    public final Link getPlayerLink() {
        return new LinkImpl(new Control(new ContextVisible(By.cssSelector("playerName"))));
    }


    /**
     * Gets label near SearchByText input.
     *
     * @return main header as {@code LabelImpl} object.
     */
    public final Label getSearchByTextLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "#content > div > form > label"))));
    }

    /**
     * Gets SearchByText input.
     *
     * @return SearchByText input as {@code TextInputImpl} object.
     */
    public final TextInput getSearchByTextInput() {
        return new TextInputImpl(new Control(new ContextVisible(By.id
                ("textToSearch"))));
    }

    /**
     * Gets CreateNewPlayer link.
     *
     * @return CreateNewPlayer as {@code LinkImpl} object.
     */
    public final Link getCreateNewPlayerLink() {
        return createNewPlayerLink = new LinkImpl(new Control(new ContextVisible(By.cssSelector("#content > a"))));
    }

    /**
     * Gets list of PlayersTableRow on current Players list page.
     *
     * @return list of {@code PlayersTableRow} objects.
     */
    public final List<PlayersTableRow> getPlayersList() {

        playersList = new ArrayList<>();
        List<Link> fullNameLinks = new WebElementsList(new ControlListWrapper(new ContextVisible
                (By.cssSelector(".playerName")))).asLinks();
        List<Link> editLinks = new WebElementsList(new ControlListWrapper(new ContextVisible
                (By.cssSelector("tr > td + td > a:not(.delete)")))).asLinks();
        List<Link> deleteLinks = new WebElementsList(new ControlListWrapper(new ContextVisible
                (By.cssSelector(".delete")))).asLinks();

        for (int i = 0; i < fullNameLinks.size(); i++) {
            playersList.add(new PlayersTableRow(fullNameLinks.get(i),
                    editLinks.get(i), deleteLinks.get(i)));
        }
        return playersList;
    }

    /**
     * Gets page links on table footer.
     *
     * @return List of {@code LinkImpl} objects.
     */
    public final List<Link> getPaginator() {
        paginator = new ArrayList<>();
        paginator = new WebElementsList(new ControlListWrapper(new ContextVisible
                (By.cssSelector(".col-lg-12 >a")))).asLinks();
        return paginator;
    }

    /**
     * turns the driver to an Alert if present.
     * <p>
     * the NoAlertPresentException will be thrown if there is no Alert.
     *
     * @return an alert instance if this page
     */
    public final Alert getAlert() {
        return new AlertImplementDec();
    }

    @Override
    public void remap() {
        playersLabel = null;
        searchByTextLabel = null;
        searchByTextInput = null;
        createNewPlayerLink = null;
        playersList = null;
        paginator = null;
        alert = null;
    }

    /**
     * Class that represents one row in players table, which contains player's
     * fullNameLink, editLink and deleteLink.
     */
    public class PlayersTableRow {

        /**
         * Represets full name link of the player.
         */
        private Link fullNameLink;
        /**
         * Represents edit link for the player.
         */
        private Link editLink;
        /**
         * Represents delete link for the player.
         */
        private Link deleteLink;

        /**
         * Constructor.
         *
         * @param fullName fullNameLink of the player.
         * @param edit     editLink for the player.
         * @param delete   deleteLink for the player.
         */
        PlayersTableRow(final Link fullName, final Link edit, final Link delete) {
            fullNameLink = fullName;
            editLink = edit;
            deleteLink = delete;
        }

        /**
         * Gets Full name link of the player's row.
         *
         * @return full name as {@code LinkImpl} object.
         */
        public final Link getFullNameLink() {
            return fullNameLink;
        }

        /**
         * Gets edit link of the player's row.
         *
         * @return editLink as {@code LinkImpl} object.
         */
        public final Link getEditLink() {
            return editLink;
        }

        /**
         * Gets delete link of the player's row.
         *
         * @return deleteLink as {@code LinkImpl} object.
         */
        public final Link getDeleteLink() {
            return deleteLink;
        }
    }
}
