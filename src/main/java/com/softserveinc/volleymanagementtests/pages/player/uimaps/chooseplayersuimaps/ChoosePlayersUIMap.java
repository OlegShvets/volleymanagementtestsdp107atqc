package com.softserveinc.volleymanagementtests.pages.player.uimaps.chooseplayersuimaps;

import com.softserveinc.volleymanagementtests.pages.contracts.Remapable;
import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.ControlListWrapper;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.WebElementsList;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

/**
 * UI Map contain all Create Player locators and initializes controls using
 * lazy initialization approach.
 *
 * @author S.Tsyganovskiy
 */
public class ChoosePlayersUIMap implements Remapable {
    private Label choosePlayer;
    private Label firstAndLastName;
    private List<PlayerTableRowUIMap> playersTableRows;
    private List<Link> paginator;


    public Label getChoosePlayer() {
        if (choosePlayer == null) {
            choosePlayer = new LabelImpl(new Control(new ContextVisible(By.cssSelector("#content>h2"))));
        }
        return choosePlayer;
    }

    public Label getFirstAndLastName() {
        if (firstAndLastName == null) {
            firstAndLastName = new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                    ".table>tbody>tr>th:first-child"))));
        }
        return firstAndLastName;
    }

    public List<PlayerTableRowUIMap> getPlayersTableRows() {
        if (playersTableRows == null) {
            getPaginator();
            playersTableRows = new ArrayList<>();
            List<Label> listOfPlayers = new WebElementsList(new ControlListWrapper(new ContextVisible
                    (By.cssSelector("tr>td:first-child")))).asLabels();
            List<Button> addPlayerButtons = new WebElementsList(new ControlListWrapper(new ContextVisible
                    (By.cssSelector("tr > td:nth-child(2)")))).asButtons();
            for (int i = 0; i < listOfPlayers.size(); i++) {
                playersTableRows.add(new PlayerTableRowUIMap(listOfPlayers.get(i), addPlayerButtons.get(i)));
            }
        }

        return playersTableRows;
    }


    public List<Link> getPaginator() {
        if (paginator == null) {
            paginator = new WebElementsList(new ControlListWrapper(new ContextVisible
                    (By.cssSelector("a[onclick^=\"change\"]")))).asLinks();
        }
        return paginator;
    }

    @Override
    public void remap() {
        choosePlayer = null;
        firstAndLastName = null;
        playersTableRows = null;
        paginator = null;
    }

}
