package com.softserveinc.volleymanagementtests.pages.player.uimaps.chooseplayersuimaps;

import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;

/**
 * Created by skir on 3/20/2017.
 */
public class PlayerTableRowUIMap {

    private Label firstAndLastNameDetails;
    private Button addPlayerToTeam;

    public PlayerTableRowUIMap(Label firstAndLastNameDetails, Button addPlayerToTeam) {
        this.firstAndLastNameDetails = firstAndLastNameDetails;
        this.addPlayerToTeam = addPlayerToTeam;
    }

    public Button getAddPlayerToTeamButton() {
        return addPlayerToTeam;
    }

    public void submitAddPlayerToTeamButton() {
        getAddPlayerToTeamButton().click();
    }

    public Label getLastAndFirstNameDetailsLabel() {
        return firstAndLastNameDetails;
    }
}
