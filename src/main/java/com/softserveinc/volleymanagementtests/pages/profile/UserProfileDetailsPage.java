package com.softserveinc.volleymanagementtests.pages.profile;

import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.profile.uimaps.UserProfileDetailsUIMap;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;

/**
 * Created by stas on 08.03.2017.
 */
public class UserProfileDetailsPage {

    private UserProfileDetailsUIMap userProfDetUIMap;

    public UserProfileDetailsPage() {
        userProfDetUIMap = new UserProfileDetailsUIMap();
    }

    public Label getProfileInfoLabel() {
        return userProfDetUIMap.getProfileInfoLabel();
    }

    public Label getLoginLabel() {
        return userProfDetUIMap.getLoginLabel();
    }

    public Label getEmailLabel() {
        return userProfDetUIMap.getEmailLabel();
    }

    public Label getPhoneLabel() {
        return userProfDetUIMap.getGetPhoneLabel();
    }

    public Label getFullNameLabel() {
        return userProfDetUIMap.getFullNameLabel();
    }

    public Label getLoginProviderLabel() {
        return userProfDetUIMap.getLoginProvLabel();
    }

    public UserProfileEditPage clickUserProfileEdit() {
        userProfDetUIMap.getEditProfileButton().click();
        return new UserProfileEditPage();
    }

    public MainPage goToMainPage() {
        return new MainPage();
    }
}
