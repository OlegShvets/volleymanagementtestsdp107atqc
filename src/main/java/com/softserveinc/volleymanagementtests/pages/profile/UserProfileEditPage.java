package com.softserveinc.volleymanagementtests.pages.profile;

import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.profile.uimaps.UserProfileEditUIMap;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;

/**
 * Created by stas on 08.03.2017.
 */
public class UserProfileEditPage {

    private UserProfileEditUIMap userProfEditUIMap;

    public UserProfileEditPage() {
        userProfEditUIMap = new UserProfileEditUIMap();
    }

    public Label getProfileInfoLabel() {
        return userProfEditUIMap.getProfileInfoLabel();
    }

    public UserProfileEditPage typeEmail(String email) {
        TextInput emailInput = userProfEditUIMap.getEmailField();
        emailInput.clear();
        emailInput.type(email);
        return this;
    }

    public TextInput getEmail() {
        return userProfEditUIMap.getEmailField();
    }

    public UserProfileEditPage typePhone(String phone) {
        TextInput phoneInput = userProfEditUIMap.getPhoneField();
        phoneInput.clear();
        phoneInput.type(phone);
        return this;
    }

    public TextInput getPhone() {
        return userProfEditUIMap.getPhoneField();
    }

    public UserProfileEditPage typeFullName(String fullName) {
        TextInput nameInput = userProfEditUIMap.getFullNameField();
        nameInput.clear();
        nameInput.type(fullName);
        return this;
    }
    public TextInput getFullName() {
        return userProfEditUIMap.getFullNameField();
    }

    public UserProfileDetailsPage clickSaveButton() {
        userProfEditUIMap.getSaveButton().click();
        return new UserProfileDetailsPage();
    }

    public Button getSaveButton() {
        return userProfEditUIMap.getSaveButton();
    }


    public MainPage goToMainPage() {
        return new MainPage();
    }


}
