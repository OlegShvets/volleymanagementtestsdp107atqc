package com.softserveinc.volleymanagementtests.pages.profile.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImplementDec;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import org.openqa.selenium.By;

/**
 * Created by stas on 08.03.2017.
 */
public class UserProfileDetailsUIMap {

    public Label getProfileInfoLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector((".col-lg-12 > h1")))));
    }

    public Label getLoginLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector((".col-lg-12 > label:nth-child(2)")))));
    }

    public Label getEmailLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector((".col-lg-12 > label:nth-child(4)")))));
    }

    public Label getGetPhoneLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector((".col-lg-12 > label:nth-child(6)")))));
    }

    public Label getFullNameLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector((".col-lg-12 > label:nth-child(8)")))));
    }

    public Label getLoginProvLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector((".col-lg-12 > label:nth-child(10)")))));
    }

    public Button getEditProfileButton() {
        return new ButtonImplementDec(By.cssSelector(".btn-warning"));
    }
}
