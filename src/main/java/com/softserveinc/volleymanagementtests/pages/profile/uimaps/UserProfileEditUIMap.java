package com.softserveinc.volleymanagementtests.pages.profile.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImplementDec;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.TextInputImpl;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;
import org.openqa.selenium.By;

/**
 * Created by stas on 08.03.2017.
 */
public class UserProfileEditUIMap {

    public Label getProfileInfoLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                ".col-lg-12 > h1"))));
    }

    public TextInput getEmailField() {
        return new TextInputImpl(new Control(new ContextVisible(By.id("Email"))));
    }

    public TextInput getPhoneField() {
        return new TextInputImpl(new Control(new ContextVisible(By.id("CellPhone"))));
    }

    public TextInput getFullNameField() {
        return new TextInputImpl(new Control(new ContextVisible(By.id("FullName"))));
    }

    public Button getSaveButton() {
        return new ButtonImplementDec(By.cssSelector(".btn-default"));
    }
}
