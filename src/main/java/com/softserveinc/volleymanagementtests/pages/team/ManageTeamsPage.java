package com.softserveinc.volleymanagementtests.pages.team;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.player.PlayerDetailsPage;
import com.softserveinc.volleymanagementtests.pages.team.uimaps.ManageTeamsUIMap;
import com.softserveinc.volleymanagementtests.pages.team.uimaps.TeamCreateUIMap;
import com.softserveinc.volleymanagementtests.pages.tournaments.TournamentDetailsPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.TournamentsPage;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Dropdown;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;

import java.util.List;

public class ManageTeamsPage {
    private ManageTeamsUIMap controls;

    public ManageTeamsPage() {
        controls = new ManageTeamsUIMap();
    }

    public Link getLinkDetailsAboutTeam(){ return  controls.getLinkDetailsAboutTeam();}

    public TeamDetailsPage getDetailsPage(){
        controls.getLinkDetailsAboutTeam().click();
        return new TeamDetailsPage();
    }

    public Button getDeleteButton(){return  controls.getDeleteButton();}

    public Button getAddTeam(){return  controls.getAddTeam();}

    public Button getSaveButton(){return  controls.getSaveButton();}

    public  ManageTeamsPage save(){
        controls.getSaveButton().click();
        return new ManageTeamsPage();
    }

    public Button getCanceleButton(){return controls.getCancelButton();}

    public TournamentDetailsPage Cancel(){
        controls.getCancelButton().click();
        return new TournamentDetailsPage();
    }

    public List<String> getAllTeamsInDropdown(int number){
        return controls.getTeamSelected(number).getOptions();
    }
    public Dropdown getTeamSelected(int teamNumber){return  controls.getTeamSelected(teamNumber);}

    public ManageTeamsPage setTeamName(String text, int teamNumber){
        controls.getTeamSelected(teamNumber).selectByVisibleText(text);

        return new ManageTeamsPage();
    }

    public MainPage goToMainPage() {
        return new MainPage();
    }

    public String getSelectedTeamLink(int teamNumber){
       return controls.getSelectedTeamLink(teamNumber).getText();
    }
}

