package com.softserveinc.volleymanagementtests.pages.team;

import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.player.ChoosePlayerPage;
import com.softserveinc.volleymanagementtests.pages.team.uimaps.TeamCreateUIMap;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;

import java.util.List;
import java.util.concurrent.TimeoutException;

/**
 * Object that represents Create Team  page and provide methods to interact
 * with it.
 *
 * @author S.Tsyganovskiy
 */
public class TeamCreatePage {
    private TeamCreateUIMap controls;

    public TeamCreatePage() {
        controls = new TeamCreateUIMap();
    }

    public final Label getCreateNewTeamLabel() {
        return controls.getCreateNewTeam();
    }

    public final Label getTeamLabel() {
        return controls.getTeamLabel();
    }


    public final Label getNameLabel() {
        return controls.getNameLabel();
    }


    public final TextInput getNameInput() {
        return controls.getNameInput();
    }

    public final TeamCreatePage nameInput(final String name) {
        controls.getNameInput().type(name);
        return this;
    }

    public final Label getErrorMessageNameInput() {
        return controls.getErrorMessageNameInput();
    }


    public final Label getCoachLabel() {
        return controls.getCoachLabel();
    }


    public final TextInput getCoachInput() {
        return controls.getCoachInput();
    }


    public final TeamCreatePage coachInput(final String name) {
        controls.getCoachInput().type(name);
        return this;
    }

    public final Label getCoachErrorInputLabel() {
        return controls.getCoachErrorInput();
    }

    public final Label getCaptainLabel() {
        return controls.getCaptainLabel();
    }


    public final TextInput getCaptainInput() {
        return controls.getCaptainInput();
    }

    public final TextInput getCaptainSmartInput() {
        return controls.getFirstAndLastCaptainNameInput();
    }

    public final TeamCreatePage captainInput(final String name) {
        controls.getCaptainInput().type(name);
        return this;
    }

    public final Button getOpenChoosingPlayersButton() {
        return controls.getOpenChoosingPlayersButton();
    }

    public final ChoosePlayerPage submitOpenChoosingPlayersButton() {
        controls.getOpenChoosingPlayersButton().click();
        new WebDriverUtils().switchToNewWindow();
        return new ChoosePlayerPage();
    }


    public final Label getAchievementsLabel() {
        return controls.getAchievementsLabel();
    }


    public final TextInput getAchievementsInput() {
        return controls.getAchievementsInput();
    }

    public final TeamCreatePage achievementsInput(final String name) {
        controls.getAchievementsInput().type(name);
        return this;
    }

    public final Label getTeamPlayersLabel() {
        return controls.getTeamPlayersLabel();
    }


    public final Button getAddPlayersButton() {
        return controls.getAddPlayersButton();
    }

    public final ChoosePlayerPage submitAddPlayersButton() {
        controls.getAddPlayersButton().click();
        new WebDriverUtils().switchToNewWindow();
        return new ChoosePlayerPage();
    }

    public final Label getFirstAndLastNameLabel() {
        return controls.getFirstAndLasttNameLabel();
    }

    public final List<Label> getPlayersList() {
        return controls.getPlayersList();
    }

    public final Button getCreateTeamButton() {
        return controls.getCreateTeamButton();
    }

    public final TeamListPage submitCreateTeamButton() {
        controls.getCreateTeamButton().click();
        return new TeamListPage();
    }

    public Link getBackToListLink() {
        return controls.getBackToListLink();
    }

    public final TeamListPage clickBackToList() {
        controls.getBackToListLink().click();
        return new TeamListPage();
    }

    public final Label getErrorMessageForCaptain() {
        return controls.getErrorMessage();
    }

    public final Label getErrorMessageForPlayersLabel() {
        return controls.getErrorMessageForPlayers();
    }

    public final Label getErrorMessageLabelForAchievements() {
        return controls.getErrorMessageForAchievements();
    }

    public TeamCreatePage logInAsCaptain() {
        return this;
    }

    public void setAllTeamFields(Team team) {


        if (team.getName() != null) {

            setTeamName(team.getName());
        }

        if (team.getCoach() != null) {
            setCoachName(team.getCoach());
        }

        if (team.getCaptain() != null) {

            setCaptain(team.getCaptain().getFirstName());

        }

        if (team.getAchievements() != null) {
            setAchievements(team.getAchievements());
        }

        if (team.getRoster().size() > 0) {
            ChoosePlayerPage choosePlayerPageToAddPlayers = submitAddPlayersButton();
            String playerFullName = team.getRoster().get(0).getLastName() + " " + team.getRoster().get(0).getFirstName();
            setPlayer(playerFullName);

        }
    }

    public void setSmartAllTeamFields(Team team) throws InterruptedException {


        if (team.getName() != null) {

            setTeamName(team.getName());
        }

        if (team.getCoach() != null) {
            setCoachName(team.getCoach());
        }

        if (team.getCaptain() != null) {
            String LAST_NAME = team.getCaptain().getLastName();
            String FULL_NAME = LAST_NAME + " " + team.getCaptain().getFirstName();
            setCaptainSmart(FULL_NAME, FULL_NAME);

        }

        if (team.getAchievements() != null) {
            setAchievements(team.getAchievements());
        }

        if (team.getRoster().size() > 0) {
            submitAddPlayersButton();
            String LAST_NAME = team.getRoster().get(0).getLastName();
            String FULL_NAME = LAST_NAME + " " + team.getRoster().get(0).getFirstName();
            setPlayerSmart(FULL_NAME, FULL_NAME);

        }
    }

    public void setCoachName(String name) {
        controls.getCoachInput().clear();
        controls.getCoachInput().type(name);
    }

    public void setTeamName(String name) {
        controls.getNameInput().clear();
        controls.getNameInput().type(name);
    }

    public void setAchievements(String text) {
        controls.getAchievementsInput().clear();
        controls.getAchievementsInput().type(text);
    }

    public void setCaptain(String text) {
        controls.getCaptainInput().clear();
        controls.getCaptainInput().type(text);
    }

    public void setCaptainSmart(String partText, String fullText) {
        controls.getCaptainInput().clear();
        controls.getCaptainInput().type(partText);
        for (Link link : controls.getCaptainSmartInput()) {
            if (link.getText().equals(fullText)) link.click();
        }
    }

    public void setPlayer(String playerFullName) {
        controls.getFirstAndLastNameInput().clear();
        controls.getFirstAndLastNameInput().type(playerFullName);
    }

    public void setPlayerSmart(String lastName, String playerFullName) throws InterruptedException {
        controls.getFirstAndLastNameInput().clear();
        controls.getFirstAndLastNameInput().type(lastName);
        for (Link link : controls.getPlayersSmartInput()) {
            if (link.getText().equals(playerFullName)) {
                link.click();
                break;
            }
        }
    }

    public MainPage goToMainPage() {
        return new MainPage();
    }


}
