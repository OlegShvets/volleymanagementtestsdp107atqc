package com.softserveinc.volleymanagementtests.pages.team;

import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.player.ChoosePlayerPage;
import com.softserveinc.volleymanagementtests.pages.team.uimaps.TeamEditUIMap;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;
import org.apache.commons.lang3.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

/**
 * Object that represents  TeamList  page and provide methods to interact
 * with it.
 *
 * @author Danil Zhyliaiev
 */
public class TeamEditPage {
    private TeamEditUIMap controls;
    
    public TeamEditPage() {
        controls = new TeamEditUIMap();
    }
    
    public Label getEditTeamLabel() {
        return controls.getEditTeamLabel();
    }
    
    public Label getTeamLabel() {
        return controls.getTeamLabel();
    }
    
    public Label getNameLabel() {
        return controls.getNameLabel();
    }
    
    public TextInput getNameInput() {
        return controls.getNameInput();
    }
    
    public final TeamEditPage setName(final String name) {
        controls.getNameInput().type(name);
        return this;
    }
    
    public Label getErrorMessageNameInput() {
        return controls.getErrorMessageNameInput();
    }
    
    
    public Label getCoachLabel() {
        return controls.getCoachLabel();
    }
    
    public TextInput getCoachInput() {
        return controls.getCoachInput();
    }
    
    public final TeamEditPage setCoach(final String name) {
        controls.getCoachInput().type(name);
        return this;
    }
    
    public Label getCaptainLabel() {
        return controls.getCaptainLabel();
    }
    
    
    public TextInput getCaptainInput() {
        return controls.getCaptainInput();
    }
    
    
    public Button getChooseCaptainButton() {
        return controls.getChooseCaptainButton();
    }
    
    public final TeamEditPage setCaptain(final String name) {
        throw new NotImplementedException("setCaptain() Method not implemented");
    }
    
    public final ChoosePlayerPage openChooseCaptainPage() {
        controls.getChooseCaptainButton().click();
        return new ChoosePlayerPage();
    }
    
    public Label getAchievementsLabel() {
        return controls.getAchievementsLabel();
    }
    
    public TextInput getAchievementsInput() {
        return controls.getAchievementsInput();
    }
    
    public final TeamEditPage setAchievements(final String name) {
        controls.getAchievementsInput().type(name);
        return this;
    }
    
    public Label getTeamPlayersLabel() {
        return controls.getTeamPlayersLabel();
    }
    
    
    public Button getAddPlayersButton() {
        return controls.getAddPlayersButton();
    }
    
    public final ChoosePlayerPage openChoosePlayersPage() {
        controls.getAddPlayersButton().click();
        return new ChoosePlayerPage();
    }
    
    public Label getFirstAndLastNameLabel() {
        return controls.getFirstAndLastNameLabel();
    }
    
    public List<TextInput> getTeamRoster() {
        return controls.getTeamRoster();
    }
    
    public Button getSaveTeamButton() {
        return controls.getSaveTeamButton();
    }
    
    public Link getBackToListLink() {
        return controls.getBackToListLink();
    }
    
    public final TeamListPage clickBackToList() {
        controls.getBackToListLink().click();
        return new TeamListPage();
    }
    
    public Label getErrorMessageLabel() {
        return controls.getErrorMessage();
    }
    
    private String getTeamId() {
        String currentUrl = new WebDriverUtils().getCurrentUrl();
        return currentUrl.substring(currentUrl.lastIndexOf('/') + 1);
    }
    
    private Player readPlayer(TextInput playerInput) {
        String[] playerFullName = playerInput.getText().split(" ");
        return new Player().newBuilder()
                .setLastName(playerFullName[0])
                .setFirstName(playerFullName[1])
                .build();
    }
    
    private List<Player> readRoster() {
        List<Player> roster = new ArrayList<>();
        
        for (TextInput player : getTeamRoster()) {
            readPlayer(player);
        }
        
        return roster;
    }
    
    public Team readTeam() {
        return new Team().newBuilder()
                .setId(getTeamId())
                .setName(getNameInput().getText())
                .setCoach(getCoachInput().getText())
                .setCaptain(readPlayer(getCaptainInput()))
                .setAchievements(getAchievementsInput().getText())
                .setRoster(readRoster())
                .build();
    }
    
    public MainPage goToMainPage() {
        return new MainPage();
    }

    public Button getUploadButton() {
        return controls.getUploadFotoButton();
    }

}
