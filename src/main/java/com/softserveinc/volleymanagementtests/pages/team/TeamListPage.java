package com.softserveinc.volleymanagementtests.pages.team;

import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.team.uimaps.TeamListUIMap;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.tools.controls.AlertImplementDec;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Alert;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;

/**
 * Object that represents  TeamList  page and provide methods to interact
 * with it.
 *
 * @author S.Tsyganovskiy
 */
public class TeamListPage {

    private TeamListUIMap controls;

    public TeamListPage() {
        controls = new TeamListUIMap();
    }

    public Label getListOfTeamsLabel() {
        return controls.getListOfTeamsLabel();
    }

    public Label getNameLabel() {
        return controls.getNameLabel();
    }

    public Button getDeleteButton() {return controls.getDeleteButton();}


    public Link getCreateTeamLink() {
        return controls.getCreateTeamLink();
    }

    public TeamCreatePage clickCreateTeamLink() {
        controls.getCreateTeamLink().click();
        return new TeamCreatePage();
    }

    public List<TeamListUIMap.TeamTableRow> getTeamList() {
        return controls.getTeamList();
    }

    public List<String> getTeams() {
        List<String> teams = new ArrayList<>();
        for (int i = 0; i < controls.getAllTeams().size(); i++){
            teams.add(i, controls.getAllTeams().get(i).getText());
        }
        return teams;
    }

    public boolean isTeamExistOnPage(Team team) {
        List<TeamListUIMap.TeamTableRow> listOfTeams = getTeamList();

        for (TeamListUIMap.TeamTableRow tableRow: listOfTeams) {
            if (tableRow.getNameTeam().getText().equals(team.getName())) {
                return true;
            }
        }

        return false;
    }

    public TeamDetailsPage showTeamDetails(Team team) {
        List<TeamListUIMap.TeamTableRow> listOfTeams = getTeamList();

        for (TeamListUIMap.TeamTableRow teamTableRow: listOfTeams) {
            if (teamTableRow.getNameTeam().getText().equals(team.getName())) {
                teamTableRow.getTeamDetails().click();
                return new TeamDetailsPage();
            }
        }

        throw new NoSuchElementException(String.format(
                "There is no team with the name %s on the page", team.getName()));
    }

    public Alert clickDeleteTeamButton(Team team) {
        List<TeamListUIMap.TeamTableRow> listOfTeams = getTeamList();

        for (TeamListUIMap.TeamTableRow teamTableRow: listOfTeams) {
            if (teamTableRow.getNameTeam().getText().equals(team.getName())) {
                teamTableRow.getDeleteTeamButton().click();
                return new AlertImplementDec();
            }
        }

        throw new NoSuchElementException(String.format(
                "There is no team with the name %s on the page", team.getName()));
    }

    public TeamDetailsPage showRandomTeamDetails() {
        getTeamList().get(new Random().nextInt(getTeamList().size()))
                .getTeamDetails()
                .click();
        return new TeamDetailsPage();
    }

    public MainPage goToMainPage() {
        return new MainPage();
    }
}
