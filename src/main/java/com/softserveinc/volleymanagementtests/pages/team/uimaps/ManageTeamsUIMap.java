package com.softserveinc.volleymanagementtests.pages.team.uimaps;

import com.softserveinc.volleymanagementtests.tools.*;
import com.softserveinc.volleymanagementtests.tools.controls.*;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Dropdown;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import org.openqa.selenium.By;

import java.util.List;

/**
 * Created by Natalia on 11.03.2017.
 */
public class ManageTeamsUIMap {

    public ManageTeamsUIMap() {
        if (!(new WebDriverUtils().getCurrentUrl().contains("/Tournaments/ManageTournamentTeams?tournamentId"))) {
            throw new RuntimeException("This is not \"ManageTournamentTeams\" page.");
        }
    }

    public final Link getLinkDetailsAboutTeam() {
        return new LinkImpl(new Control(new ContextVisible(By.cssSelector((" #tornamentRoster > tbody")))));
    }

    public Button getAddTeam() {
        return new ButtonImplementDec(By.id("addTeamToTournamentButton"));

    }

    public Button getDeleteButton() {
        return new ButtonImplementDec(By.className("deleteTeamFromTournamentButton"));
    }

    public Button getSaveButton() {
        return new ButtonImplementDec(By.id("addTeamsButton"));
    }

    public Button getCancelButton() {
        return new ButtonImplementDec(By.cssSelector("#page-wrapper > div > div > div > div > a"));
    }

    public Dropdown getTeamSelectedByNum(int num) {
        return new DropdownImpl(new Select(new ContextVisible(By.cssSelector(("#tornamentRoster > tbody > tr:nth-child(2) " +
                "> td:nth-child(1) > select > option:nth-child(" + num + ")")))));
    }

    public Dropdown getTeamSelected(int number) {
        int numb = number + 1;
        return new DropdownImpl(new Select(new ContextLongVisible(By.cssSelector((
                "#tornamentRoster > tbody > tr:nth-child(" +
                        numb + ") > td:nth-child(1) > select")))));
    }
    public Link getSelectedTeamLink(int teamNumber) {
        int number = teamNumber + 1;
        return new LinkImpl(new Control(new ContextLongVisible(By.cssSelector("" +
                "#tornamentRoster > tbody > tr:nth-child(" +number +") > td:nth-child(1) > a"))));
    }
}
