package com.softserveinc.volleymanagementtests.pages.team.uimaps;

import com.softserveinc.volleymanagementtests.tools.*;
import com.softserveinc.volleymanagementtests.tools.controls.*;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;

import java.util.List;

/**
 * UI Map contain all CreateTeam page locators and initializes controls using
 * lazy initialization approach.
 *
 * @author S.Tsyganovskiy
 */
public class TeamCreateUIMap {

    public TeamCreateUIMap() {
        if (!(new WebDriverUtils().getCurrentUrl().contains("Teams/Create"))) {
            throw new RuntimeException("This is not \"Create new team\" page.");
        }
    }

    public final Label getCreateNewTeam() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(".col-lg-12>p>a"))));
    }

    public final Label getTeamLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector("fieldset > legend"))));
    }

    public final Label getNameLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector("[for=\"Name\"]"))));
    }

    public final TextInput getNameInput() {
        return new TextInputImpl(new Control(new ContextVisible(By.id("Name"))));
    }

    public final Label getErrorMessageNameInput() {
        Label errorMessageNameInput;
        try {
            errorMessageNameInput = new LabelImpl(new Control(new ContextVisible(By.id("Name-error"))));
        } catch (TimeoutException e) {
            errorMessageNameInput = null;
        }
        return errorMessageNameInput;
    }

    public final Label getCoachLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector("[for=\"Coach\"]"))));
    }

    public final TextInput getCoachInput() {
        return new TextInputImpl(new Control(new ContextVisible(By.id("Coach"))));
    }

    public Label getCoachErrorInput() {
        LabelImpl errorMessage;
        try {
            errorMessage = new LabelImpl(new Control(new ContextVisible(By.id("Coach-error"))));
        } catch (TimeoutException e) {
            errorMessage = null;
        }
        return errorMessage;
    }

    public final Label getCaptainLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "[for=\"Captain_FullNameText\"]"))));
    }

    public final TextInput getCaptainInput() {
        return new TextInputImpl(new Control(new ContextVisible(By.id("Captain_FullName"))));
    }

    public final List<Link> getCaptainSmartInput() {
        return new WebElementsList(new ControlListWrapper(new ContextLongVisible(By.cssSelector("" +
                "ul#ui-id-1.ui-autocomplete.ui-front.ui-menu.ui-widget.ui-widget-content li")))).asLinks();

    }

    public final List<Link> getPlayersSmartInput() {
        return new WebElementsList(new ControlListWrapper(new ContextVisible
                (By.cssSelector("ul[class='ui-autocomplete ui-front " +
                        "ui-menu ui-widget ui-widget-content']:not(#ui-id-1) li")))).asLinks();
    }

    public final Button getOpenChoosingPlayersButton() {
        return new ButtonImplementDec(By.cssSelector("#addPlayerToTeamButton"));
    }

    public final Label getAchievementsLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "[for=\"Achievements\"]"))));
    }

    public final TextInput getAchievementsInput() {
        return new TextInputImpl(new Control(new ContextVisible(By.id("Achievements"))));
    }

    public final Label getTeamPlayersLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "#content>form>fieldset>div>h4"))));
    }

    public final Button getAddPlayersButton() {
        return new ButtonImplementDec(By.id("addPlayerToTeamButton"));
    }

    public final Label getFirstAndLasttNameLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "#teamRoster>tbody>tr>th"))));
    }

    public final TextInput getFirstAndLastNameInput() {
        return new TextInputImpl(new Control(new ContextVisible(By.cssSelector(
                "#teamRoster > tbody > tr:nth-child(3) > td:nth-child(1) > div > input"))));
    }

    public final TextInput getFirstAndLastCaptainNameInput() {
        return new TextInputImpl(new Control(new ContextLongVisible(By.cssSelector(
                "#teamRoster > tbody > tr.teamPlayer.captain > td:nth-child(1) > div > input"))));
    }

    public List<Label> getPlayersList() {
        return new WebElementsList(new ControlListWrapper(new ContextVisible
                (By.cssSelector("td>input")))).asLabels();
    }

    public final Button getCreateTeamButton() {
        return new ButtonImplementDec(By.id("createTeamButton"));
    }


    public final Link getBackToListLink() {
        return new LinkImpl(new Control(new ContextVisible(By.cssSelector("[href$=\"/Teams\"]"))));
    }


    public Label getErrorMessage() {
        return new LabelImpl(new Control(new ContextVisible(By.id("Captain_FullName-error"))));
    }

    public Label getErrorMessageForPlayers() {
        return new LabelImpl(new Control(new ContextVisible(By.id("0-error"))));
    }

    public Label getErrorMessageForAchievements() {
        LabelImpl errorMessage;
        try {
            errorMessage = new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                    "#page-wrapper > div > div > div > form > fieldset " +
                            "> div.multiline-editor-field > span"))));
        } catch (TimeoutException e) {
            errorMessage = null;
        }
        return errorMessage;
    }
}