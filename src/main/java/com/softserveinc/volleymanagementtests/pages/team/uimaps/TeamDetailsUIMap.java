package com.softserveinc.volleymanagementtests.pages.team.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.ControlListWrapper;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LinkImpl;
import com.softserveinc.volleymanagementtests.tools.controls.WebElementsList;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import org.openqa.selenium.By;

import java.util.List;

/**
 * UI Map contain all Team Details page locators and initializes controls using
 * lazy initialization approach.
 *
 * @author S.Tsyganovskiy
 */
public class TeamDetailsUIMap {


    public TeamDetailsUIMap() {
        if (!getTeamDetailsLabel().getText().equals("Details about team")) {
            throw new RuntimeException("This is not \"Team details\" page.");
        }
    }


    public Link getCaptainDetails() {
        return new LinkImpl(new Control(new ContextVisible(By.cssSelector("fieldset > a"))));
    }

    public List<Link> getPlayerRoaster() {
        return new WebElementsList(new ControlListWrapper(new ContextVisible
                (By.cssSelector("fieldset ul  a")))).asLinks();
    }

    public Link getEditTeamLink() {
        return new LinkImpl(new Control(new ContextVisible(By.cssSelector("[href^=\"/Teams/Edit/\"]"))));
    }

    public Link getBack() {
        return new LinkImpl(new Control(new ContextVisible(By.cssSelector("p > a:nth-child(2)"))));
    }

    public Label getTeamDetailsLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(".col-lg-12 >h2"))));
    }

    public Label getTeamNameLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                ".col-lg-12 >fieldset > legend"))));
    }

    public Label getCoachLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                ".display-label>label"))));
    }

    public Label getCoachNameLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "legend + div.display-label + div.display-field"))));
    }

    public Label getAchievementsLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "[for=\"Achievements\"]"))));
    }

    public Label getAchievementsDescriptionLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "legend + div.display-label + div.display-field"
                        + "+ div.display-label + div.display-field"))));
    }

    public Label getCaptainLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "[for=\"Captain\"]"))));
    }

    public Label getRosterLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(
                "[for=\"Roster\"]"))));
    }

    public Label getFotoLabel() {
        return new LabelImpl(new Control((new ContextVisible(By.cssSelector(
                "#page-wrapper > div > div > div > div > div > div > img")))));
    }
}
