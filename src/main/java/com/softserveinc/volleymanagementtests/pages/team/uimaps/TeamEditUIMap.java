package com.softserveinc.volleymanagementtests.pages.team.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.ControlListWrapper;
import com.softserveinc.volleymanagementtests.tools.controls.*;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;
import org.openqa.selenium.By;

import java.util.List;

/**
 * !!!This page is based on sources and may be inaccurate!!!
 * List of TeamEditPage controls and accessors.
 */
public class TeamEditUIMap {

    public TeamEditUIMap() {
        if (!getEditTeamLabel().getText().equals("Edit team")) {
            throw new RuntimeException("This is not \"Edit team\" page.");
        }
    }

    public final Label getEditTeamLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector((".col-lg-12 > h2")))));
    }

    public final Label getTeamLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(("fieldset > legend")))));
    }

    public final Label getNameLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(("[for=\"Name\"]")))));
    }

    public final TextInput getNameInput() {
        return new TextInputImpl(new Control(new ContextVisible(By.id("Name"))));
    }

    public final Label getErrorMessageNameInput() {
        return new LabelImpl(new Control(new ContextVisible(By.id("Name-error"))));
    }

    public final Label getCoachLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector("[for=\"Coach\"]"))));
    }

    public final TextInput getCoachInput() {
        return new TextInputImpl(new Control(new ContextVisible(By.id("Coach"))));
    }

    public final Label getCaptainLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(("[for=\"Captain_FullNameText\"]")))));
    }

    public final TextInput getCaptainInput() {
        return new TextInputImpl(new Control(new ContextVisible(By.id("Captain_FullName"))));
    }

    public final Button getChooseCaptainButton() {
        return new ButtonImplementDec(By.className("chooseCaptainButton"));
    }

    public final Label getAchievementsLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(("[for=\"Achievements\"]")))));
    }

    public final TextInput getAchievementsInput() {
        return new TextInputImpl(new Control(new ContextVisible(By.id("Achievements"))));
    }

    public final Label getTeamPlayersLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(("#content>form>fieldset>div>h4")))));
    }

    public final Button getAddPlayersButton() {
        return new ButtonImplementDec(By.className("[onclick=\"openChoosingPlayersWindow('ChoosingRosterWindow')\"]"));
    }

    public final Label getFirstAndLastNameLabel() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector(("#teamRoster>tbody>tr>th")))));
    }

    public List<TextInput> getTeamRoster() {
        return new WebElementsList(new ControlListWrapper(new ContextVisible
                (By.cssSelector("tr.rosterPlayer > td:first-child > input")))).asTextInputs();
    }

    public final Button getSaveTeamButton() {
        return new ButtonImplementDec(By.cssSelector("#saveTeamButton"));
    }

    public final Link getBackToListLink() {
        return new LinkImpl(new Control(new ContextVisible(By.cssSelector("[href$=\"/Teams\"]"))));
    }

    public Label getErrorMessage() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector((".validation-summary-errors>ul>li")))));
    }

    public Button getUploadFotoButton() {
        return new ButtonImpl(new Control(new ContextVisible(By.id("uploadButton"))));
    }

    public Button getDeleteFotoButton() {
        return new ButtonImpl(new Control(new ContextVisible(By.id("uploadButton"))));
    }
}
