package com.softserveinc.volleymanagementtests.pages.team.uimaps;

import com.softserveinc.volleymanagementtests.pages.contracts.Remapable;
import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.ControlListWrapper;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImplementDec;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LinkImpl;
import com.softserveinc.volleymanagementtests.tools.controls.WebElementsList;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

/**
 * UI Map contain all  TeamList page locators and initializes controls using
 * lazy initialization approach.
 *
 * @author S.Tsyganovskiy
 */
public class TeamListUIMap implements Remapable {

    private Label listOfTeamsLabel;
    private Label nameLabel;
    private Button createTeamButton;
    private Button deleteButton;
    private List<TeamTableRow> teamList;
    private List<Label> teams;

    public TeamListUIMap() {
        if (!(new WebDriverUtils().getCurrentUrl().contains("/Teams"))) {
            throw new RuntimeException("This is not \"Team list\" page.");
        }
    }

    public Button getDeleteButton() {
        return new ButtonImplementDec(By.cssSelector("td>input"));
    }

    public Label getListOfTeamsLabel() {
            return new LabelImpl(new Control(new ContextVisible(By.cssSelector(".col-lg-12 > h2"))));

    }

    public Label getNameLabel() {
          return    new LabelImpl(new Control(new ContextVisible(By.cssSelector(".table > tbody > tr > th"))));

    }

    public Link getCreateTeamLink() {
      return new LinkImpl(new Control(
              new ContextVisible(By.cssSelector(".col-lg-12 > p  > a"))));
    }

    public List<TeamTableRow> getTeamList() {
            teamList = new ArrayList<>();
            List<Label> teamNames = new WebElementsList(new ControlListWrapper(new ContextVisible
                    (By.cssSelector(".teamList")))).asLabels();
            List<Link> teamDetailsLinks = new WebElementsList(new ControlListWrapper(new ContextVisible
                    (By.cssSelector(".deleteTeamButton +a")))).asLinks();
            List<Button> deleteButtons = new WebElementsList(new ControlListWrapper(new ContextVisible
                    (By.cssSelector(".deleteTeamButton")))).asButtons();

            for (int i = 0; i < teamNames.size(); i++) {
                teamList.add(new TeamTableRow(teamNames.get(i), teamDetailsLinks.get(i),
                        deleteButtons.get(i)));
            }

        return teamList;
    }

    public List<Label> getAllTeams() {
        teams = new ArrayList<>();
        List<Label> teamNames = new WebElementsList(new ControlListWrapper(new ContextVisible
                (By.cssSelector(".teamList")))).asLabels();

        for (int i = 0; i < teamNames.size(); i++) {
            teams.add(teamNames.get(i));
        }

        return teams;
    }

    @Override
    public void remap() {
     listOfTeamsLabel = null;
     nameLabel = null;
     createTeamButton = null;
     deleteButton = null;
     teamList = null;
    }

    public class TeamTableRow {

        private Label nameTeam;
        private Link teamDetails;
        private Button deleteTeam;

        public TeamTableRow(Label nameTeam, Link teamDetails, Button deleteTeam) {
            this.nameTeam = nameTeam;
            this.teamDetails = teamDetails;
            this.deleteTeam = deleteTeam;
        }

        public Label getNameTeam() {
            return nameTeam;
        }

        public Link getTeamDetails() {
            return teamDetails;
        }

        public Button getDeleteTeamButton() {
            return deleteTeam;
        }
    }
}
