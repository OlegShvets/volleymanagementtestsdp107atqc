package com.softserveinc.volleymanagementtests.pages.tournaments;

import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.uimap.ApplyForTournamentUIMap;

/**
 * Created by stas on 11.03.2017.
 */
public class ApplyForTournamentPage {

    private ApplyForTournamentUIMap controls;

    public ApplyForTournamentPage() {
        controls = new ApplyForTournamentUIMap();
    }

    public ApplyForTournamentPage chooseTeamByIndexInDropdown(int index) {
        controls.getTeamSelectedDropdown().selectByIndex(index - 1);
        return this;
    }

    public ApplyForTournamentPage checkAgreeCheckbox() {
        if (!controls.getAgreeCheckbox().isChecked()) {
            controls.getAgreeCheckbox().check();
        }
        return this;
    }

    public ApplyForTournamentPage clickAcceptButton() {
        if (controls.getAcceptButton().isEnabled()) {
            controls.getAcceptButton().click();
        }
        return this;
    }


    public MainPage goToMainPage() {
        return new MainPage();
    }
}
