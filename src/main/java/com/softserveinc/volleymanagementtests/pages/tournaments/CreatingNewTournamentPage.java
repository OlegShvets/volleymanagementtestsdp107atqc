package com.softserveinc.volleymanagementtests.pages.tournaments;

import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.uimap.CreatingNewTournamentPageUIMap;
import com.softserveinc.volleymanagementtests.testdata.tournament.Tournament;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by Sasha on 11.03.2017.
 */
public class CreatingNewTournamentPage {
    private CreatingNewTournamentPageUIMap controls;
    private Map<String, Integer> calendar = new HashMap<>();
    
    
    public CreatingNewTournamentPage() {
        controls = new CreatingNewTournamentPageUIMap();
        calendar.put("января", 1);
        calendar.put("февраля", 2);
        calendar.put("марта", 3);
        calendar.put("апреля", 4);
        calendar.put("мая", 5);
        calendar.put("июня", 6);
        calendar.put("июля", 7);
        calendar.put("августа", 8);
        calendar.put("сентября", 9);
        calendar.put("октября", 10);
        calendar.put("ноября", 11);
        calendar.put("декабря", 12);
        
    }
    
    public TournamentsPage createTournamentWithOneGroupAndOneDivisionAndBackToList(Tournament tournament) {
        setTournamentName(tournament.getName());
        setDescription(tournament.getDescription());
        selectSeason(tournament.getTournamentSeason());
        selectScheme(tournament.getSchemeCode());
        setRegulationLink(tournament.getRegulationsLink());
        setApplyingDateStart(tournament.getApplyingPeriodStart());
        setApplyingDateEnd(tournament.getApplyingPeriodEnd());
        setTournamentDateStart(tournament.getTournamentStart());
        setTournamentDateEnd(tournament.getTournamentEnd());
        checkTransferAllowCheckbox();
        setTransferDateStart(tournament.getTransferPeriodStart());
        setTransferDateEnd(tournament.getTransferPeriodEnd());
        createTournamentClick();
        return new TournamentsPage();
    }
    
    private void setTournamentName(String name) {
        controls.getTournamentNameInput().type(name);
    }
    
    private void setDescription(String text) {
        controls.getDescriptionInput().type(text);
    }
    
    private void selectSeason(String season) {
        controls.getSeasonCheckingDropdown().selectByVisibleText(season);
    }
    
    private void selectScheme(Integer scheme) {
        controls.getSchemeCheckingDropDown().selectByVisibleText(scheme.toString());
    }
    
    private void setRegulationLink(String text) {
        controls.getTournamentRegulationsLinkInput().type(text);
    }
    
    private void setApplyingDateStart(Date date) {
        controls.getApplyingPeriodStartsInput().click();
        datePicker(date);
    }
    
    private void setApplyingDateEnd(Date date) {
        controls.getApplyingPeriodEndsInput().click();
        datePicker(date);
    }
    
    private void setTournamentDateStart(Date date) {
        controls.getTournamentPeriodStartsInput().click();
        datePicker(date);
    }
    
    private void setTournamentDateEnd(Date date) {
        controls.getTournamentPeriodEndsInput().click();
        datePicker(date);
    }
    
    private void checkTransferAllowCheckbox() {
        if (! controls.getTransferAllowCheckbox().isChecked()) {
            controls.getTransferAllowCheckbox().check();
        }
    }
    
    private void setTransferDateStart(Date date) {
        controls.getTransferPeriodStartsInput().click();
        datePicker(date);
    }
    
    private void setTransferDateEnd(Date date) {
        controls.getTransferPeriodEndsInput().click();
        datePicker(date);
    }
    
    public void addGroups(int number) {
        for (int i = 0; i < number; i++) {
            controls.getAddGroup().click();
        }
    }
    
    public void addDivisions(int number) {
        for (int i = 0; i < number; i++) {
            controls.getAddDivisionLink().click();
        }
    }
    
    public void createTournamentClick() {
        controls.getCreateTournamentButton().click();
    }
    
    public TournamentsPage gotoTournamentsPage() {
        controls.getBackToTournamentPageLink().click();
        return new TournamentsPage();
    }
    
    
    private void datePicker(Date date) {
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy");

       /*Using DateFormat format method we can create a string
           representation of a date with the defined format.*/
        
        String reportDate = df.format(date);
        String[] sDate = reportDate.split(Pattern.quote("."));
        int day = Integer.parseInt(sDate[0].toLowerCase());
        int month = Integer.parseInt(sDate[1]);
        int year = Integer.parseInt(sDate[2]);
        while (controls.getYear() != year) {
            if (controls.getYear() > year) {
                controls.getBackArrow().click();
            } else controls.getForwardArrow().click();
        }
        while (calendar.get(controls.getMonth()) != month) {
            if (calendar.get(controls.getMonth()) > month) {
                controls.getBackArrow().click();
            } else controls.getForwardArrow().click();
        }
        for (com.softserveinc.volleymanagementtests.tools.controls.contracts.Button button : controls.getDates()) {
            if (button.getText().equals(String.valueOf(day))) {
                button.click();
                break;
            }
        }
    }
    
    
    public MainPage goToMainPage() {
        return new MainPage();
    }
    
    
}
