package com.softserveinc.volleymanagementtests.pages.tournaments;

import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.uimap.EditResultUIMap;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Checkbox;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;

public class EditResultPage {
    private EditResultUIMap control;
    
    public EditResultPage() {
        this.control = new EditResultUIMap();
    }
    
    public Label getPageTitle() {
        return control.getPageTitle();
    }
    
    public Label getInfoAboutGame() {
        return control.getInfoAboutGame();
    }
    
    public EditResultPage checkIsTechnicalDefeat() {
        control.getIsTechnicalDefeat().check();
        return this;
    }
    
    public EditResultPage unCheckIsTechnicalDefeat() {
        control.getIsTechnicalDefeat().unCheck();
        return this;
    }
    
    public Boolean IsTechnicalDefeatIsCheck() {
        return control.getIsTechnicalDefeat().isChecked();
    }
    
    public TournamentsSchedulePage clickOnSaveButton() {
        control.getSaveButton().click();
        return new TournamentsSchedulePage();
    }
    
    public TournamentsSchedulePage clickOnCancelButton() {
        control.getCancelButton().click();
        return new TournamentsSchedulePage();
    }
    
    public EditResultPage setSetsScoreHome(Integer result) {
        control.getSetsScoreHome().clear();
        control.getSetsScoreHome().type(Integer.toString(result));
        return this;
    }
    
    public EditResultPage setSetsScoreAway(Integer result) {
        control.getSetsScoreAway().clear();
        control.getSetsScoreAway().type(Integer.toString(result));
        return this;
    }
    
    public EditResultPage setSetsScoreOneHome(Integer result) {
        control.getSetsScoreOneHome().clear();
        control.getSetsScoreOneHome().type(Integer.toString(result));
        return this;
    }
    
    public EditResultPage setSetsScoreOneAway(Integer result) {
        control.getSetsScoreOneAway().clear();
        control.getSetsScoreOneAway().type(Integer.toString(result));
        return this;
    }
    
    public EditResultPage setSetsScoreTwoHome(Integer result) {
        control.getSetsScoreTwoHome().clear();
        control.getSetsScoreTwoHome().type(Integer.toString(result));
        return this;
    }
    
    public EditResultPage setSetsScoreTwoAway(Integer result) {
        control.getSetsScoreTwoAway().clear();
        control.getSetsScoreTwoAway().type(Integer.toString(result));
        return this;
    }
    
    public EditResultPage setSetsScoreThreeHome(Integer result) {
        control.getSetsScoreThreeHome().clear();
        control.getSetsScoreThreeHome().type(Integer.toString(result));
        return this;
    }
    
    public EditResultPage setSetsScoreThreeAway(Integer result) {
        control.getSetsScoreThreeAway().clear();
        control.getSetsScoreThreeAway().type(Integer.toString(result));
        return this;
    }
    
    public EditResultPage setSetsScoreFourHome(Integer result) {
        control.getSetsScoreFourHome().clear();
        control.getSetsScoreFourHome().type(Integer.toString(result));
        return this;
    }
    
    public EditResultPage setSetsScoreFourAway(Integer result) {
        control.getSetsScoreFourAway().clear();
        control.getSetsScoreFourAway().type(Integer.toString(result));
        return this;
    }
    
    public EditResultPage setSetsScoreFiveHome(Integer result) {
        control.getSetsScoreFiveHome().clear();
        control.getSetsScoreFiveHome().type(Integer.toString(result));
        return this;
    }
    
    public EditResultPage setSetsScoreFiveAway(Integer result) {
        control.getSetsScoreFiveAway().clear();
        control.getSetsScoreFiveAway().type(Integer.toString(result));
        return this;
    }

    public MainPage goToMainPage() {
        return new MainPage();
    }
}
