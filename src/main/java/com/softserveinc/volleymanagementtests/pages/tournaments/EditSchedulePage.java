package com.softserveinc.volleymanagementtests.pages.tournaments;

import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.uimap.EditScheduleUIMap;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;

public class EditSchedulePage {
    EditScheduleUIMap control;
    
    public EditSchedulePage() {
        this.control = new EditScheduleUIMap();
    }
    
    public Label getErrorText() {
        return control.getErrorText();
    }
    
    public Label getPageTitle() {
        return control.getPageTitle();
    }
    
    public TournamentsSchedulePage getSaveButton() {
        control.getSaveButton().click();
        return new TournamentsSchedulePage();
    }
    
    public TournamentsSchedulePage getCancelButton() {
        control.getCancelButton().click();
        return new TournamentsSchedulePage();
    }
    
    public EditSchedulePage enterInDataField(String data) {
        control.getDataField().clear();
        control.getDataField().type(data);
        return this;
    }

    public int countRounds(){
        control.getRounds().size();
        return control.getRounds().size();
    }

    public MainPage goToMainPage() {
        return new MainPage();
    }
}
