package com.softserveinc.volleymanagementtests.pages.tournaments;

import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.uimap.ScheduleGamePageUIMap;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Sasha on 13.03.2017.
 */
public class ScheduleGamePage {
    private ScheduleGamePageUIMap controls;

    public ScheduleGamePage() {
        controls = new ScheduleGamePageUIMap();
    }



    public ScheduleGamePage selectRound(String roundName) {
        controls.getRoundDropDown().selectByVisibleText(roundName);
        return this;
    }

    public ScheduleGamePage selectHomeTeam(String homeTeamName) {
        controls.getHomeTeamDropDown().selectByVisibleText(homeTeamName);
        return this;
    }

    public ScheduleGamePage selectAwayTeam(String awayTeamName){
        controls.getAwayTeamDropDown().selectByVisibleText(awayTeamName);
        return this;
    }

    public ScheduleGamePage typeDateAndTime(Date date) {
         /*Using DateFormat format method we can create a string
           representation of a date with the defined format.*/
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String reportDate = df.format(date);
        controls.getDateAndTimeTextInput().clear();

        controls.getDateAndTimeTextInput().type(reportDate);
        return this;
    }

    public TournamentsSchedulePage saveButton() {
        controls.getSaveButton().click();
        return new TournamentsSchedulePage();
    }

    public EditSchedulePage createButton(){
        controls.getCreateButton().click();
        return  new EditSchedulePage();
    }

    public ScheduleGamePage creareOneMoreButton(){
        controls.getCreateOneMoreButton().click();
        return new ScheduleGamePage();
    }

    public EditSchedulePage cancel(){
        controls.getCancelLink().click();
        return new EditSchedulePage();
    }

    public MainPage goToMainPage() {
        return new MainPage();
    }

    public List<Label> labelsList() {
        return controls.allLabels();
    }

    public String labelDateAndTimeName() {
        return controls.getDateAndTimeLabel().getText();
    }


    public List<String> labelsNames() {
        List<String> names = new ArrayList<>();

        for (int i = 0; i < labelsList().size(); i++) {
            names.add(labelsList().get(i).getText());
        }
        return names;
    }

}
