package com.softserveinc.volleymanagementtests.pages.tournaments;

import com.softserveinc.volleymanagementtests.pages.gameresults.GameResultsPage;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.team.ManageTeamsPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.uimap.TournamentDetailsUIMap;
import com.softserveinc.volleymanagementtests.testdata.tournament.Tournament;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by stas on 09.03.2017.
 */
public final class TournamentDetailsPage {

    private TournamentDetailsUIMap tournamentDetailsUIMap;

    public TournamentDetailsPage() {
        tournamentDetailsUIMap = new TournamentDetailsUIMap();
    }

    public Label getTournamentName() {
        return tournamentDetailsUIMap.getTournamentName();
    }

    public Label getTournamentDescription() {
        return tournamentDetailsUIMap.getTournamentDescription();
    }

    public Label getTournamentSeason() {
        return tournamentDetailsUIMap.getTournamentSeason();
    }

    public Label getTournamentScheme() {
        return tournamentDetailsUIMap.getTournamentScheme();
    }

    public Link getTournamentRegulationsLink() {
        return tournamentDetailsUIMap.getTournamentRegLink();
    }

    public Label getTrnmentApplyingPerStartDate() {
        return tournamentDetailsUIMap.getTournamentApplyPeriodStart();
    }

    public Label getTrnmntApplyingPerEndDate() {
        return tournamentDetailsUIMap.getTournamentApplyPeriodEnd();
    }

    public Label getTrmntStartDate() {
        return tournamentDetailsUIMap.getTournamentStart();
    }

    public Label getTrmntEndDate() {
        return tournamentDetailsUIMap.getTournamentEnd();
    }

    public Label getTrmntTransferStartDate() {
        return tournamentDetailsUIMap.getTournamentTransferStart();
    }

    public Label getTrmntTransferEndDate() {
        return tournamentDetailsUIMap.getTournamentTransferEnd();
    }


    public GameResultsPage clickGameResults() {
        tournamentDetailsUIMap.getGameResults().click();
        return new GameResultsPage();
    }

    public ManageTeamsPage clickManageTeams() {
        tournamentDetailsUIMap.getManageTeams().click();
        return new ManageTeamsPage();
    }

    public TournamentsSchedulePage clickTournamentSchedule() {
        tournamentDetailsUIMap.getSchedule().click();
        return new TournamentsSchedulePage();
    }

    public TournamentTablePage clickTournamentTable() {
        tournamentDetailsUIMap.getTournamentTable().click();
        return new TournamentTablePage();
    }

    public ApplyForTournamentPage clickApplyForTournament() {
        tournamentDetailsUIMap.getApplyForTournament().click();
        return new ApplyForTournamentPage();
    }

    public TournamentEditPage clickEditTournament() {
        tournamentDetailsUIMap.getEditTournament().click();
        return new TournamentEditPage();
    }

    public TournamentsPage clickBackToList() {
        tournamentDetailsUIMap.getBackToList().click();
        return new TournamentsPage();
    }

    public Tournament getTournament() {
        Tournament.TournamentBuilder builder = new Tournament().newBuilder();
        DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
        try {
            builder
                    .setName(getTournamentName().getText())
                    .setDescription(getTournamentDescription().getText())
                    .setSeason(getTournamentSeason().getText())
                    .setSchemeCode(Integer.parseInt(getTournamentScheme().getText()))
                    .setRegulationsLink(getTournamentRegulationsLink().getText())
                    .setApplyingPeriodStart(dateFormat
                            .parse(getTrnmentApplyingPerStartDate().getText()))
                    .setApplyingPeriodEnd(dateFormat.parse(getTrnmntApplyingPerEndDate().getText()))
                    .setTournamentStart(dateFormat.parse(getTrmntStartDate().getText()))
                    .setTournamentEnd(dateFormat.parse(getTrmntEndDate().getText()))
                    .setTransferPeriodStart(dateFormat.parse(getTrmntTransferStartDate().getText()))
                    .setTransferPeriodEnd(dateFormat.parse(getTrmntTransferEndDate().getText()));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return builder.build();
    }

    public MainPage goToMainPage() {
        return new MainPage();
    }
}
