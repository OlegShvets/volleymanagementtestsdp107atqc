package com.softserveinc.volleymanagementtests.pages.tournaments;

import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.uimap.TournamentTablePageUIMap;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;

/**
 * Created by Иван on 11-Mar-17.
 */
public class TournamentTablePage {
    private TournamentTablePageUIMap controls;

    public TournamentTablePage() {
        this.controls = new TournamentTablePageUIMap();
    }

    public TournamentDetailsPage clickbackToTheTournamentDetailsButton() {
        controls.getBackToTheTournamentDetailsButton().click();
        return new TournamentDetailsPage();
    }

    public  Label getTextFromLabelStandingsAreNotAvailable() {
        return controls.getLabelStandingsAreNotAvailable();
    }

    public MainPage goToMainPage() {
        return new MainPage();
    }
}
