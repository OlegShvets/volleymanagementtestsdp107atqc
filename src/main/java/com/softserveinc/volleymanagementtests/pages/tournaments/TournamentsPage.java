package com.softserveinc.volleymanagementtests.pages.tournaments;

import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.uimap.TournamentsPageUIMap;
import com.softserveinc.volleymanagementtests.pages.tournaments.uimap.scheduleduimaps.TrnmntsListRowUIMap;
import com.softserveinc.volleymanagementtests.testdata.tournament.Tournament;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import org.openqa.selenium.NoSuchElementException;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by Natalia on 05.03.2017.
 */
public final class TournamentsPage {
    private final TournamentsPageUIMap controls;

    public TournamentsPage() {
        controls = new TournamentsPageUIMap();
    }

    public final void getPlayersPage() {
        controls.getPlayerPage().click();
    }

    public final Link getTeamPage() {
        return controls.getTeamPage();
    }

    public CreatingNewTournamentPage clickCreateNewTournament() {
        controls.getCreateNewTournament().click();
        return new CreatingNewTournamentPage();
    }

    public boolean isTrmntExistsinCurrTrnments(final Tournament tournament) {
        return isTournamentExists(controls.getCurrentTourmntRows(), tournament);
    }


    public TournamentDetailsPage openTournamentDetailsforCurrTrmnt(final Tournament tournament) {
        findRow(controls.getCurrentTourmntRows(),
                tr -> tr.getTournamentDetailsLink()
                        .getText().equals(tournament.getName()))
                .getTournamentDetailsLink()
                .click();
        return new TournamentDetailsPage();
    }


    public TournamentDetailsPage openTournamentDetforCurrTrmntName(final String name) {
        findRow(controls.getCurrentTourmntRows(),
                tr -> tr.getTournamentDetailsLink()
                .getText().equals(name))
               .getTournamentDetailsLink()
               .click();
        return new TournamentDetailsPage();
    }


    public boolean isTrmntExistsInUpcomingTrnments(final Tournament tournament) {
        return isTournamentExists(controls.getUpcomingTourmntRows(), tournament);
    }


    public TournamentDetailsPage openTournamentDetailsforUpcomTrnmt(final Tournament tournament) {
        findRow(controls.getUpcomingTourmntRows(),
                tr -> tr.getTournamentDetailsLink()
                        .getText().equals(tournament.getName()))
                .getTournamentDetailsLink()
                .click();
        return new TournamentDetailsPage();
    }


    public TournamentDetailsPage openTournamentDetforUpcomTrnmtName(final String name) {
        findRow(controls.getUpcomingTourmntRows(),
                tr -> tr.getTournamentDetailsLink()
                .getText().equals(name))
                .getTournamentDetailsLink()
                .click();
        return new TournamentDetailsPage();
    }



    public TournamentsPage clickFinishedTournaments() {
        controls.getShowFinishedTrmntLink().click();
        return this;
    }

    public boolean isTrmntExistsInFinishedTrnments(final Tournament tournament) {
        controls.getShowFinishedTrmntLink().click();
        return isTournamentExists(controls.getFinishedTourmntRows(), tournament);
    }



    public TournamentDetailsPage openTournamentDetailsForFinishTrmnt(final Tournament tournament) {
        controls.getShowFinishedTrmntLink().click();
        findRow(controls.getFinishedTourmntRows(),
                (tr -> tr.getTournamentDetailsLink()
                        .getText().equals(tournament.getName())))
                .getTournamentDetailsLink()
                .click();
        return new TournamentDetailsPage();
    }


    public TournamentDetailsPage openTrnmntDetForFinishTrmntName(final String name) {
        controls.getShowFinishedTrmntLink().click();
        findRow(controls.getFinishedTourmntRows(),
                tr -> tr.getTournamentDetailsLink()
                .getText().equals(name))
                .getTournamentDetailsLink()
                .click();
        return new TournamentDetailsPage();
    }

    public TournamentDetailsPage openTournamentDetForTournamentByName(final String name) {
        List<TrnmntsListRowUIMap> allTournaments = new ArrayList<>();
        allTournaments.addAll(controls.getCurrentTourmntRows());
        allTournaments.addAll(controls.getUpcomingTourmntRows());
        controls.getShowFinishedTrmntLink().click();
        allTournaments.addAll(controls.getFinishedTourmntRows());
        findRow(allTournaments, tr -> tr.getTournamentDetailsLink()
                .getText().equals(name))
        .getTournamentDetailsLink()
        .click();
        return new TournamentDetailsPage();
    }


    private boolean isTournamentExists(final List<TrnmntsListRowUIMap> list,
                                       final Tournament tournament) {
        return list
                .parallelStream()
                .anyMatch(tr -> tr.getTournamentDetailsLink()
                        .getText()
                        .equals(tournament.getName()));
    }

    private TrnmntsListRowUIMap findRow(
           final List<TrnmntsListRowUIMap> list,
           final Predicate<TrnmntsListRowUIMap> predicate) {
        return list.parallelStream()
                .filter(predicate)
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("No such tournament"));
    }

    public boolean isTournamentExistsOnPage(final Tournament tournament) {
        if (isTrmntExistsinCurrTrnments(tournament)) {
            return true;
        }
        if (isTrmntExistsInUpcomingTrnments(tournament)) {
            return true;
        }
        clickFinishedTournaments();
        if (isTrmntExistsInFinishedTrnments(tournament)) {
            return true;
        }
        return false;
    }


    public MainPage goToMainPage() {
        return new MainPage();
    }


}
