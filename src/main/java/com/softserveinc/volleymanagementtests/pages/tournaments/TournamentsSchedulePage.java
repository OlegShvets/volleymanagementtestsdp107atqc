package com.softserveinc.volleymanagementtests.pages.tournaments;

import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.uimap.scheduleduimaps.RoundElementPageUIMap;
import com.softserveinc.volleymanagementtests.pages.tournaments.uimap.scheduleduimaps.TeamGameElementUIMap;
import com.softserveinc.volleymanagementtests.pages.tournaments.uimap.scheduleduimaps.TournamentsSchedulePageUIMap;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import org.openqa.selenium.NoSuchElementException;

import java.util.List;
import java.util.stream.Collectors;

public final class TournamentsSchedulePage {
    private final TournamentsSchedulePageUIMap control;

    public TournamentsSchedulePage() {
        this.control = new TournamentsSchedulePageUIMap();
    }

    public List<RoundElementPageUIMap> getRoundElements() {
        return control.getRoundElements();
    }


    public  TournamentDetailsPage openTournamentDetails() {
        control.getNameTournament().click();
        return new TournamentDetailsPage();
    }


    public boolean isRoundExists(String roundName) {
        try {
            findRound(roundName);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean isTeamExistsInRound(final Team team, final String roundName) {
        boolean isExists;
        RoundElementPageUIMap round = findRound(roundName);
        for (TeamGameElementUIMap teamEl : round.getTeamGameElements()) {
            isExists = teamEl.getHomeTeamLabel().getText().equals(team.getName());
            if (!isExists) {
                isExists = teamEl.getAwayTeamLabel().getText().equals(team.getName());
            }
            if (isExists) {
                return true;
            }
        }
        return false;
    }



    public EditResultPage editResForTeamsInRndName(final String roundName,
                                                   final Team homeTeam, Team awayTeam) {
       findGame(findRound(roundName), homeTeam, awayTeam)
        .getResultLink()
               .click();
        return new EditResultPage();
    }


    public ScheduleGamePage editForTeamsInRndName(final String roundName,
                                                  final Team homeTeam, final Team awayTeam) {
        findGame(findRound(roundName), homeTeam, awayTeam)
        .getEditLink()
                .click();
        return new ScheduleGamePage();
    }


    public ScheduleGamePage editForTeamsInRndAndGameNumber(int roundIndex, int gameIndex) {
        List<RoundElementPageUIMap> roundEl = control.getRoundElements();
        List<TeamGameElementUIMap> games = roundEl
                .get(roundIndex)
                .getTeamGameElements();
        games.get(gameIndex)
                .getEditLink()
                .click();
        return new ScheduleGamePage();
    }

    public ScheduleGamePage editForTeamsInRndNameGameNumb(final String roundName, int gameIndex) {
        findRound(roundName)
        .getTeamGameElements()
                .get(gameIndex)
                .getEditLink()
                .click();
        return new ScheduleGamePage();
    }


    public EditResultPage resultForTeamsInRndAndGameNumber(int roundIndex, int gameIndex) {
        List<RoundElementPageUIMap> roundEl = control.getRoundElements();
        List<TeamGameElementUIMap> games = roundEl
                .get(roundIndex)
                .getTeamGameElements();
        games.get(gameIndex).getResultLink().click();
        return new EditResultPage();
    }


    public EditResultPage resForTeamsInRndNameAndGameNumber(final String roundName, int gameIndex) {
         findRound(roundName)
        .getTeamGameElements()
                 .get(gameIndex)
                 .getResultLink()
                 .click();
        return new EditResultPage();
    }


    public List<Label> getRoundlabel() {
        return control.getRoundElements()
                .stream()
                .map(RoundElementPageUIMap::getRoundLabel)
                .collect(Collectors.toList());
    }

   public Label getHomeTeamSetsScoreInRoundGame(final String round, int gameIndex) {
       return findRound(round)
               .getTeamGameElements()
               .get(gameIndex)
               .getHomeTeamSetScore();
   }

    public Label getAwayTeamSetsScoreInRoundGame(final String round, int gameIndex) {
        return findRound(round)
                .getTeamGameElements()
                .get(gameIndex)
                .getAwayTeamSetScore();
    }

    public List<Label> getSetResultsInRoundGame(final String round, int gameIndex) {
      return findRound(round).
               getTeamGameElements()
               .get(gameIndex)
               .getSetsResults();
    }



    private RoundElementPageUIMap findRound(final String roundName) {
        return control.getRoundElements().stream()
                .filter(roundEl -> roundEl.getRoundLabel().getText().equals(roundName))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("No such round"));
    }

    private TeamGameElementUIMap findGame(
           final RoundElementPageUIMap round, final Team homeTeam, final Team awayTeam) {
        return round.getTeamGameElements()
                .stream()
                .filter(teamEl -> teamEl.getHomeTeamLabel().getText()
                        .equals(homeTeam.getName())
                        && teamEl.getAwayTeamLabel()
                        .getText().equals(awayTeam.getName()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("No such game"));
    }


    public String getFinalGameGroupNameByNumber(final int gameNumber) {
        return findRound("Final")
                .getFinalRoundNamesLabels()
                .get(gameNumber - 1)
                .getText();

    }

    public MainPage goToMainPage() {
        return new MainPage();
    }



}