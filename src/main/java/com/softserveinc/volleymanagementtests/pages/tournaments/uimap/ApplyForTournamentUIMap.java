package com.softserveinc.volleymanagementtests.pages.tournaments.uimap;

import com.softserveinc.volleymanagementtests.tools.ContextLongVisible;
import com.softserveinc.volleymanagementtests.tools.Select;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImplementDec;
import com.softserveinc.volleymanagementtests.tools.controls.CheckboxImplementDec;
import com.softserveinc.volleymanagementtests.tools.controls.DropdownImpl;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Checkbox;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Dropdown;
import org.openqa.selenium.By;

/**
 * Created by Иван on 22-Mar-17.
 */
public class ApplyForTournamentUIMap {

    public Dropdown getTeamSelectedDropdown() {
        return new DropdownImpl(new Select(new ContextLongVisible(By.cssSelector((
                "#TeamId")))));
    }

    public Checkbox getAgreeCheckbox() {
        return new CheckboxImplementDec(By.cssSelector("#agree"));
    }

    public Button getAcceptButton() {
        return new ButtonImplementDec(By.cssSelector("#apply"));
    }

}
