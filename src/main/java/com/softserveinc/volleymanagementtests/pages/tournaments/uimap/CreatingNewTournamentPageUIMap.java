package com.softserveinc.volleymanagementtests.pages.tournaments.uimap;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.ControlListWrapper;
import com.softserveinc.volleymanagementtests.tools.Select;
import com.softserveinc.volleymanagementtests.tools.controls.*;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.*;
import org.openqa.selenium.By;

import java.util.List;

/**
 * Created by Sasha on 11.03.2017.
 */
public class CreatingNewTournamentPageUIMap {


    private Label month;
    private Label year;
    


    public TextInput getTournamentNameInput() {
            return new TextInputImpl(
                    new Control(new ContextVisible(By.cssSelector
                    ("#Name"))));
    }
    
    public TextInput getDescriptionInput() {

            return new TextInputImpl(
                    new Control(new ContextVisible(By.cssSelector
                    ("#Description"))));
    }
    
    public Dropdown getSeasonCheckingDropdown() {
            return new DropdownImpl(new Select(
                    new ContextVisible(By.cssSelector(("#Season")))));
    }
    
    public Dropdown getSchemeCheckingDropDown() {
            return new DropdownImpl(new Select(
                    new ContextVisible(By.cssSelector("#Scheme"))));
    }
    
    public TextInput getTournamentRegulationsLinkInput() {
            return new TextInputImpl(new Control(
                    new ContextVisible(By.cssSelector
                    ("#RegulationsLink"))));
    }
    
    public Link getAddGroup() {
            return new LinkImpl(new Control(
                    new ContextVisible(By.cssSelector
                    ("#Add_Division_0_Group"))));

    }
    
    public Link getAddDivisionLink() {
            return new LinkImpl(new Control(
                    new ContextVisible(By.cssSelector
                    ("#Add_Division"))));
    }
    
    public Button getCreateTournamentButton() {
        return new ButtonImplementDec(By
                .cssSelector("#page-wrapper > div > div > div > form > fieldset > " +
                "p > input[type=\"submit\"]"));
    }
    
    public Link getBackToTournamentPageLink() {
            return new LinkImpl(new Control(new ContextVisible(By.cssSelector
                    ("#page-wrapper > div > div > div > div > a"))));
    }
    
    public Checkbox getTransferAllowCheckbox() {
        return new CheckboxImplementDec(By.cssSelector("#transferDatesSwitcher"));
    }
    
    public TextInput getApplyingPeriodStartsInput() {
            return new TextInputImpl(new Control
                    (new ContextVisible(By.cssSelector
                    ("#ApplyingPeriodStart"))));
    }
    
    public TextInput getApplyingPeriodEndsInput() {
            return new TextInputImpl(new Control(
                    new ContextVisible(By.cssSelector
                    ("#ApplyingPeriodEnd"))));
    }
    
    public TextInput getTournamentPeriodStartsInput() {
            return new TextInputImpl(new Control(
                    new ContextVisible(By.cssSelector
                    ("#GamesStart"))));
    }
    
    public TextInput getTournamentPeriodEndsInput() {
            return new TextInputImpl(new Control(new ContextVisible(By.cssSelector
                    ("#GamesEnd"))));
        }
    
    public TextInput getTransferPeriodStartsInput() {
            return new TextInputImpl(new Control(new ContextVisible(By.cssSelector
                    ("#TransferStart"))));
    }
    
    public TextInput getTransferPeriodEndsInput() {
            return new TextInputImpl(new Control(new ContextVisible(By.cssSelector
                    ("#TransferEnd"))));
    }
    
    public List<Button> getDates() {
        return new WebElementsList(new ControlListWrapper(new ContextVisible
                (By.xpath("//*[@id=\"ui-datepicker-div\"]/table//td"))))
                .asButtons();
    }
    
    public Link getForwardArrow() {
        return new LinkImpl(new Control(new ContextVisible(By.cssSelector
                ("#ui-datepicker-div > div > a.ui-datepicker-next.ui-corner-all" +
                        " > span"))));
    }
    
    public Link getBackArrow() {
        return new LinkImpl(new Control(new ContextVisible(By.cssSelector
                ("#ui-datepicker-div > div > a.ui-datepicker-prev.ui-corner-all > span"))));
    }
    
    public String getMonth() {
        month = new LabelImpl(new Control(new ContextVisible(By
                .cssSelector(("#ui-datepicker-div > div > div > span.ui-datepicker-month")))));
        return (month.getText());
    }
    
    public int getYear() {
        year = new LabelImpl(new Control(new ContextVisible(By
                .cssSelector(("#ui-datepicker-div > div > div > span.ui-datepicker-year")))));
        return Integer.parseInt(year.getText());
    }
    
}
