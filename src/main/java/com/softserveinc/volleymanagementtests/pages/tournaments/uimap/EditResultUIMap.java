package com.softserveinc.volleymanagementtests.pages.tournaments.uimap;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.controls.*;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.*;
import org.openqa.selenium.By;

public class EditResultUIMap {
    private Label pageTitle;
    private Label infoAboutGame;

    public Label getPageTitle() {
        return pageTitle = new LabelImpl(new Control(new ContextVisible(By.cssSelector("div > h2"))));
    }

    public Label getInfoAboutGame() {
        return infoAboutGame = new LabelImpl(new Control(new ContextVisible(By.cssSelector(("form > div")))));
    }

    public Checkbox getIsTechnicalDefeat() {
        return new CheckboxImplementDec(By.id("IsTechnicalDefeat"));
    }

    public Button getSaveButton() {
        return new ButtonImplementDec(By.cssSelector("input[value='Save']"));
    }

    public Link getCancelButton() {
        return new LinkImpl(new Control(new ContextVisible(By.id("backToSchedule"))));
    }

    public TextInput getSetsScoreHome() {
       return new TextInputImpl(new Control(new ContextVisible(By.id
                ("SetsScore_Home"))));
    }

    public TextInput getSetsScoreAway() {
        return new TextInputImpl(new Control(new ContextVisible(By.id
                ("SetsScore_Away"))));
    }

    public TextInput getSetsScoreOneHome() {
        return new TextInputImpl(new Control(new ContextVisible(By.id
                ("SetScores_0__Home"))));
    }

    public TextInput getSetsScoreOneAway() {
        return new TextInputImpl(new Control(new ContextVisible(By.id
                ("SetScores_0__Away"))));
    }

    public TextInput getSetsScoreTwoHome() {
        return new TextInputImpl(new Control(new ContextVisible(By.id
                ("SetScores_1__Home"))));
    }

    public TextInput getSetsScoreTwoAway() {
        return new TextInputImpl(new Control(new ContextVisible(By.id
                ("SetScores_1__Away"))));
    }

    public TextInput getSetsScoreThreeHome() {
        return new TextInputImpl(new Control(new ContextVisible(By.id
                ("SetScores_2__Home"))));
    }

    public TextInput getSetsScoreThreeAway() {
        return new TextInputImpl(new Control(new ContextVisible(By.id
                ("SetScores_2__Away"))));
    }

    public TextInput getSetsScoreFourHome() {
        return new TextInputImpl(new Control(new ContextVisible(By.id
                ("SetScores_3__Home"))));
    }

    public TextInput getSetsScoreFourAway() {
        return new TextInputImpl(new Control(new ContextVisible(By.id
                ("SetScores_3__Away"))));
    }

    public TextInput getSetsScoreFiveHome() {
        return new TextInputImpl(new Control(new ContextVisible(By.id
                ("SetScores_4__Home"))));
    }

    public TextInput getSetsScoreFiveAway() {
        return new TextInputImpl(new Control(new ContextVisible(By.id
                ("SetScores_4__Away"))));
    }
}
