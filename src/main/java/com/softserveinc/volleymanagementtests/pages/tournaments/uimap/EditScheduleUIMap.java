package com.softserveinc.volleymanagementtests.pages.tournaments.uimap;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.ControlListWrapper;
import com.softserveinc.volleymanagementtests.tools.controls.*;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;
import org.openqa.selenium.By;

import java.util.List;

public class EditScheduleUIMap {

    public Label getErrorText() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector("GameDate-error"))));
    }
    
    public Label getPageTitle() {
        return new LabelImpl(new Control(new ContextVisible(By.cssSelector("div > h2"))));
    }
    
    public Button getSaveButton() {
        return new ButtonImplementDec(By.cssSelector("input[value='Save']"));
    }

    public Link getCancelButton() {
        return new LinkImpl(new Control(new ContextVisible(By.linkText("Cancel"))));
    }
    
    public TextInput getDataField() {
        return new TextInputImpl(new Control(new ContextVisible(By.id("GameDate"))));
    }

    public List<Label> getRounds() {
        return new WebElementsList(new ControlListWrapper(new ContextVisible
                (By.cssSelector("#page-wrapper > div > div >div > div.col-md-3 > h4")))).asLabels();
    }
}
