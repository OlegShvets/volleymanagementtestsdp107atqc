package com.softserveinc.volleymanagementtests.pages.tournaments.uimap;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.ControlListWrapper;
import com.softserveinc.volleymanagementtests.tools.Select;
import com.softserveinc.volleymanagementtests.tools.controls.*;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.*;
import org.openqa.selenium.By;

import java.util.List;

/**
 * Created by Sasha on 13.03.2017.
 */
public class ScheduleGamePageUIMap {

    public Dropdown getRoundDropDown(){
           return new DropdownImpl(new Select(
                   new ContextVisible(By.cssSelector("#Round"))));
    }

    public Dropdown getHomeTeamDropDown(){

            return new DropdownImpl(new Select(
                    new ContextVisible(By.cssSelector("#HomeTeamId"))));
    }

    public Dropdown getAwayTeamDropDown(){
            return new DropdownImpl(new Select(
                    new ContextVisible(By.cssSelector("#AwayTeamId"))));

    }

    public TextInput getDateAndTimeTextInput(){
            return new TextInputImpl(new Control
                    (new ContextVisible(By.cssSelector("#GameDate"))));

    }

    public Button getSaveButton() {
        return new ButtonImplementDec(By
                .cssSelector("#page-wrapper > div > div > div > form > input.btn.btn-default"));
    }

    public Button getCreateButton() {
        return new ButtonImplementDec(By.cssSelector("#create"));
    }

    public Button getCreateOneMoreButton() {
        return new ButtonImplementDec(By.cssSelector("#createOneMore"));
    }

    public Link getCancelLink(){
            return new LinkImpl(new Control(
                    new ContextVisible(By.cssSelector
                    ("#page-wrapper > div > div > div > a"))));

    }

    public List<Label> allLabels(){

        return new WebElementsList(new ControlListWrapper(new ContextVisible
                (By.cssSelector(".editor-label")))).asLabels();
    }

    public List<CustomElement> allControls(){
        return new WebElementsList(new ControlListWrapper(new ContextVisible
                (By.cssSelector(".editor-label")))).asCustomElements();

    }

    

    public Label getDateAndTimeLabel(){
            return new LabelImpl(new Control(
                    new ContextVisible(By
                            .cssSelector(("#page-wrapper > div > div > div > form > " +
                    "div.editor-label > label")))));
    }


}
