package com.softserveinc.volleymanagementtests.pages.tournaments.uimap;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.ControlListWrapper;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LinkImpl;
import com.softserveinc.volleymanagementtests.tools.controls.WebElementsList;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import org.openqa.selenium.By;

import java.util.List;

/**
 * Created by stas on 09.03.2017.
 */
public final class TournamentDetailsUIMap {

    private Label tournamentDescription;



    public Label getTournamentDetails() {

            return new LabelImpl(new Control(
                    new ContextVisible(By.cssSelector((".col-lg-12 > h2")))));
    }

    public Label getTournamentName() {
            return new LabelImpl(new Control(
                    new ContextVisible(By.cssSelector(("fieldset > div:nth-child(3)")))));
    }

    public Label getTournamentDescription() {

               return new LabelImpl(new Control(
                       new ContextVisible(By.cssSelector(("fieldset > div:nth-child(5)")))));

    }

    public Label getTournamentSeason() {
         return new LabelImpl(new Control(new ContextVisible(
                 By.cssSelector(("fieldset > div:nth-child(7) > label")))));
    }

    public Label getTournamentScheme() {
           return new LabelImpl(new Control(
                   new ContextVisible(By.cssSelector(("fieldset > div:nth-child(9)")))));
    }

    public Link getTournamentRegLink() {
               return new LinkImpl(new Control(
                       new ContextVisible(By
                               .cssSelector("fieldset > div:nth-child(11) > a"))));

    }

    public Label getTournamentApplyPeriodStart() {
            return new LabelImpl(new Control(
                    new ContextVisible(By.cssSelector(("fieldset > div:nth-child(13)")))));
    }

    public Label getTournamentApplyPeriodEnd() {
            return new LabelImpl(new Control(
                    new ContextVisible(By
                            .cssSelector(("fieldset > div:nth-child(15)")))));
    }

    public Label getTournamentStart() {
            return new LabelImpl(new Control(
                    new ContextVisible(By
                            .cssSelector(("fieldset > div:nth-child(17)")))));

    }

    public Label getTournamentEnd() {
            return new LabelImpl(
                    new Control(new ContextVisible(By
                            .cssSelector(("fieldset > div:nth-child(19)")))));
    }

    public Label getTournamentTransferStart() {
                return new LabelImpl(new Control(
                        new ContextVisible(By
                                .cssSelector(("fieldset > div:nth-child(21)")))));
    }

    public Label getTournamentTransferEnd() {

               return new LabelImpl(new Control(
                       new ContextVisible(By
                               .cssSelector(("fieldset > div:nth-child(23)")))));

    }

    public List<Label> getDivisions() {
           return new WebElementsList(new ControlListWrapper(new ContextVisible(By
                   .xpath(".//label[contains(@for,'Divisions_')]/../preceding-sibling::*[1]"))))
                    .asLabels();
    }



    public Link getGameResults() {
            return new LinkImpl(new Control(
                    new ContextVisible(By
                            .cssSelector("a[href*='GameResults']"))));
    }

    public Link getTournamentTable() {
            return new LinkImpl(new Control(
                    new ContextVisible(By.cssSelector("a[href*='GameReports']"))));
    }

    public Link getManageTeams() {
            return new LinkImpl(new Control(
                    new ContextVisible(By
                            .cssSelector("a[href*='ManageTournamentTeams']"))));
    }

    public Link getSchedule() {
            return new LinkImpl(new Control(
                    new ContextVisible(By.cssSelector("a[href*='Schedule']"))));
    }

    public Link getApplyForTournament() {
            return  new LinkImpl(new Control(
                    new ContextVisible(By
                            .cssSelector("a[href*='ApplyForTournament']"))));
    }

    public Link getEditTournament() {
            return new LinkImpl(new Control(
                    new ContextVisible(By.cssSelector("a[href*='Edit']"))));
    }

    public Link getBackToList() {
            return new LinkImpl(
                    new Control(new ContextVisible(By
                            .cssSelector("p > a[href='/']"))));

    }
}
