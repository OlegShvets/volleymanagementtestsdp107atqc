package com.softserveinc.volleymanagementtests.pages.tournaments.uimap;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImpl;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImplementDec;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import org.openqa.selenium.By;

/**
 * Created by Иван on 11-Mar-17.
 */
public class TournamentTablePageUIMap {
    private Button backToTheTournamentDetailsButton;
    private Label standingsAreNotAvailable;

    public Label getLabelStandingsAreNotAvailable() {

        return new LabelImpl(new Control(new ContextVisible(By.xpath("//*[@id=\"page-wrapper\"]/div/div/div/h4"))));
    }

    public Button getBackToTheTournamentDetailsButton() {
        return new ButtonImplementDec(By.cssSelector("p > a"));
    }
}
