package com.softserveinc.volleymanagementtests.pages.tournaments.uimap;

import com.softserveinc.volleymanagementtests.pages.tournaments.uimap.scheduleduimaps.TrnmntsListRowUIMap;
import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.ControlListWrapper;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LinkImpl;
import com.softserveinc.volleymanagementtests.tools.controls.WebElementsList;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import org.openqa.selenium.By;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Stas on 05.03.2017.
 */
public final class TournamentsPageUIMap {

    public Label getTournamentsLabel() {
            return new LabelImpl(new Control(
                    new ContextVisible(By.cssSelector((".col-lg-12  > h2")))));
    }
    
    
    public Link getCreateNewTournament() {
            return new LinkImpl(new Control(
                    new ContextVisible(
                            By.cssSelector(".col-lg-12 > p:nth-child(2)  > a"))));
    }
    
    public List<TrnmntsListRowUIMap> getCurrentTourmntRows() {
            return getTournamentsRows("h3:nth-of-type(1) + table");

    }
    
    public List<TrnmntsListRowUIMap> getUpcomingTourmntRows() {
            return getTournamentsRows("h3:nth-of-type(2) + table");
    }
    
    public Link getShowFinishedTrmntLink() {
            return new LinkImpl(new Control(
                    new ContextVisible(By.cssSelector(".col-lg-12 > p a"))));
    }
    
    public List<TrnmntsListRowUIMap> getFinishedTourmntRows() {
            return getTournamentsRows("#finished_table");
    }
    

    private List<TrnmntsListRowUIMap> getTournamentsRows(String parentCss) {
        ArrayList<TrnmntsListRowUIMap> tournaments = new ArrayList<>();
        List<Label> rows = new WebElementsList(new ControlListWrapper(new ContextVisible(By
                .cssSelector(String.format("%s > tbody > tr", parentCss))))).asLabels();
        for (int i = 1; i <= rows.size(); i++) {
            tournaments.add(new TrnmntsListRowUIMap(
                    String.format("%s > tbody > tr:nth-of-type(%d)", parentCss, i)));
        }
        return tournaments;
    }
    
    public final Link getPlayerPage() {
        return new LinkImpl(new Control(
                new ContextVisible(
                        By.cssSelector("#side-menu > li:nth-child(2) > a"))));
    }
    
    public final Link getTeamPage() {
        return new LinkImpl(new Control(
                new ContextVisible(
                        By.cssSelector("#side-menu > li:nth-child(3) > a"))));
    }

}
