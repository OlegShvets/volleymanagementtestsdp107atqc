package com.softserveinc.volleymanagementtests.pages.tournaments.uimap.scheduleduimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.ControlListWrapper;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.WebElementsList;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by skir on 3/20/2017.
 */
public final class RoundElementPageUIMap {

    private String parentCss;

    public RoundElementPageUIMap(String parentCss) {
        this.parentCss = parentCss;
    }


    public List<TeamGameElementUIMap> getTeamGameElements() {
      List<TeamGameElementUIMap>  teamGameElements = new ArrayList<>();
        int teamGamesSize = new WebElementsList(new ControlListWrapper(new ContextVisible(By
                .cssSelector(String.format("%s + .table-bordered > .table-bordered", parentCss)))))
                .asLabels().size();
        for (int i = 1; i <= teamGamesSize; i++) {
            teamGameElements
                    .add(new TeamGameElementUIMap(String
                            .format("%s +.table-bordered >.table-bordered:nth-of-type(%d)",
                                    parentCss, i)));
        }
        return teamGameElements;
    }

    public Label getRoundLabel() {
            return new LabelImpl(new Control(
                    new ContextVisible(
                            By.cssSelector(parentCss))));
    }

    public List<Label> getFinalRoundNamesLabels() {
            return new WebElementsList(new ControlListWrapper(new ContextVisible(By
                    .cssSelector(String.format("%s +.table-bordered h5", parentCss)))))
                    .asLabels();

    }




}
