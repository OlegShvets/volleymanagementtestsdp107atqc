package com.softserveinc.volleymanagementtests.pages.tournaments.uimap.scheduleduimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.ControlListWrapper;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LinkImpl;
import com.softserveinc.volleymanagementtests.tools.controls.WebElementsList;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import org.openqa.selenium.By;

import java.util.List;

/**
 * Created by skir on 3/20/2017.
 */
public class TeamGameElementUIMap {

    private String parentCss;

    public TeamGameElementUIMap(String parentCss) {
        this.parentCss = parentCss;
    }

    public Label getHomeTeamLabel() {
            return new LabelImpl(new Control(
                    new ContextVisible(By.cssSelector(
                    String.format("%s > .row:nth-of-type(1) >.col-md-10 strong",
                            parentCss)))));
    }

    public Label getAwayTeamLabel() {
            return new LabelImpl(new Control(
                    new ContextVisible(By.cssSelector(String.format("%s > " +
                            " .row:nth-of-type(2) >.col-md-10 strong",
                    parentCss)))));
    }

    public Label getHomeTeamSetScore() {
            return new LabelImpl(new Control(
                    new ContextVisible(By.cssSelector(String.format("%s > " +
                            " .row:nth-of-type(1) >.col-md-2 strong",
                    parentCss)))));
    }

    public Label getAwayTeamSetScore() {
            return new LabelImpl(new Control(
                    new ContextVisible(By.cssSelector(String.format("%s > " +
                            " .row:nth-of-type(2) >.col-md-2 strong",
                    parentCss)))));
    }

    public List<Label> getSetsResults() {
        return new WebElementsList(new ControlListWrapper(new ContextVisible
                (By.cssSelector(String.format("%s > .row:nth-of-type(2) >.col-md-2",
                        parentCss))))).asLabels();
    }

    public Label getSetOneScore() {
            return new LabelImpl(new Control(
                    new ContextVisible(By.cssSelector(String.format("%s > " +
                            " .row:nth-of-type(3) > .col-md-2:nth-of-type(1)",
                    parentCss)))));
    }


    public Label getSetTwoScore() {
            return new LabelImpl(new Control(
                    new ContextVisible(By.cssSelector(String.format("%s > " +
                            " .row:nth-of-type(3) > .col-md-2:nth-of-type(2)",
                    parentCss)))));
    }

    public Label getSetThreeScore() {
            return new LabelImpl(new Control(
                    new ContextVisible(By.cssSelector(String.format("%s > " +
                            " .row:nth-of-type(3) > .col-md-2:nth-of-type(3)",
                    parentCss)))));
    }

    public Label getSetFourScore() {
            return new LabelImpl(new Control
                    (new ContextVisible(By.cssSelector(String.format("%s > " +
                            " .row:nth-of-type(3) > .col-md-2:nth-of-type(4)",
                    parentCss)))));

    }

    public Label getSetFiveScore() {
            return new LabelImpl(new Control(
                    new ContextVisible(By.cssSelector(String.format("%s > " +
                            " .row:nth-of-type(3) > .col-md-2:nth-of-type(5)",
                    parentCss)))));

    }

    public Label getDataOfFirstGame() {
            return new LabelImpl(new Control(
                    new ContextVisible(By.cssSelector(String.format("%s:nth-of-type(1) " +
                            ".col-md-10.text-right",
                    parentCss)))));
    }

    public Link getResultLink() {
            return new LinkImpl(new Control(
                    new ContextVisible(By.cssSelector(String.format("%s a[href*='Results']",
                    parentCss)))));
    }

    public Link getEditLink() {
        return new LinkImpl(new Control(
                new ContextVisible(By.cssSelector(String.format("%s a[href*='Schedule']",
                    parentCss)))));
        }
}
