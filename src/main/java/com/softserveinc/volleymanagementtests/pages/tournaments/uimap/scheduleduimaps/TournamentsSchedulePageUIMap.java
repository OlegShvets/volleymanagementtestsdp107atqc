package com.softserveinc.volleymanagementtests.pages.tournaments.uimap.scheduleduimaps;

import com.softserveinc.volleymanagementtests.pages.tournaments.uimap.scheduleduimaps.RoundElementPageUIMap;
import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.ControlListWrapper;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LinkImpl;
import com.softserveinc.volleymanagementtests.tools.controls.WebElementsList;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

public final class TournamentsSchedulePageUIMap {

    public List<RoundElementPageUIMap> getRoundElements() {
        int roundAmount = new WebElementsList(new ControlListWrapper(new ContextVisible(By
                .cssSelector(".col-md-3 h4")))).asLinks().size();
       List<RoundElementPageUIMap> roundElements = new ArrayList<>();
        for (int i = 1; i <= roundAmount; i++) {
            roundElements.add(new RoundElementPageUIMap(
                    String.format(".col-md-3 h4:nth-of-type(%d)", i)));
        }
        return roundElements;
    }

    public Label getScheduleFor() {
        return new LabelImpl(new Control(
                new ContextVisible(By.cssSelector("div > h2"))));
    }

    public Link getNameTournament() {
        return new LinkImpl(new Control(
                new ContextVisible(By.cssSelector("div > h2 > a"))));
    }

    public List<Label> getNameOfStageTournament() {
        return new WebElementsList(new ControlListWrapper(new ContextVisible(By
                .cssSelector("div > h4")))).asLabels();
    }

}
