package com.softserveinc.volleymanagementtests.pages.tournaments.uimap.scheduleduimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LinkImpl;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import org.openqa.selenium.By;

/**
 * Created by skir on 3/22/2017.
 */
public final class TrnmntsListRowUIMap {

    private String parentCss;

    public TrnmntsListRowUIMap(String parentCss) {
     this.parentCss = parentCss;
    }
    public Link getTournamentDetailsLink() {

        return new LinkImpl(new Control(
                new ContextVisible(By
                        .cssSelector(String.format("%s a", parentCss)))));
    }


    public Label getTournamentSeasonLabel() {
        return new LabelImpl(
                new Control(new ContextVisible(By
                        .cssSelector(String.format("%s label", parentCss)))));
    }
}
