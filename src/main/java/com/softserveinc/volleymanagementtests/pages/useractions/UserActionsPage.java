package com.softserveinc.volleymanagementtests.pages.useractions;

import com.softserveinc.volleymanagementtests.pages.login.GoogleLoginPage;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.profile.UserProfileDetailsPage;
import com.softserveinc.volleymanagementtests.pages.useractions.uimaps.UserActionUiMap;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;

/**
 * Created by stas on 07.03.2017.
 */
public class UserActionsPage {

    private UserActionUiMap userActionUiMap;

    public UserActionsPage() {
        userActionUiMap = new UserActionUiMap();
    }

    public GoogleLoginPage clickLogin() {
        userActionUiMap.getLoginLink().click();
        return new GoogleLoginPage();
    }

    public Link geyLoginLink() {
        return userActionUiMap.getLoginLink();
    }

    public UserProfileDetailsPage clickUserProfile() {
        userActionUiMap.getUserProfileLink().click();
        return new UserProfileDetailsPage();
    }

    public Link getUserProfileLink() {
        return userActionUiMap.getUserProfileLink();
    }

    public void clickSettings() {
        userActionUiMap.getSettingsLink().click();
    }

    public Link getSettingsLink() {
        return  userActionUiMap.getSettingsLink();
    }

    public MainPage clickLogout() {
        userActionUiMap.getLogoutLink().click();
        return new MainPage();
    }

    public Link getLogoutLink() {
        return userActionUiMap.getLogoutLink();
    }
}
