package com.softserveinc.volleymanagementtests.pages.useractions.uimaps;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.controls.LinkImpl;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import org.openqa.selenium.By;

/**
 * Created by stas on 07.03.2017.
 */
public class UserActionUiMap {
    private Link loginLink;
    private Link userProfileLink;
    private Link settingsLink;
    private Link logoutLink;

    public Link getLoginLink() {
        if (loginLink == null) {
            loginLink = new LinkImpl(new Control(new ContextVisible(By.linkText("Login"))));


        }
        return loginLink;
    }


    public Link getUserProfileLink() {
        if (userProfileLink == null) {
            userProfileLink = new LinkImpl(new Control(new ContextVisible(By.cssSelector("li.dropdown.open > ul > " +
                    "li:nth-child(1) > a"))));

        }
        return userProfileLink;
    }

    public Link getSettingsLink() {
        if (settingsLink == null) {
            settingsLink = new LinkImpl(new Control(new ContextVisible(By.cssSelector("li.dropdown.open > ul > " +
                    "li:nth-child(2) > a"))));
        }
        return settingsLink;
    }

    public Link getLogoutLink() {
        if (logoutLink == null) {
            logoutLink = new LinkImpl(new Control(new ContextVisible(By.cssSelector("li.dropdown.open > ul > " +
                    "li:nth-child(4) > a"))));
        }
        return logoutLink;
    }
}
