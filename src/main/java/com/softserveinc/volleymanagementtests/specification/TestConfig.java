package com.softserveinc.volleymanagementtests.specification;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * the class contains getters of the parameters out of the .properties file
 * which are used to specify the potentially various tools and the settings
 * to run the framework and system under test.
 */
public final class TestConfig {

    public String getSystemUnderTestBaseUrl() {
        String relativeFileLocation = "config.properties";
        Properties property = new Properties();

        try (FileInputStream fileInputStream = new FileInputStream(relativeFileLocation)) {
            property.load(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String systemUnderTestBaseUrl =
                property.getProperty("systemUnderTestBaseUrl");

        return systemUnderTestBaseUrl;

    }

    public String getCurrentAuthorizedUser() {
        String relativeFileLocation = "config.properties";
        Properties property = new Properties();

        try (FileInputStream fileInputStream = new FileInputStream(relativeFileLocation)) {
            property.load(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String currentUser =
                property.getProperty("current.user");

        return currentUser;

    }

    public String getJDBCDriver() {
        String relativeFileLocation = "config.properties";
        Properties property = new Properties();

        try (FileInputStream fileInputStream = new FileInputStream(relativeFileLocation)) {
            property.load(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String jdbcDriver =
                property.getProperty("jdbcDriver");

        return jdbcDriver;
    }

    public String getGoogleUser() {
        String relativeFileLocation = "config.properties";
        Properties property = new Properties();

        try (FileInputStream fileInputStream = new FileInputStream(relativeFileLocation)) {
            property.load(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String user =
                property.getProperty("googleAccount");

        return user;
    }

    public String getGooglePass() {
        String relativeFileLocation = "config.properties";
        Properties property = new Properties();

        try (FileInputStream fileInputStream = new FileInputStream(relativeFileLocation)) {
            property.load(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String password =
                property.getProperty("googleAccountPassword");

        return password;
    }


    public String getDBConnectionString() {

        String relativeFileLocation = "config.properties";
        Properties property = new Properties();

        try (FileInputStream fileInputStream = new FileInputStream(relativeFileLocation)) {
            property.load(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String dbConnectionString = String.format(
                "jdbc:jtds:sqlserver://%s/%s;instance=%s;user=%s;password=%s;" +
                        "namedPipe=%s",
                property.getProperty("db.host"),
                property.getProperty("db.name"),
                property.getProperty("db.instance"),
                property.getProperty("db.user"),
                property.getProperty("db.password"),
                property.getProperty("db.namedPipe")
        );

        return dbConnectionString;
    }

    public String getLocalPort() {
        String relativeFileLocation = "config.properties";
        Properties property = new Properties();

        try (FileInputStream fileInputStream = new FileInputStream(relativeFileLocation)) {
            property.load(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String localPort =
                property.getProperty("local.port");

        return localPort;
    }

    public String getGoogleUserMail() {
        String relativeFileLocation = "config.properties";
        Properties property = new Properties();

        try (FileInputStream fileInputStream = new FileInputStream(relativeFileLocation)) {
            property.load(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String user =
                property.getProperty("googleUserAccount");

        return user;
    }

    public String getGoogleuserPass() {
        String relativeFileLocation = "config.properties";
        Properties property = new Properties();

        try (FileInputStream fileInputStream = new FileInputStream(relativeFileLocation)) {
            property.load(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String password =
                property.getProperty("googleUserAccountPassword");

        return password;
    }
}
