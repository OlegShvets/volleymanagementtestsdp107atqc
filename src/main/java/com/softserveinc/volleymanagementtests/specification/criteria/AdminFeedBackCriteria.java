package com.softserveinc.volleymanagementtests.specification.criteria;

import com.softserveinc.volleymanagementtests.pages.admin.requestpage.AdminFeedBackPage;
import com.softserveinc.volleymanagementtests.specification.Specifiable;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;

public class AdminFeedBackCriteria implements Specifiable{
    private Specification specification;
    private AdminFeedBackPage adminFeedBackPage;

    public AdminFeedBackCriteria(AdminFeedBackPage adminFeedBackPage, Specification specification) {
        this.specification = specification;
        this.adminFeedBackPage = adminFeedBackPage;
    }

    public AdminFeedBackCriteria matchURL(String pageURL) {
        specification.add(new WebDriverUtils().getCurrentUrl().equals(pageURL),
                String.format("The Admin request page isn't on expected page"
                                + ":%n expected [%s]%n found [%s]", pageURL,
                        new WebDriverUtils().getCurrentUrl()));
        return this;
    }

    @Override
    public Specification next() {
        return this.specification;
    }
}
