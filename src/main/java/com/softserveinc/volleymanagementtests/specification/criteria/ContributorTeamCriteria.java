package com.softserveinc.volleymanagementtests.specification.criteria;

import com.softserveinc.volleymanagementtests.specification.Specifiable;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.contributor.ContributorTeam;

import java.util.List;

public class ContributorTeamCriteria implements Specifiable {
    private ContributorTeam contributorTeam;
    private Specification specification;

    public ContributorTeamCriteria(
            final ContributorTeam contributorTeam,
            final Specification specification) {
        if (contributorTeam == null) {
            throw new IllegalArgumentException("Incoming contributor team can't be null");
        }

        this.contributorTeam = contributorTeam;
        this.specification = specification;
    }

    public ContributorTeamCriteria containedIn(List<ContributorTeam> teamList) {
        specification.add(teamList.contains(contributorTeam),
                String.format("%s:%n searched for: [%s]%n list to search in:%s;",
                        "The contributors team not found in the list", contributorTeam, teamList));

        return this;
    }


    @Override
    public Specification next() {
        return specification;
    }
}
