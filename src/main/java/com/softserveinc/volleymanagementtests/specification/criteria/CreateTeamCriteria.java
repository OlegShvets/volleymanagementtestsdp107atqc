package com.softserveinc.volleymanagementtests.specification.criteria;

import com.softserveinc.volleymanagementtests.pages.team.TeamCreatePage;
import com.softserveinc.volleymanagementtests.specification.Specifiable;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import org.openqa.selenium.NotFoundException;

import java.util.List;
import java.util.concurrent.TimeoutException;

/**
 * Author S.Tsiganovskiy
 */
public class CreateTeamCriteria implements Specifiable {
    private TeamCreatePage teamCreatePage;
    private Specification specification;

    public CreateTeamCriteria(TeamCreatePage teamCreatePage, Specification specification) {
        if (teamCreatePage == null) {
            throw new NotFoundException("the income TeamCreatePage == null");
        }
        this.teamCreatePage = teamCreatePage;
        this.specification = specification;
    }

    public CreateTeamCriteria isAllFieldsFilledInCorrectly(Team team) {
        boolean allFieldsFilledInCorrectly = true;
        if (team.getCoach() != null && team.getAchievements() != null) {
            if (!teamCreatePage.getNameInput().getText().equals(team.getName())
                    && teamCreatePage.getCoachInput().getText().equals(team.getCoach())
                    && teamCreatePage.getAchievementsInput().getText().equals(team.getAchievements())
                    && teamCreatePage.getCaptainInput().getText().equals(team.getCaptain().getLastName() + " "
                    + team.getCaptain().getFirstName())
                    && isPlayersListEquals(team)) {
                allFieldsFilledInCorrectly = false;
            }
        }

        specification.add(allFieldsFilledInCorrectly, String.format("The team's fields aren't filled in correctly"));
        return this;
    }

    public CreateTeamCriteria areThereNoErrorMessages(Team team) {
        boolean noErrorMessages = true;
        if ((!teamCreatePage.getErrorMessageNameInput().isDisplayed())
                && (!teamCreatePage.getCoachErrorInputLabel().isDisplayed())
                && (!teamCreatePage.getErrorMessageLabelForAchievements().isDisplayed())
                && (!teamCreatePage.getErrorMessageForCaptain().isDisplayed())
                && (!teamCreatePage.getErrorMessageForPlayersLabel().isDisplayed())) {
            noErrorMessages = false;
        }
        specification.add(noErrorMessages, String.format("The team's fields are filled correctly." +
                " But we got error messages."));
        return this;
    }

    private boolean isPlayersListEquals(Team team) {
        List<Label> playersListUI = teamCreatePage.getPlayersList();
        boolean playerList = true;
        if (team.getRoster() != null) {
            List<Player> playerListTestData = team.getRoster();
            for (int i = 0; i < playerListTestData.size(); i++) {
                for (int j = 0; j < playersListUI.size(); j++) {
                    if (!(playerListTestData.get(i).getLastName() + " " + playerListTestData.get(i).getFirstName())
                            .equals(playersListUI.get(j).getText())) {
                        playerList = false;
                    }
                }
            }
        }
        return playerList;
    }

    @Override
    public Specification next() {
        return this.specification;
    }
}
