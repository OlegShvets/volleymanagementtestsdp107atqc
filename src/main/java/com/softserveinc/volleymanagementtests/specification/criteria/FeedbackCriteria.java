package com.softserveinc.volleymanagementtests.specification.criteria;

import com.softserveinc.volleymanagementtests.dal.models.FeedbackDal;
import com.softserveinc.volleymanagementtests.specification.Specification;

/**
 * Created by stas on 17.03.2017.
 */
public class FeedbackCriteria {
    private FeedbackDal feedbackDal;
    private Specification specification;

    public FeedbackCriteria(final FeedbackDal feedbackDal, final Specification specification) {
        if (feedbackDal == null) {
            throw new NullPointerException("Feedback is null");
        }
        this.feedbackDal = feedbackDal;
        this.specification = specification;
    }

    public FeedbackCriteria equalToAnotherFeedback(FeedbackDal feedbackDal) {
        specification.add(this.feedbackDal.equals(feedbackDal),
                "Feedbacks are not equal");
        return this;
    }
}
