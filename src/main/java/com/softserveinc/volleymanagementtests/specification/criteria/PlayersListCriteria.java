package com.softserveinc.volleymanagementtests.specification.criteria;

import com.softserveinc.volleymanagementtests.dal.models.PlayerDal;
import com.softserveinc.volleymanagementtests.pages.player.PlayerDetailsPage;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.specification.Specifiable;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.DalToPlayer;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NotFoundException;

import java.util.List;

/**
 * Karabaza Anton on 09.03.2016.
 */
public class PlayersListCriteria implements Specifiable {
    private PlayersListPage playersListPage;
    private Specification specification;

    public PlayersListCriteria(PlayersListPage playersListPage, Specification specification) {
        if (playersListPage == null) {
            throw new NotFoundException("the income PlayersListPage == null");
        }
        this.playersListPage = playersListPage;
        this.specification = specification;
    }

    public PlayersListCriteria isPlayerDisplayedOnList(PlayerDal player) {
        boolean isPlayerDisplayed;

        do {
            isPlayerDisplayed = playersListPage.isPlayerExistOnCurrentPage(
                   new DalToPlayer().map(player));
            if (isPlayerDisplayed) {
                playersListPage.goToPage(1);
                break;
            }
        }
        while (playersListPage.goNextPage());

        specification.add(isPlayerDisplayed, String.format("The player %s %s isn't displayed on"
                        + "PlayersListPage:%n expected [%s]%n found [%s]", player.getLastName(),
                player.getFirstName(), "true", "false"));
        return this;
    }

    public PlayersListCriteria isPlayerDisplayedOnListAfterSearhMatch(String playerName) {
        String actualPlayerName;
        boolean result = true;
        actualPlayerName = playersListPage.getPlayersLinkAfterSearch().getText();
        result = (actualPlayerName.equals(playerName));
        specification.add(result, "The Player isn't exist");
        return this;
    }

    public PlayersListCriteria isPlayerDisplayedOnListWithCorrectDetails(PlayerDal player) {
        boolean isPlayerDisplayed = false;
        List<String> players;
        PlayerDetailsPage detailsPage;

        do {
            players = playersListPage.getPlayersOnCurrentPage();

            for (int i = 0; i < playersListPage.getCountOfPlayersOnCurrPage(); i++) {
                if (players.get(i).equals(player.getLastName() + " " + player.getFirstName())) {
                    detailsPage = playersListPage.showPlayerDetailsByPosition(i);
                    Player readPlayer = detailsPage.readPlayer();

                    playersListPage = detailsPage.clickBackLink();

                    if (readPlayer.equals(new DalToPlayer().map(player))) {
                        isPlayerDisplayed = true;
                        playersListPage.goToPage(1);
                        break;
                    }
                }
            }
        }
        while (!isPlayerDisplayed && playersListPage.goNextPage());

        specification.add(isPlayerDisplayed, String.format("The player %s %s isn't displayed "
                        + "with correct details:%n expected [%s]%n found [%s]", player.getLastName(),
                player.getFirstName(), "true", "false"));
        return this;
    }

    public PlayersListCriteria matchPageIndex(String pageIndex) {
        specification.add(pageIndex.equals(playersListPage.getCurrentPageIndex()),
                String.format("The PlayersListPage isn't on expected page"
                                + ":%n expected [%s]%n found [%s]", pageIndex,
                        playersListPage.getCurrentPageIndex()));
        return this;
    }

    public PlayersListCriteria matchURL(String pageURL) {
        specification.add(new WebDriverUtils().getCurrentUrl().equals(pageURL),
                String.format("The PlayersListPage isn't on expected page"
                                + ":%n expected [%s]%n found [%s]", pageURL,
                        new WebDriverUtils().getCurrentUrl()));
        return this;
    }

    public final PlayersListCriteria ifUnexpectedAlertPresent() {
        try {
            playersListPage.getAlert();
            specification.add(false, "There is an unexpected alert present on the page.");
        } catch (NoAlertPresentException e) {
            return this;
        }
        return this;
    }

    public final PlayersListCriteria isCountOfPlayersMatches(int count) {
        boolean isCntMatched = (count == playersListPage.getCountOfPlayersOnCurrPage());
        specification.add(isCntMatched,
                String.format("Players count is not matched, expected [%s], actual [%s]",
                        String.valueOf(count),
                        String.valueOf(playersListPage.getCountOfPlayersOnCurrPage())));

        return this;
    }

    @Override
    public Specification next() {
        return this.specification;
    }
}
