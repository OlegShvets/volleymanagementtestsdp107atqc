package com.softserveinc.volleymanagementtests.specification.criteria;

import com.softserveinc.volleymanagementtests.pages.tournaments.ScheduleGamePage;
import com.softserveinc.volleymanagementtests.specification.Specifiable;
import com.softserveinc.volleymanagementtests.specification.Specification;
import org.openqa.selenium.NotFoundException;

import java.util.List;

/**
 * Created by Sasha on 14.03.2017.
 */
public final class ScheduleGamePageCriteria implements Specifiable {
    
    private ScheduleGamePage shGamePage;
    /**
     * the Specification the validation result will be added to.
     */
    private Specification specification;
    
    public ScheduleGamePageCriteria(ScheduleGamePage shGamePage, Specification specification) {
        if (shGamePage == null) {
            throw new NotFoundException("the income PlayersListPage == null");
        }
        this.shGamePage = shGamePage;
        this.specification = specification;
    }
    
    public ScheduleGamePageCriteria isDateAndTimeLabelPresentsOnly(List<String> list, int number) {
        boolean result = (list.size() == number);
        StringBuilder labelNames = new StringBuilder();
        if (! result) {
            
            for (int i = 0; i < list.size(); i++) {
                labelNames.append(list.get(i));
            }
            
            specification.add(result, String.format("%s There are more fields to set. Not only for date." +
                    " Expected number of fields: %s", number, "Actual: " + list.size() + " " + labelNames));
        }
        return this;
        
    }
    
    public ScheduleGamePageCriteria isLabelsNameIsCorrect(String name) {
        
        boolean result = (shGamePage.labelsList().size() == 1 && shGamePage.labelDateAndTimeName().equals(name));
        specification.add(result, String.format("Wrong name for textInputField" + System.lineSeparator() +
                " Expected: " + shGamePage.labelDateAndTimeName() +
                System.lineSeparator()
                + "Actual: " + name));
        return this;
    }
    
    @Override
    public Specification next() {
        return specification;
    }
}
