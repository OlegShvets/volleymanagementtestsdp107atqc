package com.softserveinc.volleymanagementtests.specification.criteria;

import com.softserveinc.volleymanagementtests.specification.Specifiable;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.team.Team;

/**
 * @author Danil Zhyliaiev
 */
public class TeamCriteria implements Specifiable{
    private Team team;
    private Specification specification;

    public TeamCriteria(Team team, Specification specification) {
        this.team = team;
        this.specification = specification;
    }

    public TeamCriteria equalTo(Team anotherTeam) {
        specification.add(team.equals(anotherTeam),
                String.format("%s:%n expected [%s]%n but found [%s]; ",
                        "Teams are not equal", team.toString(), anotherTeam.toString()));
        return this;
    }

    @Override
    public Specification next() {
        return specification;
    }
}
