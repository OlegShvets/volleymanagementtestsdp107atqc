package com.softserveinc.volleymanagementtests.specification.criteria;

import com.softserveinc.volleymanagementtests.dal.models.TeamDal;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.player.PlayerDetailsPage;
import com.softserveinc.volleymanagementtests.pages.team.TeamDetailsPage;
import com.softserveinc.volleymanagementtests.pages.team.TeamListPage;
import com.softserveinc.volleymanagementtests.specification.Specifiable;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.testdata.team.mappers.DalToTeam;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.openqa.selenium.NotFoundException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Karabaza Anton on 22.03.2016.
 */
public class TeamListCriteria implements Specifiable {

    private TeamListPage teamListPage;
    private Specification specification;

    public TeamListCriteria(TeamListPage teamListPage, Specification specification) {
        if (teamListPage == null) {
            throw new NotFoundException("the income TeamListPage == null");
        }
        this.teamListPage = teamListPage;
        this.specification = specification;
    }

    public TeamListCriteria isTeamDisplayedOnList(TeamDal team) throws SQLException,
            ClassNotFoundException {

        specification.add(teamListPage.isTeamExistOnPage(new DalToTeam().map(team)),
                String.format("The team %s isn't displayed on TeamListPage.", team.getName()));
        return this;
    }

    /**
     * Method helps to verify that the equivalent team exist on TeamListPage
     * and its details equivalent too.
     * After use, teamListPage object must be recreated manually.
     *
     * @param team - team to check the equivalence on details page.
     * @return TeamListCriteria.
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public TeamListCriteria isTeamDisplayedOnListWithCorrectDetails(TeamDal team)
            throws SQLException, ClassNotFoundException {

        TeamDetailsPage teamDetailsPage = teamListPage.showTeamDetails(
               new DalToTeam().map(team));

        Team readTeam = teamDetailsPage.readTeam();

        PlayerDetailsPage playerDetailsPage = teamDetailsPage.clickCaptainDetails();
        Player captain = playerDetailsPage.readPlayer();
        playerDetailsPage.clickBackLink();
        int roastersLinksSize = teamDetailsPage.getPlayerRoaster().size();
        List<Player> roasters = new ArrayList<>();

        for (int i = 0; i < roastersLinksSize; i++) {
            TeamDetailsPage teamDetPage = new TeamDetailsPage();
            teamDetPage.getPlayerRoaster().get(i).click();
            PlayerDetailsPage playerDetPage = new PlayerDetailsPage();
            Player player = playerDetPage.readPlayer();
            System.out.println(player.getId());
            roasters.add(player);
            playerDetPage.clickBackLink();
        }
        readTeam = new Team().newBuilder(readTeam)
                .setCaptain(captain)
                .setRoster(roasters)
                .build();
    
        new WebDriverUtils().load(new PageUrls().TEAMS_LIST_URL);

        specification.add(readTeam.equals(new DalToTeam().map(team)),
                String.format("The team %s isn't displayed on "
                        + "TeamDetailsPage correctly.", team.getName()));
        return this;
    }

    @Override
    public Specification next() {
        return this.specification;
    }
}
