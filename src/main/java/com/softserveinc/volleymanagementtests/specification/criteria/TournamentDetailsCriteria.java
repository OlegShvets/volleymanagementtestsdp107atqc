package com.softserveinc.volleymanagementtests.specification.criteria;

import com.softserveinc.volleymanagementtests.pages.tournaments.TournamentDetailsPage;
import com.softserveinc.volleymanagementtests.specification.Specifiable;
import com.softserveinc.volleymanagementtests.specification.Specification;
import org.openqa.selenium.NotFoundException;

import java.util.Objects;

/**
 * Created by Иван on 12-Mar-17.
 */
public class TournamentDetailsCriteria implements Specifiable {

    private TournamentDetailsPage tournamentDetailsPage;
    private Specification specification;

    public TournamentDetailsCriteria(TournamentDetailsPage tournamentDetailsPage, Specification specification) {
        if (tournamentDetailsPage == null) {
            throw new NotFoundException("the income TextInput == null");
        }
        this.tournamentDetailsPage = tournamentDetailsPage;
        this.specification = specification;
    }

    public TournamentDetailsCriteria schemeMatch(final String expectedResult) {
        String actualResult = tournamentDetailsPage.getTournamentScheme().getText();
        specification.add(Objects.equals(actualResult, expectedResult),
                String.format("%s:%n  expected [%s]%n but found [%s]; ",
                        "The tournament SchemeCode does not match",
                        expectedResult, actualResult));
        return this;
    }

    @Override
    public Specification next() {
        return specification;
    }
}
