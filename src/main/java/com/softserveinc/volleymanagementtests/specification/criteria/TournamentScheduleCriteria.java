package com.softserveinc.volleymanagementtests.specification.criteria;

import com.softserveinc.volleymanagementtests.pages.tournaments.TournamentsSchedulePage;
import com.softserveinc.volleymanagementtests.specification.Specifiable;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import org.openqa.selenium.NotFoundException;

/**
 * Created by stas on 13.03.2017.
 */
public class TournamentScheduleCriteria implements Specifiable {

    private TournamentsSchedulePage tournamentsSchedulePage;
    private Specification specification;


    public TournamentScheduleCriteria(TournamentsSchedulePage tourSchedPage, Specification specification) {
        if (tourSchedPage == null) {
            throw new NotFoundException("the income TextInput == null");
        }
        this.tournamentsSchedulePage = tourSchedPage;
        this.specification = specification;
    }

    public TournamentScheduleCriteria isTeamPresentInRound(String expRoundName, Team homeTeam) {
        boolean result = tournamentsSchedulePage.isTeamExistsInRound(homeTeam, expRoundName);
        specification.add(result, String.format("%s team is not present in round %s", homeTeam.getName(), expRoundName));
        return this;
    }

    public TournamentScheduleCriteria isTeamNotPresentInRound(String expRoundName, Team homeTeam) {
        boolean result = tournamentsSchedulePage.isTeamExistsInRound(homeTeam, expRoundName);
        specification.add(!result, String.format("%s team present in round %s", homeTeam.getName(), expRoundName));
        return this;
    }

    public TournamentScheduleCriteria checkFinalRoundQuantity(final int expectedResult) {
        int actualResult = tournamentsSchedulePage.getRoundElements().get(tournamentsSchedulePage.getRoundElements()
                .size() - 1).getTeamGameElements().size();
        specification.add(actualResult == expectedResult, String.format("%s:%n  expected [%d]%n but found [%d]; ",
                "Final round quantity does not match",
                expectedResult, actualResult));
        return this;
    }

    public TournamentScheduleCriteria checkFinalRoundNames(final String expFirstGameName
            , final String expSecondGameName) {

        String actualFirstGameName = tournamentsSchedulePage.getFinalGameGroupNameByNumber(1);
        String actualSecondGameName = tournamentsSchedulePage.getFinalGameGroupNameByNumber(2);

        specification.add(actualFirstGameName.equals(expFirstGameName)
                        && actualSecondGameName.equals(expSecondGameName),
                String.format("%s :%n expected [%s and %s]%n but found [%s and %s]; ",
                        "Final round names does not match", expFirstGameName, expSecondGameName
                        , actualFirstGameName, actualSecondGameName));
        return this;
    }

    @Override
    public Specification next() {
        return specification;
    }
}
