package com.softserveinc.volleymanagementtests.specification.criteria;


import com.softserveinc.volleymanagementtests.pages.admin.users.UsersListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.user.User;

/**
 * Created by stas on 19.03.2017.
 */
public class UserListCriteria {

    private UsersListPage usersListPage;
    private Specification specification;

    public UserListCriteria(UsersListPage UsersListPage, Specification specification) {
        this.usersListPage = UsersListPage;
        this.specification = specification;
    }

    public UserListCriteria isUserPresent(User user) {
        specification.add(usersListPage
                .isUserWithNameIsPresent(user.getUserName()),
                String.format("User [%s] is not present on the page", user.getUserName()));
        return this;
    }

}


