package com.softserveinc.volleymanagementtests.testdata;

/**
 * Class for alert message String's.
 *
 * @author Ivan Podorozhnyi
 */

public final class AlertMessages {

    public String getAlertMessage(String alert) {
        String alertString = "";
        switch (alert) {
            case "Do you want to delete player":
                alertString = "Are you sure you want to delete player %s ?";
                break;
            case "Player was deleted":
                alertString = "Player was deleted successfully";
                break;
        }
        return alertString;
    }
}
