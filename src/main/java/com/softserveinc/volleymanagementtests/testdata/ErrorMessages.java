package com.softserveinc.volleymanagementtests.testdata;

import com.softserveinc.volleymanagementtests.testdata.player.Player;

/**
 * Class for error message String's.
 *
 * @author Danil Zhyliaiev
 */
public final class ErrorMessages {
    
    // Player errors
    public String getPlayerErrorMessage(String error) {
        String errorString = "";
        switch (error) {
            case "Player first name empty error":
                errorString = "Name can not be empty";
                break;
            case "Player last name empty error":
                errorString = "Last name can not be empty";
                break;
            case "Player first name must be letters only error":
                errorString = "Name should contain only letters and spaces," +
                        " or \" - \", \" ' \" characters (no more than one consecutive)" +
                        ". Name must not end with a special character.";
                break;
            case "Player last name must be letters only error":
                errorString = "Last name should contain only letters and spaces," +
                        " or \" - \", \" ' \" characters (no more than one consecutive)" +
                        ". Last name must not end with a special character.";
                break;
            case "Player first name length error":
                errorString = "Name can not contain more than 60 symbols";
                break;
            case "Player last name length error":
                errorString = "Last name can not contain more than 60 symbols";
                break;
            case "Player birth year invalid input error":
                errorString = "The field Year of birth must be a number.";
                break;
            case "Player height invalid input error":
                errorString = "The field Height must be a number.";
                break;
            case "Player weight invalid input error":
                errorString = "The field Weight must be a number.";
                break;
            case "Player birth year range error":
                errorString = "Invalid year of birth";
                break;
            case "Player height range error":
                errorString = "Invalid height";
                break;
            case "Player weight range error":
                errorString = "Invalid weight";
                break;
        }
        return errorString;
    }
    
    // Team errors
    public String getTeamErrorMessage(String error) {
        
        String errorString = "";
        switch (error) {
            case "Team name empty error":
                errorString = "The field can not be empty";
                break;
            case "Captain in other team error":
                errorString = "The player is a member of the other team";
                break;
            case "Captain is not chosen error":
                errorString = "The captain is need to be chosen";
                break;
            case "Team name length error":
                errorString = "Name can not contain more than 30 symbols";
                break;
            case "Team coach name length error":
                errorString = "Coach can not contain more than 60 symbols";
                break;
            case "Team achievements length error":
                errorString = "The field can not contain more than 4000 symbols";
                break;
            case "Team coach name invalid input error":
                errorString = "Coach should contain only letters and spaces, or \" - \", \" ' \" characters" +
                        " (no more than one consecutive). Coach must not end with a special character.";
                break;
        }
        return errorString;
    }
    
    public String getPlayerBirthYearInvalidInputErrorWithDot(Player player) {
        StringBuilder error = new StringBuilder();
        error.append("The value '");
        error.append(player.getBirthYear());
        error.append("' is not valid for Year of birth.");
        return error.toString();
    }
    
    public String getHeightInvalidInputErrorWithDot(Player player) {
        StringBuilder error = new StringBuilder();
        error.append("The value '");
        error.append(player.getHeight());
        error.append("' is not valid for Height.");
        return error.toString();
    }
    
    public String getWeightInvalidInputErrorWithDot(Player player) {
        StringBuilder error = new StringBuilder();
        error.append("The value '");
        error.append(player.getWeight());
        error.append("' is not valid for Weight.");
        return error.toString();
    }
}
