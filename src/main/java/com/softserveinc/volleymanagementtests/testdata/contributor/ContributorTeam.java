package com.softserveinc.volleymanagementtests.testdata.contributor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Danil Zhyliaiev
 */
public class ContributorTeam {
    private String teamName;
    private List<String> members;

    public String getTeamName() {
        return teamName;
    }

    public List<String> getMembers() {
        return members;
    }

    public ContributorTeamBuilder newBuilder() {
        return new ContributorTeam().new ContributorTeamBuilder();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ContributorTeam that = (ContributorTeam) o;

        if (getTeamName() != null && !getTeamName().equals(that.getTeamName())
                || getTeamName() == null && that.getTeamName() != null) {
            return false;
        }

        return (getMembers() != null && getMembers().equals(that.getMembers()))
                || (getMembers() == null && that.getMembers() == null);
    }

    @Override
    public int hashCode() {
        int result;

        if (getTeamName() != null) {
            result = getTeamName().hashCode();
        } else {
            result = 0;
        }

        if (getMembers() != null) {
            result = 31 * result + getMembers().hashCode();
        } else {
            result = 31 * result;
        }

        return result;
    }

    @Override
    public String toString() {
        return "ContributorTeam{"
                + "teamName='" + teamName + '\''
                + ", members=" + members
                + '}';
    }

    public class ContributorTeamBuilder {
        public ContributorTeamBuilder setMembers(final List<String> membersArg) {
            members = membersArg;
            return this;
        }

        public ContributorTeamBuilder setTeamName(final String teamNameArg) {
            teamName = teamNameArg;
            return this;
        }

        public ContributorTeamBuilder addMember(final String member) {
            if (members == null) {
                members = new ArrayList<>();
            }

            members.add(member);

            return this;
        }

        public ContributorTeam build() {
            return ContributorTeam.this;
        }
    }
}
