package com.softserveinc.volleymanagementtests.testdata.contributor.mappers;

import com.softserveinc.volleymanagementtests.dal.models.ContributorDal;
import com.softserveinc.volleymanagementtests.dal.models.ContributorTeamDal;
import com.softserveinc.volleymanagementtests.dal.repositories.ContributorRepository;
import com.softserveinc.volleymanagementtests.testdata.Mapper;
import com.softserveinc.volleymanagementtests.testdata.contributor.ContributorTeam;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Danil Zhyliaiev
 */
public class DalToContributorTeam implements Mapper<ContributorTeam, ContributorTeamDal> {


    @Override
    public ContributorTeam map(
            final ContributorTeamDal source)  {
        ContributorTeam.ContributorTeamBuilder builder = new ContributorTeam().newBuilder();

        if (source.getName() == null) {
            builder.setTeamName("");
        } else {
            builder.setTeamName(source.getName());
        }

        if (source.getId() == null) {
            builder.setMembers(new ArrayList<>());
        } else {
            for (ContributorDal contributorDal
                    : new ContributorRepository().getTeamMembersById(source.getId())) {
                builder.addMember(contributorDal.getName());
            }
        }

        return builder.build();
    }
}
