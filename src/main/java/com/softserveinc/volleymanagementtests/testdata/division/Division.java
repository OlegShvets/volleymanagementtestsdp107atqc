package com.softserveinc.volleymanagementtests.testdata.division;


/**
 * Created by stas on 12.03.2017.
 */
public final class Division {

    private int id;
    private String name;
    private int tournamentId;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getTournamentId() {
        return tournamentId;
    }

    public DivisionBuilder newBuilder() {
        return new Division().new DivisionBuilder();
    }

    public class DivisionBuilder {

        public DivisionBuilder setId(int id) {
            Division.this.id = id;
            return this;
        }

        public DivisionBuilder setName(final String name) {
            Division.this.name = name;
            return this;
        }

        public DivisionBuilder setTournamentID(int tournamentId) {
            Division.this.tournamentId = tournamentId;
            return this;
        }

        public Division build() {
            return Division.this;
        }

    }
}
