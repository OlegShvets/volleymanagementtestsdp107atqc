package com.softserveinc.volleymanagementtests.testdata.division;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * Created by stas on 12.03.2017.
 */
public final class DivisionTestData {

    private final int AVERAGE_NAME = 10;

    public Division getValidDivision() {
        Division.DivisionBuilder builder = new Division().newBuilder();
        builder.setName(RandomStringUtils.random(new DivisionTestData().AVERAGE_NAME, true, true));
        return builder.build();
    }
}
