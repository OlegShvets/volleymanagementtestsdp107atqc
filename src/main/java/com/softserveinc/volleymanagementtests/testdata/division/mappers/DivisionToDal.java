package com.softserveinc.volleymanagementtests.testdata.division.mappers;

import com.softserveinc.volleymanagementtests.dal.models.DivisionDal;
import com.softserveinc.volleymanagementtests.testdata.Mapper;
import com.softserveinc.volleymanagementtests.testdata.division.Division;

/**
 * Created by stas on 12.03.2017.
 */
public class DivisionToDal implements Mapper<DivisionDal, Division> {


    @Override
    public DivisionDal map(final Division source)  {
        DivisionDal.DivisionBuilder divisionDalBuilder = DivisionDal.newBuilder();
        divisionDalBuilder.setTournamentId(source.getId());

        if (source.getName() == null) {
            divisionDalBuilder.setName("");
        }
        divisionDalBuilder.setName(source.getName());
        divisionDalBuilder.setTournamentId(source.getTournamentId());
        return divisionDalBuilder.build();
    }
}
