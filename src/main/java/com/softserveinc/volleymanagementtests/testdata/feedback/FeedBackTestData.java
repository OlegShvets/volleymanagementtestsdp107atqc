package com.softserveinc.volleymanagementtests.testdata.feedback;

import com.softserveinc.volleymanagementtests.dal.models.FeedbackDal;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Date;

/**
 * Created by stas on 17.03.2017.
 */
public final class FeedBackTestData {


    public FeedbackDal getValidNotReadFeedback() {
        return FeedbackDal
                .newBuilder()
                .setUserEmail(String.format("%s@%s.com",
                        RandomStringUtils.random(10, true, true),
                        RandomStringUtils.random(5, true ,true)))
                .setContent(RandomStringUtils.random(15, true, true))
                .setFeedBackDate(new Date())
                .setStatusCode(0)
                .setAdminName(RandomStringUtils.random(20, true, true))
                .setUpdateDate(new Date())
                .setUserEnv(RandomStringUtils.random(30, true, true))
                .build();
    }

    public static FeedbackDal getValidUserFeedback() {
        return FeedbackDal
                .newBuilder()
                .setUserEmail("volleyManagementUser@gmail.com")
                .setContent(RandomStringUtils.random(15, true, true))
                .setFeedBackDate(new Date())
                .setStatusCode(0)
                .setAdminName(RandomStringUtils.random(20, true, true))
                .setUpdateDate(new Date())
                .setUserEnv(RandomStringUtils.random(30, true, true))
                .build();
    }
}
