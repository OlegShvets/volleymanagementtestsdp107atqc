package com.softserveinc.volleymanagementtests.testdata.group;

/**
 * Created by stas on 12.03.2017.
 */
public final class Group {

    private int id;
    private String name;
    private int divisionId;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getDivisionId() {
        return divisionId;
    }

    public GroupBuilder newBuilder() {
        return new Group().new GroupBuilder();
    }

    public class GroupBuilder {

        public GroupBuilder setId(int id) {
            Group.this.id = id;
            return this;
        }

        public GroupBuilder setName(final String name) {
            Group.this.name = name;
            return this;
        }

        public GroupBuilder setDivisionId(int divisionId) {
            Group.this.divisionId = divisionId;
            return this;
        }

        public Group build() {
            return Group.this;
        }
    }
}
