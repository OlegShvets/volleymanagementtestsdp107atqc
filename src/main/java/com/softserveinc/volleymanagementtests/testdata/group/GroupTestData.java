package com.softserveinc.volleymanagementtests.testdata.group;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * Created by stas on 12.03.2017.
 */
public class GroupTestData {

    private final int AVERAGE_NAME  = 10;


    public Group getValidGroup() {
        Group.GroupBuilder builder = new Group().newBuilder();
        builder.setName(RandomStringUtils.random(AVERAGE_NAME, true, true));
        return builder.build();
    }
}
