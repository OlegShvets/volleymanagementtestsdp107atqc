package com.softserveinc.volleymanagementtests.testdata.group;

import com.softserveinc.volleymanagementtests.dal.models.DivisionDal;
import com.softserveinc.volleymanagementtests.dal.models.GroupDal;
import com.softserveinc.volleymanagementtests.testdata.Mapper;
import com.softserveinc.volleymanagementtests.testdata.division.Division;
import com.softserveinc.volleymanagementtests.testdata.division.mappers.DivisionToDal;

import java.sql.SQLException;

/**
 * Created by stas on 12.03.2017.
 */
public class GroupToDal implements Mapper<GroupDal, Group> {


    @Override
    public GroupDal map(Group source) {
        GroupDal.GroupDalBuilder groupBuilder = GroupDal.newBuilder();
        groupBuilder.setId(source.getId());

        if (source.getName() == null) {
            groupBuilder.setName("");
        }
        groupBuilder.setName(source.getName());
        groupBuilder.setDivisionId(source.getDivisionId());
        return groupBuilder.build();
    }
}
