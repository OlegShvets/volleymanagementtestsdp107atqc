package com.softserveinc.volleymanagementtests.testdata.player;

import com.softserveinc.volleymanagementtests.testdata.player.fields.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author J.Bodnar 22.02.2016.
 */
public final class PlayerTestData {
    private final int AVERAGE_NAME_LENGTH = 8;
    
    private List<Player> validPlayersList;
    private List<Player> playersListForSearching;
    
    public PlayerTestData() {
    }
    
    {
        validPlayersList = new ArrayList<>();
        validPlayersList.add(new Player().newBuilder()
                .setFirstName("Absorbing")
                .setLastName("Man")
                .setBirthYear("1980")
                .setHeight("190")
                .setWeight("89")
                .build());
        validPlayersList.add(new Player().newBuilder()
                .setFirstName("Adam")
                .setLastName("Warlock")
                .setBirthYear("1970")
                .setHeight("200")
                .setWeight("90")
                .build());
        validPlayersList.add(new Player().newBuilder()
                .setFirstName("Agent")
                .setLastName("Brand")
                .setBirthYear("1990")
                .setHeight("180")
                .setWeight("80")
                .build());
        validPlayersList.add(new Player().newBuilder()
                .setFirstName("Anna")
                .setLastName("Kravinoff")
                .setBirthYear("1988")
                .setHeight("175")
                .setWeight("50")
                .build());
        
        playersListForSearching = new ArrayList<>();
        playersListForSearching.add(new Player().newBuilder()
                .setFirstName("search").setLastName("TEXT")
                .setBirthYear("1900").setHeight("10").setWeight("10").build());
        playersListForSearching.add(new Player().newBuilder()
                .setFirstName("ASearchA").setLastName("ATextA")
                .setBirthYear("1900").setHeight("10").setWeight("10").build());
        playersListForSearching.add(new Player().newBuilder()
                .setFirstName("earchS").setLastName("extT")
                .setBirthYear("1900").setHeight("10").setWeight("10").build());
        playersListForSearching.add(new Player().newBuilder()
                .setFirstName("searcH").setLastName("teXt")
                .setBirthYear("1900").setHeight("10").setWeight("10").build());
    }
    
    public Player getValidPlayer() {
        return new Player().newBuilder()
                .setFirstName(new FirstName().getRandomFirstName(new PlayerTestData()
                        .AVERAGE_NAME_LENGTH))
                .setLastName(new LastName().getRandomLastName(new PlayerTestData()
                        .AVERAGE_NAME_LENGTH))
                .setBirthYear(new BirthYear().getRandomBirthYear())
                .setHeight(new Height().getRandomHeight())
                .setWeight(new Weight().getRandomWeight()).build();
    }
    
    public List<Player> getValidPlayersList() {
        return this.validPlayersList;
    }
    
    public List<Player> getPlayersListForSearching() {
        return playersListForSearching;
    }
    
    
    public Player getInvalidPlayerByFirstName(Violations violation) {
        return new Player().newBuilder(new PlayerTestData().getValidPlayer())
                .setFirstName(new FirstName().getInvalidBy(violation)).build();
    }
    
    public Player getInvalidPlayerByLastName(Violations violation) {
        return new Player().newBuilder(new PlayerTestData().getValidPlayer())
                .setLastName(new LastName().getInvalidBy(violation)).build();
    }
    
    public Player getInvalidPlayerByBirthYear(Violations violation) {
        return new Player().newBuilder(new PlayerTestData().getValidPlayer())
                .setBirthYear(new BirthYear().getInvalidBy(violation)).build();
    }
    
    public Player getInvalidPlayerByHeight(Violations violation) {
        return new Player().newBuilder(new PlayerTestData().getValidPlayer())
                .setHeight(new Height().getInvalidBy(violation)).build();
    }
    
    public Player getInvalidPlayerByWeight(Violations violation) {
        return new Player().newBuilder(new PlayerTestData().getValidPlayer())
                .setWeight(new Weight().getInvalidBy(violation)).build();
    }
}
