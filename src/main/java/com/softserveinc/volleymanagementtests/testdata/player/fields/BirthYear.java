package com.softserveinc.volleymanagementtests.testdata.player.fields;

import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;

/**
 * @author J.Bodnar 22.02.2016.
 */
public class BirthYear {
    
    public final int MAX_VALUE = 2100;
    public final int MIN_VALUE = 1900;
    public final int FIELD_LENGTH = 4;
    private final char[] NOT_ALLOWED_CHARACTERS =
            {'!', '?', '@', '#', '%', '^', '&', '*', '(', ')', ',', '/', '.'};
    
    public String getInvalidBy(Violations violation) {
        switch (violation) {
            case MIN_VALUE_VIOLATED:
                return "" + (new BirthYear().MIN_VALUE - 1);
            case MAX_VALUE_VIOLATED:
                return "" + (new BirthYear().MAX_VALUE + 1);
            case CONTAINS_LETTERS:
                return RandomStringUtils.random(new BirthYear().FIELD_LENGTH, true, true);
            case CONTAIN_NOT_ALLOWED_CHARACTERS:
                return RandomStringUtils
                        .random(new Random().nextInt(new BirthYear().FIELD_LENGTH)
                                + 1, new BirthYear().NOT_ALLOWED_CHARACTERS);
            case NEGATIVE_NUMBER:
                return "-" + RandomStringUtils.random(new BirthYear().FIELD_LENGTH, false, true);
            case NOT_INTEGER_NUMBER:
                return new BirthYear().getRandomNotIntegerBirthYear();
            case NOT_INTEGER_NUMBER_WITH_DOT:
                return new BirthYear().getRandomNotIntegerBirthYear().replace(',', '.');
            case NOT_INTEGER_NUMBER_WITH_COMMA:
                return new BirthYear().getRandomNotIntegerBirthYear().replace('.', ',');
            default:
                throw new RuntimeException(String.format(
                        "%s violation is not available for the %s field",
                        violation.toString(), "birth year"));
        }
    }
    
    private String getRandomNotIntegerBirthYear() {
        return String.format("%s%s%s",
                new Random().nextInt(new BirthYear().MAX_VALUE -
                        new BirthYear().MIN_VALUE) + (new BirthYear().MIN_VALUE),
                RandomStringUtils.random(1, ',', '.'),
                RandomStringUtils
                        .random(2, '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'));
        
    }
    
    public String getRandomBirthYear() {
        return String.valueOf(new Random().nextInt(new BirthYear().MAX_VALUE -
                new BirthYear().MIN_VALUE + 1) + (new BirthYear().MIN_VALUE));
    }
}
