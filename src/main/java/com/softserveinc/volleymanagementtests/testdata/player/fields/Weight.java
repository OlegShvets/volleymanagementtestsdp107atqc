package com.softserveinc.volleymanagementtests.testdata.player.fields;

import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;

/**
 * @author J.Bodnar 22.02.2016.
 */
public class Weight {
    public final int MAX_VALUE = 500;
    public final int MIN_VALUE = 10;
    public final int MAX_FIELD_LENGTH = 3;
    private final char[] NOT_ALLOWED_CHARACTERS =
            {'!', '?', '@', '#', '%', '^', '&', '*', '(', ')', ',', '/', '.'};

    public String getInvalidBy(Violations violation) {
        switch (violation) {
            case MIN_VALUE_VIOLATED:
                return "" + (MIN_VALUE - 1);
            case MAX_VALUE_VIOLATED:
                return "" + (MAX_VALUE + 1);
            case CONTAINS_LETTERS:
                return RandomStringUtils.random(MAX_FIELD_LENGTH, true, true);
            case CONTAIN_NOT_ALLOWED_CHARACTERS:
                return RandomStringUtils.random(
                        new Random().nextInt(MAX_FIELD_LENGTH) + 1, NOT_ALLOWED_CHARACTERS);
            case NEGATIVE_NUMBER:
                return "-" + RandomStringUtils.random(MAX_FIELD_LENGTH, false, true);
            case NOT_INTEGER_NUMBER:
                return getRandomNotIntegerWeight();
            case NOT_INTEGER_NUMBER_WITH_DOT:
                return getRandomNotIntegerWeight().replace(',', '.');
            case NOT_INTEGER_NUMBER_WITH_COMMA:
                return getRandomNotIntegerWeight().replace('.', ',');
            default:
                throw new RuntimeException(String.format(
                        "%s violation is not available for the %s field",
                        violation.toString(), "weight"));
        }
    }

    private String getRandomNotIntegerWeight() {
        return String.format("%s%s%s",
                new Random().nextInt(MAX_VALUE - MIN_VALUE) + (MIN_VALUE),
                RandomStringUtils.random(1, ',', '.'),
                RandomStringUtils.random(
                        2, '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'));
    }

    public String getRandomWeight() {
        return String.valueOf(new Random().nextInt(MAX_VALUE - MIN_VALUE + 1) + (MIN_VALUE));
    }

}
