package com.softserveinc.volleymanagementtests.testdata.team;

import com.softserveinc.volleymanagementtests.testdata.player.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Class represents Team entity as a test data set.
 * Based on Builder pattern.
 *
 * @author Danil Zhyliaiev
 */
public class Team {
    private String id;
    private String name;
    private String coach;
    private String achievements;
    private Player captain;
    private List<Player> roster = new ArrayList<>();

    public Team setId(String id) {
        this.id  = id;
        return this;
    }
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Player getCaptain() {
        return captain;
    }

    public String getCoach() {
        return coach;
    }

    public String getAchievements() {
        return achievements;
    }

    public List<Player> getRoster() {
        return roster;
    }

    public TeamBuilder newBuilder() {
        return this.new TeamBuilder();
    }

    public TeamBuilder newBuilder(Team team){
        return this.new TeamBuilder()
                .setId(team.getId())
                .setName(team.getName())
                .setCoach(team.getCoach())
                .setAchievements(team.getAchievements())
                .setCaptain(team.getCaptain())
                .setRoster(team.getRoster());
    }

    @Override
    public String toString() {
        StringBuilder team = new StringBuilder("Team{");

        if ((id != null) && (id.length() > 0)) {
            team.append("id='").append(id).append("'");
        }

        if ((name != null) && (name.length() > 0)) {
            team.append("name='").append(name).append("'");
        }

        if ((coach != null) && (coach.length() > 0)) {
            team.append("coach='").append(coach).append("'");
        }

        if ((achievements != null) && (achievements.length() > 0)) {
            team.append("achievements='").append(achievements).append("...'");
        }

        if (captain != null) {
            team.append("captain='")
                    .append(captain.getFirstName())
                    .append(" ")
                    .append(captain.getLastName()).append("'");
        }

        if ((roster != null) && (roster.size() > 0)) {
            team.append("roster='");

            for (Player player : roster) {
                team.append(player.getLastName()).append(";");
            }

            team.append("'");
        }

        return team.append("}").toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Team team = (Team) o;
        return Objects.equals(id, team.id) &&
                Objects.equals(name, team.name) &&
                Objects.equals(coach, team.coach) &&
                Objects.equals(achievements, team.achievements) &&
                Objects.equals(captain, team.captain) &&
                Objects.equals(roster, team.roster);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, coach, achievements, captain, roster);
    }

    public class TeamBuilder {
        private TeamBuilder() { }

        public TeamBuilder setId(String idArg) {
            id = idArg;
            return this;
        }

        public TeamBuilder setName(String nameArg) {
            name = nameArg;
            return this;
        }

        public TeamBuilder setCaptain(Player captainArg) {
            captain = captainArg;
            return this;
        }

        public TeamBuilder setCoach(String coachArg) {
            coach = coachArg;
            return this;
        }

        public TeamBuilder setAchievements(String achievementsArg) {
            achievements = achievementsArg;
            return this;
        }

        public TeamBuilder setRoster(List<Player> rosterArg) {
            roster = rosterArg;
            return this;
        }

        public TeamBuilder addPlayer(Player playerArg) {
            roster.add(playerArg);
            return this;
        }

        public Team build() {
            return Team.this;
        }
    }
}