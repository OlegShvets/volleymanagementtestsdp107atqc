package com.softserveinc.volleymanagementtests.testdata.tournament;

import java.util.Date;


/**
 * Created by stas on 09.03.2017.
 */
public final class Tournament {
    private int id;
    private String name;
    private String description;
    private String regulationsLink;
    private int schemeCode;
    private String tournamentSeason;
    private Date applyingPeriodStart;
    private Date applyingPeriodEnd;
    private Date tournamentStart;
    private Date tournamentEnd;
    private Date transferPeriodStart;
    private Date transferPeriodEnd;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getRegulationsLink() {
        return regulationsLink;
    }

    public int getSchemeCode() {
        return schemeCode;
    }

    public String getTournamentSeason() {
        return tournamentSeason;
    }

    public Date getApplyingPeriodStart() {
        return applyingPeriodStart;
    }

    public Date getApplyingPeriodEnd() {
        return applyingPeriodEnd;
    }

    public Date getTournamentStart() {
        return tournamentStart;
    }

    public Date getTournamentEnd() {
        return tournamentEnd;
    }

    public Date getTransferPeriodStart() {
        return transferPeriodStart;
    }

    public Date getTransferPeriodEnd() {
        return transferPeriodEnd;
    }


    public TournamentBuilder newBuilder() {
        return this.new TournamentBuilder();
    }

    public Tournament newBuilder(Tournament tournament) {
        return this.new TournamentBuilder()
                .setId(tournament.id)
                .setName(tournament.name)
                .setDescription(tournament.description)
                .setRegulationsLink(tournament.regulationsLink)
                .setSchemeCode(tournament.schemeCode)
                .setSeason(tournament.tournamentSeason)
                .setApplyingPeriodStart(tournament.applyingPeriodStart)
                .setApplyingPeriodEnd(tournament.applyingPeriodEnd)
                .setTournamentStart(tournament.tournamentStart)
                .setTournamentEnd(tournament.tournamentEnd)
                .setTransferPeriodStart(tournament.transferPeriodStart)
                .setTransferPeriodEnd(tournament.transferPeriodEnd)
                .build();

    }


    public class TournamentBuilder {

        public TournamentBuilder setId(int id) {
            Tournament.this.id = id;
            return this;
        }

        public TournamentBuilder setName(final String name) {
            Tournament.this.name = name;
            return this;
        }

        public TournamentBuilder setDescription(final String description) {
            Tournament.this.description = description;
            return this;
        }

        public TournamentBuilder setRegulationsLink(final String regulationsLink) {
            Tournament.this.regulationsLink = regulationsLink;
            return this;
        }

        public TournamentBuilder setSchemeCode(int schemeCode) {
            Tournament.this.schemeCode = schemeCode;
            return this;
        }

        public TournamentBuilder setSeason(final String season) {
            Tournament.this.tournamentSeason = season;
            return this;
        }

        public TournamentBuilder setApplyingPeriodStart(final Date date) {
            Tournament.this.applyingPeriodStart = date;
            return this;
        }

        public TournamentBuilder setApplyingPeriodEnd(final Date date) {
            Tournament.this.applyingPeriodEnd = date;
            return this;
        }

        public TournamentBuilder setTournamentStart(final Date date) {
            Tournament.this.tournamentStart = date;
            return this;
        }

        public TournamentBuilder setTournamentEnd(final Date date) {
            Tournament.this.tournamentEnd = date;
            return this;
        }

        public TournamentBuilder setTransferPeriodStart(final Date date) {
            Tournament.this.transferPeriodStart = date;
            return this;
        }

        public TournamentBuilder setTransferPeriodEnd(final Date date) {
            Tournament.this.transferPeriodEnd = date;
            return this;
        }

        public Tournament build() {
            return Tournament.this;
        }

    }

}
