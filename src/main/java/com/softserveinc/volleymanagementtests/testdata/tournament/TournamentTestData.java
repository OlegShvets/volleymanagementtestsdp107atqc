package com.softserveinc.volleymanagementtests.testdata.tournament;

import org.apache.commons.lang3.RandomStringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by stas on 12.03.2017.
 */
public final class TournamentTestData {

    private final int averageNameLength;
    private final int averageDescLength;
    private final int averageRegLinkLength;

    public TournamentTestData() {
        this.averageNameLength = 30;
        this.averageDescLength = 12;
        this.averageRegLinkLength = 15;
    }

    public Tournament getValidPlayOffTournament() {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Tournament tournament = null;
        try {
            tournament = new Tournament().newBuilder()
                    .setName(RandomStringUtils.random(averageNameLength, true, true))
                    .setDescription(RandomStringUtils.random(averageDescLength, true, true))
                    .setSchemeCode(4)
                    .setRegulationsLink(RandomStringUtils.random(averageRegLinkLength, true, true))
                    .setSeason("2017/2018")
                    .setApplyingPeriodStart(dateFormat.parse("31.03.2017"))
                    .setApplyingPeriodEnd(dateFormat.parse("09.04.2017"))
                    .setTournamentStart(dateFormat.parse("16.04.2017"))
                    .setTournamentEnd(dateFormat.parse("14.08.2017"))
                    .setTransferPeriodStart(dateFormat.parse("17.04.2017"))
                    .setTransferPeriodEnd(dateFormat.parse("08.05.2017"))
                    .build();
        } catch (ParseException e) {
            throw new RuntimeException("error while parsing", e);
        }
                return tournament;
    }
}
