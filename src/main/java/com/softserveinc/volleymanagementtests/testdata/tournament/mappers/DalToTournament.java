package com.softserveinc.volleymanagementtests.testdata.tournament.mappers;

import com.softserveinc.volleymanagementtests.dal.models.TournamentDal;
import com.softserveinc.volleymanagementtests.testdata.Mapper;
import com.softserveinc.volleymanagementtests.testdata.tournament.Tournament;


/**
 * Created by skir on 3/10/2017.
 */
public final class DalToTournament implements Mapper<Tournament, TournamentDal> {

    @Override
    public Tournament map(TournamentDal source)  {
        Tournament.TournamentBuilder builder = new Tournament().newBuilder();

        builder.setId(source.getId());
        builder.setName(source.getName());
        if (source.getDescription() == null) {
            builder.setDescription("");
        }
        builder.setDescription(source.getDescription());
        if (source.getRegulationsLink() == null) {
            builder.setRegulationsLink("");
        }
        builder.setRegulationsLink(source.getRegulationsLink());
        builder.setSchemeCode(source.getSchemeCode());

        String seasonOffset = String.valueOf(source.getSeasonOffset());
        int lastSeasonDigit =  Integer.parseInt(seasonOffset
                .substring(seasonOffset.length() - 1));
        String season = String.format("201%s/201%s", lastSeasonDigit, lastSeasonDigit + 1);
        builder.setSeason(season);
        builder.setApplyingPeriodStart(source.getApplyingPeriodStart());
        builder.setApplyingPeriodEnd(source.getApplyingPeriodEnd());
        builder.setTournamentStart(source.getTournamentStart());
        builder.setTournamentEnd(source.getTournamentEnd());
        builder.setTransferPeriodStart(source.getTransferStart());
        builder.setTransferPeriodEnd(source.getTransferEnd());
        return builder.build();
    }
}
