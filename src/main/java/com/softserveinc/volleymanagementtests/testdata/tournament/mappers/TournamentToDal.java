package com.softserveinc.volleymanagementtests.testdata.tournament.mappers;

import com.softserveinc.volleymanagementtests.dal.models.TournamentDal;
import com.softserveinc.volleymanagementtests.testdata.Mapper;
import com.softserveinc.volleymanagementtests.testdata.tournament.Tournament;

/**
 * Created by skir on 3/10/2017.
 */
public class TournamentToDal implements Mapper<TournamentDal, Tournament> {


    @Override
    public TournamentDal map(Tournament source)  {
        TournamentDal.TournamentDalBuilder builder = TournamentDal.newBuilder();

        if (source.getId() > 0) {
            builder.setId(source.getId());
        }
        builder.setId(null);
        builder.setName(source.getName());
        builder.setDescription(source.getDescription());
        builder.setSchemeCode(source.getSchemeCode());

        /*regex will be enhanced in the future*/
        int offset  = Integer.parseInt(1 + source.getTournamentSeason().replaceAll("[/].*.$", "")
                .substring(2,4));
        builder.setSeasonOffset(offset);

        if (source.getRegulationsLink() == null) {
            builder.setRegulationsLink("");
        }
        builder.setRegulationsLink(source.getRegulationsLink());
        builder.setApplyingPeriodStart(source.getApplyingPeriodStart());
        builder.setApplyingPeriodEnd(source.getTransferPeriodEnd());
        builder.setTournamentStart(source.getTournamentStart());
        builder.setTournamentEnd(source.getTournamentEnd());
        builder.setTransferStart(source.getTransferPeriodStart());
        builder.setTransferEnd(source.getTransferPeriodEnd());

        return builder.build();
    }
}
