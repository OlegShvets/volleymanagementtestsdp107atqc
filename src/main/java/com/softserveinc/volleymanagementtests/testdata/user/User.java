package com.softserveinc.volleymanagementtests.testdata.user;

/**
 * Created by stas on 19.03.2017.
 */
public final class User {

    private int id;
    private String userName;
    private String fullName;
    private String cellPhone;
    private String email;
    private String playerId;
    private boolean isBlocked;

    public int getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getFullName() {
        return fullName;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public String getPlayerId() {
        return playerId;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public String getEmail() {
        return email;
    }

    public User.UserBuilder newBuilder() {
        return this.new UserBuilder();
    }

    public class UserBuilder {

        private UserBuilder(){}

        public UserBuilder setId (int id) {
            User.this.id = id;
            return this;
        }

        public UserBuilder setUserName(String name) {
            User.this.userName = name;
            return this;
        }

        public UserBuilder setFullName(String fullName) {
            User.this.fullName = fullName;
            return this;
        }

        public UserBuilder setEmail(String email) {
            User.this.email = email;
            return this;
        }

        public UserBuilder setCellPhone (String cellPhone) {
            User.this.cellPhone = cellPhone;
            return this;
        }

        public UserBuilder setPlayerId (String playerId) {
            User.this.playerId = playerId;
            return this;
        }

        public UserBuilder setBlocked(boolean isblocked) {
            User.this.isBlocked = isblocked;
            return this;
        }

        public User build() {
            return User.this;
        }

    }
}
