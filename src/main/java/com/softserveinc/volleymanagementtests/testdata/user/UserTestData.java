package com.softserveinc.volleymanagementtests.testdata.user;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * Created by stas on 20.03.2017.
 */
public class UserTestData {

    private final int AVERAGE_NAME = 10;
    private final int AVERAGE_FULL_NAME = 20;
    public final int AVERAGE_EMAIL = 12;
    public final int AVERAGE_PHONE = 11;

    public User getNonBlockedUser() {
        return new User().newBuilder()
                .setUserName(RandomStringUtils.random(AVERAGE_NAME, true, false))
                .setFullName(RandomStringUtils.random(AVERAGE_FULL_NAME, true, false))
                .setCellPhone(RandomStringUtils.random(AVERAGE_PHONE, false, true))
                .setEmail(RandomStringUtils.random(AVERAGE_EMAIL, true, true))
                .setBlocked(false)
                .build();
    }


    public User getBlockedUser() {
        return new User().newBuilder()
                .setUserName(RandomStringUtils.random(AVERAGE_NAME, true, false))
                .setFullName(RandomStringUtils.random(AVERAGE_FULL_NAME, true, false))
                .setCellPhone(RandomStringUtils.random(AVERAGE_PHONE, false, true))
                .setEmail(RandomStringUtils.random(AVERAGE_EMAIL, true, true))
                .setBlocked(true)
                .build();
    }
}
