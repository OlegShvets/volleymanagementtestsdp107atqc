package com.softserveinc.volleymanagementtests.testdata.user.mappers;

import com.softserveinc.volleymanagementtests.dal.models.UserDal;
import com.softserveinc.volleymanagementtests.testdata.Mapper;
import com.softserveinc.volleymanagementtests.testdata.user.User;

/**
 * Created by stas on 22.03.2017.
 */
public final class DalToUser implements Mapper<User, UserDal> {

    @Override
    public User map(UserDal source)  {
        User.UserBuilder builder = new User().newBuilder();
        builder.setId(source.getId());
        if (source.getUserName() == null) {
            builder.setUserName("");
        }
        builder.setUserName(source.getUserName());
        if (source.getFullName() == null) {
            builder.setFullName("");
        }
        builder.setFullName(source.getFullName());
        if (source.getCellPhone() == null) {
            builder.setCellPhone("");
        }
        builder.setCellPhone(source.getCellPhone());

        if (source.getEmail() == null) {
            builder.setEmail("");
        }
        builder.setEmail(source.getEmail());

        if (source.getPlayerId() == null) {
            builder.setPlayerId("");
        }
        builder.setPlayerId(source.getPlayerId());
        builder.setBlocked(source.getIsBlocked() == 0);
        return builder.build();
    }
}
