package com.softserveinc.volleymanagementtests.testdata.user.mappers;

import com.softserveinc.volleymanagementtests.dal.models.UserDal;
import com.softserveinc.volleymanagementtests.testdata.Mapper;
import com.softserveinc.volleymanagementtests.testdata.user.User;

/**
 * Created by stas on 19.03.2017.
 */
public final class UserToDal implements Mapper<UserDal, User> {

    @Override
    public UserDal map(User source)  {
        UserDal.UserDalBuilder builder = new UserDal.UserDalBuilder();
        builder.setId(source.getId())
                .setFullName(source.getFullName())
                .setName(source.getUserName())
                .setCellPhone(source.getCellPhone())
                .setEmail(source.getEmail())
                .setPlayerId(source.getPlayerId());

        if (source.isBlocked()) {
            builder.setBlocked(1);
        } else {
            builder.setBlocked(0);
        }
        return builder.build();
    }
}
