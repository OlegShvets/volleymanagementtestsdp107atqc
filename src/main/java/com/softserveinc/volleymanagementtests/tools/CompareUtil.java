package com.softserveinc.volleymanagementtests.tools;

import java.awt.*;
import java.awt.image.PixelGrabber;

/**
 * Created by shvets on 23.03.2017.
 */

public class CompareUtil {
    public boolean compareImage(String baseFile, String actualFile) {
        boolean compareResult = false;
        Image baseImage = Toolkit.getDefaultToolkit().getImage(baseFile);
        Image actualImage = Toolkit.getDefaultToolkit().getImage(actualFile);
            PixelGrabber baseImageGrab = new PixelGrabber(baseImage, 0, 0, -1, -1, false);
            PixelGrabber actualImageGrab = new PixelGrabber(actualImage, 0, 0, -1, -1, false);
            int[] baseImageData = null;
            int[] actualImageData = null;
        try {
            if (baseImageGrab.grabPixels()) {
                baseImageData = (int[]) baseImageGrab.getPixels();
            }
        } catch (InterruptedException e) {
            throw new ExceptionHandler("Interrupted  thread", e);
        }
        try {
            if (actualImageGrab.grabPixels()) {
                actualImageData = (int[]) actualImageGrab.getPixels();
            }
        } catch (InterruptedException e) {
            throw new ExceptionHandler("Interrupted  thread", e);
        }
        if ((baseImageGrab.getHeight() != actualImageGrab.getHeight()) ||
                (baseImageGrab.getWidth() != actualImageGrab.getWidth())) {
                compareResult = false;
            } else if (java.util.Arrays.equals(baseImageData, actualImageData)) {
                compareResult = true;
            }
        return compareResult;
    }
}