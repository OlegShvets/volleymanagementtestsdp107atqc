package com.softserveinc.volleymanagementtests.tools;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;


/**
 * Created by shvets on 23.03.2017.
 */
public class FileUploader {
    public void uploadFile() {
        String path = new File("resourses/test.jpg").getAbsolutePath();
        StringSelection test = new StringSelection(path);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(test, null);
        Robot rb = null;
        try {
            rb = new Robot();
            Thread.sleep(500);
            rb.keyPress(KeyEvent.VK_CONTROL);
            rb.keyPress(KeyEvent.VK_V);
            rb.keyRelease(KeyEvent.VK_V);
            rb.keyRelease(KeyEvent.VK_CONTROL);
            rb.keyPress(KeyEvent.VK_ENTER);
            rb.keyRelease(KeyEvent.VK_ENTER);
        } catch (AWTException e) {
            throw new ExceptionHandler("Abstract window error", e);
        } catch (InterruptedException e) {
            throw new ExceptionHandler("Interrupted  thread", e);
        }
    }
}
