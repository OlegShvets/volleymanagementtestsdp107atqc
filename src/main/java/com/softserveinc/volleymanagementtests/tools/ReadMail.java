package com.softserveinc.volleymanagementtests.tools;

import javax.mail.*;
import java.io.IOException;
import java.util.Properties;


/**
 * Created by Shvets on 18.03.2017.
 */

public class ReadMail {

    public String readMailContent(String keyword, String user, String password) {
        Properties properties = new Properties();
        properties.put("mail.imap.host", "imap.gmail.com");
        properties.put("mail.imap.port", 993);
        properties.setProperty("mail.imap.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.setProperty("mail.imap.socketFactory.fallback", "false");
        properties.setProperty("mail.imap.socketFactory.port", String.valueOf(993));
        Session session = Session.getDefaultInstance(properties);
        Store store;
        try {
            store = session.getStore("imap");
        } catch (NoSuchProviderException e) {
            throw new ExceptionHandler("Mail provider problem", e);
        }
        try {
            store.connect("smtp.gmail.com", user, password);
            Folder folderInbox;
            folderInbox = store.getFolder("INBOX");
            folderInbox.open(Folder.READ_ONLY);
            Message[] message = folderInbox.getMessages();

            for (Message aMessage : message) {
                try {
                    if ((aMessage.getContent().toString()).trim().contains(keyword)) {
                        return (aMessage.getContent().toString()).trim();
                    }
                } catch (MessagingException | IOException e) {
                    throw new ExceptionHandler("Error reading mail content", e);
            }
        }
            folderInbox.close(true);
            store.close();
        } catch (MessagingException e) {
            throw new ExceptionHandler("Error reading mail content", e);
        }
        return "Message not found";
    }
}


