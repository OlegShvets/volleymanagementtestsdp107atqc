package com.softserveinc.volleymanagementtests.tools;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;

/**
 * Created by shvets on 27.03.2017.
 */
public class ScreenMaker {
    public void makeScreenShot(String outputName) {
        File screenshotFile = ((TakesScreenshot) new WebDriverUtils()
                .getDriver())
                .getScreenshotAs(OutputType.FILE);
        try {
            String path = new File("resourses/" + outputName + ".jpg").getAbsolutePath();
            FileUtils.copyFile(screenshotFile, new File(path));
        } catch (IOException e) {
            throw new ExceptionHandler("write file error", e);
        }
    }
}

