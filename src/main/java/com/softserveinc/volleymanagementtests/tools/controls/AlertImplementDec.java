package com.softserveinc.volleymanagementtests.tools.controls;

import com.softserveinc.volleymanagementtests.tools.AlertWrapper;
import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Alert;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import org.openqa.selenium.By;

/**
 * Created by Sasha on 18.03.2017.
 */
public class AlertImplementDec implements Alert {

    private Alert alert;

    public AlertImplementDec() {
        this.alert = new AlertImpl(new AlertWrapper(new WebDriverUtils().getDriver().switchTo().alert()));
    }

    @Override
    public void accept() {
alert.accept();
    }

    @Override
    public void dismiss() {
alert.dismiss();
    }

    @Override
    public String getText() {
        return alert.getText();
    }
}
