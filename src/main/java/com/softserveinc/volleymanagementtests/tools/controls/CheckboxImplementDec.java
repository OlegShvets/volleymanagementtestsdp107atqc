package com.softserveinc.volleymanagementtests.tools.controls;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.Control;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Checkbox;
import org.openqa.selenium.By;

/**
 * Created by Sasha on 18.03.2017.
 */
public class CheckboxImplementDec implements Checkbox {

    private Checkbox checkbox;

    public CheckboxImplementDec(By by) {
        this.checkbox = new CheckboxImpl(new Control(new ContextVisible(by)));
    }


    @Override
    public void check() {
        if (!checkbox.isChecked()) {
            checkbox.check();
        }
    }

    @Override
    public void unCheck() {
        if (checkbox.isChecked()) {
            checkbox.check();
        }
    }

    @Override
    public boolean isChecked() {
        return checkbox.isChecked();
    }

    @Override
    public boolean isEnabled() {
        return checkbox.isEnabled();
    }
   }
