/**
 * Tools classes are stored here.
 * Classes to provide the low level access to the Selenium WebDriver,
 * configuring it and wraps it in order to make it(it's components)
 * more polymorphous.
 */
package com.softserveinc.volleymanagementtests.tools;
