package com.softserveinc.volleymanagementtests.tests.admin;

import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.admin.adminmainpage.AdminMainPage;
import com.softserveinc.volleymanagementtests.pages.login.LoginPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.TournamentsPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.specification.TestConfig;
import com.softserveinc.volleymanagementtests.tools.ReadMail;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import javax.mail.MessagingException;
import java.io.IOException;

/**
 * Test is checking the functionality of admin approve request function.
 *
 * @author Ivan Podorozhnyi
 */

@Features("Admin")
@Stories("ApproveRequest")
public class ApproveRequestTest {

    private Specification specification;

    @BeforeClass
    public final void loginWithCurrentProfile() {
        new LoginPage().logInWithCurrentProfile();
        specification = new Specification();

        new TournamentsPage()
                .openTournamentDetforUpcomTrnmtName("Big Game")
                .clickApplyForTournament()
                .chooseTeamByIndexInDropdown(1)
                .checkAgreeCheckbox()
                .clickAcceptButton();

        new WebDriverUtils().load(new PageUrls().ADMIN_MAIN_PAGE);

        new AdminMainPage()
                .clickRequests()
                .clickApplyTeamForTrnmentRequests()
                .clickConfirmButton(1);
    }

    @Test
    public final void adminApproveRequest() throws InterruptedException, MessagingException, IOException {

        String expectedReplyMessage = "Your apply was confirmed!";

        String actualReplyMessage = new ReadMail().readMailContent(expectedReplyMessage
                , new TestConfig().getGoogleUser()
                , new TestConfig().getGooglePass());

        specification.forThe(actualReplyMessage)
                .valueContains(expectedReplyMessage)
                .next()
                .check();
    }

    @AfterTest
    public void tearDown() {
        new WebDriverUtils().stop();
    }

}
