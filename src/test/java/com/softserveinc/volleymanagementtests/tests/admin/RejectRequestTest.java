package com.softserveinc.volleymanagementtests.tests.admin;

import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.admin.adminmainpage.AdminMainPage;
import com.softserveinc.volleymanagementtests.pages.login.LoginPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.TournamentsPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.specification.TestConfig;
import com.softserveinc.volleymanagementtests.tools.ReadMail;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.UUID;

/**
 * Test is checking the functionality of admin reject request function.
 *
 * @author Ivan Podorozhnyi
 */

@Features("Admin")
@Stories("RejectRequest")
public class RejectRequestTest {

    private Specification specification;

    @BeforeClass
    public final void loginWithCurrentProfile() {
        new LoginPage().logInWithCurrentProfile();
        specification = new Specification();

        new TournamentsPage()
                .openTournamentDetforUpcomTrnmtName("Big Game")
                .clickApplyForTournament()
                .chooseTeamByIndexInDropdown(1)
                .checkAgreeCheckbox()
                .clickAcceptButton();
    }

    @Test
    public final void adminRejectRequest() throws InterruptedException, MessagingException, IOException {

        new WebDriverUtils().load(new PageUrls().ADMIN_MAIN_PAGE);

        String expectedRejectMessage = UUID.randomUUID().toString();

        new AdminMainPage()
                .clickRequests()
                .clickApplyTeamForTrnmentRequests()
                .clickDeclineButton(1)
                .setReasonForDecline(expectedRejectMessage)
                .clickDeclineButton();

        String actualRejectMessage = new ReadMail()
                .readMailContent(expectedRejectMessage
                        , new TestConfig().getGoogleUser()
                        , new TestConfig().getGooglePass());

        specification.forThe(actualRejectMessage)
                .valueContains(expectedRejectMessage)
                .next()
                .check();
    }

    @AfterTest
    public void tearDown() {
        new WebDriverUtils().stop();
    }
}
