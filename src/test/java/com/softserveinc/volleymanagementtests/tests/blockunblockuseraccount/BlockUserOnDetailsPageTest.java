package com.softserveinc.volleymanagementtests.tests.blockunblockuseraccount;

import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.admin.adminmainpage.AdminMainPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class BlockUserOnDetailsPageTest {
    @BeforeMethod
    public void setUp() {
        new WebDriverUtils().load(new PageUrls().ADMIN_MAIN_PAGE);
    }
    
    @AfterTest
    public void tearDown() {
//        new WebDriverUtils().stop();
    }
    
    @Features("Block/Unblock user account")
    @Stories("Block user on Details page")
    @Issue("https://volleymanagement.visualstudio.com/VolleyMgmt%20-%20BS/_backlogs/Iteration/DP-103/Sprint%204?_dialog=workitem")
    @Severity(SeverityLevel.NORMAL)
    @Test
    public void BlockUserOnDetailsPage() {
        String blockedUser = "Aleksandr Shvets";
        String expectedResult = "Unblock";


        
        new Specification()
                .forThe(new AdminMainPage()
                        .clickAllUsers()
                        .clickUserDetails(blockedUser)
                        .clickOnBlockButton()
                        .getBlockButton())
                .textMatch(expectedResult)
                .next()
                .check();
    }
}
