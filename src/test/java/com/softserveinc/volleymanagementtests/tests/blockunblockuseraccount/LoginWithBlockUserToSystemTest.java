package com.softserveinc.volleymanagementtests.tests.blockunblockuseraccount;

import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class LoginWithBlockUserToSystemTest {
    @BeforeMethod
    public void setUp() {
        new WebDriverUtils().load(new PageUrls().BASE_URL);
    }
    
    @AfterTest
    public void tearDown() {
        new WebDriverUtils().stop();
    }
    
    @Features("Block/Unblock user account")
    @Stories("Login with bloked user to system")
    @Issue("https://volleymanagement.visualstudio.com/VolleyMgmt%20-%20BS/_backlogs/Iteration/DP-103/Sprint%204?_dialog=workitem")
    @Severity(SeverityLevel.NORMAL)
    @Test
    public void LoginWithBlockUserToSystem() {
        String expectedResult = "Sorry, you are blocked because some security issues." +
                " Please, contact system administrator";

        new Specification()
                .forThe(new MainPage()
                        .clickUserMenu()
                        .clickLogin()
                        .loginWithBlockedUser()
                        .getErrorMessageOnBlockedPage())
                .textMatch(expectedResult)
                .next()
                .check();

        
    }
}
