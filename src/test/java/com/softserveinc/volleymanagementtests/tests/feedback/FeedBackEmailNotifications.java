package com.softserveinc.volleymanagementtests.tests.feedback;

import com.softserveinc.volleymanagementtests.dal.models.FeedbackDal;
import com.softserveinc.volleymanagementtests.dal.repositories.FeedbackRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.feedback.FeedBackPage;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.specification.TestConfig;
import com.softserveinc.volleymanagementtests.testdata.feedback.FeedBackTestData;
import com.softserveinc.volleymanagementtests.tools.ReadMail;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by shvets on 18.03.2017.
 */

public class FeedBackEmailNotifications {
    private Specification specification;
    private FeedbackRepository feedbackRepository;
    private FeedbackDal testFeedback;

    @BeforeMethod
    public void setUp() {
        new WebDriverUtils().load(new PageUrls().BASE_URL);
        new MainPage().clickUserMenu().clickLogin().clickGoogleLoginButton().clickTournamentsPage();
        new WebDriverUtils().load(new PageUrls().TURNAMENTS_URL);
        specification = new Specification();
        testFeedback = FeedBackTestData.getValidUserFeedback();
        feedbackRepository = new FeedbackRepository();
    }

    @AfterMethod
    public void tearDown() {
        new WebDriverUtils().stop();
        deleteFeedBackFromDb(testFeedback);
    }

    private void deleteFeedBackFromDb(FeedbackDal feedbackDal) {
        feedbackRepository.deleteById(feedbackDal.getId());
    }

    @Features("Email notifications")
    @Stories("Email notification User after submitting feedback on site")
    @Issue("https://volleymanagement.visualstudio.com/VolleyMgmt%20-%20BS/_workitems?id=734&_a=edit&triage=false")
    @Severity(SeverityLevel.NORMAL)
    @Test
    public void userFeedBackMailReceiving() {
        FeedBackPage feedBackPage = new MainPage().clickFeedBackLink();
        feedBackPage.setFeedbackDataAndSend(testFeedback);
        while(true){
            if (feedBackPage.getFeedBackResponse().isDisplayed()) {
                break;
            }
        }
        String KEYWORD = "Thank you for your feedback!";
        specification
                .forThe(new ReadMail().readMailContent(
                        KEYWORD,
                        new TestConfig().getGoogleUserMail(),
                        new TestConfig().getGoogleuserPass()))
                .valueMatch(KEYWORD)
                .next()
                .check();
    }

    @Features("Email notifications")
    @Stories("Email notification Admin after submitting feedback on site by user")
    @Issue("https://volleymanagement.visualstudio.com/VolleyMgmt%20-%20BS/_workitems?id=734&_a=edit&triage=false")
    @Severity(SeverityLevel.NORMAL)
    @Test
    public void adminFeedBackMailReceiving() {
        FeedBackPage feedBackPage = new MainPage().clickFeedBackLink();
        feedBackPage.setFeedbackDataAndSend(testFeedback);
        while(true){
            if (feedBackPage.getFeedBackResponse().isDisplayed()) {
                break;
            }
        }
        String key = testFeedback.getContent();
        specification
                .forThe(new ReadMail().readMailContent(
                        key,
                        new TestConfig().getGoogleUser(),
                        new TestConfig().getGooglePass()))
                .valueContains(key)
                .next()
                .check();
    }
}