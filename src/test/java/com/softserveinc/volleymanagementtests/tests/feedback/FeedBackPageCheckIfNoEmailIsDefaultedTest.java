package com.softserveinc.volleymanagementtests.tests.feedback;

import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.feedback.FeedBackPage;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by stas on 18.03.2017.
 */
public class FeedBackPageCheckIfNoEmailIsDefaultedTest {
    
    private Specification specification;
    
    @BeforeMethod
    public void setUp() {
        new WebDriverUtils().load(new PageUrls().BASE_URL);
        specification = new Specification();
        new MainPage().clickFeedBackLink();
    }
    
    @AfterMethod
    public void tearDown() {
        new WebDriverUtils().stop();
    }

    @Stories("Give feedback")
    @Description("In this test case we make sure that no email is defaulted if user is not logged in")
    @Severity(SeverityLevel.NORMAL)
    @Issue("https://volleymanagement.visualstudio.com/VolleyMgmt%20-%20BS/_workitems?id=832&_a=edit")
    @Test
    public void feedBackElementsTest() {
        FeedBackPage feedBackPage = new FeedBackPage();
        specification.forThe(feedBackPage.getEmail())
                .textMatch("")
                .next().
                check();
        
    }
}
