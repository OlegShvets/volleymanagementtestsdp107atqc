package com.softserveinc.volleymanagementtests.tests.feedback;

import com.softserveinc.volleymanagementtests.dal.models.FeedbackDal;
import com.softserveinc.volleymanagementtests.dal.repositories.FeedbackRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.feedback.FeedBackPage;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.feedback.FeedBackTestData;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by stas on 16.03.2017.
 */
public class FeedbackCreateAndCheckInDbTest {

    private Specification specification;
    private FeedbackRepository feedbackRepository;
    private FeedbackDal testFeedback;


    @BeforeMethod
    public void setUp() {
        new WebDriverUtils().load(new PageUrls().BASE_URL);
        specification = new Specification();
        testFeedback = new FeedBackTestData().getValidNotReadFeedback();
        feedbackRepository = new FeedbackRepository();
    }

    @AfterMethod
    public void tearDown() {
        new WebDriverUtils().stop();
        deleteFeedBackFromDb(testFeedback);

    }

    @Stories("Give feedback")
    @Description("In this test case we make sure that user can post feedback and it's created in database")
    @Severity(SeverityLevel.NORMAL)
    @Issue("https://volleymanagement.visualstudio.com/VolleyMgmt%20-%20BS/_workitems?id=831&_a=edit")
    @Test
    public void feedBackCreateTest() {
        FeedBackPage feedBackPage = new MainPage()
                .clickFeedBackLink();
        feedBackPage.setFeedbackDataAndSend(testFeedback);
        specification
                .forThe(testFeedback.equals(getFeedbackFromDb()))
                .next()
                .check();

    }


    private FeedbackDal getFeedbackFromDb() {
        return feedbackRepository
                .getByEmail(testFeedback.getUserEmail())
                .get(0);
    }

    private void deleteFeedBackFromDb(FeedbackDal feedbackDal) {
        feedbackRepository.deleteById(feedbackDal.getId());
    }


}
