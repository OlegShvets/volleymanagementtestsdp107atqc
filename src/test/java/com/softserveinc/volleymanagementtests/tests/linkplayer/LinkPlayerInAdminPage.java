package com.softserveinc.volleymanagementtests.tests.linkplayer;

import com.softserveinc.volleymanagementtests.dal.models.PlayerDal;
import com.softserveinc.volleymanagementtests.dal.models.UserDal;
import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.dal.repositories.UserRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.admin.adminmainpage.AdminMainPage;
import com.softserveinc.volleymanagementtests.pages.admin.link.LinkPlayerRequestsPage;
import com.softserveinc.volleymanagementtests.pages.admin.userdetailspage.UserDetailsPage;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.testdata.user.User;
import com.softserveinc.volleymanagementtests.testdata.user.UserTestData;
import com.softserveinc.volleymanagementtests.testdata.user.mappers.UserToDal;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import java.sql.SQLException;

/**
 * Created by stas on 21.03.2017.
 */
public class LinkPlayerInAdminPage {

    private WebDriverUtils webDriverUtils;
    private Player playerToConfirm;
    private Player playerToDecline;
    private User userWithLink;
    private User userWithNoLink;


    @BeforeMethod
    public void setUp()  {
        prepareDb();
        webDriverUtils = new WebDriverUtils();
        webDriverUtils.load(new PageUrls().BASE_URL);
        new MainPage()
                .clickUserMenu()
                .clickLogin()
                .clickGoogleLoginButton();
        webDriverUtils.load(new PageUrls().ADMIN_MAIN_PAGE);
    }

    @AfterMethod
    public void tearDown() {
       webDriverUtils.stop();
    }

    private void prepareDb() {
        playerToConfirm = new PlayerTestData().getValidPlayer();
        playerToDecline = new PlayerTestData().getValidPlayer();
        PlayerRepository playerRepository = new PlayerRepository();
        UserRepository userRepo = new UserRepository();
        PlayerDal playerConfDal = playerRepository.create(new PlayerToDal()
                .map(playerToConfirm));
        PlayerDal playerDecDal = playerRepository.create(new PlayerToDal()
                .map(playerToDecline));
        userWithLink = new UserTestData().getNonBlockedUser();
        userWithNoLink = new UserTestData().getBlockedUser();
        int userConfKey = userRepo.create(new UserToDal().map(userWithLink));
        int userNoConfKey = userRepo.create(new UserToDal().map(userWithNoLink));
        UserDal userConfDal = userRepo.getById(userConfKey);
        UserDal userNoConFDal = userRepo.getById(userNoConfKey);
        playerRepository.setPlayerLinkRequest(userConfDal.getId(), playerConfDal.getId());
        playerRepository.setPlayerLinkRequest(userNoConFDal.getId(), playerDecDal.getId());
    }


    @Stories("Link player to user")
    @Description("In this test case we make sure that admin is able to confirm or decline " +
            "player link request, and after linking player information is present in user" +
            "details window")
    @Severity(SeverityLevel.NORMAL)
    @Issue("https://volleymanagement.visualstudio.com/VolleyMgmt%20-%20BS/_workitems?id=893&_a=edit")
    @Test
    public void testUserPresenseInThePage() {
       LinkPlayerRequestsPage linkPage = new AdminMainPage()
                .clickRequests()
                .clickPlayerLinkRequests();
        linkPage.confirmLinkRequestForPlayerName(playerToConfirm.getLastName())
                .declineLinkRequestForPlayerName(playerToDecline.getLastName());
        Specification specification = new Specification();
        specification
                .forThe(linkPage.isPlayerPresentInTheList(playerToConfirm.getLastName()))
                .isFalse()
                .next()
                .forThe(linkPage.isPlayerPresentInTheList(playerToDecline.getLastName()))
                .next();

        webDriverUtils.load(new PageUrls().ADMIN_MAIN_PAGE);
        UserDetailsPage userDetails = new AdminMainPage()
                .clickAllUsers()
                .clickUserDetails(userWithLink.getFullName());

        specification
                .forThe(userDetails.getLinkedPlayerName())
                .textMatch(playerToConfirm.getFirstName())
                .next()
                .forThe(userDetails.getLinkedPlayerLastName())
                .textMatch(playerToConfirm.getLastName())
                .next()
                .forThe(userDetails.getLinkedPlayerYearOfBirth())
                .textMatch(playerToConfirm.getBirthYear())
                .next()
                .forThe(userDetails.getLinkedPlayerHeight())
                .textMatch(playerToConfirm.getHeight())
                .next()
                .forThe(userDetails.getLinkedPlayerWeight())
                .textMatch(playerToConfirm.getWeight());

        webDriverUtils.load(new PageUrls().ADMIN_MAIN_PAGE);
                userDetails = new AdminMainPage()
                .clickAllUsers()
                .clickUserDetails(userWithNoLink.getFullName());

                specification.forThe(userDetails.getNoLinkedPlayers())
                        .textMatch("No player assigned to user");

                specification.check();
    }
}
