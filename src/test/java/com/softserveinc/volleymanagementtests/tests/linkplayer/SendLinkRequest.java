package com.softserveinc.volleymanagementtests.tests.linkplayer;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.player.PlayerDetailsPage;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import java.sql.SQLException;

/**
 * Created by stas on 21.03.2017.
 */
public class SendLinkRequest {

    private WebDriverUtils webDriverUtils;
    private Player player;


    @BeforeMethod
    public void setUp()  {
        player = new PlayerTestData().getValidPlayer();
        new PlayerRepository().create(new PlayerToDal().map(player));
        webDriverUtils = new WebDriverUtils();
        webDriverUtils.load(new PageUrls().BASE_URL);


    }

    @AfterMethod
    public void tearDown() {
        webDriverUtils.stop();
        new PlayerRepository()
                .deletePlayer(new PlayerToDal().map(player));
    }


    @Stories("Link player to user")
    @Description("In this test case we make sure that there is Link to my account button" +
            "in the player details page and proper message is displayed after it's pressed")
    @Severity(SeverityLevel.NORMAL)
    @Issue("https://volleymanagement.visualstudio.com/VolleyMgmt%20-%20BS/_workitems?id=893&_a=edit")
    @Test
    public void isLinkButtonPresentAndClickable() {
       PlayersListPage playersListPage = new MainPage()
                .clickUserMenu()
                .clickLogin()
                .clickGoogleLoginButton()
                .clickPlayerListPage();
      PlayerDetailsPage playersDetailsPage = playersListPage
               .searchForPlayer(player.getFirstName())
                .showPlayerDetails(player.getLastName() + " " + player.getFirstName());

       Specification specification = new Specification();
       specification
               .forThe(playersDetailsPage.getLinkButton())
               .textMatch("Link to my account")
               .next()
               .forThe(playersDetailsPage
                       .clickLinkButton()
                       .getAdminApprovalText())
               .textMatch(String.format("After admin approval " +
                       "you will be linked with %s %s",
                       player.getFirstName(), player.getLastName()));

       specification.check();

    }
}
