package com.softserveinc.volleymanagementtests.tests.player.pagination;

import com.softserveinc.volleymanagementtests.dal.models.PlayerDal;
import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.*;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.ArrayList;
import java.util.List;

/**
 * Karabaza Anton on 15.03.2016.
 */
@Features("Player")
@Stories("Pagination")
public class PaginationTest {
    private PlayersListPage playersListPage;
    private List<Player> createdTestPlayers;
    
    
    @BeforeClass
    public void setUp() {
        createdTestPlayers = new ArrayList<>();
        new WebDriverUtils().load(new PageUrls().BASE_URL);
        playersListPage = new MainPage()
                .clickPlayerListPage();
    }
    
    @BeforeMethod
    public void createPlayersIfNeeded() throws Exception {
        if (playersListPage.getPaginator().size() < 2) {
            PlayerRepository playerRepository = new PlayerRepository();
            for (int i = 0; i < 11; i++) {
                Player testPlayer = new PlayerTestData().getValidPlayer();
                createdTestPlayers = new ArrayList<>();
                createdTestPlayers.add(testPlayer);
                PlayerDal playerToCreate = new PlayerToDal().map(testPlayer);
                playerRepository.create(playerToCreate);
            }
            new WebDriverUtils().refresh();
            playersListPage = new PlayersListPage();
        }
    }
    
    @AfterMethod
    public void deletePlayersIfNeeded() throws Exception {
        if (! createdTestPlayers.isEmpty()) {
            PlayerRepository playerRepo = new PlayerRepository();
            PlayerToDal playerToDalMap = new PlayerToDal();
            for (Player player : createdTestPlayers) {
                playerRepo.deletePlayer(playerToDalMap.map(player));
            }
        }
        
    }
    
    
    @AfterTest
    public final void tearDown() {
        new WebDriverUtils().stop();
    }
    
    @Test
    public void testPagination() {
        Specification specification = new Specification();
        
        specification.forThe(playersListPage.goToPage(2))
                .matchPageIndex("2").next().
                forThe(playersListPage.goToPage(3))
                .matchPageIndex("3").next().
                forThe(playersListPage.goToPage(1))
                .matchPageIndex("1").next();
        
        specification.check();
    }
}
