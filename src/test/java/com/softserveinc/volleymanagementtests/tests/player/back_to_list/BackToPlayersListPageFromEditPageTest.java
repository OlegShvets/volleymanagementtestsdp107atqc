package com.softserveinc.volleymanagementtests.tests.player.back_to_list;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.player.PlayerEditPage;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.sql.SQLException;

/**
 * @author Karabaza Anton on 20.03.2016.
 */
@Features("Player")
@Stories("Back link")
public class BackToPlayersListPageFromEditPageTest {
    
    PlayerEditPage playerEditPage;
    PlayersListPage playersListPage;
    
    @BeforeMethod
    public void setUp() {
        new WebDriverUtils().load(new PageUrls().PLAYERS_LIST_URL);
        playersListPage = new PlayersListPage();
        
        if (playersListPage.getCountOfPlayersOnCurrentPage() == 0) {
            new PlayerRepository().create(new PlayerToDal()
                    .map(new PlayerTestData().getValidPlayer()));
    
            new WebDriverUtils().refresh();
            playersListPage = new PlayersListPage();
        }
        
        playerEditPage = playersListPage.editRandomPlayer();
    }
    
    @AfterMethod
    public void tearDown() {
        new WebDriverUtils().stop();
    }
    
    @Test
    public void testBackLinkOnEditPage() {
        
        playersListPage = playerEditPage.returnBackToList();
        
        new Specification()
                .forThe(playersListPage)
                .matchURL(new PageUrls().PLAYERS_LIST_URL)
                .next()
                .check();
    }
}
