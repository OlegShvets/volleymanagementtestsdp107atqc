package com.softserveinc.volleymanagementtests.tests.player.create;

import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.player.PlayerCreatePage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Test based on TC "TC_InValid By BirthYear Player Creation".
 *
 * @author S.Tsyganovskiy
 */
@Features("Player")
@Stories("Create")
public class PlayerCreationWithInvalidBirthYearTest {
    @DataProvider
    public final Object[][] validDataProvider() {
        
        return new Object[][]{
                {new PlayerTestData()
                        .getInvalidPlayerByBirthYear(
                        Violations.NEGATIVE_NUMBER),
                        new ErrorMessages().getPlayerErrorMessage("Player birth year range error")},
                {new PlayerTestData()
                        .getInvalidPlayerByBirthYear(
                        Violations.MAX_VALUE_VIOLATED),
                        new ErrorMessages().getPlayerErrorMessage("Player birth year range error")},
                {new PlayerTestData()
                        .getInvalidPlayerByBirthYear(
                        Violations.MIN_VALUE_VIOLATED),
                        new ErrorMessages().getPlayerErrorMessage("Player birth year range error")},
                {new PlayerTestData()
                        .getInvalidPlayerByBirthYear(
                        Violations.CONTAINS_LETTERS),
                        new ErrorMessages().getPlayerErrorMessage("Player birth year invalid input error")},
                {new PlayerTestData()
                        .getInvalidPlayerByBirthYear(
                        Violations.CONTAIN_NOT_ALLOWED_CHARACTERS),
                        new ErrorMessages().getPlayerErrorMessage("Player birth year invalid input error")}};
    }
    
    @Test(dataProvider = "validDataProvider")
    public void testPlayerCreationWithInvalidBirthYear(Player player,
                                                       String errorMessage) throws Exception {
        
        PlayerCreatePage playerCreatePage = new PlayerCreatePage()
                .setAllPlayerFields(player)
                .submitButtonCreate();
        
        
      new Specification()
                .forThe(playerCreatePage)
                .isAllFieldsFilledInCorrectly(player)
                .next()
                .forThe(playerCreatePage.getErrorInputBirthYearMessage())
                .textMatch(errorMessage)
                .next()
                .check();
    }
    
    @BeforeMethod
    public void setUp() {
        new WebDriverUtils().load(new PageUrls().PLAYER_CREATE_URL);
    }
    
    @AfterTest
    public void tearDownFinal() {
        new WebDriverUtils().stop();
    }
}
