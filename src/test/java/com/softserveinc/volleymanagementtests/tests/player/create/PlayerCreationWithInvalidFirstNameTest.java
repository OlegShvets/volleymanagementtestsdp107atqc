package com.softserveinc.volleymanagementtests.tests.player.create;

import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.player.PlayerCreatePage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Player")
@Stories("Create")
public class PlayerCreationWithInvalidFirstNameTest {
    
    @DataProvider
    public final Object[][] validDataProvider() {
        
        return new Object[][]{
                {new PlayerTestData()
                        .getInvalidPlayerByFirstName(
                        Violations.EMPTY_FIELD),
                        new ErrorMessages().getPlayerErrorMessage("Player first name empty error")},
                {new PlayerTestData()
                        .getInvalidPlayerByFirstName(
                        Violations.MAX_LENGTH_VIOLATED),
                        new ErrorMessages().getPlayerErrorMessage("Player first name length error")},
                {new PlayerTestData()
                        .getInvalidPlayerByFirstName(
                        Violations.CONTAINS_NUMBERS),
                        new ErrorMessages().getPlayerErrorMessage("Player first name must be letters only error")},
                {new PlayerTestData()
                        .getInvalidPlayerByFirstName(
                        Violations.CONTAIN_NOT_ALLOWED_CHARACTERS),
                        new ErrorMessages().getPlayerErrorMessage("Player first name must be letters only error")}};
    }
    
    @Test(dataProvider = "validDataProvider")
    public void testPlayerCreationWithInvaliFirstName(Player player,
                                                      String errorMessage) throws Exception {
        
        PlayerCreatePage playerCreatePage = new PlayerCreatePage()
                .setAllPlayerFields(player)
                .submitButtonCreate();
        
        new Specification()
                .forThe(playerCreatePage)
                .isAllFieldsFilledInCorrectly(player)
                .next()
                .forThe(playerCreatePage.getErrorInputFirstNameMessage())
                .textMatch(errorMessage)
                .next()
                .check();
    }
    
    @BeforeMethod
    public void setUp() {
        new WebDriverUtils().load(new PageUrls().PLAYER_CREATE_URL);
    }
    
    @AfterTest
    public void tearDownFinal() {
        new WebDriverUtils().stop();
    }
}
