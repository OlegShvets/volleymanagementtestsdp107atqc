package com.softserveinc.volleymanagementtests.tests.player.create;

import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.player.PlayerCreatePage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Test based on TC "TC_InValid By Last Name Player Creation".
 *
 * @author S.Tsyganovskiy
 */
@Features("Player")
@Stories("Create")
public class PlayerCreationWithInvalidLastNameTest {
    @DataProvider
    public final Object[][] validDataProvider() {
        
        return new Object[][]{
                {new PlayerTestData()
                        .getInvalidPlayerByLastName(
                        Violations.EMPTY_FIELD),
                        new ErrorMessages().getPlayerErrorMessage("Player last name empty error")},
                {new PlayerTestData()
                        .getInvalidPlayerByLastName(
                        Violations.MAX_LENGTH_VIOLATED),
                        new ErrorMessages().getPlayerErrorMessage("Player last name length error")},
                {new PlayerTestData()
                        .getInvalidPlayerByLastName(
                        Violations.CONTAINS_NUMBERS),
                        new ErrorMessages().getPlayerErrorMessage("Player last name must be letters only error")},
                {new PlayerTestData()
                        .getInvalidPlayerByLastName(
                        Violations.CONTAIN_NOT_ALLOWED_CHARACTERS),
                        new ErrorMessages().getPlayerErrorMessage("Player last name must be letters only error")}};
    }
    
    @Test(dataProvider = "validDataProvider")
    public void testPlayerCreationWithInvalidLastName(Player player,
                                                      String errorMessage) throws Exception {
        
        PlayerCreatePage playerCreatePage = new PlayerCreatePage()
                .setAllPlayerFields(player)
                .submitButtonCreate();
        
       new Specification()
                .forThe(playerCreatePage)
                .isAllFieldsFilledInCorrectly(player)
                .next()
                .forThe(playerCreatePage.getErrorInputLastNameMessage())
                .textMatch(errorMessage)
                .next()
                .check();
    }
    
    @BeforeMethod
    public void setUp() {
        new WebDriverUtils().load(new PageUrls().PLAYER_CREATE_URL);
    }
    
    @AfterTest
    public void tearDownFinal() {
        new WebDriverUtils().stop();
    }
}
