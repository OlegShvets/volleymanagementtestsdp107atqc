package com.softserveinc.volleymanagementtests.tests.player.create;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.player.PlayerCreatePage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Test based on TC "TC_ Valid Player Creation".
 *
 * @author S.Tsyganovskiy
 */
@Features("Player")
@Stories("Create")
public class PlayerCreationWithValidDataTest {
    
    @DataProvider
    public Object[][] validDataProvider() {
        int validPlayersCount = new PlayerTestData().getValidPlayersList().size();
        Player[] validPlayers = new Player[validPlayersCount];
        new PlayerTestData().getValidPlayersList().toArray(validPlayers);
        
        Object[][] data = new Object[validPlayersCount][1];
        
        for (int i = 0; i < validPlayersCount; i++) {
            data[i][0] = validPlayers[i];
        }
        return data;
    }
    
    @Test(dataProvider = "validDataProvider")
    public void testValidPlayerCreation(Player player) throws Exception {
        
        PlayerCreatePage playerCreatePage = new PlayerCreatePage()
                .setAllPlayerFields(player);
        
        new Specification()
                .forThe(playerCreatePage)
                .isAllFieldsFilledInCorrectly(player)
                .next()
                .check();
        
        playerCreatePage.submitButtonCreate();
        
        Assert.assertTrue(new PlayerRepository()
                .isPlayerExistsInDataBase(new PlayerToDal().map(player)));
    }
    
    @BeforeMethod
    public void setUp() {
        new WebDriverUtils().load(new PageUrls().PLAYER_CREATE_URL);
    }
    
    @AfterTest
    public void tearDownFinal() {
        new WebDriverUtils().stop();
    }
}
