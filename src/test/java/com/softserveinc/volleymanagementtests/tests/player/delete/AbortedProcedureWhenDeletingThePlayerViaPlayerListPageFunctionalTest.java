package com.softserveinc.volleymanagementtests.tests.player.delete;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tests.player.delete.basebeforeafter.DeletePlayerBaseTest;
import org.openqa.selenium.NoAlertPresentException;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * testing the delete player functionality via the ui tools.
 * aborted on half-way.
 */
@Features("Player")
@Stories("Delete")
public class AbortedProcedureWhenDeletingThePlayerViaPlayerListPageFunctionalTest
        extends DeletePlayerBaseTest {
    @Test
    public final void testAbortedProcedureWhenDeletingThePlayerViaPlayerListPageFunctional()
            throws Exception {
        String fullName = playerDal.getLastName() + " " + playerDal.getFirstName();
        PlayersListPage playersListPage = new PlayersListPage();
        playersListPage = playersListPage
                .searchForPlayer(fullName);
        playersListPage.clickDeleteLink(playerDal.getId());
        while (true) {
            try {
                com.softserveinc.volleymanagementtests.tools.controls.contracts.Alert alert = playersListPage.getAlert();
                alert.dismiss();
            } catch (NoAlertPresentException e) {
                break;
            }
        }
        
        new Specification()
                // there should not be any Alert on the page
                .forThe(playersListPage)
                .ifUnexpectedAlertPresent()
                .next()
                
                // the player should be displayed on the page
                .forThe(new PlayerRepository().getById(playerDal.getId()).equals(playerDal))
                .isTrue()
                .next()
                .check();
    }
}
