package com.softserveinc.volleymanagementtests.tests.player.delete;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tests.player.delete.basebeforeafter.DeletePlayerBaseTest;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * testing the delete player functionality via the ui tools.
 * end to end.
 */
@Features("Player")
@Stories("Delete")
public class CompletedProcedureWhenDeletingThePlayerViaPlayerListPageFunctionalTest
        extends DeletePlayerBaseTest {
    
    @Test
    public final void testCompletedProcedureWhenDeletingThePlayerViaPlayerListPageFunctional()
            throws Exception {
        String fullName = playerDal.getLastName() + " " + playerDal.getFirstName();
        PlayersListPage playersListPage = new PlayersListPage();
        playersListPage = playersListPage
                .searchForPlayer(fullName);
        playersListPage.clickDeleteLink(playerDal.getId());
        
        while (true) {
            try {
                Alert alert = playersListPage.getAlert();
                alert.accept();
                Thread.sleep(5000);
                Alert alert1 = playersListPage.getAlert();
                alert1.accept();
            } catch (NoAlertPresentException e) {
                break;
            }
        }
        
        new Specification()
                // there should not be any Alert on the page
                .forThe(playersListPage)
                .ifUnexpectedAlertPresent() // floating bug here // shown when run with debugger (stepover)
                // the page index should be the same as the deleted player have been at.
                .isCountOfPlayersMatches(0)
                //.matchPageIndex(playerPageIndexAt)
                .next()
                
                // there should not exist the player in the db
                // if we use getById-the exception will be thrown
                
                .forThe(new PlayerRepository().getAll().contains(playerDal))
                .isFalse()
                .next()
                .check();
    }
}
