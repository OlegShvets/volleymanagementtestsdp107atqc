package com.softserveinc.volleymanagementtests.tests.player.delete.alerts;

import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.AlertMessages;
import com.softserveinc.volleymanagementtests.tests.player.delete.basebeforeafter.DeletePlayerBaseTest;
import com.softserveinc.volleymanagementtests.tools.controls.AlertImplementDec;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import javax.accessibility.AccessibleKeyBinding;

/**
 * validating the Alert popup which appears during the deleting the player
 * via ui from the PlayersListPage.
 */
@Features("Player")
@Stories("Delete")
public class AlertPopupWhenDeletingThePlayerViaPlayerListPageFunctionalTest
        extends DeletePlayerBaseTest {
    @Test
    public final void testAlertPopupWhenDeletingThePlayerViaPlayerListPageFunctional() {
        
        String fullName = playerDal.getLastName() + " " + playerDal.getFirstName();
        PlayersListPage playersListPage = new PlayersListPage();
        playersListPage = playersListPage
                .searchForPlayer(fullName);
        
        playersListPage.clickDeleteLink(playerDal.getId());
        while (true) {
            try {
                Alert alert = new AlertImplementDec();
                //alert = playersListPage.getAlert();
             new Specification()
                        .forThe(alert)
                        .textMatch(String.format(
                                new AlertMessages().getAlertMessage("Do you want to delete player"),
                                playerDal.getLastName().concat(" ").concat(playerDal
                                        .getFirstName())))
                        .next().check();
                alert.dismiss();
            } catch (NoAlertPresentException e) {
                break;
            }
        }
    }
}
