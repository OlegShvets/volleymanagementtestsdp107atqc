package com.softserveinc.volleymanagementtests.tests.player.delete.alerts;

import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.AlertMessages;
import com.softserveinc.volleymanagementtests.tests.player.delete.basebeforeafter.DeletePlayerBaseTest;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Player")
@Stories("Delete")
public class ConfirmPopupWhenDeletingThePlayerViaPlayerListPageFunctionalTest
        extends DeletePlayerBaseTest {
    
    @Test
    public final void testConfirmPopupWhenDeletingThePlayerViaPlayerListPageFunctional() {
        String fullName = playerDal.getLastName() + " " + playerDal.getFirstName();
        PlayersListPage playersListPage = new PlayersListPage();
        playersListPage = playersListPage
                .searchForPlayer(fullName);
        
        playersListPage.clickDeleteLink(playerDal.getId());
        while (true) {
            try {
                Alert alert = playersListPage.getAlert();
                alert.accept();
                Thread.sleep(5000);
                Alert alert1 = playersListPage.getAlert();
                alert1.accept();
                
                // element the confirm modal dialog to validate it
                new Specification()
                        .forThe(alert1)
                        .textMatch(new AlertMessages().getAlertMessage("Player was deleted"))
                        .next().check();
                alert.dismiss();
            } catch (NoAlertPresentException e) {
                break;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

