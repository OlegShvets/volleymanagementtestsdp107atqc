package com.softserveinc.volleymanagementtests.tests.player.delete.basebeforeafter;

import com.softserveinc.volleymanagementtests.dal.models.PlayerDal;
import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.login.LoginPage;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

/**
 * preconditions and after test actions on deleting the player.
 */
public class DeletePlayerBaseTest {
    
    protected PlayersListPage playersListPage;
    protected PlayerDal playerDal;
    
    @BeforeClass()
    public final void setUp() throws Exception {
        LoginPage loginPage = new LoginPage();
        loginPage.logInWithCurrentProfile();
        
        // set the player to db
        playerDal = new PlayerRepository()
                .create(new PlayerToDal().map(new PlayerTestData().getValidPlayer()));
        
        new WebDriverUtils().load(new PageUrls().TURNAMENTS_URL);
        new WebDriverUtils().load(new PageUrls().PLAYERS_LIST_URL);
        playersListPage = new PlayersListPage();
    }
    @AfterClass
    public final void tearDown(){
        new WebDriverUtils().stop();
    }
}
