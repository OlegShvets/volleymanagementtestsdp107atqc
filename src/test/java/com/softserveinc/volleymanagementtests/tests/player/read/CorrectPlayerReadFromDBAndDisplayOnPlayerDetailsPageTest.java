package com.softserveinc.volleymanagementtests.tests.player.read;

import com.softserveinc.volleymanagementtests.dal.models.PlayerDal;
import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Karabaza Anton on 06.03.2016.
 */
@Features("Player")
@Stories("Read")
public class CorrectPlayerReadFromDBAndDisplayOnPlayerDetailsPageTest {
    
    private PlayersListPage playersListPage;
    private PlayerDal firstPlayer;
    
    @BeforeMethod
    public void setUp() throws Exception {
        PlayerRepository playerRepository = new PlayerRepository();
        firstPlayer = new PlayerDal.PlayerDalBuilder()
                .id(null)
                .firstName("M")
                .lastName("M")
                .birthYear(1980)
                .height(180)
                .weight(95)
                .build();
        playerRepository.create(firstPlayer);
        playersListPage = new PlayersListPage();
        playersListPage.searchForPlayer("M M");
    }
    
    @Test
    public void testCorrectPlayerRead() {
        new Specification()
                .forThe(playersListPage)
                .isPlayerDisplayedOnListAfterSearhMatch("M M")
                .next()
                .check();
    }
}