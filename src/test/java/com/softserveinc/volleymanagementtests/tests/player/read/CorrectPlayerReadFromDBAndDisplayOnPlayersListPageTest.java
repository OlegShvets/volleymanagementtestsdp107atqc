package com.softserveinc.volleymanagementtests.tests.player.read;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Karabaza Anton on 02.03.2016.
 */
@Features("Player")
@Stories("Read")
public class CorrectPlayerReadFromDBAndDisplayOnPlayersListPageTest {
    
    private PlayersListPage playersListPage;
    private Player playerValid;
    
    
    @BeforeMethod
    public void setUp() throws Exception {
        playerValid = new PlayerTestData().getValidPlayer();
    
        new WebDriverUtils().load(new PageUrls().PLAYERS_LIST_URL);
        playersListPage = new PlayersListPage();
        new PlayerRepository().create(new PlayerToDal().map(playerValid));
        String fullName = (playerValid.getLastName() + " " + playerValid
                .getFirstName());
        
        playersListPage.searchForPlayer(fullName);
    }
    
    @AfterTest
    public final void tearDown() {
        new WebDriverUtils().stop();
    }
    
    @Test
    public void testCorrectPlayerRead() {
        String fullName = (playerValid.getLastName() + " " + playerValid
                .getFirstName());
        
        new Specification()
                .forThe(playersListPage)
                .isPlayerDisplayedOnListAfterSearhMatch(fullName)
                .next()
                .check();
    }
}
