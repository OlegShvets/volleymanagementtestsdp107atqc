package com.softserveinc.volleymanagementtests.tests.player.search;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.sql.SQLException;

/**
 * @author J.Bodnar
 */
@Features("Player")
@Stories("Search")
public class PlayerSearchTest {
    
    @BeforeClass
    public void setUp()  {
        PlayerRepository playerRepository = new PlayerRepository();
        for (Player player : new PlayerTestData().getPlayersListForSearching()) {
            playerRepository.create(new PlayerToDal().map(player));
        }
    
        new WebDriverUtils().load(new PageUrls().BASE_URL);
        new MainPage()
                .clickUserMenu()
                .clickLogin()
                .clickGoogleLoginButton()
                .clickPlayerListPage();
        
    }
    
    @AfterClass
    public void tearDown(){
        PlayerRepository playerRepository = new PlayerRepository();
        for (Player player : new PlayerTestData().getPlayersListForSearching()) {
            playerRepository.deletePlayer( new PlayerToDal().map(player));
        }
        new WebDriverUtils().stop();
    }
    
    @DataProvider
    private Object[][] textForSearchingProvider() {
        
        return new Object[][]{
                {"search", 3}, {"SEARCH", 3}, {"Sear", 3},
                {"earc", 4}, {"TEXT", 3}, {"text", 3},
                {"Tex", 3}, {"exT", 4}, {"Search Text", 2},
                {"searChText", 0}, {"sea ext", 0}, {"arch te", 2},
                {"sea rch", 0}, {"sefrtgarch", 0}};
    }
    
    @Test(dataProvider = "textForSearchingProvider")
    public final void testPlayerSearch(final String textForSearching, final int expRowsNumber) {
        Specification specification = new Specification();
        PlayersListPage playersListPage = new PlayersListPage();
        playersListPage.searchForPlayer(textForSearching);
        specification.forThe(playersListPage).isCountOfPlayersMatches(expRowsNumber);
        specification.check();
        
        
    }
    
}
