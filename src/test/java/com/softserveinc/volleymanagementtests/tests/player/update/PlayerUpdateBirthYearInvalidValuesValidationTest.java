package com.softserveinc.volleymanagementtests.tests.player.update;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.login.LoginPage;
import com.softserveinc.volleymanagementtests.pages.player.PlayerEditPage;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.*;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Test based on TC "TC4_PlayerUpdate_BirthYearInvalidValuesValidation".
 *
 * @author Danil Zhyliaiev
 */
@Features("Player")
@Stories("Update")
public class PlayerUpdateBirthYearInvalidValuesValidationTest {
    
    private PlayerEditPage playerEditPage;
    
    /**
     * DataProvider for test method containing players with preset invalid
     * birth year field and expected error messages.
     *
     * @return Player-String pairs as {@code Object[][]}.
     */
    @DataProvider
    private Object[][] invalidPlayerProvider() {
        return new Object[][]{
                {new PlayerTestData()
                        .getInvalidPlayerByBirthYear(Violations.MAX_VALUE_VIOLATED),
                        new ErrorMessages().getPlayerErrorMessage("Player birth year range error")},
                {new PlayerTestData()
                        .getInvalidPlayerByBirthYear(Violations.MIN_VALUE_VIOLATED),
                        new ErrorMessages().getPlayerErrorMessage("Player birth year range error")},
                {new PlayerTestData()
                        .getInvalidPlayerByBirthYear(Violations.CONTAINS_LETTERS),
                        new ErrorMessages().getPlayerErrorMessage("Player birth year invalid input error")},
                {new PlayerTestData().getInvalidPlayerByBirthYear(
                        Violations.CONTAIN_NOT_ALLOWED_CHARACTERS),
                        new ErrorMessages().getPlayerErrorMessage("Player birth year invalid input error")},
                {new PlayerTestData().getInvalidPlayerByBirthYear(Violations.NEGATIVE_NUMBER),
                        new ErrorMessages().getPlayerErrorMessage("Player birth year range error")},
                {new PlayerTestData().getInvalidPlayerByBirthYear(
                        Violations.NOT_INTEGER_NUMBER_WITH_COMMA),
                        new ErrorMessages().getPlayerErrorMessage("Player birth year invalid input error")}
        };
    }
    
    @Test(dataProvider = "invalidPlayerProvider")
    public final void testPlayerUpdateBirthYearInvalidValuesValidation(
            final Player invalidPlayer, final String error) {
        
        playerEditPage.setBirthYear(invalidPlayer.getBirthYear());
        playerEditPage.saveWithInvalidFields();
        
        new Specification()
                .forThe(playerEditPage.getBirthYearInput())
                .textMatch(invalidPlayer.getBirthYear())
                .next()
                .forThe(playerEditPage.getBirthYearErrorLabel())
                .textMatch(error)
                .next()
                .check();
    }
    
    @Test
    public final void testPlayerUpdateWhereBirthYearIsNotIntegerWithDotSeparator() {
        
        Player invalidPlayer = new PlayerTestData()
                .getInvalidPlayerByBirthYear(Violations.NOT_INTEGER_NUMBER_WITH_DOT);
        String error = new ErrorMessages().getPlayerBirthYearInvalidInputErrorWithDot(invalidPlayer);
        
        playerEditPage.setBirthYear(invalidPlayer.getBirthYear());
        playerEditPage.saveWithInvalidFields();
        
        new Specification()
                .forThe(playerEditPage.getBirthYearInput())
                .textMatch(invalidPlayer.getBirthYear())
                .next()
                .forThe(playerEditPage.getBirthYearValidationErrorLabel())
                .textMatch(error)
                .next()
                .check();
    }
    
    @BeforeClass
    public final void loginWithCurrentProfile() {
        new LoginPage().logInWithCurrentProfile();
    }
    
    @BeforeMethod
    public final void setUp() throws Exception {
    
        new WebDriverUtils().load(new PageUrls().PLAYERS_LIST_URL);
        
        PlayersListPage playersListPage = new PlayersListPage();
        
        if (playersListPage.getCountOfPlayersOnCurrentPage() == 0) {
            new PlayerRepository().create(new PlayerToDal()
                    .map(new PlayerTestData().getValidPlayer()));
    
            new WebDriverUtils().refresh();
            
            playersListPage = new PlayersListPage();
        }
        
        playerEditPage = playersListPage.editRandomPlayer();
    }
    
    @AfterTest
    public final void tearDown() {
        new WebDriverUtils().stop();
    }
}
