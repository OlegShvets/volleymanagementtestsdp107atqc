package com.softserveinc.volleymanagementtests.tests.player.update;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.login.LoginPage;
import com.softserveinc.volleymanagementtests.pages.player.PlayerEditPage;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.*;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Test based on TC "TC2_PlayerUpdate_FirstNameInvalidValuesValidation".
 *
 * @author Danil Zhyliaiev
 */
@Features("Player")
@Stories("Update")
public class PlayerUpdateFirstNameInvalidValuesValidationTest {

    private PlayersListPage playersListPage;

    /**
     * DataProvider for test method containing players with random invalid
     * first name field and expected error messages.
     *
     * @return Player-String pairs as {@code Object[][]}.
     */
    @DataProvider
    private Object[][] invalidPlayerProvider() {
        return new Object[][]{
                {new PlayerTestData()
                        .getInvalidPlayerByFirstName(Violations.EMPTY_FIELD),
                        new ErrorMessages().getPlayerErrorMessage("Player first name empty error")},
                {new PlayerTestData()
                        .getInvalidPlayerByFirstName(Violations.MAX_LENGTH_VIOLATED),
                        new ErrorMessages().getPlayerErrorMessage("Player first name length error")},
                {new PlayerTestData()
                        .getInvalidPlayerByFirstName(Violations.CONTAINS_NUMBERS),
                        new ErrorMessages().getPlayerErrorMessage("Player first name must be letters only error")},
                {new PlayerTestData().getInvalidPlayerByFirstName(
                        Violations.CONTAIN_NOT_ALLOWED_CHARACTERS),
                        new ErrorMessages().getPlayerErrorMessage("Player first name must be letters only error")}
        };
    }

    @Test(dataProvider = "invalidPlayerProvider")
    public final void testPlayerUpdateFirstNameInvalidValuesValidation(
            final Player invalidPlayer, final String error) {

        PlayerEditPage playerEditPage = playersListPage.editRandomPlayer();
        playerEditPage.setFirstName(invalidPlayer.getFirstName());
        playerEditPage.saveWithInvalidFields();

        new Specification()
                .forThe(playerEditPage.getFirstNameInput())
                .textMatch(invalidPlayer.getFirstName())
                .next()
                .forThe(playerEditPage.getNameErrorLabel())
                .textMatch(error)
                .next()
                .check();
    }

    @BeforeTest
    public final void loginWithCurrentProfile() {
        new LoginPage().logInWithCurrentProfile();
    }

    @BeforeMethod
    public final void setUp() throws Exception {

        new WebDriverUtils().load(new PageUrls().PLAYERS_LIST_URL);

        playersListPage = new PlayersListPage();

        if (playersListPage.getCountOfPlayersOnCurrentPage() == 0) {
            new PlayerRepository().create(new PlayerToDal()
                    .map(new PlayerTestData().getValidPlayer()));

            new WebDriverUtils().refresh();

            playersListPage = new PlayersListPage();
        }
    }

    @AfterTest
    public final void tearDown() {
        new WebDriverUtils().stop();
    }
}
