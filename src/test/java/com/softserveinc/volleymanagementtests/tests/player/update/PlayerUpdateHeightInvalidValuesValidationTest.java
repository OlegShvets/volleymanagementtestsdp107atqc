package com.softserveinc.volleymanagementtests.tests.player.update;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.login.LoginPage;
import com.softserveinc.volleymanagementtests.pages.player.PlayerEditPage;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.*;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Test based on TC "TC5_PlayerUpdate_HeightInvalidValuesValidation".
 *
 * @author Danil Zhyliaiev
 */
@Features("Player")
@Stories("Update")
public class PlayerUpdateHeightInvalidValuesValidationTest {

    private PlayerEditPage playerEditPage;

    /**
     * DataProvider for test method containing players with preset invalid
     * height field and expected error messages.
     *
     * @return Player-String pairs as {@code Object[][]}.
     */
    @DataProvider
    private Object[][] invalidPlayerProvider() {
        return new Object[][]{
                {new PlayerTestData()
                        .getInvalidPlayerByHeight(Violations.MAX_VALUE_VIOLATED),
                        new ErrorMessages().getPlayerErrorMessage("Player height range error")},
                {new PlayerTestData()
                        .getInvalidPlayerByHeight(Violations.MIN_VALUE_VIOLATED),
                        new ErrorMessages().getPlayerErrorMessage("Player height range error")},
                {new PlayerTestData()
                        .getInvalidPlayerByHeight(Violations.CONTAINS_LETTERS),
                        new ErrorMessages().getPlayerErrorMessage("Player height invalid input error")},
                {new PlayerTestData().getInvalidPlayerByHeight(Violations.CONTAIN_NOT_ALLOWED_CHARACTERS),
                        new ErrorMessages().getPlayerErrorMessage("Player height invalid input error")},
                {new PlayerTestData().getInvalidPlayerByHeight(Violations.NEGATIVE_NUMBER),
                        new ErrorMessages().getPlayerErrorMessage("Player height range error")},
                {new PlayerTestData().getInvalidPlayerByHeight(Violations.NOT_INTEGER_NUMBER_WITH_COMMA),
                        new ErrorMessages().getPlayerErrorMessage("Player height invalid input error")}
        };
    }

    @Test(dataProvider = "invalidPlayerProvider")
    public final void testPlayerUpdateHeightInvalidValuesValidation(
            final Player invalidPlayer, final String error) {
        playerEditPage.setHeight(invalidPlayer.getHeight());
        playerEditPage.saveWithInvalidFields();

        new Specification()
                .forThe(playerEditPage.getHeightInput())
                .textMatch(invalidPlayer.getHeight())
                .next()
                .forThe(playerEditPage.getHeightErrorLabel())
                .textMatch(error)
                .next()
                .check();
    }

    @Test
    public final void testPlayerUpdateWhereHeightIsNotIntegerWithDotSeparator() {
        Player invalidPlayer = new PlayerTestData()
                .getInvalidPlayerByHeight(Violations.NOT_INTEGER_NUMBER_WITH_DOT);
        String error = new ErrorMessages().getHeightInvalidInputErrorWithDot(invalidPlayer);

        playerEditPage.setHeight(invalidPlayer.getHeight());
        playerEditPage.saveWithInvalidFields();

        new Specification()
                .forThe(playerEditPage.getHeightInput())
                .textMatch(invalidPlayer.getHeight())
                .next()
                .forThe(playerEditPage.getHeightValidationErrorLabel())
                .textMatch(error)
                .next()
                .check();
    }

    @BeforeClass
    public final void loginWithCurrentProfile() {
        new LoginPage().logInWithCurrentProfile();
    }

    @BeforeMethod
    public final void setUp() throws Exception {

        new WebDriverUtils().load(new PageUrls().PLAYERS_LIST_URL);

        PlayersListPage playersListPage = new PlayersListPage();

        if (playersListPage.getCountOfPlayersOnCurrentPage() == 0) {
            new PlayerRepository().create(new PlayerToDal()
                    .map(new PlayerTestData().getValidPlayer()));

            new WebDriverUtils().refresh();
            playersListPage = new PlayersListPage();
        }
        playerEditPage = playersListPage.editRandomPlayer();
    }

    @AfterTest
    public final void tearDown() {
        new WebDriverUtils().stop();
    }
}
