package com.softserveinc.volleymanagementtests.tests.player.update;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.login.LoginPage;
import com.softserveinc.volleymanagementtests.pages.player.PlayerEditPage;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.*;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Test based on TC "TC3_PlayerUpdate_LastNameInvalidValuesValidation".
 *
 * @author Danil Zhyliaiev
 */
@Features("Player")
@Stories("Update")
public class PlayerUpdateLastNameInvalidValuesValidationTest {

    protected PlayerEditPage playerEditPage;

    @BeforeTest
    public final void loginWithCurrentProfile() {
        new LoginPage().logInWithCurrentProfile();
    }

    @BeforeMethod
    public final void setUp() throws Exception {

        new WebDriverUtils().load(new PageUrls().PLAYERS_LIST_URL);

        PlayersListPage playersListPage = new PlayersListPage();

        if (playersListPage.getCountOfPlayersOnCurrentPage() == 0) {
            new PlayerRepository().create(new PlayerToDal()
                    .map(new PlayerTestData().getValidPlayer()));

            new WebDriverUtils().refresh();

            playersListPage = new PlayersListPage();
        }

        playerEditPage = playersListPage.editRandomPlayer();
    }

    @AfterTest
    public final void tearDown() {
        new WebDriverUtils().stop();
    }

    /**
     * DataProvider for test method containing players with random invalid
     * last name field and expected error messages.
     *
     * @return Player-String pairs as {@code Object[][]}.
     */
    @DataProvider
    private Object[][] invalidPlayerProvider() {
        return new Object[][]{
                {new PlayerTestData().getInvalidPlayerByLastName(Violations.EMPTY_FIELD),
                        new ErrorMessages().getPlayerErrorMessage("Player last name empty error")},
                {new PlayerTestData()
                        .getInvalidPlayerByLastName(Violations.MAX_LENGTH_VIOLATED),
                        new ErrorMessages().getPlayerErrorMessage("Player last name length error")},
                {new PlayerTestData()
                        .getInvalidPlayerByLastName(Violations.CONTAINS_NUMBERS),
                        new ErrorMessages().getPlayerErrorMessage("Player last name must be letters only error")},
                {new PlayerTestData().getInvalidPlayerByLastName(
                        Violations.CONTAIN_NOT_ALLOWED_CHARACTERS),
                        new ErrorMessages().getPlayerErrorMessage("Player last name must be letters only error")}
        };
    }

    @Test(dataProvider = "invalidPlayerProvider")
    public final void testPlayerUpdateLastNameInvalidValuesValidation(
            final Player invalidPlayer, final String error) {
        playerEditPage.setLastName(invalidPlayer.getLastName());
        playerEditPage.saveWithInvalidFields();

        new Specification()
                .forThe(playerEditPage.getLastNameInput())
                .textMatch(invalidPlayer.getLastName())
                .next()
                .forThe(playerEditPage.getLastNameErrorLabel())
                .textMatch(error)
                .next()
                .check();
    }
}
