package com.softserveinc.volleymanagementtests.tests.player.update;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.login.LoginPage;
import com.softserveinc.volleymanagementtests.pages.player.PlayerEditPage;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.*;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Test based on TC "TC6_PlayerUpdate_WeightInvalidValuesValidation".
 *
 * @author Danil Zhyliaiev
 */
@Features("Player")
@Stories("Update")
public class PlayerUpdateWeightInvalidValuesValidationTest {

    private PlayerEditPage playerEditPage;

    /**
     * DataProvider for test method containing players with preset invalid
     * weight field and expected error messages.
     *
     * @return Player-String pairs as {@code Object[][]}.
     */
    @DataProvider
    private Object[][] invalidPlayerProvider() {
        return new Object[][]{
                {new PlayerTestData()
                        .getInvalidPlayerByWeight(Violations.MAX_VALUE_VIOLATED),
                        new ErrorMessages().getPlayerErrorMessage("Player weight range error")},
                {new PlayerTestData()
                        .getInvalidPlayerByWeight(Violations.MIN_VALUE_VIOLATED),
                        new ErrorMessages().getPlayerErrorMessage("Player weight range error")},
                {new PlayerTestData()
                        .getInvalidPlayerByWeight(Violations.CONTAINS_LETTERS),
                        new ErrorMessages().getPlayerErrorMessage("Player weight invalid input error")},
                {new PlayerTestData().getInvalidPlayerByWeight(Violations.CONTAIN_NOT_ALLOWED_CHARACTERS),
                        new ErrorMessages().getPlayerErrorMessage("Player weight invalid input error")},
                {new PlayerTestData().getInvalidPlayerByWeight(Violations.NEGATIVE_NUMBER),
                        new ErrorMessages().getPlayerErrorMessage("Player weight range error")},
                {new PlayerTestData().getInvalidPlayerByWeight(Violations.NOT_INTEGER_NUMBER_WITH_COMMA),
                        new ErrorMessages().getPlayerErrorMessage("Player weight invalid input error")}
        };
    }

    @Test(dataProvider = "invalidPlayerProvider")
    public final void testPlayerUpdateWeightInvalidValuesValidation(
            final Player invalidPlayer, final String error) {
        playerEditPage.setWeight(invalidPlayer.getWeight());
        playerEditPage.saveWithInvalidFields();

        new Specification()
                .forThe(playerEditPage.getWeightInput())
                .textMatch(invalidPlayer.getWeight())
                .next()
                .forThe(playerEditPage.getWeightErrorLabel())
                .textMatch(error)
                .next()
                .check();
    }

    @Test
    public final void testPlayerUpdateWhereWeightIsNotIntegerWithDotSeparator() {
        Player invalidPlayer = new PlayerTestData()
                .getInvalidPlayerByWeight(Violations.NOT_INTEGER_NUMBER_WITH_DOT);
        String error = new ErrorMessages().getWeightInvalidInputErrorWithDot(invalidPlayer);

        playerEditPage.setWeight(invalidPlayer.getWeight());
        playerEditPage.saveWithInvalidFields();

        new Specification()
                .forThe(playerEditPage.getWeightInput())
                .textMatch(invalidPlayer.getWeight())
                .next()
                .forThe(playerEditPage.getWeightValidationErrorLabel())
                .textMatch(error)
                .next()
                .check();
    }

    @BeforeClass
    public final void loginWithCurrentProfile() {
        new LoginPage().logInWithCurrentProfile();
    }

    @BeforeMethod
    public final void setUp() throws Exception {

        new WebDriverUtils().load(new PageUrls().PLAYERS_LIST_URL);
        PlayersListPage playersListPage = new PlayersListPage();

        if (playersListPage.getCountOfPlayersOnCurrentPage() == 0) {
            new PlayerRepository().create(new PlayerToDal()
                    .map(new PlayerTestData().getValidPlayer()));

            new WebDriverUtils().refresh();
            playersListPage = new PlayersListPage();
        }
        playerEditPage = playersListPage.editRandomPlayer();
    }

    @AfterTest
    public final void tearDown() {
        new WebDriverUtils().stop();
    }
}
