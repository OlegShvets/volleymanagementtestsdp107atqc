package com.softserveinc.volleymanagementtests.tests.standingPage;

import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.tournaments.TournamentDetailsPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.TournamentTablePage;
import com.softserveinc.volleymanagementtests.pages.tournaments.TournamentsPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by Natalia on 15.03.2017.
 */
public class TestStandingPage {
    @BeforeMethod
    public void setUp() {
        new WebDriverUtils().load(new PageUrls().TURNAMENTS_URL);
    }
    
    @AfterTest
    public void tearDownFinal() {
        new WebDriverUtils().stop();
    }
    
    @Features("Standing Page")
    @Stories("StandingPageCheck ")
    @Severity(SeverityLevel.NORMAL)
    @Issue("https://volleymanagement.visualstudio.com/VolleyMgmt%20-%20BS/Dp-107%20ATQC/_backlogs?level=Backlog%20items&showParents=false&_a=backlog&_dialog=workitem")
    @Test
    public final void StandingPageCheck() {
        new TournamentsPage().openTournamentDetforUpcomTrnmtName("Tour Playoff #3");
        TournamentDetailsPage tournamentDetailsPage = new TournamentDetailsPage();
        tournamentDetailsPage.clickTournamentTable();
        String expectedResult = "Standings are not available for this tournament";
        
        new Specification()
                .forThe(new TournamentTablePage()
                        .getTextFromLabelStandingsAreNotAvailable())
                .textMatch(expectedResult)
                .next()
                .check();
    }
}

