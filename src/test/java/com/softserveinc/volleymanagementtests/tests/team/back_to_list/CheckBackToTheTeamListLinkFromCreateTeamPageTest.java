package com.softserveinc.volleymanagementtests.tests.team.back_to_list;

import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.team.TeamCreatePage;
import com.softserveinc.volleymanagementtests.pages.team.TeamListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Team")
@Stories("Back link")
public class CheckBackToTheTeamListLinkFromCreateTeamPageTest {
    
    @BeforeMethod
    public void setUp() {
        new WebDriverUtils().load(new PageUrls().BASE_URL);
        new MainPage()
                .clickUserMenu()
                .clickLogin()
                .clickGoogleLoginButton()
                .clickTeamListPage()
                .clickCreateTeamLink();
    }
    
    @AfterMethod
    public void tearDown() {
        new WebDriverUtils().stop();
    }
    
    @Test
    public void testCheckBackToThePlayersListLink() {
        String expectedResult = "List of teams";
        
        TeamListPage teamListPage = new TeamCreatePage().clickBackToList();
        new Specification()
                .forThe(teamListPage
                        .getListOfTeamsLabel())
                .textMatch(expectedResult)
                .next()
                .check();
    }
}
