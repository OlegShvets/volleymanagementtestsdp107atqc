package com.softserveinc.volleymanagementtests.tests.team.back_to_list;


import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.team.TeamDetailsPage;
import com.softserveinc.volleymanagementtests.pages.team.TeamListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.sql.SQLException;

@Features("Team")
@Stories("Back link")
public class CheckBackToTheTeamListLinkFromTeamDetailsPageTest {
    
    @BeforeClass
    public void setUp() throws SQLException, ClassNotFoundException {
        new WebDriverUtils().load(new PageUrls().BASE_URL);
        new MainPage()
                .clickUserMenu()
                .clickLogin()
                .clickGoogleLoginButton()
                .clickTeamListPage();
    }
    
    @AfterClass
    public void tearDown() {
        new WebDriverUtils().stop();
    }
    
    @Test
    public void testCheckBackToThePlayersListLink() {
        String expectedResult = "List of teams";
        
        TeamDetailsPage teamDetailsPage = new TeamListPage().showRandomTeamDetails();
        teamDetailsPage.clickBackLink();
        TeamListPage teamListPage = new TeamListPage();
        
        new Specification()
                .forThe(teamListPage
                        .getListOfTeamsLabel())
                .textMatch(expectedResult)
                .next()
                .check();
    }
}
