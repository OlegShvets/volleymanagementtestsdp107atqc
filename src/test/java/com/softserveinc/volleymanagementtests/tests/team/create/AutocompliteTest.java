package com.softserveinc.volleymanagementtests.tests.team.create;


import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.dal.repositories.TeamRepository;
import com.softserveinc.volleymanagementtests.pages.team.TeamCreatePage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.testdata.team.TeamTestData;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Team")
@Stories("Autocomplite")
public class AutocompliteTest extends TestBase {
    
    @Test()
    public void testAutocomplitingCaptainsInput() throws Exception {
        TeamCreatePage teamCreatePage = new TeamCreatePage();

        String FULL_NAME = "Gaqrxvnr Dlmhjsst";
        String PART_NAME = "mh";

        teamCreatePage.setCaptainSmart(PART_NAME, FULL_NAME);
        System.out.println();
//        Specification specification = new Specification();
//        specification.forThe(teamCreatePage.getCaptainInput().getText())
//                .valueMatch(FULL_NAME)
//                .next()
//                .forThe(teamCreatePage.getCaptainSmartInput().getText())
//                .valueMatch(FULL_NAME)
//                .next()
//                .check();
//         teamCreatePage.submitCreateTeamButton();


    }
}
