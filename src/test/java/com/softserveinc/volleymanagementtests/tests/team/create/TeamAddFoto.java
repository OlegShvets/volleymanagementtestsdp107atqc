package com.softserveinc.volleymanagementtests.tests.team.create;

import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.team.TeamDetailsPage;
import com.softserveinc.volleymanagementtests.pages.team.TeamEditPage;
import com.softserveinc.volleymanagementtests.pages.team.TeamListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tools.CompareUtil;
import com.softserveinc.volleymanagementtests.tools.FileUploader;
import com.softserveinc.volleymanagementtests.tools.ScreenMaker;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by shvets on 22.03.2017.
 */
public class TeamAddFoto {
    private Specification specification;

    @BeforeMethod
    public void setUp() {
        new WebDriverUtils().load(new PageUrls().BASE_URL);
        new MainPage().clickUserMenu().clickLogin().clickGoogleLoginButton().clickTournamentsPage();
        new WebDriverUtils().load(new PageUrls().TEAMS_LIST_URL);
        specification = new Specification();
    }

    @AfterTest
    public void tearDown() {
        new WebDriverUtils().stop();
    }

    @Features("Photo of the team")
    @Stories("On Details Team Page displayed button 'Add foto' ")
    @Issue("https://volleymanagement.visualstudio.com/VolleyMgmt%20-%20BS/_backlogs/TaskBoard/DP-103/Sprint%207?_a=requirements")
    @Severity(SeverityLevel.NORMAL)
    @Test
    public void findAddFotoButtonOnDetailsTeamPage() {
        specification
                .forThe(new TeamListPage()
                        .showRandomTeamDetails()
                        .clickEditTeamLink()
                        .getUploadButton())
                .textMatch("Upload photo");
    }

    @Features("Photo of the team")
    @Stories("Administrator can add foto of Team on Edit Team Page")
    @Issue("https://volleymanagement.visualstudio.com/VolleyMgmt%20-%20BS/_backlogs/TaskBoard/DP-103/Sprint%207?_a=requirements")
    @Severity(SeverityLevel.NORMAL)
    @Test
    public void checkTeamFotoOnDetailsTeamPage() {
        String baseFoto = "Base foto";
        String uploadFoto = "Upload Foto";
        new TeamListPage().showRandomTeamDetails();
        new ScreenMaker().makeScreenShot(baseFoto);
        new TeamDetailsPage().clickEditTeamLink().getUploadButton().click();
        new FileUploader().uploadFile();
        new TeamEditPage().getSaveTeamButton().click();
        while (true) {
            if (new WebDriverUtils().getTitle().equals("Details about team")) {
                break;
            }
        }
        new ScreenMaker().makeScreenShot(uploadFoto);
        specification
                .forThe(new CompareUtil().compareImage(baseFoto, uploadFoto))
                .isFalse()
                .next()
                .check();
    }
}
