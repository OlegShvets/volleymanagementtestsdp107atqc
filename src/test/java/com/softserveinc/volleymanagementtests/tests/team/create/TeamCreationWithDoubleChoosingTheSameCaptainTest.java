package com.softserveinc.volleymanagementtests.tests.team.create;


import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.dal.repositories.TeamRepository;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.team.TeamCreatePage;
import com.softserveinc.volleymanagementtests.pages.team.TeamListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.testdata.team.TeamTestData;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Team")
@Stories("Create")
public class TeamCreationWithDoubleChoosingTheSameCaptainTest extends TestBase {
    @DataProvider
    public Object[][] validDataProvider() {
        
        return new Object[][]{
                {new TeamTestData().getValidTeam(), new TeamTestData().getValidTeam(), new ErrorMessages().getTeamErrorMessage("Captain in other team error")}
        };
    }
    
    @Test(dataProvider = "validDataProvider")
    public void testTeamCreationWithDoubleChoosingTheSameCaptain(Team team1, Team team2, String errorMessage) throws Exception {
        TeamCreatePage teamCreatePage = new TeamCreatePage();
        
        //put players into repositories
        
        new PlayerRepository().create(new PlayerToDal().map(team1.getCaptain()));
        new PlayerRepository().create(new PlayerToDal().map(team2.getCaptain()));
        
        if (team1.getRoster() != null) {
            for (Player player : team1.getRoster()) {
                new PlayerRepository().create(new PlayerToDal().map(player));
            }
        }
        
        if (team1.getRoster() != null) {
            for (Player player : team2.getRoster()) {
                new PlayerRepository().create(new PlayerToDal().map(player));
            }
        }
        
        
        String FULL_NAME = team1.getCaptain().getLastName() + " " + team1.getCaptain().getFirstName();
        String SECOND_PLAYER_LAST_NAME = team2.getRoster().get(0).getLastName();
        String SECOND_PLAYER_FULL_NAME = SECOND_PLAYER_LAST_NAME + " " + team2.getRoster().get(0).getFirstName();
        
        
        //set all fields for first team on teamCreatePage
        
        teamCreatePage.setSmartAllTeamFields(team1);
        TeamCreatePage teamCreatePageNew = teamCreatePage
                .submitCreateTeamButton()
                .goToMainPage()
                .clickTeamListPage()
                .clickCreateTeamLink();



        //set fields for second team where captain is a captain from first team
        
        teamCreatePageNew.setTeamName(team2.getName());
        teamCreatePageNew.setCoachName(team2.getCoach());
        teamCreatePageNew.setCaptain(FULL_NAME);
        teamCreatePageNew.setAchievements(team2.getAchievements());

        
        new Specification()
                .forThe(teamCreatePage.getErrorMessageForCaptain())
                .isVisible()
                .next()
                .forThe(teamCreatePage.getErrorMessageForCaptain())
                .textMatch(errorMessage)
                .next()
                .check();
        teamCreatePageNew.submitCreateTeamButton();
        
        Assert.assertFalse(new TeamRepository().isTeamExistsInDataBaseByName(team2.getName()));
    }
}
