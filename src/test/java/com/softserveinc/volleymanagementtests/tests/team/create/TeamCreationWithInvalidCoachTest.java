package com.softserveinc.volleymanagementtests.tests.team.create;


import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.dal.repositories.TeamRepository;
import com.softserveinc.volleymanagementtests.pages.team.TeamCreatePage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.testdata.team.TeamTestData;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Team")
@Stories("Create")
public class TeamCreationWithInvalidCoachTest extends TestBase {
    @DataProvider
    public Object[][] invalidDataProvider() {

        return new Object[][]{
                {new TeamTestData().getInvalidTeamByCoach(Violations.CONTAINS_NUMBERS),
                        new ErrorMessages().getTeamErrorMessage("Team coach name invalid input error")}
                , {new TeamTestData().getInvalidTeamByCoach(Violations.MAX_LENGTH_VIOLATED),
                new ErrorMessages().getTeamErrorMessage("Team coach name length error")}
                , {new TeamTestData().getInvalidTeamByCoach(Violations.CONTAIN_NOT_ALLOWED_CHARACTERS),
                new ErrorMessages().getTeamErrorMessage("Team coach name invalid input error")}
        };
    }

    @Test(dataProvider = "invalidDataProvider")
    public void testTeamCreationWithInvalidCoach(Team team, String errorMessage) throws Exception {
        TeamCreatePage teamCreatePage = new TeamCreatePage();
        new PlayerRepository().create(new PlayerToDal().map(team.getCaptain()));

        if (team.getRoster() != null) {
            for (Player player : team.getRoster()) {
                new PlayerRepository().create(new PlayerToDal().map(player));
            }
        }

        teamCreatePage.setSmartAllTeamFields(team);

        Specification specification = new Specification();
        specification
                .forThe(teamCreatePage)
                .isAllFieldsFilledInCorrectly(team)
                .next()
                .forThe(teamCreatePage.getCoachErrorInputLabel())
                .textMatch(errorMessage)
                .next()
                .check();

        teamCreatePage.submitCreateTeamButton();


        Assert.assertFalse(new TeamRepository().isTeamExistsInDataBaseByName(team.getName()));
    }
}
