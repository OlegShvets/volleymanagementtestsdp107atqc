package com.softserveinc.volleymanagementtests.tests.team.create;


import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.dal.repositories.TeamRepository;
import com.softserveinc.volleymanagementtests.pages.team.TeamCreatePage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.testdata.team.TeamTestData;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.concurrent.TimeoutException;

@Features("Team")
@Stories("Create")
public class TeamCreationWithInvalidNameTest extends TestBase {
    @DataProvider
    public Object[][] invalidDataProvider() {

        return new Object[][]{
                {new TeamTestData().getInvalidTeamByName(Violations.EMPTY_FIELD),
                        new ErrorMessages().getTeamErrorMessage("Team name empty error")}
                , {new TeamTestData().getInvalidTeamByName(Violations.MAX_LENGTH_VIOLATED),
                new ErrorMessages().getTeamErrorMessage("Team name length error")}
        };
    }

    @Test(dataProvider = "invalidDataProvider")
    public void teamCreationWithInvalidName(Team team, String errorMessage) throws Exception {
        TeamCreatePage teamCreatePage = new TeamCreatePage();
        new PlayerRepository().create(new PlayerToDal().map(team.getCaptain()));

        if (team.getRoster() != null) {
            for (Player player : team.getRoster()) {
                new PlayerRepository().create(new PlayerToDal().map(player));
            }
        }

        teamCreatePage.setSmartAllTeamFields(team);

        Specification specification = new Specification();
        specification
                .forThe(teamCreatePage.getErrorMessageNameInput())
                .isVisible()
                .next()
                .forThe(teamCreatePage.getErrorMessageNameInput())
                .textMatch(errorMessage)
                .next()
                .check();

        teamCreatePage.submitCreateTeamButton();

        Assert.assertFalse(new TeamRepository().isTeamExistsInDataBaseByName(team.getName()));
    }
}
