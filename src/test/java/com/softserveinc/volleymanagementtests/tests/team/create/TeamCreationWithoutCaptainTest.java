package com.softserveinc.volleymanagementtests.tests.team.create;


import com.softserveinc.volleymanagementtests.dal.repositories.TeamRepository;
import com.softserveinc.volleymanagementtests.pages.team.TeamCreatePage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.testdata.team.TeamTestData;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Team")
@Stories("Create")
public class TeamCreationWithoutCaptainTest extends TestBase {
    @DataProvider
    public Object[][] invalidDataProvider() {

        return new Object[][]{
                {new TeamTestData().getInvalidTeamWithoutCaptain(), new ErrorMessages().getPlayerErrorMessage("Captain is not chosen error")}
        };
    }

    @Test(dataProvider = "invalidDataProvider")
    public void testTeamCreationWithoutCaptain(Team team, String errorMessage) throws Exception {
        TeamCreatePage teamCreatePage = new TeamCreatePage();

        teamCreatePage.setSmartAllTeamFields(team);
        new Specification()
                .forThe(teamCreatePage.getErrorMessageForCaptain())
                .isVisible()
                .next()
                .forThe(teamCreatePage.getErrorMessageForCaptain())
                .textMatch(errorMessage)
                .next()
                .check();

        teamCreatePage.submitCreateTeamButton();

        Assert.assertFalse(new TeamRepository().isTeamExistsInDataBaseByName(team.getName()));
    }
}
