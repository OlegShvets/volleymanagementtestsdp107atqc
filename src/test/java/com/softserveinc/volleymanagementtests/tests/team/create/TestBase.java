package com.softserveinc.volleymanagementtests.tests.team.create;

import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.team.TeamCreatePage;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.sql.SQLException;

/**
 * @author S.Tsyganovskiy
 */
public class TestBase {
    
    @BeforeMethod
    public final void setUp() throws SQLException, ClassNotFoundException {
        new WebDriverUtils().load(new PageUrls().BASE_URL);
        new MainPage().clickUserMenu().clickLogin().clickGoogleLoginButton().clickTournamentsPage();
        new WebDriverUtils().load(new PageUrls().TURNAMENTS_URL);
        MainPage mainPage = new MainPage();
        TeamCreatePage teamCreatePage = mainPage.clickTeamListPage().clickCreateTeamLink();
        teamCreatePage.logInAsCaptain();
    }
    
    @AfterMethod
    public final void tearDown() {
        new WebDriverUtils().stop();
    }
    
    
}
