package com.softserveinc.volleymanagementtests.tests.team.create;

import com.softserveinc.volleymanagementtests.dal.models.DivisionDal;
import com.softserveinc.volleymanagementtests.dal.models.GroupDal;
import com.softserveinc.volleymanagementtests.dal.models.TeamDal;
import com.softserveinc.volleymanagementtests.dal.models.TournamentDal;
import com.softserveinc.volleymanagementtests.dal.repositories.DivisionRepository;
import com.softserveinc.volleymanagementtests.dal.repositories.GroupRepository;
import com.softserveinc.volleymanagementtests.dal.repositories.TeamRepository;
import com.softserveinc.volleymanagementtests.dal.repositories.TournamentRepository;
import com.softserveinc.volleymanagementtests.testdata.division.DivisionTestData;
import com.softserveinc.volleymanagementtests.testdata.division.mappers.DivisionToDal;
import com.softserveinc.volleymanagementtests.testdata.group.GroupTestData;
import com.softserveinc.volleymanagementtests.testdata.group.GroupToDal;
import com.softserveinc.volleymanagementtests.testdata.tournament.TournamentTestData;
import com.softserveinc.volleymanagementtests.testdata.tournament.mappers.TournamentToDal;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TournamentTest {
    @Test()
    public void testTournamentCreation() throws SQLException, ClassNotFoundException {
        TournamentDal tournamentDal = new TournamentToDal()
                .map(new TournamentTestData().getValidPlayOffTournament());
        int tournamentKey = new TournamentRepository().create(tournamentDal);
        DivisionDal divisionDal = new DivisionToDal().map(new DivisionTestData()
                .getValidDivision());
        divisionDal.setTournamentId(tournamentKey);
        int divisionKey = new DivisionRepository().create(divisionDal);
        GroupDal groupDal = new GroupToDal().map(new GroupTestData().getValidGroup());
        groupDal.setDivisionId(divisionKey);
        new GroupRepository().create(groupDal);
        
        List<TeamDal> teams = new ArrayList<>();
        teams.add(new TeamRepository().getById(1));
        teams.add(new TeamRepository().getById(2));
        new TournamentRepository().addTeamToTournament(tournamentDal, teams);
    }
}