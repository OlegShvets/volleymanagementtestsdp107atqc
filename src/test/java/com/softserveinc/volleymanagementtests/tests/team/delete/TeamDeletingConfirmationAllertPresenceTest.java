package com.softserveinc.volleymanagementtests.tests.team.delete;

import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.team.TeamDetailsPage;
import com.softserveinc.volleymanagementtests.pages.team.TeamListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * @author J.Bodnar
 */
@Features("Team")
@Stories("Delete")
public class TeamDeletingConfirmationAllertPresenceTest {
    
    @BeforeMethod
    public void setUp() {
        new WebDriverUtils().load(new PageUrls().TEAMS_LIST_URL);
    }
    
    @Test
    public void TestConfirmationAlertPresence() {
        TeamListPage teamListPage = new TeamListPage();
        TeamDetailsPage teamDetailsPage = new TeamListPage().showRandomTeamDetails();
        Team testTeam = teamDetailsPage.readTeam();
        teamDetailsPage.clickBackLink();
        
        new Specification().
                forThe(teamListPage.clickDeleteTeamButton(testTeam))
                .textMatch(String.format("Are you sure you want to delete a " +
                        "team? \"%s\" ?", testTeam.getName()))
                .next()
                .check();
    }
    
    @AfterMethod
    public final void tearDown() throws Exception {
        new WebDriverUtils().stop();
    }
}


