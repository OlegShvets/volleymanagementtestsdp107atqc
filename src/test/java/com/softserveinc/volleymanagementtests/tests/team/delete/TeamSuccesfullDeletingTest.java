package com.softserveinc.volleymanagementtests.tests.team.delete;

import com.softserveinc.volleymanagementtests.dal.repositories.TeamRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.team.TeamDetailsPage;
import com.softserveinc.volleymanagementtests.pages.team.TeamListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.AlertImplementDec;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.sql.SQLException;

/**
 * @author J.Bodnar
 */
@Features("Team")
@Stories("Delete")
public class TeamSuccesfullDeletingTest {
    
    @BeforeMethod
    public void setUp() {
        new WebDriverUtils().load(new PageUrls().TEAMS_LIST_URL);
    }
    
    @Test
    public void TestSucesfullTeamDeleting() throws SQLException, ClassNotFoundException {
        TeamListPage teamListPage = new TeamListPage();
        TeamDetailsPage teamDetailsPage = new TeamListPage().showRandomTeamDetails();
        Team testTeam = teamDetailsPage.readTeam();
        teamDetailsPage.clickBackLink();
        teamListPage.clickDeleteTeamButton(testTeam);
        new AlertImplementDec().accept();
        
        new Specification().
                forThe(new TeamRepository().isTeamExistsInDataBaseByName
                        (testTeam.getName()))
                .isFalse()
                .next()
                .check();
    }
    
    @AfterMethod
    public final void tearDown() throws Exception {
        new WebDriverUtils().stop();
    }
}



