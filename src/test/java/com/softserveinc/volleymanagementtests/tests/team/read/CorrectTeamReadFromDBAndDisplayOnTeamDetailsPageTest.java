package com.softserveinc.volleymanagementtests.tests.team.read;

import com.softserveinc.volleymanagementtests.dal.models.PlayerDal;
import com.softserveinc.volleymanagementtests.dal.models.TeamDal;
import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.dal.repositories.TeamRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.team.TeamListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.sql.SQLException;

/**
 * @author Karabaza Anton on 23.03.2016.
 */
public class CorrectTeamReadFromDBAndDisplayOnTeamDetailsPageTest {
    TeamListPage teamListPage;
    TeamDal team;
    
    @BeforeMethod
    public void setUp() throws Exception {
        PlayerRepository playerRepository = new PlayerRepository();
        TeamRepository teamRepository = new TeamRepository();
        PlayerDal player1 = playerRepository.create(new PlayerToDal()
                .map(new PlayerTestData().getValidPlayer()));
        
        PlayerDal player2 = playerRepository
                .create(new PlayerToDal()
                        .map(new PlayerTestData().getValidPlayer()));
        
        team = TeamDal.newBuilder()
                .setName(RandomStringUtils.random(10, true, false))
                .setCoach(RandomStringUtils.random(7, true, false))
                .setAchievements(RandomStringUtils.random(13, true, false))
                .setCaptainId(playerRepository
                        .create(new PlayerToDal()
                                .map(new PlayerTestData().getValidPlayer()))
                        .getId())
                .build();
        
        teamRepository.create(team);
        playerRepository.setPlayerToTeam(player1.getId(), team.getId());
        playerRepository.setPlayerToTeam(player2.getId(), team.getId());
        
        new WebDriverUtils().load(new PageUrls().BASE_URL);
        teamListPage = new MainPage()
                .clickUserMenu()
                .clickLogin()
                .clickGoogleLoginButton()
                .clickTeamListPage();
    }
    
    @AfterTest
    public final void tearDown() {
        new WebDriverUtils().stop();
    }
    
    @Features("Team")
    @Stories("Read")
    @Test
    public void testCorrectTeamReadFromDBAndDisplayOnTeamDetailsPage() throws SQLException,
            ClassNotFoundException {

        new Specification()
                .forThe(teamListPage)
                .isTeamDisplayedOnListWithCorrectDetails(team)
                .next()
                .check();
    }
}
