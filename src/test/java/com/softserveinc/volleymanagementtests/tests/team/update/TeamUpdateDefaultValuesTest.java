package com.softserveinc.volleymanagementtests.tests.team.update;

import com.softserveinc.volleymanagementtests.dal.models.PlayerDal;
import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.dal.repositories.TeamRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.team.TeamDetailsPage;
import com.softserveinc.volleymanagementtests.pages.team.TeamEditPage;
import com.softserveinc.volleymanagementtests.pages.team.TeamListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.DalToPlayer;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.testdata.team.TeamTestData;
import com.softserveinc.volleymanagementtests.testdata.team.mappers.TeamToDal;
import com.softserveinc.volleymanagementtests.tools.AllureOnFailListener;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.*;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.sql.SQLException;

@Features("Team")
@Stories("Create")
@Listeners({AllureOnFailListener.class})
public class TeamUpdateDefaultValuesTest {
    private TeamListPage teamListPage;
    private TeamDetailsPage teamDetailsPage;
    
    
    @BeforeClass
    public void setUp() {
        new WebDriverUtils().load(new PageUrls().BASE_URL);
        teamListPage = new MainPage()
                .clickUserMenu()
                .clickLogin()
                .clickGoogleLoginButton()
                .clickTeamListPage();
    }
    
    @BeforeMethod
    public final void createTestTeamIfNeeded() throws SQLException, ClassNotFoundException {
        if (teamListPage.getTeamList().isEmpty()) {
            Team team = new TeamTestData().getValidTeam();
            PlayerDal captainDal = new PlayerRepository()
                    .create(new PlayerToDal()
                            .map(team.getCaptain()));
            
            team = new Team().newBuilder(team).setCaptain(new DalToPlayer().map(captainDal))
                    .build();
            new TeamRepository().create(new TeamToDal().map(team));
            new WebDriverUtils().refresh();
            
            teamListPage = new TeamListPage();
        }
        teamDetailsPage = teamListPage.showRandomTeamDetails();
    }
    
    @AfterTest
    public final void tearDown() {
        new WebDriverUtils().stop();
    }
    
    @Test
    public final void testTeamUpdateDefaultFieldValues() {
        Specification specification = new Specification();
        Team team = teamDetailsPage.readTeam();
        TeamEditPage teamEditPage = teamDetailsPage.clickEditTeamLink();
        new Specification()
                .forThe(teamEditPage.readTeam())
                .equalTo(team);
        specification.check();
        
    }
}
