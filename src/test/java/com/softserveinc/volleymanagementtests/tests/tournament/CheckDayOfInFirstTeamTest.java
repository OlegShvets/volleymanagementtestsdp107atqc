package com.softserveinc.volleymanagementtests.tests.tournament;

import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.tournaments.TournamentDetailsPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class CheckDayOfInFirstTeamTest {
    private final String expectedResult = "Day off";
    
    @BeforeMethod
    public void setUp() {
        new WebDriverUtils().load(new PageUrls().TOUR_PLAYOFF_2);
    }
    
    @AfterTest
    public void tearDownFinal() {
        new WebDriverUtils().stop();
    }
    
    @Features("Schedule Page")
    @Stories("Check data of game")
    @Severity(SeverityLevel.NORMAL)
    @Issue("https://volleymanagement.visualstudio.com/VolleyMgmt%20-%20BS/Dp-107%20ATQC/_backlogs?level=Backlog%20items&showParents=false&_a=backlog&_dialog=workitem")
    @Test
    public void testFirstGameHaveDayOff() {
        
        new Specification()
                .forThe(new TournamentDetailsPage()
                        .clickTournamentSchedule()
                        .getRoundElements()
                        .get(0)
                        .getTeamGameElements()
                        .get(0)
                        .getDataOfFirstGame())
                .textMatch(expectedResult)
                .next()
                .check();
    }
}
