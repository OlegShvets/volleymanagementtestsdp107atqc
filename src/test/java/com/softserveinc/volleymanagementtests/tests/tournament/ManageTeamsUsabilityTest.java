package com.softserveinc.volleymanagementtests.tests.tournament;

import com.softserveinc.volleymanagementtests.dal.models.TournamentDal;
import com.softserveinc.volleymanagementtests.dal.repositories.TournamentRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.team.ManageTeamsPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.*;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.testdata.tournament.Tournament;
import com.softserveinc.volleymanagementtests.testdata.tournament.TournamentTestData;
import com.softserveinc.volleymanagementtests.testdata.tournament.mappers.TournamentToDal;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sasha on 14.03.2017.
 */
public class ManageTeamsUsabilityTest {
    private List<String> existingTeamsList;


    @Features("Tournament")
    @Stories("ManageTeamsUsability")
    @BeforeMethod
    public void setUp() {
        existingTeamsList = new ArrayList<>();


        new WebDriverUtils().load(new PageUrls().BASE_URL);
        new MainPage().clickUserMenu().clickLogin().clickGoogleLoginButton().clickTournamentsPage();

        //create new Tournament and open page for managing teams for it

        Tournament tournament = new TournamentTestData().getValidPlayOffTournament();
        MainPage mainPage = new MainPage();
        existingTeamsList = mainPage.clickTeamListPage().getTeams();
        existingTeamsList.add("No team is selected");
        mainPage.clickTournamentsPage();
        TournamentsPage tournamentsPage = new TournamentsPage();
        CreatingNewTournamentPage creating = tournamentsPage.clickCreateNewTournament();
        tournamentsPage = creating.createTournamentWithOneGroupAndOneDivisionAndBackToList(tournament);
        TournamentDetailsPage details = tournamentsPage.openTournamentDetailsforUpcomTrnmt(tournament);
        details.clickManageTeams();
    }

    @AfterMethod
    public void tearDown() {
        new WebDriverUtils().stop();
    }

    @Test
    public void checkManagingTeams() {
        Specification specification = new Specification();
        ManageTeamsPage manageTeamsPage = new ManageTeamsPage();
        manageTeamsPage.getAddTeam().click();
        List<String> teamsInList = new ManageTeamsPage().getAllTeamsInDropdown(1);

        specification
                .forThe(manageTeamsPage.getSaveButton().getText())
                .valueMatch("Save")
                .next()
                .forThe(manageTeamsPage.getAddTeam().getText())
                .valueMatch("+ Add team")
                .next()
                .forThe(teamsInList)
                .containsAll(existingTeamsList)
                .next();
        teamsInList.clear();
        new ManageTeamsPage()
                .setTeamName(existingTeamsList.get(0), 1)
                .save()
               .getSelectedTeamLink(1);

        new ManageTeamsPage()
                .getAddTeam()
                .click();
        teamsInList = new ManageTeamsPage().getAllTeamsInDropdown(2);
        specification
                .forThe(teamsInList)
                .listSizeEquals(existingTeamsList.size() - 1)
                .next()
                .check();
    }
}