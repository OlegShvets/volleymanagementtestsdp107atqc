package com.softserveinc.volleymanagementtests.tests.tournament;

import com.softserveinc.volleymanagementtests.dal.models.PlayerDal;
import com.softserveinc.volleymanagementtests.dal.models.TeamDal;
import com.softserveinc.volleymanagementtests.dal.models.TournamentDal;
import com.softserveinc.volleymanagementtests.dal.repositories.*;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.team.ManageTeamsPage;
import com.softserveinc.volleymanagementtests.pages.team.TeamListPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.ScheduleGamePage;
import com.softserveinc.volleymanagementtests.pages.tournaments.TournamentDetailsPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.TournamentsPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.TournamentsSchedulePage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.division.DivisionTestData;
import com.softserveinc.volleymanagementtests.testdata.division.mappers.DivisionToDal;
import com.softserveinc.volleymanagementtests.testdata.group.GroupTestData;
import com.softserveinc.volleymanagementtests.testdata.group.GroupToDal;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.testdata.team.TeamTestData;
import com.softserveinc.volleymanagementtests.testdata.team.mappers.TeamToDal;
import com.softserveinc.volleymanagementtests.testdata.tournament.Tournament;
import com.softserveinc.volleymanagementtests.testdata.tournament.TournamentTestData;
import com.softserveinc.volleymanagementtests.testdata.tournament.mappers.TournamentToDal;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Sasha on 14.03.2017.
 */
public class ManageTeamsUsabilityUsingDBTest {
    private List<String> existingTeams;
    private Tournament tournament;
    private TournamentDal tournamentDal;
    private ManageTeamsPage manageTeamsPage;


    private void prepareDb(int teamCount) {
        Player player = new PlayerTestData().getValidPlayer();
        PlayerRepository playerRepo = new PlayerRepository();
        PlayerDal playerDal = playerRepo.create(new PlayerToDal().map(player));
        List<Team> teams = new TeamTestData().getValidTeams(teamCount);
        TeamToDal teamMapper = new TeamToDal();
        TeamRepository teamRepository = new TeamRepository();
        List<TeamDal> teamsDal = teams.stream()
                .map(teamMapper::map)
                .collect(Collectors.toList());
        teamsDal.forEach(teamDal -> teamDal
                .setCaptainId(playerDal.getId()));
        for (TeamDal team : teamsDal) {
            teamRepository.create(team);
        }
        TournamentRepository trmnentRepo = new TournamentRepository();
        TournamentToDal trmntMapper = new TournamentToDal();
        tournament = new TournamentTestData().getValidPlayOffTournament();
        tournamentDal = trmntMapper.map(tournament);
        int trmentKey = trmnentRepo.create(tournamentDal);
        trmnentRepo.addTeamToTournament(tournamentDal, teamsDal);
        DivisionToDal divisionMapper = new DivisionToDal();
        DivisionRepository divisionRep = new DivisionRepository();
        int divisionId = divisionRep.create(divisionMapper
                .map(new DivisionTestData().getValidDivision())
                .setTournamentId(trmentKey));
        GroupToDal groupMapper = new GroupToDal();
        GroupRepository groupRepo = new GroupRepository();
        groupRepo.create(groupMapper.map(new GroupTestData().getValidGroup())
                .setDivisionId(divisionId));
    }

    @Features("Tournament")
    @Stories("ManageTeamsUsability")
    @BeforeMethod
    public void setUp() throws InterruptedException {
        existingTeams = new ArrayList<>();
        prepareDb(0);
        new WebDriverUtils().load(new PageUrls().BASE_URL);
        new MainPage()
                .clickUserMenu()
                .clickLogin()
                .clickGoogleLoginButton()
                .clickTeamListPage();
        existingTeams = new TeamListPage().getTeams();
        existingTeams.add("No team is selected");

        TournamentsPage tournamentsPage = new MainPage()
                .clickTournamentsPage();
        manageTeamsPage = tournamentsPage
                .openTournamentDetailsforUpcomTrnmt(tournament)
                .clickManageTeams();
    }

    @AfterMethod
    public void tearDown() throws SQLException, ClassNotFoundException {
        new WebDriverUtils().stop();
        new TournamentRepository().deleteById(tournamentDal.getId());
    }

    @Test
    public void checkManageTeamsUsability() {
        Specification specification = new Specification();
        manageTeamsPage.getAddTeam().click();
        List<String> teamsInDropdown = new ManageTeamsPage().getAllTeamsInDropdown(1);

        specification
                .forThe(manageTeamsPage.getSaveButton().getText())
                .valueMatch("Save")
                .next()
                .forThe(manageTeamsPage.getAddTeam().getText())
                .valueMatch("+ Add team")
                .next()
                .forThe(teamsInDropdown)
                .containsAll(existingTeams)
                .next();
        teamsInDropdown.clear();
        manageTeamsPage
                .setTeamName(existingTeams.get(0), 1)
                .save()
                .getSelectedTeamLink(1);

        manageTeamsPage
                .getAddTeam()
                .click();
        teamsInDropdown = manageTeamsPage.getAllTeamsInDropdown(2);
        specification
                .forThe(teamsInDropdown)
                .listSizeEquals(existingTeams.size() - 1)
                .next()
                .check();
    }
}
