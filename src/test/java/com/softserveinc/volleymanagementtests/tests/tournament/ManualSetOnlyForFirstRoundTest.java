package com.softserveinc.volleymanagementtests.tests.tournament;

import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.team.ManageTeamsPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.*;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.testdata.tournament.Tournament;
import com.softserveinc.volleymanagementtests.testdata.tournament.TournamentTestData;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by Sasha on 14.03.2017.
 */
public class ManualSetOnlyForFirstRoundTest {
    private Team awayTestTeam2;

    @Features("Tournament")
    @Stories("ManualSetOnlyForFirstRoundTest")
    @BeforeMethod
    public void setUp() {
        new WebDriverUtils().load(new PageUrls().BASE_URL);
        new MainPage().clickUserMenu().clickLogin().clickGoogleLoginButton().clickTournamentsPage();
        new WebDriverUtils().load(new PageUrls().TURNAMENTS_URL);

        //create Tournament with 4 teams

        Tournament tournament = new TournamentTestData().getValidPlayOffTournament();
        Team homeTestTeam1 = new Team().newBuilder().setName("First Order").build();
        Team awayTestTeam1 = new Team().newBuilder().setName("Empire").build();
        Team homeTestTeam2 = new Team().newBuilder().setName("Rebelion").build();
        awayTestTeam2 = new Team().newBuilder().setName("Avengers").build();
        TournamentsPage tournamentsPage = new TournamentsPage();
        CreatingNewTournamentPage creating = tournamentsPage.clickCreateNewTournament();
        tournamentsPage = creating.createTournamentWithOneGroupAndOneDivisionAndBackToList(tournament);
        TournamentDetailsPage details = tournamentsPage.openTournamentDetailsforUpcomTrnmt(tournament);
        ManageTeamsPage manageTeamsPage = details.clickManageTeams();
        manageTeamsPage = addTeams(manageTeamsPage, homeTestTeam1.getName(), 1);
        manageTeamsPage = addTeams(manageTeamsPage, awayTestTeam1.getName(), 2);
        manageTeamsPage = addTeams(manageTeamsPage, homeTestTeam2.getName(), 3);
        manageTeamsPage = addTeams(manageTeamsPage, awayTestTeam2.getName(), 4);
        manageTeamsPage.Cancel();
    }

    private ManageTeamsPage addTeams(ManageTeamsPage manageTeamsPage, String teamName, int teamNumber) {
        manageTeamsPage.getAddTeam().click();
        manageTeamsPage.setTeamName(teamName, teamNumber);
        manageTeamsPage = manageTeamsPage.save();
        manageTeamsPage.getSelectedTeamLink(teamNumber);
        return manageTeamsPage;
    }

    @AfterMethod
    public void tearDown() {
        new WebDriverUtils().stop();
    }

    @Test
    public void checkManualSetOnlyForFirstRound() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        TournamentDetailsPage details = new TournamentDetailsPage();
        details
                .clickTournamentSchedule()
                .editForTeamsInRndNameGameNumb("Semifinal", 1)
                .selectAwayTeam(awayTestTeam2.getName())
                .typeDateAndTime(dateFormat.parse("2017-04-19T16:39:57"))
                .saveButton();

        ScheduleGamePage editSchedule = new TournamentsSchedulePage()
                .editForTeamsInRndNameGameNumb("Final", 1);
        new Specification()
                .forThe(editSchedule)
                .isDateAndTimeLabelPresentsOnly(editSchedule.labelsNames(), 1)
                .next()
                .forThe(editSchedule)
                .isLabelsNameIsCorrect("Date and time")
                .next()
                .check();
    }
}