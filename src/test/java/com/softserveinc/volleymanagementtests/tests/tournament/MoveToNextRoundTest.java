package com.softserveinc.volleymanagementtests.tests.tournament;

import com.softserveinc.volleymanagementtests.dal.models.PlayerDal;
import com.softserveinc.volleymanagementtests.dal.models.TeamDal;
import com.softserveinc.volleymanagementtests.dal.models.TournamentDal;
import com.softserveinc.volleymanagementtests.dal.repositories.*;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.login.LoginPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.*;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.division.DivisionTestData;
import com.softserveinc.volleymanagementtests.testdata.division.mappers.DivisionToDal;
import com.softserveinc.volleymanagementtests.testdata.group.GroupTestData;
import com.softserveinc.volleymanagementtests.testdata.group.GroupToDal;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.testdata.team.TeamTestData;
import com.softserveinc.volleymanagementtests.testdata.team.mappers.TeamToDal;
import com.softserveinc.volleymanagementtests.testdata.tournament.Tournament;
import com.softserveinc.volleymanagementtests.testdata.tournament.TournamentTestData;
import com.softserveinc.volleymanagementtests.testdata.tournament.mappers.TournamentToDal;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by stas on 13.03.2017.
 */
public class MoveToNextRoundTest {
    
    private Specification specification;
    private Tournament tournament;
    private TournamentDal tournamentDal;
    private List<Team> teams;
    
    private void prepareDb(int teamCount) {
        Player player = new PlayerTestData().getValidPlayer();
        PlayerRepository playerRepo = new PlayerRepository();
        PlayerDal playerDal = playerRepo.create(new PlayerToDal().map(player));
        teams = new TeamTestData().getValidTeams(teamCount);
        TeamToDal teamMapper = new TeamToDal();
        TeamRepository teamRepository = new TeamRepository();
        List<TeamDal> teamsDal = teams.stream()
                .map(teamMapper::map)
                .collect(Collectors.toList());
        teamsDal.forEach(teamDal -> teamDal
                .setCaptainId(playerDal.getId()));
        for (TeamDal team : teamsDal) {
            teamRepository.create(team);
        }
        TournamentRepository trmnentRepo = new TournamentRepository();
        TournamentToDal trmntMapper = new TournamentToDal();
        tournament = new TournamentTestData().getValidPlayOffTournament();
        tournamentDal = trmntMapper.map(tournament);
        int trmentKey = trmnentRepo.create(tournamentDal);
        trmnentRepo.addTeamToTournament(tournamentDal, teamsDal);
        DivisionToDal divisionMapper = new DivisionToDal();
        DivisionRepository divisionRep = new DivisionRepository();
        int divisionId = divisionRep.create(divisionMapper
                .map(new DivisionTestData().getValidDivision())
                .setTournamentId(trmentKey));
        GroupToDal groupMapper = new GroupToDal();
        GroupRepository groupRepo = new GroupRepository();
        groupRepo.create(groupMapper.map(new GroupTestData().getValidGroup())
                .setDivisionId(divisionId));
    }


    @BeforeMethod
    public void setUp() throws SQLException, ClassNotFoundException {
        prepareDb(8);
        specification = new Specification();
        new WebDriverUtils().load(new PageUrls().GOOGLE_LOGIN_URL);
        LoginPage loginPage = new LoginPage();
        loginPage.clickGoogleLoginButton();
        new WebDriverUtils().load(new PageUrls().TURNAMENTS_URL);
    }
    
    @AfterMethod
    public void tearDown() throws SQLException, ClassNotFoundException {
        new WebDriverUtils().stop();
        new TournamentRepository().deleteById(tournamentDal.getId());
    }


    @Stories("PlayOffScheme")
    @Description("In this test we make sure that winner team is moved to the next " +
            "round in playOff scheme")
    @Severity(SeverityLevel.NORMAL)
    @Issue("https://volleymanagement.visualstudio.com/VolleyMgmt%20-%20BS/_workitems?id=831&_a=edit")
    @Test
    public void checkAutomaticMoveToTheNextRound() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        TournamentsPage tournamentsPage = new TournamentsPage();
        TournamentDetailsPage detailsPage =
                tournamentsPage.openTournamentDetforUpcomTrnmtName(tournament.getName());
        TournamentsSchedulePage schedule = detailsPage.clickTournamentSchedule();
        Team homeFirstTeam = teams.get(0);
        Team awayFirstTeam = teams.get(1);
        ScheduleGamePage edit = schedule
                .editForTeamsInRndNameGameNumb("Quarter final", 0);
        edit.selectAwayTeam(awayFirstTeam.getName());
        edit.typeDateAndTime(dateFormat.parse("2017-04-19T16:39:57"));
        EditResultPage resultPage = edit.saveButton()
                .resForTeamsInRndNameAndGameNumber("Quarter final", 0);
        schedule = resultPage.setSetsScoreOneHome(25)
                .setSetsScoreOneAway(15)
                .setSetsScoreTwoHome(25)
                .setSetsScoreTwoAway(15)
                .setSetsScoreThreeHome(25)
                .setSetsScoreThreeAway(15)
                .setSetsScoreHome(3)
                .setSetsScoreAway(0)
                .clickOnSaveButton();
        
        
        specification.forThe(schedule).
                isTeamPresentInRound("Semifinal", homeFirstTeam)
                .next()
                .forThe(schedule)
                .isTeamNotPresentInRound("Semifinal", awayFirstTeam);
        
        
        Team homeSecondTeam = teams.get(2);
        Team awaySecondTeam = teams.get(3);
        edit = schedule.editForTeamsInRndNameGameNumb("Quarter final", 1);
        edit.selectHomeTeam(homeSecondTeam.getName());
        edit.selectAwayTeam(awaySecondTeam.getName());
        edit.typeDateAndTime(dateFormat.parse("2017-04-19T16:39:57"));
        resultPage = edit.saveButton().resForTeamsInRndNameAndGameNumber("Quarter final", 1);
        schedule = resultPage.setSetsScoreOneHome(15)
                .setSetsScoreOneAway(25)
                .setSetsScoreTwoHome(15)
                .setSetsScoreTwoAway(25)
                .setSetsScoreThreeHome(15)
                .setSetsScoreThreeAway(25)
                .setSetsScoreHome(0)
                .setSetsScoreAway(3)
                .clickOnSaveButton();
        
        specification.forThe(schedule).
                isTeamPresentInRound("Semifinal", awaySecondTeam)
                .next()
                .forThe(schedule)
                .isTeamNotPresentInRound("Semifinal", homeSecondTeam);
        
        
        specification.check();
    }
    
    
}