package com.softserveinc.volleymanagementtests.tests.tournament.count_rounds;

import com.softserveinc.volleymanagementtests.dal.models.PlayerDal;
import com.softserveinc.volleymanagementtests.dal.models.TeamDal;
import com.softserveinc.volleymanagementtests.dal.models.TournamentDal;
import com.softserveinc.volleymanagementtests.dal.repositories.*;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.TournamentDetailsPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.TournamentsPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.division.DivisionTestData;
import com.softserveinc.volleymanagementtests.testdata.division.mappers.DivisionToDal;
import com.softserveinc.volleymanagementtests.testdata.group.GroupTestData;
import com.softserveinc.volleymanagementtests.testdata.group.GroupToDal;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.testdata.team.TeamTestData;
import com.softserveinc.volleymanagementtests.testdata.team.mappers.TeamToDal;
import com.softserveinc.volleymanagementtests.testdata.tournament.Tournament;
import com.softserveinc.volleymanagementtests.testdata.tournament.TournamentTestData;
import com.softserveinc.volleymanagementtests.testdata.tournament.mappers.TournamentToDal;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.*;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Natalia on 23.03.2017.
 * For tournament with Playoff scheme:
 Number of rounds:
 2 teams - 1 round
 3-4 teams - 2 rounds
 5-8 teams - 3 rounds
 "2^(n-1) +1" - "2^n" teams - "n" rounds
 */
public class CountRounds {
    List<Team> teams;
    private List<Tournament> tournaments = new ArrayList<>();
    private WebDriverUtils webDriverUtils;

    private Tournament createTournamentinDb(int teamCount)  {
        Tournament tournament = new Tournament();
        Player player = new PlayerTestData().getValidPlayer();
        PlayerRepository playerRepo = new PlayerRepository();
        PlayerDal playerDal = playerRepo.create(new PlayerToDal().map(player));
        teams = new TeamTestData().getValidTeams(teamCount);
        TeamToDal teamMapper = new TeamToDal();
        TeamRepository teamRepository = new TeamRepository();
        List<TeamDal> teamsDal = teams.stream()
                .map(teamMapper::map)
                .collect(Collectors.toList());
        teamsDal.forEach(teamDal -> teamDal
                .setCaptainId(playerDal.getId()));
        for (TeamDal team : teamsDal) {
            teamRepository.create(team);
        }
        TournamentRepository trmnentRepo = new TournamentRepository();
        TournamentToDal trmntMapper = new TournamentToDal();
        tournament = new TournamentTestData().getValidPlayOffTournament();
        TournamentDal tourNamentDal = trmntMapper.map(tournament);
        int trmentKey = trmnentRepo.create(tourNamentDal);
        trmnentRepo.addTeamToTournament(tourNamentDal, teamsDal);
        DivisionToDal divisionMapper = new DivisionToDal();
        DivisionRepository divisionRep = new DivisionRepository();
        int divisionId = divisionRep.create(divisionMapper
                .map(new DivisionTestData().getValidDivision())
                .setTournamentId(trmentKey));
        GroupToDal groupMapper = new GroupToDal();
        GroupRepository groupRepo = new GroupRepository();
        groupRepo.create(groupMapper.map(new GroupTestData().getValidGroup())
                .setDivisionId(divisionId));
        return tournament;
    }


    @BeforeClass
    public void setUp() throws SQLException, ClassNotFoundException, InterruptedException {
        tournaments.add(createTournamentinDb(2));
        tournaments.add(createTournamentinDb(4));
        tournaments.add(createTournamentinDb(8));
        tournaments.add(createTournamentinDb(16));
        webDriverUtils = new WebDriverUtils();
        webDriverUtils.load(new PageUrls().BASE_URL);
        new MainPage()
                .clickUserMenu()
                .clickLogin()
                .clickGoogleLoginButton()
                .clickTournamentsPage();
    }

    @AfterClass
    public void tearDownFinal() {
        webDriverUtils.stop();
    }

    @BeforeMethod
    public void reloadPage() {
        webDriverUtils.load(new PageUrls().TURNAMENTS_URL);
    }

    @DataProvider
    public Object[][] tournamentDataProvider() {
        return new Object [][] {{tournaments.get(0), 1}, {tournaments.get(1), 2},
                {tournaments.get(2), 3}, {tournaments.get(3), 4}};
    }


    @Features("Schedule Page")
    @Stories("Count rounds. Number of rounds:" +
            "        2 teams - 1 round")
    @Severity(SeverityLevel.NORMAL)
    @Issue("https://volleymanagement.visualstudio.com/VolleyMgmt%20-%20BS/Dp-107%20ATQC/_backlogs?level=Backlog%20items&showParents=false&_a=backlog&_dialog=workitem")
    //Test for 2 teams - 1 rounds
    @Test (dataProvider = "tournamentDataProvider")
    public final void CountRoundTest1Teams1Rounds(Tournament tournament, int teamCount)  {
        new TournamentsPage()
                .openTournamentDetforUpcomTrnmtName(tournament.getName());

        new Specification()
                .forThe(new TournamentDetailsPage()
                        .clickTournamentSchedule()
                        .getRoundlabel()
                        .size() == teamCount).isTrue()
                .next()
                .check();
    }


}
