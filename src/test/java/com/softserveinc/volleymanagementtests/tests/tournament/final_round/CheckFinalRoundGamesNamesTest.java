package com.softserveinc.volleymanagementtests.tests.tournament.final_round;

import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.tournaments.TournamentDetailsPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.TournamentsPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Test is checking the game names in final round in PlayOff scheme.
 * @author Ivan Podorozhnyi
 */
public class CheckFinalRoundGamesNamesTest {
    
    private Specification specification;

    @BeforeMethod
    public void setUp() {
        new WebDriverUtils().load(new PageUrls().BASE_URL);
        specification = new Specification();
    }
    
    @Features("Tournament")
    @Stories("Check final round")
    @Test
    public void checkFinalRoundGamesNames() {
        
        TournamentDetailsPage tournamentDetailsPage = new TournamentsPage()
                .openTournamentDetforUpcomTrnmtName("playOff New");
        
        specification.forThe(tournamentDetailsPage)
                .schemeMatch("PlayOff")
                .next()
                .forThe(tournamentDetailsPage.clickTournamentSchedule())
                .checkFinalRoundNames("Bronze match:", "Final match:")
                .next()
                .check();
    }
    
    @AfterTest
    public void tearDown() {
        new WebDriverUtils().stop();
    }
}
