package com.softserveinc.volleymanagementtests.tests.tournament.tournament_application;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.login.LoginPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.TournamentDetailsPage;
import com.softserveinc.volleymanagementtests.pages.tournaments.TournamentsPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tools.ReadMail;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.mail.MessagingException;
import java.io.IOException;

/**
 * Created by Natalia on 23.03.2017.
 */
public class ApplyForTournament {

    public final String ADMIN_MAIL = "volleymanagementtesting@gmail.com";
    public final String ADMIN_PASSWORD = "volleymanagementtesting107";

    @BeforeClass
    public void setUp() throws  InterruptedException {
        new WebDriverUtils().load(new PageUrls().TURNAMENTS_URL);
        new WebDriverUtils().load(new PageUrls().GOOGLE_LOGIN_URL);
        LoginPage loginPage = new LoginPage();
        loginPage.clickGoogleLoginButton();
        new WebDriverUtils().load(new PageUrls().TURNAMENTS_URL);
    }

    @AfterMethod
    public void tearDownFinal() {
       new WebDriverUtils().stop();
    }


    @Test
    public final void applyForTournament() throws InterruptedException, MessagingException, IOException {
        new TournamentsPage().openTournamentDetforCurrTrmntName("Tour Playoff #2");
        String expectedResult = "Tournament Request";
        ReadMail reader =new ReadMail();
        new TournamentDetailsPage()
                .clickApplyForTournament()
                .chooseTeamByIndexInDropdown(5)
                .checkAgreeCheckbox()
                .clickAcceptButton();
        new Specification().forThe(reader.readMailContent(expectedResult , ADMIN_MAIL,
                ADMIN_PASSWORD)).valueContains((expectedResult)).next().check();
    }
}