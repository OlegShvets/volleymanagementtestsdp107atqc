package com.softserveinc.volleymanagementtests.tests.user;

import com.softserveinc.volleymanagementtests.dal.repositories.UserRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.admin.adminmainpage.AdminMainPage;
import com.softserveinc.volleymanagementtests.pages.admin.userdetailspage.UserDetailsPage;
import com.softserveinc.volleymanagementtests.pages.admin.users.UsersListPage;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.user.User;
import com.softserveinc.volleymanagementtests.testdata.user.mappers.DalToUser;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;
/**
 * Created by stas on 20.03.2017.
 */
public class CheckBlockButtonInUserDetailsActiveTest {
    private WebDriverUtils webDriverUtils;
    private UsersListPage usersListPage;
    private User user;


    @BeforeMethod
    public void setUp() {
        user = new DalToUser().map(new UserRepository().getById(2));
        webDriverUtils = new WebDriverUtils();
        webDriverUtils.load(new PageUrls().BASE_URL);
        new MainPage()
                .clickUserMenu()
                .clickLogin()
                .clickGoogleLoginButton();
        webDriverUtils.load(new PageUrls().ADMIN_MAIN_PAGE);
        AdminMainPage adminMainPage = new AdminMainPage();
        usersListPage = adminMainPage.clickActiveUsers();
    }

    @AfterMethod
    public void tearDown() {
        webDriverUtils.stop();
    }



    @Stories("Active users")
    @Description("In this test case we make sure that user information and block/unblock button" +
            "are present in user details page accessed from Active Users page")
    @Severity(SeverityLevel.NORMAL)
    @Issue("https://volleymanagement.visualstudio.com/VolleyMgmt%20-%20BS/_workitems?id=832&_a=edit")
    @Test
    public void testUserPresenseInThePage() {
        UserDetailsPage userDetailsPage = usersListPage
                .clickUserDetails(user.getFullName());
        Specification specification = new Specification();
        specification.forThe(userDetailsPage.personName())
                .textMatch(user.getFullName())
                .next()
                .forThe(userDetailsPage.userName())
                .textMatch(user.getUserName())
                .next()
                .forThe(userDetailsPage.email())
                .textMatch(user.getEmail())
                .next()
                .forThe(userDetailsPage.phoneOfUser())
                .textMatch(user.getCellPhone())
                .next()
                .forThe(userDetailsPage.getBlockButton())
                .textMatch("Block");

        userDetailsPage.clickOnBlockButton();

        specification.forThe(userDetailsPage.getBlockButton())
                .textMatch("Unblock");
        userDetailsPage.clickOnBlockButton();

        specification.forThe(userDetailsPage.getBlockButton())
                .textMatch("Block");

        specification.check();
    }
}
