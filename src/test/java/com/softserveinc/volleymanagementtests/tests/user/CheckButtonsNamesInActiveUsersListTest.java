package com.softserveinc.volleymanagementtests.tests.user;

import com.softserveinc.volleymanagementtests.dal.repositories.UserRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.admin.adminmainpage.AdminMainPage;
import com.softserveinc.volleymanagementtests.pages.admin.users.UsersListPage;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.user.User;
import com.softserveinc.volleymanagementtests.testdata.user.mappers.DalToUser;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by stas on 22.03.2017.
 */
public class CheckButtonsNamesInActiveUsersListTest {
    WebDriverUtils webDriverUtils;
    private User activeUser;


    @BeforeMethod
    public void setUp()  {
        activeUser = new DalToUser().map(new UserRepository().getById(2));
        webDriverUtils = new WebDriverUtils();
        webDriverUtils.load(new PageUrls().BASE_URL);
        new MainPage()
                .clickUserMenu()
                .clickLogin()
                .clickGoogleLoginButton();
        webDriverUtils.load(new PageUrls().ADMIN_MAIN_PAGE);
    }


    @AfterMethod
    public void tearDown() {
        webDriverUtils.stop();
    }


    @Stories("Active users")
    @Description("In this test case we make sure that block/unblock buttons are present for user" +
            "in Active users list")
    @Severity(SeverityLevel.NORMAL)
    @Issue("https://volleymanagement.visualstudio.com/VolleyMgmt%20-%20BS/_workitems?id=899&_a=edit")
    @Test
    public void testUserDetailButtonPresence() {
        Specification specification = new Specification();

        AdminMainPage adminMainPage = new AdminMainPage();
        specification.forThe(adminMainPage.getActiveUsersLink())
                .textMatch("Active users")
                .next();
        UsersListPage usersListPage = adminMainPage.clickActiveUsers();

        specification.forThe(usersListPage
                .getDetailsButtonForUser(activeUser.getFullName()))
                .textMatch("Details")
                .next().
                forThe(usersListPage.getUserBlockButtonForUser(activeUser.getFullName()))
                .textMatch("Block")
                .next()
                .forThe(usersListPage.clickUsersBlock(activeUser.getFullName())
                        .getUserBlockButtonForUser(activeUser.getFullName()))
                .textMatch("Unblock")
                .next()
                .forThe(usersListPage.clickUsersBlock(activeUser.getFullName())
                        .getUserBlockButtonForUser(activeUser.getFullName()))
                .textMatch("Block");
        specification.check();
    }
}
