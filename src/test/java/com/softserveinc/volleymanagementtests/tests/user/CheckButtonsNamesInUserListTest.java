package com.softserveinc.volleymanagementtests.tests.user;

import com.softserveinc.volleymanagementtests.dal.repositories.UserRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;

import com.softserveinc.volleymanagementtests.pages.admin.users.UsersListPage;
import com.softserveinc.volleymanagementtests.pages.admin.adminmainpage.AdminMainPage;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.user.User;
import com.softserveinc.volleymanagementtests.testdata.user.UserTestData;
import com.softserveinc.volleymanagementtests.testdata.user.mappers.UserToDal;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by stas on 19.03.2017.
 */
public class CheckButtonsNamesInUserListTest {

    WebDriverUtils webDriverUtils;
    private User notBlockedUser;
    private User blockedUser;


    @BeforeMethod
    public void setUp()  {
        webDriverUtils = new WebDriverUtils();
        UserTestData userTestData = new UserTestData();
        UserRepository userRepository = new UserRepository();
        notBlockedUser = userTestData.getNonBlockedUser();
        blockedUser = userTestData.getBlockedUser();
        userRepository.create(new UserToDal().map(notBlockedUser));
        userRepository.create(new UserToDal().map(blockedUser));
        webDriverUtils.load(new PageUrls().BASE_URL);
        new MainPage()
                .clickUserMenu()
                .clickLogin()
                .clickGoogleLoginButton();
        webDriverUtils.load(new PageUrls().ADMIN_MAIN_PAGE);
    }


    @AfterMethod
    public void tearDown() {
        webDriverUtils.stop();
        UserRepository userRepository = new UserRepository();
        userRepository.deleteByName(notBlockedUser.getUserName());
        userRepository.deleteByName(blockedUser.getUserName());
    }


    @Stories("AllUsersList")
    @Description("In this test case we make sure that block/unblock buttons are present for user" +
            "in All users list")
    @Severity(SeverityLevel.NORMAL)
    @Issue("https://volleymanagement.visualstudio.com/VolleyMgmt%20-%20BS/_workitems?id=899&_a=edit")
    @Test
    public void testUserDetailButtonPresence() {
        Specification specification = new Specification();

        AdminMainPage adminMainPage = new AdminMainPage();

        specification.forThe(adminMainPage.getAllUsersLink())
                .textMatch("All users")
                .next();
      UsersListPage UsersListPage = adminMainPage.clickAllUsers();

        specification.forThe(UsersListPage
                .getDetailsButtonForUser(notBlockedUser.getFullName()))
                .textMatch("Details")
                .next().
                forThe(UsersListPage.getUserBlockButtonForUser(notBlockedUser.getFullName()))
                .textMatch("Block")
                .next()
                .forThe(UsersListPage.getUserBlockButtonForUser(blockedUser.getFullName()))
                .textMatch("Unblock");

        specification.check();
    }
}
