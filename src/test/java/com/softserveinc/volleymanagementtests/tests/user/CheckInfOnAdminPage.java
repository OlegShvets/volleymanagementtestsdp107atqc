package com.softserveinc.volleymanagementtests.tests.user;

import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.admin.adminmainpage.AdminMainPage;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by shvets on 21.03.2017.
 */
public class CheckInfOnAdminPage {
    private Specification specification;

    @BeforeMethod
    public void setUp() {
        new WebDriverUtils().load(new PageUrls().BASE_URL);
        new MainPage().clickUserMenu().clickLogin().clickGoogleLoginButton().clickTournamentsPage();
        new WebDriverUtils().load(new PageUrls().ADMIN_MAIN_PAGE);
        specification = new Specification();
    }

    @AfterMethod
    public void tearDown() {
        new WebDriverUtils().stop();
    }

    @Features("Admin FeedBack tests")
    @Stories("On Admins dashboard there have to be a button 'Active users'.")
    @Issue("https://volleymanagement.visualstudio.com/VolleyMgmt%20-%20BS/_backlogs/TaskBoard/DP-103/Sprint%205?_a=requirements")
    @Severity(SeverityLevel.NORMAL)
    @Test
    public void isActiveUsersLinkVisble() {

        String expName = "Active users";
        specification
                .forThe(new AdminMainPage()
                        .getActiveUsersLink()
                        .getText())
                .valueMatch(expName)
                .next()
                .check();
    }

    @Features("Admin FeedBack tests")
    @Stories("On the page displayed Label 'Active users' ")
    @Issue("https://volleymanagement.visualstudio.com/VolleyMgmt%20-%20BS/_backlogs/TaskBoard/DP-103/Sprint%205?_a=requirements")
    @Severity(SeverityLevel.NORMAL)
    @Test
    public void isActiveUsersPageHasCorrectLabel() {

        String expName = "Active users";
        specification
                .forThe(new AdminMainPage()
                        .clickActiveUsers()
                        .getUserListLabel())
                .textMatch(expName)
                .next()
                .check();
    }

    @Features("Admin FeedBack tests")
    @Stories("On Page displayed button 'Block', 'Details' and Label 'Person's name' ")
    @Issue("https://volleymanagement.visualstudio.com/VolleyMgmt%20-%20BS/_backlogs/TaskBoard/DP-103/Sprint%205?_a=requirements")
    @Severity(SeverityLevel.NORMAL)
    @Test
    public void isActiveUsersPageCorrectInformTable() {
        String expName = "Person's name";
        String expButtonBlockName = "Block";
        String expButtonDetailsName = "Details";

        specification
                .forThe(new AdminMainPage()
                        .clickActiveUsers()
                .UserTableNameLabel())
                .textMatch(expName)
                .next()
                .forThe(new AdminMainPage()
                        .clickActiveUsers()
                        .getUserListTableRow(0)
                        .getUserDetails())
                .textMatch(expButtonDetailsName)
                .next()
                .forThe(new AdminMainPage()
                        .clickActiveUsers()
                        .getUserListTableRow(0)
                        .getUserBlock())
                .textMatch(expButtonBlockName)
                .next();

        specification.check();
    }
}
