package com.softserveinc.volleymanagementtests.tests.user;

import com.softserveinc.volleymanagementtests.dal.models.PlayerDal;
import com.softserveinc.volleymanagementtests.dal.models.UserDal;
import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.dal.repositories.UserRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.admin.users.UsersListPage;
import com.softserveinc.volleymanagementtests.pages.admin.adminmainpage.AdminMainPage;
import com.softserveinc.volleymanagementtests.pages.admin.userdetailspage.UserDetailsPage;
import com.softserveinc.volleymanagementtests.pages.main.MainPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.testdata.user.User;
import com.softserveinc.volleymanagementtests.testdata.user.UserTestData;
import com.softserveinc.volleymanagementtests.testdata.user.mappers.UserToDal;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by stas on 20.03.2017.
 */
public class UserIsPresentInTheUserDetailsAllTest {
    private WebDriverUtils webDriverUtils;
    private UsersListPage UsersListPage;
    private User user;
    private Player player;


    @BeforeMethod
    public void setUp()  {
        prepareDb();
        webDriverUtils = new WebDriverUtils();
        webDriverUtils.load(new PageUrls().BASE_URL);
        new MainPage()
                .clickUserMenu()
                .clickLogin()
                .clickGoogleLoginButton();
        webDriverUtils.load(new PageUrls().ADMIN_MAIN_PAGE);
        AdminMainPage adminMainPage = new AdminMainPage();
        UsersListPage = adminMainPage.clickAllUsers();
    }

    private void prepareDb() {
        UserTestData userTestData = new UserTestData();
        UserRepository userRepository = new UserRepository();
        PlayerRepository playerRepository = new PlayerRepository();
        user = userTestData.getNonBlockedUser();
        UserDal userDal = new UserToDal().map(user);
        int userKey = userRepository.create(userDal);
        userRepository.mapRoleToUser(userKey, 1);
        userRepository.mapRoleToUser(userKey, 2);
        userRepository.mapRoleToUser(userKey, 3);
        player = new PlayerTestData().getValidPlayer();
        PlayerDal playerDal = playerRepository.create(new PlayerToDal().map(player));
        userRepository.setPlayerIdToUser(userKey, playerDal.getId());
    }

    @AfterMethod
    public void tearDown() {
       webDriverUtils.stop();
        new UserRepository().deleteByName(user.getUserName());
    }


    @Stories("AllUsersList")
    @Description("In this test case we make sure that user information is present in " +
            "user details window accessed from All Users List")
    @Severity(SeverityLevel.NORMAL)
    @Issue("https://volleymanagement.visualstudio.com/VolleyMgmt%20-%20BS/_workitems?id=899&_a=edit")
    @Test
    public void testUserPresenseInThePage() {
        UserDetailsPage userDetailsPage = UsersListPage
                .clickUserDetails(user.getFullName());
        Specification specification = new Specification();

        specification
                .forThe(userDetailsPage.email())
                .textMatch(user.getEmail())
                .next()
                .forThe(userDetailsPage.personName())
                .textMatch(user.getFullName())
                .next()
                .forThe(userDetailsPage.userName())
                .textMatch(user.getUserName())
                .next()
                .forThe(userDetailsPage.phoneOfUser())
                .textMatch(user.getCellPhone())
                .next()
                .forThe(userDetailsPage.userRolesId().get(0))
                .textMatch("1")
                .next()
                .forThe(userDetailsPage.usersRoles().get(0))
                .textMatch("Administrator")
                .next()
                .forThe(userDetailsPage.userRolesId().get(1))
                .textMatch("2")
                .next()
                .forThe(userDetailsPage.usersRoles().get(1))
                .textMatch("TournamentAdministrator")
                .next()
                .forThe(userDetailsPage.userRolesId().get(2))
                .textMatch("3")
                .next()
                .forThe(userDetailsPage.usersRoles().get(2))
                .textMatch("User")
                .next()
                .forThe(userDetailsPage.getLinkedPlayerName())
                .textMatch(player.getFirstName())
                .next()
                .forThe(userDetailsPage.getLinkedPlayerLastName())
                .textMatch(player.getLastName())
                .next()
                .forThe(userDetailsPage.getLinkedPlayerYearOfBirth())
                .textMatch(player.getBirthYear())
                .next()
                .forThe(userDetailsPage.getLinkedPlayerHeight())
                .textMatch(player.getHeight())
                .next()
                .forThe(userDetailsPage.getLinkedPlayerWeight())
                .textMatch(player.getWeight());

        specification.check();
    }


}
