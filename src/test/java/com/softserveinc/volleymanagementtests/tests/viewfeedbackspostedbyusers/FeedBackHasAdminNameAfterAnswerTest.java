package com.softserveinc.volleymanagementtests.tests.viewfeedbackspostedbyusers;

import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.admin.adminmainpage.AdminMainPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class FeedBackHasAdminNameAfterAnswerTest {
    @BeforeMethod
    public void setUp() {
        new WebDriverUtils().load(new PageUrls().ADMIN_MAIN_PAGE);
    }
    
    @AfterTest
    public void tearDown() {
        new WebDriverUtils().stop();
    }
    
    @Features("Admin FeedBack tests")
    @Stories("If feedback status is \"Answered\" or \"Closed\" there also have to be displayed " +
            "Date and Admins name who replied or closed item.")
    @Issue("https://volleymanagement.visualstudio.com/VolleyMgmt%20-%20BS/_backlogs/Iteration/DP-103/Sprint%204?_dialog=workitem")
    @Severity(SeverityLevel.NORMAL)
    @Test
    public void isFeedBackHasStatusAnsweredAfterAnswerOnFeedBack() {

        String expectedResult = new AdminMainPage()
                .clickRequests()
                .getFeedBackByID("9")
                .getReplay()
                .clearMessage()
                .writeMessage("Test Message")
                .send()
                .getFeedBackByID("9")
                .openDetails()
                .getAdminName()
                .getText();

        new Specification()
                .forThe(new AdminMainPage()
                        .clickRequests()
                        .getFeedBackByID("9")
                        .openDetails()
                        .getAdminName())
                .textMatch(expectedResult)
                .next()
                .check();
    }
}
